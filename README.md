# 操作规范
- 1, 本地新建一个项目功能模块，如：签到功能 
> git branch dev-qiandao
- 2, 查看当前是否新建分支成功 
> git branch  
- 3, 切换到 dev-qiandao 分支上
> git checkout dev-qiandao
- 4, 在当前 dev-qiandao 开发签到功能，开发测试OK，合并代码到 master 分支
- 5, 我用的 sourceTree 工具合并，命令合并方式请咨询 百度
- 6, 测试环境测试无误，合并代码到 正式生产分支 stabble
- 7, 建议在本地合并代码，dev-qiandao 合并到 stabble 分支
- 8, 提交 stabble 分支，服务器 pull stabble 分支。更新完成







## master 开发分支
## stabble 正式发布分支


## 查看远程分支
    git branch -a 
    
## 查看本地分支
    git branch
    
## 更新正式服务器代码
    git pull origin stabble
    
## 更新测试服务器代码
    git pull origin master

## 切换本地分支
    git checkout master   
    git checkout stabble
    
## 其他常用、实用的命令，共享到此