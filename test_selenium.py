#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import time
import sys
from redis import StrictRedis

def index_of_str(s1, s2):
    n1=len(s1)
    n2=len(s2)
    for i in range(n1-n2+1):
        if s1[i:i+n2]==s2:
            return i
    else:
            return -1

arg = sys.argv
account_id = arg[1]
path = arg[2]
#print(account_id)
#print(path)
#sys.exit(0)
#print(cookie_str)
r = StrictRedis(host='47.104.1.173', port=6379, db=0, password='video0987654321',decode_responses=True)
cookie_str = r.get('account_cookie:'+account_id)
#print(cookie_str)
chrome_driver="C:\Program Files\python\Lib\site-packages\selenium\webdriver\chrome\chromedriver.exe"
 #chrome_driver='/usr/local/lib/python3.6/site-packages/selenium/webdriver/chrome/chromedriver'
driver=webdriver.Chrome(executable_path=chrome_driver)
# options = webdriver.FirefoxOptions()

# options.add_argument('--headless')
# options.add_argument('--no-sandbox')
# options.add_argument('--disable-dev-shm-usage')
# driver = webdriver.Firefox(options=options)
# binary_location = '/usr/bin/google-chrome'
# chrome_driver_binary= '/usr/bin/chromedriver'
# options = webdriver.ChromeOptions()
# options.binary_location = binary_location #谷歌地址
# options.add_argument('--no-sandbox')#解决DevToolsActivePort文件不存在的报错

# options.add_argument('window-size=1920x3000') #指定浏览器分辨率
# options.add_argument('--disable-gpu') #谷歌文档提到需要加上这个属性来规避bug
# options.add_argument('--hide-scrollbars') #隐藏滚动条, 应对一些特殊页面
# options.add_argument('blink-settings=imagesEnabled=false') #不加载图片, 提升速度
# options.add_argument('--headless') #浏览器不提供可视化页面. linux下如果系统不支持可视化不加这条会启动失败
# chromedriver = chrome_driver_binary
# #os.environ["webdriver.chrome.driver"] = chromedriver
# driver = webdriver.Chrome(chrome_options=options,executable_path=chromedriver)

driver.delete_all_cookies()
driver.get('https://www.zhihu.com/')
#driver = webdriver.Chrome()     # 创建Chrome对象.
#name,value,domain,path,expires,sameSite,httpOnly,secure
#_xsrf,Oqh1wePn3oUUtvHv71EFbbtef6jWtEHm,zhihu.com,/,"Sun, 29 May 2022 07:29:25 GMT",None,false,false

cookie_list = cookie_str.split(';')
for each in cookie_list:
	#cookie_split = each.split('=')
	pos = index_of_str(each,'=')
	name = each[0:pos]
	value = each[(pos+1):]
	#print(name)
	#print(value)
	cookie_tmp = {
		'domain':'.zhihu.com',
		'name':name.replace(' ',''),
		'value':value.replace(' ','')
	}
	driver.add_cookie(cookie_tmp)

driver.get('https://zhuanlan.zhihu.com/write')

WebDriverWait(driver,10).until(EC.visibility_of_element_located((By.XPATH,'//button[@class="Button Editable-control Button--plain"]')))
 #WebDriverWait(driver,10).until(EC.visibility_of_element_located((By.XPATH,'//svg[contains(@class,"Zi--InsertImage")]')))
driver.find_element_by_xpath('//button[@class="Button Editable-control Button--plain"]').click()
driver.find_element_by_xpath('//div[contains(@class,"InputLike")]/input[@type="file"]').send_keys('C://Users/Administrator/Documents/123.jpg')
#'Image FocusPlugin--unfocused Image--isBlock'
WebDriverWait(driver,10).until(EC.visibility_of_element_located((By.XPATH,'//img[@class="Image FocusPlugin--unfocused Image--isBlock"]')))
image = driver.find_element_by_xpath('//img[@class="Image FocusPlugin--unfocused Image--isBlock"]')
img_src = image.get_attribute('src')

# 操作这个对象.
# driver.get('http://kuaichuan.360.cn/#/login')
# #time.sleep(2)
# driver.find_element_by_xpath('//*[@id="wrap_box"]/header/div/div[2]/a[2]').click()
# WebDriverWait(driver,10).until(EC.visibility_of_element_located((By.XPATH,'//div[@class="quc-input"]/input')))
# #time.sleep(2)
# #ActionChains(driver).double_click(login_button).perform()
# driver.find_element_by_xpath('//div[@class="quc-input"]/input').send_keys('18374871690')
# driver.find_element_by_xpath('/html/body/div[7]/div[2]/div/div/div/div[2]/form/div[3]/div/div/div/span').click()
# #ActionChains(driver).double_click(login_button).perform()
#time.sleep(2)
print(img_src)
driver.quit()   # 使用完, 记得关闭浏览器, 不然chromedriver.exe进程为一直在内存中.
