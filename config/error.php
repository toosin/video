<?php

return [
    'NOT_EXIST' => ['code' => 1001, 'msg' => '记录不存在'],
    'PARAM_EMPTY' => ['code' => 1002, 'msg' => '参数不能为空'],
    'PARAM_ERROR' => ['code' => 1003, 'msg' => '参数错误'],
    'TASK_IN_PROGRESS' => ['code' => 1004, 'msg' => '任务执行中'],
    'BALANCE_NOT_ENOUGH' => ['code' => 2001, 'msg' => '账户余额不足'],
    'PHONE_FORMAT_ERROR' => ['code' => 2007, 'msg' => '手机号码格式错误'],
    'LOGIN_VERIFY_ERROR' => ['code' => 10010, 'msg' => '账号或密码错误！'],
    'PWD_LENGTH_ERROR' => ['code' => 2008, 'msg' => '密码长度为6-15位'],
    'ALREADY_ADD' => ['code' => 2009, 'msg' => '账号已经添加过了'],
    'NOT_VIP_ACCOUNT' => ['code' => 2012, 'msg' => '该账号非vip账号'],
    'SITE_NOT_EXIST' => ['code' => 2013, 'msg' => '站点不存在'],
    'ACCOUNT_TYPE_ERROR' => ['code' => 2011, 'msg' => '该账号类型不能添加'],
    'ACCOUNT_NOT_EXIST' => ['code' => 2014, 'msg' => '账号不存在'],

    //渠道
    'ACCOUNT_PASSWORD_NOT_EMPTY' => ['code' => 10003, 'msg' => '账号密码不能为空'],
    'LOGIN_VERIFY_ERROR' => ['code' => 10003, 'msg' => '账号密码错误'],
    'PHONE_NOT_REGISTERED' => ['code' => 10003, 'msg' => '号码尚未注册'],
    'VERIFY_CODE_ERROR' => ['code' => 10004, 'msg' => '验证码错误'],
    'PHONE_CODE_ERROR' => ['code' => 10004, 'msg' => '验证码未找到'],
    'SMS_CODE_ERROR' => ['code' => 10007, 'msg' => '短信验证码错误'],
    'PASSWORD_NOT_SAME' => ['code' => 10008, 'msg' => '密码不一致'],
    'SMS_SEND_ERROR' => ['code' => 10011, 'msg' => '验证码发送失败'],
    'SMS_GET_TOO_OFTEN' => ['code' => 10012, 'msg' => '验证码获取过于频繁'],
    'PASSWORD_WRONG' => ['code' => 10012, 'msg' => '密码错误'],
    'CHANNEL_NOT_ENABLED' => ['code' => 10012, 'msg' => '账号正在审核，暂不能登录'],
    'USER_NOT_FOUND' => ['code' => 10013, 'msg' => '用户不存在'],
    'REGISTER_NUMBER_ERROR' => ['code' => 10013, 'msg' => '注册编号错误'],
    'COMPANY_EXIST' => ['code' => 10014, 'msg' => '公司名称已经存在'],
    'UPLOAD_FAILED' => ['code' => 10015, 'msg' => '上传失败'],
    'CONTENT_MAINTAIN' => ['code' => 10015, 'msg' => '该书正在内容精修中，10月15日恢复正常，敬请期待！'],


    // 管理
    'MANAGE_NOT_ENABLED' => ['code' => 10101, 'msg' => '账号暂不可用'],
    'TEST_BOOK_LIMIT_NUM' => ['code' => 10102, 'msg' => '已有测书数量达到上限'],
    'BOOK_NOT_EXIST' => ['code' => 10103, 'msg' => '上架书库里没找到该bid'],


    'CASH_ACCOUNT_NOT_SET' => ['code' => 10006, 'msg' => '提现账户未设置'],
    'CASH_ACCOUNT_EXIST' => ['code' => 10006, 'msg' => '提现账户已存在'],
    'INSUFFICIENT_BALANCE' => ['code' => 10006, 'msg' => '余额不足'],
    'LESS_THAN_LOWEST_WITHDRAW_MONEY' => ['code' => 10006, 'msg' => '提现不能低于100'],

    'PHONE_REGISTERED' => ['code' => 10009, 'msg' => '手机号码已注册'],
    'PROMOTION_NAME_NOT_EMPTY' => ['code' => 10005, 'msg' => '推广名称不能为空'],
    'NOT_LOGIN' => ['code' => 1001, 'msg' => '尚未登陆'],

    'NO_AUTHORIZED_OFFICIAL_ACCOUNT' => ['code' => 10012, 'msg' => '无授权公众号'],
    'READ_USER_NOT_EXIST' => ['code' => 10012, 'msg' => '阅读用户不存在'],
    'USER_NOT_SUBSCRIBE' => ['code' => 10012, 'msg' => '用户未关注'],
    'ACCOUNT_FRONZEN' => ['code' => 10012, 'msg' => '账户被冻结，请咨询官方客服'],
    'DRAW_ONLY_ONCE_A_DAY' => ['code' => 10012, 'msg' => '每天仅允许提现一次'],

    'HANDLE_FAILED' => ['code' => 10013, 'msg' => '操作失败'],
    'SAME_ACCOUNT_ERROR' => ['code' => 20028, 'msg' => '两个账号不能相同'],


    //各个模块自己写错误码
    //
    //图书  100001
    'UNKNOWN_ERROR' => ['code' => '10013', 'msg' => '未知错误'],
    'LACK_PARAM' => ['code' => '10014', 'msg' => '缺少参数'],
    //
    //2000


    //Notice

    'NAME_EXIST' => ['code' => 50003, 'msg' => '名称已经存在'],
    //Finance
    'FORBID_CHANGE' => ['code' => 60001, 'msg' => '禁止修改'],
    'WITHDRAW_CASHES_FAILED' => ['code' => 10012, 'msg' => '提现失败'],

    'WITHDRAW_CASH_AMOUNT' => ['code' => 60002, 'msg' => '提现金额必须 >= 100'],
    'WITHDRAW_CASH_AMOUNT_MORE' => ['code' => 60002, 'msg' => '提现金额不能大于20万'],
    'WITHDRAW_CASH_AMOUNT_INSUFFICIEN' => ['code' => 60003, 'msg' => '可提现金额不足'],
    'WITHDRAW_CASH_AMOUNT_FROZEN' => ['code' => 60004, 'msg' => '账号已冻结'],
    'WITHDRAW_CASH_AMOUNT_NO_CHANGE' => ['code' => 60005, 'msg' => '不能修改状态'],
    'WITHDRAW_CASH_AMOUNT_ACCOUNT' => ['code' => 60006, 'msg' => '账号未设置'],
    'PAYMENT_CHANNEL_AMOUNT_WITHOUT' => ['code' => 60007, 'msg' => '渠道可打款金额不足，请走人工打款'],
    'LIQUIDATED_STAT_AMOUNT_WITHOUT' => ['code' => 60008, 'msg' => '渠道可清算金额不足'],
    'COMMISSION_RATE_WITHOUT' => ['code' => 60009, 'msg' => '结算比例在 0.1 到 0.99 之间'],
    'WITHDRAW_CASH_TODAY_USE' => ['code' => 60010, 'msg' => '您今天已经提现过了！'],
    'PAYMENT_AUTO_NOT_OPEN' => ['code' => 60007, 'msg' => '自动打款暂未开通，请人工打款！'],
    //'PAYMENT_AUTO_NOT_OPEN'=>['code'=>60011,'msg'=>'自动打款暂未开通，请人工打款！'],
    'PAYMENT_WITHDRAW_MONEY_TOO' => ['code' => 60012, 'msg' => '已成功打款！'],
    'PAYMENT_WITHDRAW_MONEY' => ['code' => 60013, 'msg' => '打款金额错误，提现金额 - 手续费 = 打款金额'],

    //officialAccount
    'FORCESUBSCRIBEUSER_IS_EXIST' => ['code' => 80003, 'msg' => '该用户已经强关'],
    'ADDFORCESUBSCRIBEUSER_UNSUCCESSFUL' => ['code' => 80004, 'msg' => '添加强关用户失败'],
    'UPDATEFORCESUBSCRIBEUSER_UNSUCCESSFUL' => ['code' => 80004, 'msg' => '更新强关用户失败'],
    'CANCELFORCESUBSCRIBEUSER_UNSUCCESSFUL' => ['code' => 80004, 'msg' => '强关用户取关失败'],
    'FORCESUBSCRIBEUSER_NOT_FOUND' => ['code' => 80003, 'msg' => '无法获取用户'],
    'TEMEPATES_NOT_FOUND' => ['code' => 80003, 'msg' => '无法找到模板'],
    'TEMEPATES_UNSUCCESSFUL' => ['code' => 80003, 'msg' => '模板消息操作失败'],
    'TEMEPATES_IS_EXIST' => ['code' => 80003, 'msg' => '模板已经存在'],
    'WECHAT_USER_REQUEST_ERROR' => ['code' => 80004, 'msg' => '新增用户接口调用失败'],
    'OFFICIAL_ACCOUNT_IS_EXIST' => ['code' => 80003, 'msg' => '公众号已经存在'],
    'OFFICIAL_ACCOUNT_FAILED' => ['code' => 80004, 'msg' => '公众号授权失败'],
    'UPDATE_OFFICIAL_ACCOUNT_FAILED' => ['code' => 80004, 'msg' => '更新公众号失败'],
    'CANCEL_OFFICIAL_ACCOUNT_FAILED' => ['code' => 80004, 'msg' => '取消公众号授权失败'],
    'OFFICIAL_ACCOUNT_NOT_FOUND' => ['code' => 80003, 'msg' => '公众号不存在'],
    'EVENT_NOT_FOUND' => ['code' => 80003, 'msg' => '事件未配置'],
    'HAS_NO_CUSTOMS' => ['code' => 80003, 'msg' => '无法获取客服消息'],
    'CUSTOMS_SETTING_FAILED' => ['code' => 80004, 'msg' => '客服消息操作失败'],
    'CUSTOMS_SEND_FAILED' => ['code' => 80004, 'msg' => '客服消息发送失败'],
    'OFFICIAL_IMGTXT_FAILED' => ['code' => 80004, 'msg' => '素材创建失败'],
    'OFFICIAL_IMGTXT_DELFAILED' => ['code' => 80004, 'msg' => '素材删除失败'],
    'OFFICIAL_IMGTXT_FOUNDFAILED' => ['code' => 80004, 'msg' => '未找到素材'],
    'CUSTOMS_DOUBLE_FAILED' => ['code' => 80008, 'msg' => '你已经在1小时内创建过该客服消息，不能重复创建'],
    'TEMPLATE_DOUBLE_FAILED' => ['code' => 80008, 'msg' => '你已经在1小时内创建过该模板消息，不能重复创建'],
    'TEMPLATEMSG_IS_SEDDING' => ['code' => 80008, 'msg' => '该模板消息正在发送中，当前不允许操作'],
    'CUSTOMMSG_IS_SEDDING' => ['code' => 80008, 'msg' => '该客服消息正在发送中，当前不允许操作'],
    'KEYWORD_NOT_FOUND' => ['code' => 80009, 'msg' => '无法找到关键字'],
    'SMART_PUSHS_UNSUCCESSFUL' => ['code' => 80008, 'msg' => '自定义操作失败'],
    'SMART_PUSHS_NOT_ALL_EDIT' => ['code' => 80008, 'msg' => '修改自定义前必须先编辑好男女的内容'],

    'KEYWORDS_UNSUCCESSFUL' => ['code' => 80009, 'msg' => '关键字操作失败'],
    'OLD_APP_NOT_ALLOW_DELETE' => ['code' => 80010, 'msg' => '公众号已有一定数量注册用户不允许删除'],
    'NOT_FOUND_OFFICIAL_ACCOUNT' => ['code' => 80011, 'msg' => '公众号不存在'],
    'SHORT_URL_ERROR' => ['code' => 80012, 'msg' => '短连接转化失败，请稍后再试'],
    'SHORT_URL_NOT_ALLOW_ERROR' => ['code' => 80013, 'msg' => '内容不允许使用短链接，请重新编辑'],

    'CHECK_TEMPLATE_LIMIT_NUM' => ['code' => 80014, 'msg' => '请只勾选1个公众号测试'],


    //订阅
    'NOT_ORDERED' => ['code' => 10012, 'msg' => '未订购'],

    //wap
    'WAP_NOT_LOGIN' => ['code' => 10001, 'msg' => '未登录'],
    'WAP_NOT_ORDERED' => ['code' => 10002, 'msg' => '未订购'],
    'WAP_PARAM_ERROR' => ['code' => 10003, 'msg' => '参数错误'],
    'WAP_SYS_ERROR' => ['code' => 10004, 'msg' => '系统错误'],
    'WAP_SIGN_ERROR' => ['code' => 10005, 'msg' => '签名有误'],
    'WAP_NOT_EXIST' => ['code' => 10007, 'msg' => '资源不存在'],
    'WAP_ADDED_SHELF' => ['code' => 10008, 'msg' => '已添加到书架'],
    'WAP_INSUFFICIENT_BALANCE' => ['code' => 10009, 'msg' => '余额不足'],
    'WAP_ORDER_FAILED' => ['code' => 10010, 'msg' => '订购失败'],
    'WAP_PRODUCT_NOT_FOUND' => ['code' => 10011, 'msg' => '产品找不到'],
    'WAP_NOT_SUBSCRIBE' => ['code' => 10012, 'msg' => '未关注'],
    'WAP_LACK_PARAM' => ['code' => 10013, 'msg' => '缺少参数'],
    'WAP_BOOK_INSUFFICIENT_BALANCE' => ['code' => 10014, 'msg' => '单本余额不足'],
    'WAP_CHAPTER_INSUFFICIENT_BALANCE' => ['code' => 10015, 'msg' => '章订余额不足'],
    'WAP_CHAPTER_BUY' => ['code' => 10016, 'msg' => '购买章节'],
    'WAP_BOOK_BUY' => ['code' => 10017, 'msg' => '购买图书'],
    'WAP_OFF_SHELF' => ['code' => 10018, 'msg' => '图书下架'],
    'WAP_BOOK_BALANCE_PAY' => ['code' => 10019, 'msg' => '购买图书'],
    'WAP_BOOK_SECOND_BALANCE_PAY' => ['code' => 10020, 'msg' => '第二次图书订购余额不足'],
    'WAP_CHAPTER_SECOND_BALANCE_PAY' => ['code' => 10021, 'msg' => '第二次章节订购余额不足'],
    'WAP_DOMAIN_NOT_MATCH' => ['code' => 10022, 'msg' => '域名不匹配'],
    'WAP_LINK_SUBSCRIBE' => ['code' => 10023, 'msg' => '链接强关'],
    'WAP_HAD_ON_SHELF' => ['code' => 10024, 'msg' => '已经在书架上了'],
    //送礼
    'WAP_SEND_GIFT_FAILED' => ['code' => 10028, 'msg' => '送礼失败！请稍后再试！'],

    'BIND_ERROR'=>['code'=>100010,'msg'=>'账号绑定失败！'],
    'NEED_REFRESH_QRCODE'=>['code'=>100011,'msg'=>'二维码失效，请刷新二维码！'],
    'NO_SCAN'=>['code'=>100012,'msg'=>'没有扫码！'],
    'USER_CANCL_SCAN'=>['code'=>100013,'msg'=>'您取消了扫码！'],
    'TITLE_LENGTH_LIMIT'=>['code'=>100014,'msg'=>'标题或双标题不得小于5个字不得大于30字！'],
    'AUTH_ERROR'=>['code'=>100015,'msg'=>'授权失败！'],
    'UPLOAD_IMG_ERROR'=>['code'=>100015,'msg'=>'图片上传失败！'],
    'PUBLISH_FAILED'=>['code'=>100016,'msg'=>'发布失败'],
    'VIDEO_EXIT' => ['code'=>100015,'msg'=>'视频上传失败1！'],
    'EXTENSION_ERROR' => ['code'=>100015,'msg'=>'视频后缀名有误！'],
    'ISVALID_ERROR' => ['code'=>100015,'msg'=>'视频上传失败1！'],
    'FILE_EXISTS' => ['code'=>100015,'msg'=>'没有创建改文件夹！'],
    'MOVE_FAIL' => ['code'=>100015,'msg'=>'移动视频有误！'],


    //视频相关
    'NOT_OPEN'=>['code'=>110001,'msg'=>'该功能暂未开放！'],
    'VIDEO_NOT_EXIST'=>['code'=>110000,'msg'=>'视频不存在！'],
    'VIDEO_COVER_NOT_EXIST'=>['code'=>110002,'msg'=>'视频封面图不存在！'],
    'VIDEO_COVER_NOT_MATCH'=>['code'=>110003,'msg'=>'视频封面类型不匹配！'],
    'VIDEO_UPLOAD_INFO_ERROR'=>['code'=>110004,'msg'=>'获取视频上传信息失败！'],

    //栏目
    'COLUMN_EXISTS' => ['code'=>110020,'msg'=>'栏目已存在！'],
    'COLUMN_ADD_FAILED' => ['code'=>110021,'msg'=>'栏目添加失败'],

];
