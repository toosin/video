<?php
return [
    'douyin' => [
        'uri' => 'https://open.douyin.com',
        'client_key' => 'awg2wfm2g9lwmza2',
        'client_secret' => '70fdc06ddc79d0b0b8da4354854c19c5',
        'response_type' => 'code',
        'scope' => 'user_info,video.create,video.data',
        'redirect_uri' => 'http://media.uxiu.com:8081/api/authDouyin',
        'state' => '',
        //'grant_type' => 'authorization_code',
        'grant_type' => [
            'refresh_token' => 'refresh_token',
            'authorization_code' => 'authorization_code',
        ],
        'connect_api' => '/platform/oauth/connect',
        'access_token_api' => '/oauth/access_token',
        'userinfo_api' => '/oauth/userinfo',
        'upload_video_api' => '/video/upload',
        'create_video_api' => '/video/create',
    ],

    'youku' => [
        'auth_api' => 'https://openapi.youku.com/v2/oauth2/authorize',
        'access_token_api' => 'https://openapi.youku.com/router/rest.json',
        'action' => 'youku.user.authorize.token.get',
        'format' => 'json',
        'version' => '3.0',
        'redirect_uri' => 'http://video.zkh168.com/',
        'client_id' => 'xxxxxx',
        'response_type' => 'code',
    ],
];