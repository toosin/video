/**
 * 各种检测功能
 */
function check_amount(amount){
	var reg = /^\d+(\.\d+)?$/;
    return amount.match(reg); 
}

function check_number(number){
	var reg = /^(\d+)$/;
    return number.match(reg); 
}

function check_app_limit_amount_sub(){
	var app_id = $('#app_id').val();
	if(!check_number(app_id)){
		alert('请输入正确的app_id格式！');
		return false;
	}   
	
	var limit_amount = $('#limit_amount').val();
	if(!check_amount(limit_amount)){
		alert('请输入正确的金额格式！');
		return false;
	}     
	
	return true;
}

function check_fee_code_sub(){
	var code = $('#code').val();
	if(!check_number(code)){
		alert('请输入正确的code格式！');
		return false;
	}   
	
	var fee = $('#fee').val();
	if(!check_amount(fee)){
		alert('请输入正确的金额格式！');
		return false;
	}     
	
	return true;
}

function check_cutoff_sub(){
	var app_id = $('#app_id').val();
	if(!check_number(app_id)){
		alert('请输入正确的app_id格式！');
		return false;
	}   
	
	var start_amount = $('#start_amount').val();
	if(!check_amount(start_amount)){
		alert('请输入正确的金额格式！');
		return false;
	}  
	
	var probability = $('#probability').val();
	if(!check_amount(probability)){
		alert('请输入正确的概率格式！');
		return false;
	}
	
	return true;
}

function check_direct_rdo_task_sub(){
	return true;
}

