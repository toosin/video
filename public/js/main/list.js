 $().ready(function(){  
	
    $('#dataTables-example tr th').on('click',function(){
    	$('#sort_column').val($(this).attr('sort_column'));
    	
    	var sort_type = $(this).attr('sort_type');

    	if(sort_type=='asc') {
    		sort_type = 'desc';
    	} else {
    		sort_type = 'asc';
    	}
    	$(this).attr('sort_type',sort_type);
    	$('#sort_type').val(sort_type);

    	$('form').submit();
    })
 })
    
    
function init_sort_info(last_sort_column,last_sort_type) {
	$('th[sort_column='+last_sort_column+']').attr('sort_type',last_sort_type);
	
	var sort_img_both = 'fa fa-sort fa-fw';
	var sort_img_asc = 'fa fa-sort-asc fa-fw';
	var sort_img_desc = 'fa fa-sort-desc fa-fw';
//			alert($('th[sort_column='+last_sort_column+']').find('i').attr('class'))
	if(last_sort_type == 'asc') {
		$('th[sort_column='+last_sort_column+']').find('i').attr('class',sort_img_asc);
	} else if(last_sort_type == 'desc'){
		$('th[sort_column='+last_sort_column+']').find('i').attr('class',sort_img_desc);
	} else {
		$('th[sort_column='+last_sort_column+']').find('i').attr('class',sort_img_both);
	}
			
 }
 
function collect_add(obj,book_id)
{
    $(obj).html('处理中..')
    $.ajax({
        type:'GET',
        url:'/literature/collections/add/'+book_id,
        dataType: "json",
        success: function(data){
            if(data['status']==1)
            {
                $(obj).html('取消收藏').attr('onclick','collect_del(this,'+book_id+')')
            }
        }
    })
}

function collect_del(obj,book_id)
{
    $(obj).html('处理中..')
    $.ajax({
        type:'GET',
        url:'/literature/collections/remove/'+book_id,
        dataType: "json",
        success: function(data){
            if(data['status']==1)
            {
                $(obj).html('收藏').attr('onclick','collect_add(this,'+book_id+')')
            }
        }
    })
}