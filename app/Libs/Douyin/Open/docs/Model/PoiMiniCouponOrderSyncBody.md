# PoiMiniCouponOrderSyncBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_id** | **string** | 小程序的appid | 
**user_open_id** | **string** | 用户的抖音小程序openid | 
**ext_user_id** | **string** | 外部用户Id | [optional] 
**order_type** | [**\Douyin\Open\Model\CouponOrderType**](CouponOrderType.md) |  | 
**date_time** | **int** | 发送请求的时间，精确到毫秒 | 
**order_detail** | **string** | 订单的细节，不同的订单业务有不同的结构体，请具体询问业务方字段结构，参考$order_groupon_detail(团购) | 
**ext_shop_info** | [**\Douyin\Open\Model\ExtShopInfo**](ExtShopInfo.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

