# PoiSupplierSyncEntryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_id** | **string** | 小程序的appid | [optional] 
**path** | **string** | 服务路径 | [optional] 
**params** | **string** | 服务参数json | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

