# SupplierAttributes2204

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **int** | 上线状态(1:上线，2:下线) | [optional] 
**entry** | [**\Douyin\Open\Model\SupplierAttributes2201Entry**](SupplierAttributes2201Entry.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

