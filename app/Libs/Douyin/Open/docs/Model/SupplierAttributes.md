# SupplierAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_1101** | [**\Douyin\Open\Model\HotelServiceFacility[]**](HotelServiceFacility.md) | 酒店服务 | [optional] 
**_1102** | [**\Douyin\Open\Model\HotelServiceFacility[]**](HotelServiceFacility.md) | 酒店设施 | [optional] 
**_1103** | [**\Douyin\Open\Model\HotelServiceFacility[]**](HotelServiceFacility.md) | 酒店特色项目 | [optional] 
**_1104** | [**\Douyin\Open\Model\SupplierAttributes1104**](SupplierAttributes1104.md) |  | [optional] 
**_1105** | **int** | 下单模板。1 - 国内模板, 2 - 海外模板 | [optional] 
**_3101** | [**\Douyin\Open\Model\SupplierAttributes3101**](SupplierAttributes3101.md) |  | [optional] 
**_3102** | **string** | 玩法介绍(不超过500个汉字) | [optional] 
**_3103** | [**\Douyin\Open\Model\SupplierAttributes3103[]**](SupplierAttributes3103.md) | 服务设施列表 | [optional] 
**_3104** | **string** | 景点介绍(不超过1000个汉字) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

