# PoiMiniCateringOrderSyncResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Douyin\Open\Model\PoiMiniCateringOrderSyncResponseData**](PoiMiniCateringOrderSyncResponseData.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

