# SpuAttributes1213

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cancel_ammend** | [**\Douyin\Open\Model\SpuAttributes1213Response**](SpuAttributes1213Response.md) |  | [optional] 
**extra** | **string** | 退改政策自定义内容 | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

