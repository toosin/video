# PoiSpuSyncBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplier_ext_id** | **string** | 接入方店铺ID | 
**spu_ext_id** | **string** | 接入方SPU ID | 
**spu_type** | **int** | spu类型号，1-酒店民宿，90-景区门票，91-团购券 | [optional] 
**status** | [**\Douyin\Open\Model\OnlineStatus**](OnlineStatus.md) |  | 
**name** | **string** | SPU名称 | 
**order** | **int** | SPU展示顺序,降序 | [optional] 
**description** | **string** | SPU描述 | [optional] 
**attributes** | [**\Douyin\Open\Model\SpuAttributes**](SpuAttributes.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

