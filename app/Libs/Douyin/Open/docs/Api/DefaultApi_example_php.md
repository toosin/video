###DefaultApi_devtoolMonitorCallPost
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Douyin\Open\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$access_token = "clt.943da17996fb5cebfbc70c044c3fc25a57T54DcjT6HNKGqnUdxzy1KcxFnZ"; // string | 调用/oauth/client_token/生成的token，此token不需要用户授权。
$time_type = "time_type_example"; // string | 时间类型
$begin_time = "begin_time_example"; // string | 开始日期(20190101)
$end_time = "end_time_example"; // string | 结束日期(20190101)
$body = new \Douyin\Open\Model\DevtoolMonitorCallBody(); // \Douyin\Open\Model\DevtoolMonitorCallBody | 

try {
    $result = $apiInstance->devtoolMonitorCallPost($access_token, $time_type, $begin_time, $end_time, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->devtoolMonitorCallPost: ', $e->getMessage(), PHP_EOL;
}
?>
```
