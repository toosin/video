<?php
namespace App\ViewComposers;

use App\Admin\Models\AdminUser;
use App\Modules\User\Models\Manage;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use DB;

class ProfileComposer {

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Request $request)
    {
        //service container 会自动解析所需的参数
        $uid = $request->session()->get('uid');
        if($uid) {
            $userInfo = Manage::where(['id'=>$uid])->first()->toArray();
            $this->users = $userInfo;
        }else{
            $this->users = ['name'=>'anonymous'];
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('user', ['name'=>$this->users['name']]);
    }

}