<?php

namespace App\Jobs;

use App\Modules\Jobs\Services\UpdateShareService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use Illuminate\Support\Facades\Log;

class Share implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($param)
    {
        //
        $this->data = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        try{
            $data = $this->data;
            UpdateShareService::updateUserHelpNum($data->puid);
            $res = DB::table('reward_details')
                ->where('task_id','=',$data->task_id)
                ->get();
            Log::info(json_encode($res,JSON_UNESCAPED_UNICODE));
            if($res){
                $res = $res->toArray();
                foreach ($res as $value){
                    UpdateShareService::updateUserHelpSucNum($value->uid);
                }
            }
        }catch (\Exception $e){
            Log::error('Share Queue error:'.$e->getCode().$e->getMessage());
        }
        Log::info("Share:executed!".json_encode($data,JSON_UNESCAPED_UNICODE));
    }
}
