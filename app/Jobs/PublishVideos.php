<?php

namespace App\Jobs;


use App\Modules\Platform\Services\ReleaseDouyinServices;
use App\Modules\Platform\Services\ReleaseKuaishouServices;
use App\Modules\Publish\Services\VideoPublishServices;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class PublishVideos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $publishs;
    protected $accountInfo;
    protected $videoInfo;
    protected $formData;
    protected $coverInfo;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($publishs,$accountInfo,$videoInfo,$formData,$coverInfo)
    {
        //
        //Log::debug(var_export($data,true));
        //$this->data = $data;
        $this->publishs = $publishs;
        $this->formData = $formData;
        $this->coverInfo = $coverInfo;
        $this->accountInfo = $accountInfo;
        $this->videoInfo = $videoInfo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $publishs = $this->publishs;
        $formData=$this->formData;
        $coverInfo = $this->coverInfo;
        $accountInfo = $this->accountInfo;
        $videoInfo = $this->videoInfo;
        if($publishs->platform_id == 19) {
            ReleaseDouyinServices::release($accountInfo,$videoInfo,$formData,$publishs);
        }
        if($publishs->platform_id == 21) {
            ReleaseKuaishouServices::release($accountInfo,$videoInfo,$formData,$coverInfo,$publishs);
        }
        if($publishs->platform_id == 14) {
            VideoPublishServices::publishAiqiyi($accountInfo,$videoInfo,$formData,$publishs);
        }
        if($publishs->platform_id == 18) {
            VideoPublishServices::publishHaokan($accountInfo,$videoInfo,$formData,$coverInfo,$publishs);
        }
        if($publishs->platform_id == 16) {
            VideoPublishServices::publishYouku2($accountInfo,$videoInfo,$formData,$publishs);
        }

    }
}
