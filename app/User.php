<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable =
        ['id', 'openid', 'unionid', 'distribution_channel_id', 'head_img',
            'register_ip', 'send_order_id', 'balance', 'sex', 'country', 'city',
            'province', 'nickname', 'charge_balance', 'reward_balance', 'is_new'
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
