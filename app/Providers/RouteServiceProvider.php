<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function map(Router $router)
    {

        $this->mapWebRoutes($router);
        #$this->mapRRoutes($router);
    }

    protected function mapOfficialRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/Manage/OfficialAccountRoutes.php');
        });
    }

    protected function mapSendOrderRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/Manage/SendOrderRoutes.php');
        });
    }

    //UI界面
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/Manage/WebRoutes.php');
        });
    }
    //Vip Channel
    protected function mapVipWebRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/VipChannel/WebRoutes.php');
        });
    }

    protected function mapVipRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/VipChannel/Routes.php');
        });
    }

    protected function mapBookRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/VipChannel/BookRoutes.php');
        });
    }

    protected function mapChannelRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/VipChannel/ChannelRoutes.php');
        });
    }

    protected function mapRRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/VipChannel/Routes.php');
        });
    }

    protected function mapWithDrawRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/VipChannel/WithdrawRoutes.php');
        });
    }

    protected function mapAccountRoutes(Router $router)
    {
        $router->group([
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/Routes/VipChannel/AccountRoutes.php');
        });
    }
}
