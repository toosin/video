<?php

namespace App\Providers;
use DB;
use Illuminate\Routing\Route;
use Illuminate\Support\ServiceProvider;
use Log;
use Illuminate\Http\Request;
class SQLServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        DB::listen(function ($query) use($request) {
                try{
                    $sql_mode = str_replace('?','%s',$query->sql);
                    $sql = vsprintf($sql_mode,$query->bindings);
                    //Log::info('TIME:'.$query->time.'--SQL:'.$sql.'----route:'.$request->url());
                }catch (\Exception $e){
                    Log::info($e);
                }


        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
