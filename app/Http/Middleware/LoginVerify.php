<?php

namespace App\Http\Middleware;

use Closure;

class LoginVerify
{
    /**
     * 判断是否后台是否登录.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $uid = $request->session()->get('uid');
        if(!$uid){
            return redirect()->action('\App\Http\Controllers\Manage\User\LoginController@login')
                ->withInput();
        }

        $response = $next($request);
        return $response;
    }
}
