<?php

namespace App\Http\Middleware;

use Closure;

class VipChannelAuth
{
    /**
     * 判断vip后台是否登录.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $uid = session('uid');
        $user_type = session('user_type');
        if (!$uid || empty($user_type)) {
            return response()->error('NOT_LOGIN');
        }
        $response = $next($request);
        return $response;
    }
}
