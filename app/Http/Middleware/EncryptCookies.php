<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;

class EncryptCookies extends BaseEncrypter
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected static $serialize = true;
    protected $except = [
        'u',
        'send_order_id',
        'from',
        'send_order_continue',
        'send_order_flag',
        'force_subscribe_name',
        'sub_random_num',
        'auth_redirect'
    ];
}
