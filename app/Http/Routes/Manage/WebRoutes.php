<?php
/*Route::group(['domain'=>env('MANAGE_DOMAIN'),'namespace'=>'App\Http\Controllers\Manage\User'],function(){

    Route::get('login','LoginController@login');
    Route::post('login/dologin','LoginController@dologin');

});

Route::group(['domain'=>env('VOICE_CLASS'),'namespace'=>'App\Http\Controllers\ClassVoice\Voice'],function(){

    Route::any('voice/index','ClassController@index');
    Route::any('voice/voiceClassList','ClassController@voiceClassList');
    Route::any('voice/voiceClassContent','ClassController@voiceClassContent');
    Route::any('voice/voiceClassShare','ClassController@voiceClassShare');
    Route::any('voice/voiceIndex','ClassController@voiceIndex');
    Route::any('voice/voiceUnpaid','ClassController@voiceUnpaid');
    Route::any('voice/pay','ClassController@pay');
    Route::any('voice/payCallBack','ClassController@payCallBack');

    Route::any('{slug}','ClassController@index')->where('slug', '(.*)?');;

});*/


Route::group(['domain'=>env('VIDEO_DOMAIN'),'namespace'=>'App\Http\Controllers\Video\User','prefix'=>'api'],function(){

    Route::post('register','LoginController@register');
    Route::post('login','LoginController@login');
    Route::post('logout','LoginController@logout');
    Route::post('getUser','LoginController@getUser');
    Route::post('resetPassword','LoginController@resetPassword');

});

Route::group(['domain'=>env('VIDEO_DOMAIN'),'namespace'=>'App\Http\Controllers\Video\Accounts','prefix'=>'api'],function(){

    Route::post('bind','BindAccountsController@bind');
    Route::post('platforms_list','BindAccountsController@platformsList');
    Route::get('getQrcode/{platform_id}','BindAccountsController@getQrcode');
    Route::post('getScanResult','BindAccountsController@getScanResult');
    Route::any('bindedAccountList','BindAccountsController@bindedAccountList');
    Route::any('auth1','BindAccountsController@auth1');
    Route::any('auth-mp-qq','BindAccountsController@authMpQq');
    Route::any('jump','BindAccountsController@jump');
    Route::any('jumpDouyin','BindAccountsController@jumpDouyin');
    Route::any('authDouyin','BindAccountsController@authDouyin');
    Route::any('jumpYouku','BindAccountsController@jumpYouku');
    Route::any('authYouku','BindAccountsController@authYouku');
    Route::any('auth_wb','BindAccountsController@authWeibo');
    Route::any('delete_binded_platform','BindAccountsController@deletePlatfromAccount');
    //Route::any('upload_img','BindAccountsController@upload_img');
    Route::any('add_column_accounts','ColumnsController@addColumnAccounts');
    Route::any('get_columns','ColumnsController@getColumns');
    Route::any('get_column_accounts','ColumnsController@getColumnAccounts');
    Route::any('delete_column','ColumnsController@deleteColumn');

    Route::get('jumpAuthQQ','BindAccountsController@jumpAuthQQ');
    Route::get('jumpAuthWeibo','BindAccountsController@jumpAuthWeibo');


});
Route::group(['domain'=>env('VIDEO_DOMAIN'),'namespace'=>'App\Http\Controllers\Video\Publish','prefix'=>'api'],function(){

    Route::post('upload_img','ArticlePublishController@upload_img');
    Route::post('upload_img_batch','ArticlePublishController@uploadImgMultiple');
    Route::post('get_image_hash','ArticlePublishController@get_image_hash');
    Route::post('mutiple_publish','ArticlePublishController@mutiple_publish');
    Route::post('upload_img_url','ArticlePublishController@upload_img_by_link');

});
Route::group(['domain'=>env('VIDEO_DOMAIN'),'namespace'=>'App\Http\Controllers\Video\Release','prefix'=>'api'],function(){

    Route::post('uploadVideo','ReleaseVideoController@uploadVideo');
    Route::post('uploadVideoCover','ReleaseVideoController@uploadVideoCover');
    Route::post('releaseVideo','ReleaseVideoController@releaseVideo');
});

Route::group(['domain'=>env('REMOTE_DOMAIN'),'namespace'=>'App\Http\Controllers\Python\Image','prefix'=>'api'],function(){

    Route::post('upload_zhihu_img','ImageUploadController@upload_zhihu');
    Route::post('test_exec','ImageUploadController@test_exec');

});
Route::group(['domain'=>env('VIDEO_DOMAIN'),'namespace'=>'App\Http\Controllers\Video\Publish','prefix'=>'api'],function(){

    Route::post('publised_list','PublishedController@publised_list');
    //Route::post('test_exec','ImageUploadController@test_exec');

});

Route::group(['domain'=>env('VIDEO_DOMAIN'),'namespace'=>'App\Http\Controllers\Video\User'],function(){

    Route::any('{slug}','WelcomeController@welcome')->where('slug', '(.*)?');
});



