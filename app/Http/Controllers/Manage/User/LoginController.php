<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/11/26
 * Time: 18:22
 */
namespace App\Http\Controllers\Manage\User;

use App\Http\Controllers\Controller;
use App\Modules\Manage\Services\UserService;
use Illuminate\Http\Request;

use App\Modules\Manage\Models\User;

class LoginController extends Controller
{
    public $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(Request $request)
    {
        $user = User::where('phone',$request->phone)->first();
        if($user){
            return response()->json([
                'code' => 1,
                'msg' => 'account already exists'
            ]);
        }
        $user = new User();
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->save();

        return $this->login($request);
    }

    public function login(Request $request)
    {
        $credentials = request(['phone', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['code' => 1,'msg' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function logout()
    {
        auth('api')->logout();
        return response()->json([
            'code' => 0,
            'msg' => 'Successfully logged out'
        ]);
    }

    public function getUser()
    {
        $user = auth('api')->user();
        return response()->json([
            'code' => 0,
            'data' => $user
        ]);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'code' => 0,
            'msg' => 'success'
        ])->cookie('token',$token,auth('api')->factory()->getTTL());
    }
}