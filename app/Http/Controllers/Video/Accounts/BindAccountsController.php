<?php
/**
 * Created by PhpStorm.
 * User: tushengxiang
 * Date: 2019/5/27
 * Time: 9:35 AM
 */
namespace App\Http\Controllers\Video\Accounts;

use App\Http\Controllers\Video\Accounts\Transformers\AccountsListTransformer;
use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Platforms;
use App\Modules\Platform\Services\PlatformAccountServices;
use EasyWeChat\Factory;
use GuzzleHttp\Client;
use Hashids;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Modules\Platform\Services\BindPlatformServices;
use Redis;
use Sunra\PhpSimple\HtmlDomParser;

class BindAccountsController extends Controller {

    public function platformsList(Request $request)
    {
        /*if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }*/
        $type = $request->input('type','');
        $res = Platforms::getAll(compact('type'));
        return response()->success($res);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bind(Request $request) {
        $platform_id = $request->input('platform_id','');
        if(empty($platform_id)) {
            return response()->error('PARAM_EMPTY');
        }
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        try { //
            if($platform_id == 2) { //搜狗号 账密
                $user_name = $request->input('user','');
                $password = $request->input('password','');

                $res = $this->bindSougou($user_name,$password,$platform_id);
                if($res) {
                    return response()->success($res);
                }
                return response()->error('BIND_ERROR');
            }

            if($platform_id == 8) { //一点资讯 账密
                $user_name = $request->input('user','');
                $password = $request->input('password','');

                $res = $this->bindYiDian($user_name,$password,$platform_id);
                if($res) {
                    return response()->success($res);
                }
                return response()->error('BIND_ERROR');
            }

            if($platform_id == 21) { //快手 二维码
                return BindPlatformServices::getKuaishou($request);
            }

            if($platform_id == 11) { //知乎 二维码
                return BindPlatformServices::bindZhihu($request);
            }

            if($platform_id == 1) { //百家号
                return BindPlatformServices::bindBaiJh($request);
            }

            if($platform_id == 3) { //快传号
                return BindPlatformServices::bindKuaiChuan($request);
            }
            if($platform_id == 6) { //大鱼号
                return BindPlatformServices::bindDayuhao($request);
            }
            if($platform_id == 7) { //今日头条
                return BindPlatformServices::bindToutiao($request);
            }
            if($platform_id == 4) { //搜狐号
                return BindPlatformServices::bindSouhu($request);
            }

            if($platform_id == 18){ //好看视频
                return BindPlatformServices::bindBaiJh($request);
            }
            if($platform_id == 16) { //优酷
                return BindPlatformServices::youkuUserInfo($request);
            }

        }catch (\Exception $e){
            \Log::error('bind error '.$platform_id.':'.':'.':'.$e->getMessage());
            return response()->error('BIND_ERROR');
        }
    }

    /**
     * 绑定搜狗号
     * @param $user
     * @param $password
     * @param $platform_id
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function bindSougou($user,$password,$platform_id) {
        $client = new Client();
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $uid = auth('api')->user()->id;
        $response = $client->request('POST','http://mp.sogou.com/api/login',[
            'form_params' => [
                'email' => $user,
                'pwd' => $password,
            ]
        ]);
        $header = $response->getHeader('Set-Cookie');
        $body = $response->getBody()->getContents();
        $next_url = json_decode($body,true);
        if($next_url) {
            //var_dump($next_url);
            //return;
            $nextUrl = $next_url['nextUrl'];
            if($nextUrl=='/dashboard') {
                $cookie_str = $this->extractCookie($header);
                $login_info = PlatformAccountServices::getSougouDashbord($cookie_str);
                if(!is_array($login_info)) {
                    return false;
                }
                $platform_info = Platforms::getByid($platform_id);
                $flag = ['account'=>$user];
                $data = [
                    'cookie'=>$cookie_str,
                    'user_id'=>$uid,
                    'platform_id'=>$platform_id,
                    'password'=>$password,
                    'platform'=>$platform_info->platform,
                    'avatar'=>$login_info['avatar'],
                    'nickname'=>$login_info['name'],
                    'binded_time'=>date('Y-m-d H:i:s'),
                    //'nickname'=>$login_info['name'],
                    'status'=>1
                ];
                $res = PlatformAccounts::updateOrCreate($flag,$data);
                return $res;
            }

        }
        return false;
    }


    #绑定好看视频
    private function bindBeatifuVideo($user,$password,$platform_id)
    {
        //获取基本信息

        $client = new Client();
        $response = $client->request('POST','http://baijiahao.baidu.com/builderinner/open/resource/query/fansData',[
            'form_params' => [
                'username' => $user,
                'password' => $password,
            ]
        ]);

    }

    /**
     * 绑定一点资讯
     * @param $user
     * @param $password
     * @param $platform_id
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function bindYiDian($user,$password,$platform_id) {
        $uid=auth('api')->user()->id;
        $client = new Client();
        $response = $client->request('POST','https://mp.yidianzixun.com/sign_in',[
            'form_params' => [
                'username' => $user,
                'password' => $password,
            ]
        ]);
        $header = $response->getHeader('Set-Cookie');
        $body = $response->getBody()->getContents();
        $res = json_decode($body,true);
        //var_dump($body);
        if(is_array($res) && $res['status'] == 'success') {

            $cookie_str =  $this->extractCookie($header);
            $platform_info = Platforms::getByid($platform_id);
            \Log::info('yidian cookie:'.$cookie_str);
            $flag = ['account'=>$user];
            $data = [
                'user_id'=>$uid,
                'cookie'=>$cookie_str,
                'platform_id'=>$platform_id,
                'password'=>$password,
                'platform'=>$platform_info->platform,
                'avatar'=>$res['profile_url'],
                'nickname'=>$res['username'],
                'binded_time'=>date('Y-m-d H:i:s'),
                'status'=>1
            ];
            //var_dump($data);
            $account_info = PlatformAccounts::updateOrCreate($flag,$data);
            return $account_info;
        }
        return false;
    }

    /**
     * 获取扫码登录二维码
     * @param $platform_id
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getQrcode($platform_id) {
        //$platform_id =  $request->input('platform_id','');
        header('Content-Type: image/png');
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $uid = auth('api')->user()->id;
        if($platform_id == 11) { //知乎

            $client = new Client(['base_uri'=>'https://www.zhihu.com','timeout'=>2.0]);
            //$jar = new \GuzzleHttp\Cookie\CookieJar();
            $response = $client->request('GET','/signin');
            $header = $response->getHeader('Set-Cookie');
            $cookie_str = $this->extractCookie($header);
            $response_ = $client->request('POST','/udid',
                    ['headers' => [
                        'Cookie'=>$cookie_str
                    ]]
            );
            $header2 = $response_->getHeader('Set-Cookie');
            $cookie_str2 = $this->extractCookie($header2);
            $cookie_str3 = $cookie_str.';'.$cookie_str2;
            $response2 = $client->request('POST','/api/v3/account/api/login/qrcode',
                    ['headers' => [
                        'Cookie'=>$cookie_str3
                    ]]
            );
            $token_raw = json_decode($response2->getBody()->getContents(),true);
            $token =$token_raw['token'];
            $response3 = $client->request('GET','/api/v3/account/api/login/qrcode/'.$token.'/image',['headers' => [
                'Cookie'=>$cookie_str3
            ]]);
            $contents = $response3->getBody()->getContents();
            //file_put_contents(storage_path('qrcode.png'),$contents);

            Redis::hMset('qrcode_info:'.$platform_id.':'.$uid,['cookie'=>$cookie_str3,'token'=>$token]);
            \Log::info('cookie_get_qrcode:'.$cookie_str3);
            echo $contents;
        }

        if($platform_id == 21) { //快手
            $img = BindPlatformServices::getKuaishouQrcode($platform_id);
            //echo '<img src="' . $img . '">';
            echo base64_decode($img);
        }

        if($platform_id == 12) { //大众点评
            echo BindPlatformServices::getDazhongQrcode($platform_id);
        }
        if($platform_id == 14) { //爱奇艺二维码展示
            echo BindPlatformServices::getAiqiyiQrcode($platform_id);
        }
        if($platform_id == 16) { //优酷二维码展示
            echo BindPlatformServices::getYoukuQrcode($platform_id);
        }
        if($platform_id == 17) { //搜狐视频二维码展示
            echo BindPlatformServices::getSouhuQrcode($platform_id);
        }

    }

    /**
     * 查询扫码结果
     * @param Request $request
     * @return mixed
     */
    public function getScanResult(Request $request) {
        $platform_id = $request->input('platform_id');
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        if($platform_id == 11) {
            return BindPlatformServices::getZhihuScanResult($request);
        }
        if($platform_id == 21) { //快手
            return BindPlatformServices::getKuaishouScanResult($request);
        }
        if($platform_id == 12) { //大众点评
            return BindPlatformServices::getDazhongScanResult($request);
        }
        if($platform_id == 14) { //aiqiyi
            return BindPlatformServices::getAiqiyiScanResult($request);
        }
        if($platform_id == 16) { //youku
            return BindPlatformServices::getYoukuScanResult($request);
        }

    }

    /**
     * 获取绑定的账号列表
     * @param Request $request
     * @return mixed
     */
    public function bindedAccountList(Request $request) {
        $type = $request->input('type','');
        $paginate = $request->input('paginate',30);
        //TODO uid修改成正式ID
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $uid= auth('api')->user()->id;
        $res = PlatformAccountServices::getBindedAccountsList($uid,compact('type'),$paginate);
        return response()->pagination(new AccountsListTransformer(),$res);
    }

    public function bindKuaichuan(Request $request) {
        $client = new Client();
        [
            [
                'label'=>'封面',
                'field'=>'cover',
                'type'=>'radio',
                'type_options'=>[['label'=>'单图','field'=>'single'],['label'=>'多图','field'=>'multiple']],
                'required' => 'true'
            ],
            [
                'label'=>'文章简介',
                'field'=>'describe',
                'type'=>'text',
                'required'=>'true'
            ]
        ];
    }

    public function jumpAuthWeibo(Request $request) {
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $uid = auth('api')->user()->id;;
        $uid = Hashids::encode($uid);
        $url = 'https://api.weibo.com/oauth2/authorize?client_id=2393108284&response_type=code&redirect_uri=http://video.zkh168.com/api/auth_wb?sign='.$uid;//'client_id=2393108284&response_type=code&redirect_uri=http://video.zkh168.com/api/auth_wb';
        header('Location:'.$url);
    }

    public function authWeibo(Request $request) {
        //$uid = Hashids::decode($request->input('sign',''))[0];//
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $uid = auth('api')->user()->id;
        /*$appid = '2393108284';//'a0cb9471652cbe4fd878e407c2d148b1';
        $app_secret = '094e2f4920b199885b87dd491cc09005';//'ae4be80fd2d5c3612b1b6a098f9f639393e3c05f';
        */
        $appid = '3305331824';
        $app_secret = '8dd4cd7ea11e85c22ae9c3fe9e8d815f';
        $code = $request->input('code','');
        //var_dump($code);
        $client = new Client();
        $redirect_url = 'http://media.uxiu.com:8081/api/auth_wb';
        $res = $client->request('POST','https://api.weibo.com/oauth2/access_token?client_id='.$appid.'&client_secret='.$app_secret.'&grant_type=authorization_code&redirect_uri='.$redirect_url.'&code='.$code);
        $auth_info = $res->getBody()->getContents();
        $auth_info = json_decode($auth_info,true);
        $platform_uid = $auth_info['uid'];
        $access_token = $auth_info['access_token'];
        $expired_in = $auth_info['expires_in'];
        $expired_at = date('Y-m-d H:i:s',time()+$expired_in - 24*3600);
        //获取用户信息
        $user_info_response = $client->request('GET','https://api.weibo.com/2/users/show.json?access_token='.$access_token.'&uid='.$platform_uid);
        $user_info_json = $user_info_response->getBody()->getContents();
        $user_info = json_decode($user_info_json,true);
        $nick_name = $user_info['screen_name'];
        $avatar =  $user_info['profile_image_url'];
        $extra_info = $user_info_json;
        $platform_id = 10;
        $platform_info = Platforms::getByid($platform_id);
        $flag = ['nickname'=>$nick_name,'user_id'=>$uid];
        $update = ['platform_user_id'=>$platform_uid,
            'avatar'=>$avatar,'extra_info'=>$extra_info,
            'access_token'=>$access_token,'access_token_expire'=>$expired_at,
            'status'=>1,'platform_id'=>$platform_id,'platform'=>$platform_info['platform']
        ];
        $res = PlatformAccounts::updateOrCreate($flag,$update);
        header('Location:'.'http://media.uxiu.com:8081/');
        //return redirect()->route('/');
        //$url = 'https://api.weibo.com/oauth2/authorize?client_id=2393108284&response_type=code&redirect_uri=http://video.zkh168.com';

    }

    public function jumpAuthQQ(Request $request) {
        //https://auth.om.qq.com/omoauth2/authorize?response_type=code&client_id=1c805ef8c87d1d46e1375d08728fde77&redirect_uri=http://video.zkh168.com/api/auth1&state=1
        $user_id = auth('api')->user()->id;
        //$url = 'https://auth.om.qq.com/omoauth2/authorize?response_type=code&client_id=1c805ef8c87d1d46e1375d08728fde77&redirect_uri=http://media.uxiu.com:8081/api/auth1&state='.$user_id;
        $url = 'https://auth.om.qq.com/omoauth2/authorize?response_type=code&client_id=b765aa5fca64fe39c4d3e43a1c2392be&redirect_uri=http://media.uxiu.com:8081/api/auth1&state='.$user_id;
        header('Location:'.$url);
    }
    public function auth1(Request $request) {
        //$uid= auth('api')->user()->id;
        $uid = $request->input('state','');
        $code = $request->input('code','');
        $client = new Client();
        //$appid = '1c805ef8c87d1d46e1375d08728fde77';//'a0cb9471652cbe4fd878e407c2d148b1';
        //$app_secret = '0f2589b93f38efc8bbe8c1ca17be51bea2466c07';//'ae4be80fd2d5c3612b1b6a098f9f639393e3c05f';
        $appid = 'b765aa5fca64fe39c4d3e43a1c2392be';//'a0cb9471652cbe4fd878e407c2d148b1';
        $app_secret = 'ff90cf0603631f39484f6cc56ac0af1d85c7b1b9';//'ae4be80fd2d5c3612b1b6a098f9f639393e3c05f';
        $res  = $client->request('POST','https://auth.om.qq.com/omoauth2/accesstoken?grant_type=authorization_code&client_id='.$appid.'&client_secret='.$app_secret.'&code='.$code);
        $response = $res->getBody()->getContents();
        \Log::info('test_response:'.$response);
        //var_dump($response);
        //"{"code":"0","data":{"access_token":"WMOFDTKFOCO-Q92ZFO9DRQ","expires_in":7200,"openid":"b4c38dafb0081a9dbefcf555bbdb4e74","refresh_token":"-X3LWZEUXRY4ICZ0FJJYRA"}}
        $auth_info = json_decode($response,true);
        if($auth_info['code'] == 0) {
            $auth_data = $auth_info['data'];

            Redis::hMSet('qq_om_access_token_info:'.$uid,['access_token'=>$auth_data['access_token'],'refresh_token'=>$auth_data['refresh_token']]);
            $get_base_info_url = 'https://api.om.qq.com/media/basicinfoauth?access_token='.$auth_data['access_token'].'&openid='.$auth_data['openid'];
            $base_info_response = $client->request('GET',$get_base_info_url);
            $base_info_response_r = $base_info_response->getBody()->getContents();
            $base_info = json_decode($base_info_response_r,true);
            if($base_info['code'] == 0) {
                $platform_id = 5;
                $platform_info = Platforms::getByid($platform_id);

                $flag = ['openid'=>$auth_data['openid']];
                $data = [
                    'user_id'=>$uid,
                    //'cookie'=>$cookie_str,
                    'platform_id'=>$platform_id,
                    //'password'=>$password,
                    'platform'=>$platform_info->platform,
                    'avatar'=>$base_info['data']['header'],
                    'nickname'=>$base_info['data']['nick'],
                    'status'=>1
                ];
                $account_info = PlatformAccounts::updateOrCreate($flag,$data);
                header('Location:'.'http://media.uxiu.com:8081/');
                //return $account_info;
            }
        }
        \Log::error('auth1_error:'.$response);
        return response()->error('AUTH_ERROR');
    }

    public function authMpQq(Request $request) {
        //$uid = 1;
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $uid = auth('api')->user()->id;
        $state = $request->input('state','');
        $code = $request->input('code','');
        $client = new Client();
        $appid = '1c805ef8c87d1d46e1375d08728fde77';//'a0cb9471652cbe4fd878e407c2d148b1';
        $app_secret = '0f2589b93f38efc8bbe8c1ca17be51bea2466c07';//'ae4be80fd2d5c3612b1b6a098f9f639393e3c05f';
        $res  = $client->request('POST','https://auth.om.qq.com/omoauth2/accesstoken?grant_type=authorization_code&client_id='.$appid.'&client_secret='.$app_secret.'&code='.$code);
        $response = $res->getBody()->getContents();
        \Log::info('test_response:'.$response);
        //var_dump($response);
        //"{"code":"0","data":{"access_token":"WMOFDTKFOCO-Q92ZFO9DRQ","expires_in":7200,"openid":"b4c38dafb0081a9dbefcf555bbdb4e74","refresh_token":"-X3LWZEUXRY4ICZ0FJJYRA"}}
        $auth_info = json_decode($response,true);
        if($auth_info['code'] == 0) {
            $auth_data = $auth_info['data'];
            $get_base_info_url = 'https://api.om.qq.com/media/basicinfoauth?access_token='.$auth_data['access_token'].'&openid='.$auth_data['openid'];
            $base_info_response = $client->request('GET',$get_base_info_url);
            $base_info_response_r = $base_info_response->getBody()->getContents();
            $base_info = json_decode($base_info_response_r,true);
            if($base_info['code'] == 0) {
                $platform_id = 15;
                $platform_info = Platforms::getByid($platform_id);

                $account_info = PlatformAccounts::getByOpenIdAndPlatform($auth_data['openid'],$platform_info->platform);



                $data = [
                    'user_id'=>$uid,
                    //'cookie'=>$cookie_str,
                    'platform_id'=>$platform_id,
                    //'password'=>$password,
                    'platform'=>$platform_info->platform,
                    'avatar'=>$base_info['data']['header'],
                    'nickname'=>$base_info['data']['nick'],
                    'status'=>1
                ];
                if (empty($account_info)) {
                    $flag = ['openid' => $auth_data['openid']];
                } else {
                    $flag = ['id' => $account_info['id']];
                }
                $account_info = PlatformAccounts::updateOrCreate($flag,$data);

                Redis::hMSet('qq_om_access_account_token_info:'.$account_info['id'],['access_token'=>$auth_data['access_token'],'refresh_token'=>$auth_data['refresh_token']]);


                return $account_info;

            }
        }
        \Log::error('auth1_error:'.$response);
        return response()->error('AUTH_ERROR');
    }

    //抖音跳转
    public function jumpDouyin()
    {
        $config = config('key.douyin');
        $client_key = $config['client_key'];
        $response_type = $config['response_type'];
        $scope = $config['scope'];
        $redirect_uri = $config['redirect_uri'];
        $queryStr = http_build_query([
            'client_key' => $client_key,
            'response_type' => $response_type,
            'scope' => $scope,
            'redirect_uri' => $redirect_uri,
        ]);
        $url = $config['uri'] . $config['connect_api'] . '?' . $queryStr;
        header('Location:'.$url);
    }

    //抖音授权
    public function authDouyin(Request $request)
    {
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $user_id= auth('api')->user()->id;
        //$user = auth('api')->user()->id;

        $input = $request->input();
        $code = $input['code'];

        $config = config('key.douyin');

        $queryStr = http_build_query([
            'client_key' => $config['client_key'],
            'client_secret' => $config['client_secret'],
            'code' => $code,
            'grant_type' => $config['grant_type']['authorization_code'],
        ]);

        $client = new Client();
        //获取access_token
        $res = $client->request('GET',$config['uri'] . $config['access_token_api'] . '?' . $queryStr);
        $response = $res->getBody()->getContents();
        $auth_info = json_decode($response,true);

        if(isset($auth_info['data']['error_code']) && $auth_info['data']['error_code'] == 0){
            $platform_id = 19;

            $queryStr = http_build_query([
                'access_token' => $auth_info['data']['access_token'],
                'open_id' => $auth_info['data']['open_id'],
            ]);
            //获取用户信息
            $user_info = $client->request('GET',$config['uri'] . $config['userinfo_api'] . '?' . $queryStr);
            $response = $user_info->getBody()->getContents();
            $user_info = json_decode($response,true);

            $platform_info = Platforms::getByid($platform_id);
            $flag = ['user_id' => $user_id,'openid' => $auth_info['data']['open_id']];
            $data = [
                'nickname' => $user_info['data']['nickname'],
                'avatar' => $user_info['data']['avatar'],
                'access_token'=>$auth_info['data']['access_token'],
                'platform' => $platform_info->platform,
                'platform_id' => $platform_id,
                'status' => 1
            ];
            $account_info = PlatformAccounts::updateOrCreate($flag,$data);
            Redis::hMSet('access_token_info:' . $platform_id . ':' . $user_id.':'.$account_info['id'],['access_token' => $auth_info['data']['access_token'],'refresh_token' => $auth_info['data']['refresh_token']]);

            header('Location:/');
        }

    }

    //优酷跳转
    public function jumpYouku()
    {
        $config = config('key.youku');
        $client_id = $config['client_id'];
        $response_type = $config['response_type'];
        $redirect_uri = $config['redirect_uri'];
        $queryStr = http_build_query([
            'client_id' => $client_id,
            'response_type' => $response_type,
            'redirect_uri' => $redirect_uri,
            //'state' => 'xyz',
        ]);
        $url = $config['auth_api'] . '?' . $queryStr;
        header('Location:'.$url);
    }

    //优酷授权
    public function authYouku(Request $request)
    {
        $user = auth('api')->user()->id;
        if(!$user){
            $user_id = 1;
        }else{
            $user_id = $user->id;
        }

        $input = $request->input();
        $code = $input['code'];

        $config = config('key.youku');

        $client = new Client();
        //获取access_token
        $time = time();
        $res = $client->request('POST',$config['access_token_api'],[
            'form_params' => [
                'action' => $config['action'],
                'client_id' => $config['client_id'],
                'code' => $code,
                'format' => $config['format'],
                'timestamp' => $time,
                'version' => $config['version'],
                'sign' => md5(urlencode('action=' . $config['action'] . ',client_id=' . $config['client_id'] . ',code=' . $code . ',format=' . $config['format'] . ',timestamp=' . $time . ',version=' . $config['version']) . 'secret'),
            ]
        ]);
        $response = $res->getBody()->getContents();
        $auth_info = json_decode($response,true);

        if(isset($auth_info['errno']) && $auth_info['errno'] == 0){
            $platform_id = 16;
            Redis::hMSet('access_token_info:' . $platform_id . ':' . $user_id,['access_token' => $auth_info['token']['accessToken'],'refresh_token' => $auth_info['token']['refreshToken']]);
            return response()->success();
        }
    }

    public function deletePlatfromAccount(Request $request) {
        $id = $request->input('id','');
        if(empty($id)) {
            return response()->error('PARAM_EMPTY');
        }
        $account_info = PlatformAccounts::getById($id);
        if(empty($account_info)) {
            return response()->error('PARAM_ERROR');
        }
        $account_info->status = 2;
        $account_info->save();
        return response()->success();
    }

    /**
     * 提取有效cookie
     * @param $header
     * @return string
     */
    private function extractCookie($header) {
        $cookie_filter_arr = [];
        foreach($header as $key=>$cookie) {
            $each_cookie = explode(';',$cookie);
            $cookie_filter_arr[] = $each_cookie[0];
        }
        $cookie_str = implode(';',$cookie_filter_arr);
        return $cookie_str;
    }
}
