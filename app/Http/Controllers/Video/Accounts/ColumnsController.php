<?php
/**
 * Created by PhpStorm.
 * User: tushengxiang
 * Date: 2019/5/27
 * Time: 9:35 AM
 */
namespace App\Http\Controllers\Video\Accounts;

use App\Http\Controllers\Video\Accounts\Transformers\AccountsListTransformer;
use App\Modules\Platform\Models\ColumnAccounts;
use App\Modules\Platform\Models\Columns;
use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Platforms;
use App\Modules\Platform\Services\PlatformAccountServices;
use EasyWeChat\Factory;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Modules\Platform\Services\BindPlatformServices;
use Redis;
use Sunra\PhpSimple\HtmlDomParser;

class ColumnsController extends Controller {

    public function addColumnAccounts(Request $request) {
        $user_id = auth('api')->user()->id;
        $column_name = $request->input('column_name','');
        $type = $request->input('column_type',1);
        $column_id  = $request->input('column_id','');
        $accounts_list = $request->input('accounts_list','');
        $accounts_list_arr = explode(',',$accounts_list);
        if(empty($column_id)) { //添加
            if(empty($column_name) || empty($accounts_list)) {
                return response()->error('PARAM_EMPTY');
            }
            $column_info = Columns::getByName($column_name,$user_id,$type);
            if($column_info) {
                return response()->error('COLUMN_EXISTS');
            }
            $column_data = ['column_name'=>$column_name,'status'=>1,'user_id'=>$user_id,'type'=>$type];
            $column_added = Columns::create($column_data);
            if(empty($column_added)) {
                return response()->error('COLUMN_ADD_FAILED');
            }
            $column_id = $column_added->id;
        }
        foreach ($accounts_list_arr as $key=>$each) {
            $column_account_add = array(
                'column_id'=>$column_id,
                'platform_account_id'=>$each,
            );
            $column_account_up = array(
                'status'=>1,
                'type'=>$type
            );
            $res = ColumnAccounts::updateOrCreate($column_account_add,$column_account_up);
        }
        return response()->success();
    }

    public function getColumns(Request $request) {
        $user_id = auth('api')->user()->id;
        $type = $request->input('type',1);
        $columns = Columns::getUserColumns($user_id,$type);
        foreach ($columns as $key=>$column) {
            $column->column_accounts = ColumnAccounts::getColumnAccounts($column->id);
            $column->bind_account_num =count($column->column_accounts);
            //$column->column_accounts = ColumnAccounts::getColumnAccounts($column->id);
        }
        return response()->success($columns);
    }

    public function getColumnAccounts(Request $request) {
        $user_id =  auth('api')->user()->id;
        $column_id = $request->input('column_id','');
        if(empty($column_id)) {
            return response()->error('PARAM_EMPTY');
        }
        $res = ColumnAccounts::getColumnAccounts($column_id);
        return response()->success($res);
    }

    public function deleteColumn(Request $request) {
        $id= $request->input('id');
        if(empty($id)) {
            return response()->error('PARAM_EMPTY');
        }

        $column = Columns::getById($id);
        if(empty($column)) {
            return response()->error('PARAM_ERROR');
        }
        $column->status = 0;
        $column->save();
        return response()->success();
    }
}
