<?php
/**
 * Created by PhpStorm.
 * Date: 2017/3/31
 * Time: 14:02
 */

namespace App\Http\Controllers\Video\Accounts\Transformers;

class platformsListTransformer
{
    public function transform($item)
    {
        return [
            'id'=>$item->id,
            'platform' => $item->platform,
            'desc' => $item->desc,
            'type' => $item->type
        ];
    }
}