<?php
/**
 * Created by PhpStorm.
 * Date: 2017/3/31
 * Time: 14:02
 */

namespace App\Http\Controllers\Video\Accounts\Transformers;

class AccountsListTransformer
{
    public function transform($item)
    {
        return [
            'id'=>$item->id,
            'platform_id' => $item->platform_id,
            'platform_name' => $item->platform_name,
            'platform_icon' => $item->platform_icon,
            'extral_fields'=>$item->extral_fields,
            'nickname' => $item->nickname,
            'avatar' => $item->avatar,
            'status' => $item->status,
            'auth_url' => $item->auth_url

        ];
    }
}