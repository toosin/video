<?php
/**
 * Created by PhpStorm.
 * Date: 2017/3/31
 * Time: 14:02
 */

namespace App\Http\Controllers\Video\Publish\Transformers;

use Hashids;

class PublishedTransformer
{
    public function transform($item)
    {   //WAIT_UP 等待上传，UPLOADING 正在上传，UPLOAD_SUCCESS 上传成功，UPLOAD_FAILED 上传失败，
        // REVIEWING 审核中，REVIEW_SUCCESS 审核通过，REVIEW_FAILED 审核不通过
        switch ($item->status) {
            case 'WAIT_UP':$item->status = '等待上传';break;
            case 'UPLOADING':$item->status = '正在上传';break;
            case 'UPLOAD_SUCCESS':$item->status = '上传成功';break;
            case 'UPLOAD_FAILED':$item->status = '上传失败';break;
            case 'REVIEWING':$item->status = '审核中';break;
            case 'REVIEW_SUCCESS':$item->status = '审核通过';break;
            case 'REVIEW_FAILED':$item->status = '审核不通过';break;
            case 'WAITING_UPLOAD':$item->status = '等待上传';break;

        }
        return [
            'id' => $item->id,
            'title'=>$item->title,
            'publish_time'=>$item->publish_time,
            'platform_name'=>$item->platform_name,
            'account_name'=>$item->nickname,
            'reason'=>$item->reason,
            'status'=>$item->status
        ];
    }
}