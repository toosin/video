<?php
/**
 * Created by PhpStorm.
 * User: tushengxiang
 * Date: 2019/5/27
 * Time: 9:35 AM
 */
namespace App\Http\Controllers\Video\Publish;

use App\Http\Controllers\Video\Accounts\Transformers\AccountsListTransformer;
use App\Http\Controllers\Video\Publish\Transformers\PublishedTransformer;
use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Platforms;
use App\Modules\Platform\Models\Publishes;
use App\Modules\Platform\Services\PlatformAccountServices;
use App\Modules\Publish\Services\ArticlePublishServices;
use EasyWeChat\Factory;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Modules\Platform\Services\BindPlatformServices;
use Redis;
use Sunra\PhpSimple\HtmlDomParser;

class PublishedController extends Controller {
    public function publised_list(Request $request) {
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $uid = auth('api')->user()->id;
        $status = $request->input('status','');
        $type = $request->input('type',1);
        $res = Publishes::getPublishedList($uid,compact('status','type'));
        return  response()->pagination(new PublishedTransformer(),$res);
    }
}