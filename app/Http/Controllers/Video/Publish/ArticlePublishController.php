<?php
/**
 * Created by PhpStorm.
 * User: tushengxiang
 * Date: 2019/5/27
 * Time: 9:35 AM
 */
namespace App\Http\Controllers\Video\Publish;

use App\Http\Controllers\Video\Accounts\Transformers\AccountsListTransformer;
use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Platforms;
use App\Modules\Platform\Models\Publishes;
use App\Modules\Platform\Services\PlatformAccountServices;
use App\Modules\Publish\Services\ArticlePublishServices;
use EasyWeChat\Factory;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Modules\Platform\Services\BindPlatformServices;
use Redis;
use Sunra\PhpSimple\HtmlDomParser;

class ArticlePublishController extends Controller {

    public function mutiple_publish(Request $request) {
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $json = file_get_contents('php://input');
        \Log::info('json:'.$json);
        $res = json_decode($json,true);

        $accounts = $res['accounts'];
        $publish_error_msgs = [];
        foreach ($accounts as $key=>$account) {
            $account_info = PlatformAccounts::getById($account['accountId']);
            $platform_id = $account_info->platform_id;
            if($platform_id == 8) { //一点资讯发文
                try{
                    $content = $account['formData']['content'];
                    $rep = '<div custom-type="image" class="insert-image-container insert-plugin-item" image_src="${1}" image_format="" image_caption=""><img data-caption="true" src="${1}" data-format="" title="" image_caption="" contenteditable="false"><div class="image-caption-wrap" contenteditable="false"><input type="text" class="image-caption-input" placeholder="添加图注（不超过50字）" maxlength="50" value="" onchange="event &amp;&amp; event.target &amp;&amp; event.target.value &amp;&amp; this.parentElement.parentElement&amp;&amp; (this.parentElement.parentElement.setAttribute(\'image_caption\', event.target.value))"><span class="image-caption-icon"></span></div>';

                    //preg_replace('/<[\s]*img(.+?)src[\s]*=[\s]*[\'|\"](.+)[\'|\"]([.|\]*?)>/','<div custom-type="image" class="insert-image-container insert-plugin-item" image_src="${1}" image_format="" image_caption=""><img data-caption="true" src="${1}" data-format="" title="" image_caption="" contenteditable="false"><div class="image-caption-wrap" contenteditable="false"><input type="text" class="image-caption-input" placeholder="添加图注（不超过50字）" maxlength="50" value="" onchange="event &amp;&amp; event.target &amp;&amp; event.target.value &amp;&amp; this.parentElement.parentElement&amp;&amp; (this.parentElement.parentElement.setAttribute(\'image_caption\', event.target.value))"><span class="image-caption-icon"></span></div>',$f);
                    //$content_rep = preg_replace('/<\s*img\s+[^>]*?src\s*=\s*(\'|\")(.*?)\\1[^>]*?\/?\s*>/i',$rep,$content);
                    $content_rep = preg_replace("/<img.+?src=[\"|\\'](.+?)[\"|\\'].*?>/",$rep,$content);
                    $to_up = array('activity_id'=>0,'all_data'=>[],'appealStatus'=>'','audios'=>[],'can_delete'=>true,'can_editor'=>true,'cate'=>'','cateB'=>'','comment'=>'','content'=>$content_rep,'original'=>0,
                        'reward'=>0,
                        'videos'=>[],
                        'audios'=>[],
                        'votes'=>['vote_id'=>'','vote_options'=>'','vote_end_time'=>'','vote_title'=>'','vote_type'=>'','vote_isAdded'=>false],
                        'images'=>[],
                        'goods'=>[],
                        'is_mobile'=>0,
                        'status'=>0,
                        'import_url'=>'',
                        'import_hash'=>null,
                        'image_urls'=>[],
                        'minTimingHour'=>3,
                        'maxTimingDay'=>7,
                        'tags'=>[],
                        'isPubed'=>false,
                        'lastSaveTime'=>'',
                        'dirty'=>false,
                        'editorType'=>'articleEditor',
                        "activity_id"=> 0,
                        "join_activity"=> 0,
                        "video_cate"=> "",
                        "video_cateB"=> "",
                        "type"=> 32,
                        "subUseStatus"=> -1,
                        "push_status"=> 9,
                        "push_failed_reason"=> "",
                        "can_delete"=> true,
                        "can_editor"=> true,
                        "scheduleTime"=> "",
                        "updatedTime"=> time(),
                        "all_data"=> [],
                        "newsId"=> "",
                        "comment"=> "",
                        "appealStatus"=>  "",
                        "isStick"=>  "",
                        'title'=>$account['formData']['title'],
                        "date"=>  "",
                        "editable"=>  1,
                        "data"=>  [],
                        "subTitleData"=>  [],
                        "ugcVideo"=>  0,
                        "notSaveToStore"=>  true,

                    );

                    $to_up['coverType']=isset($account['formData']['covers_type'])?$account['formData']['covers_type']:'default';

                    if($to_up['coverType'] == '0'||$to_up['coverType'] == 0){
                        $to_up['coverType'] = 'default';
                    }
                    if($to_up['coverType'] == '1'||$to_up['coverType'] == 1){
                        $to_up['coverType'] = 'single';
                    }
                    if($to_up['coverType'] == '3'||$to_up['coverType'] == 3){
                        $to_up['coverType'] = 'three';
                    }
                    $to_up['covers']=(isset($account['formData']['covers'])&&$account['formData']['covers'])?$account['formData']['covers']:[];
                    $to_up["subTitle"] = (isset($account['formData']['sub_title'])&&$account['formData']['sub_title'])?$account['formData']['sub_title']:'';
                    $to_up["hasSubTitle"] = (isset($account['formData']['sub_title'])&&$account['formData']['sub_title'])?1:0;
                    if ($to_up["hasSubTitle"]==1) {
                        if(mb_strlen($account['formData']['sub_title'],'UTF-8')<5||mb_strlen($account['formData']['sub_title'],'UTF-8')>30) {
                            return response()->error('TITLE_LENGTH_LIMIT');
                        }
                    }
                    //if (isset($account['formData']['sub_title'])&&$account['formData']['sub_title']) {
                    /*if(mb_strlen($account['formData']['title'],'UTF-8')<5||mb_strlen($account['formData']['title'],'UTF-8')>30) {
                        return response()->error('TITLE_LENGTH_LIMIT');
                    }*/
                    //}
                    $client = new Client();
                    //\Log::info(json_encode($to_up,JSON_UNESCAPED_UNICODE));
                    $response = $client->request('POST','https://mp.yidianzixun.com/model/Article?_rk=1575646351443',['json'=>$to_up,'headers'=>['Cookie'=>$account_info['cookie']]]);
                    $ret = $response->getBody()->getContents();
                    $ret = json_decode($ret,true);
                    \Log::info($ret);
                    if(isset($ret['id'])){
                        if(isset($account['formData']['send_time'])&& !empty($account['formData']['send_time'])){
                            $response_schedule = $client->request('GET','https://mp.yidianzixun.com/api/post/schedule/create?id='.$ret['id'].'&pub_time='.(strtotime($account['formData']['send_time'])*1000).'&min_timing_hour=3&max_timing_day=7');
                            $response_schedule_format = json_decode($response_schedule->getBody()->getContents(),true);
                            \Log::info('ddddddddddddddd-:'.$response_schedule->getBody()->getContents());
                        }
                        $url = 'https://mp.yidianzixun.com/api/post/publish?post_id='.$ret['id'].'&_rk=1575647431698';
                        $re = $client->request('GET',$url,['headers'=>['Cookie'=>$account_info['cookie']]]);
                        $res = $re->getBody()->getContents();
                        $publish_data = [
                            'title'=>$account['formData']['title'],
                            'platform_id'=>$account_info->platform_id,
                            'platform_account_id'=>$account_info->id,
                            'user_id'=>$account_info->user_id,
                            'content_id'=>$ret['id'],
                            'contents_info'=>json_encode($to_up,JSON_UNESCAPED_UNICODE),
                            'response'=>$res
                        ];
                        if(isset($account['formData']['send_time'])&& !empty($account['formData']['send_time'])){
                            $publish_data['status'] = 'UPLOADING';
                            $publish_data['timer'] = $account['formData']['send_time'];
                        }
                        Publishes::create($publish_data);
                        \Log::info($res);
                        //return response()->success();
                    }else{
                        $publish_data = [
                            'title'=>$account['formData']['title'],
                            'platform_id'=>$account_info->platform_id,
                            'platform_account_id'=>$account_info->id,
                            'user_id'=>$account_info->user_id,
                            'content_id'=>$ret['id'],
                            'contents_info'=>json_encode($to_up,JSON_UNESCAPED_UNICODE),
                            'response'=>json_encode($ret),
                            'status'=>'UPLOAD_FAILED',
                            'reason'=>isset($ret['reason'])?$ret['reason']:''
                        ];
                        if(isset($account['formData']['send_time'])&& !empty($account['formData']['send_time'])){
                            $publish_data['status'] = 'UPLOADING';
                            $publish_data['timer'] = $account['formData']['send_time'];
                        }
                        Publishes::create($publish_data);
                        \Log::info('8 publish failed');
                    }
                }catch (\Exception $e) {
                    \Log::error('yidian publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '一点号';
                }

                //return response()->error('PUBLISH_FAILED');
            }
            if($platform_id == 5) {
                try{
                    $res = ArticlePublishServices::publishQQ($account,$account_info);
                }catch (\Exception $e){
                    \Log::error('qq publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '企鹅号';
                }
            }
            if($platform_id == 11) {
                try{
                    $res = ArticlePublishServices::publishZhiHu($account,$account_info);
                    if($res === false) {
                        //return response()->error('PUBLISH_FAILED');
                        \Log::error('11 publish failed');
                        $publish_error_msgs[] = '知乎';
                    }
                }catch (\Exception $e){
                    $publish_error_msgs[] = '知乎号';
                    \Log::error('知乎发文失败：'.$e->getMessage());
                }

                //return response()->success();
            }
            if($platform_id == 1) {
                try{
                    ArticlePublishServices::publishBjh($account,$account_info);
                }catch (\Exception $e){
                    \Log::error('baijiahao publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '百家号';
                }

            }
            if($platform_id == 10) {
                try{
                    ArticlePublishServices::publishWeibo($account,$account_info);
                }catch (\Exception $e){
                    \Log::error('weibo publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '微博';
                }

            }
            if($platform_id == 3) {
                try{
                    ArticlePublishServices::publishKuaichuan($account,$account_info);
                }catch (\Exception $e){
                    \Log::error('kuaichuanhao publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '快传号';
                }

            }
            if($platform_id == 6) {
                $res = [
                    ['label'=>'封面类型','field'=>'image_style','type'=>'radio','require'=>1,'type_option'=>[
                        ['label'=>'单图','field'=>1],
                        ['label'=>'三图','field'=>3],
                        //['label'=>'自动','field'=>0,'default'=>1]
                    ]],
                    ['label'=>'封面','field'=>'image_list','type'=>'image','require'=>1],
                    ['label'=>'作者名称','field'=>'author','type'=>'text','min_words'=>0,'max_words'=>10],
                ];
                try{
                    ArticlePublishServices::publishDayuhao($account,$account_info);
                }catch (\Exception $e) {
                    \Log::error('dayuhao publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '大鱼号';
                }

            }
            if($platform_id == 7) {
                try{
                    ArticlePublishServices::publishToutiao($account,$account_info);
                }catch (\Exception $e){
                    \Log::error('toutiao publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '头条号';
                }

            }
            if($platform_id == 4) {
                try{
                    ArticlePublishServices::publishSouhu($account,$account_info);
                }catch (\Exception $e){
                    \Log::error('souhu publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '搜狐号';
                }

            }
            if($platform_id == 2) {
                try{
                    ArticlePublishServices::publishSougou($account,$account_info);
                }catch (\Exception $e){
                    \Log::error('sougou publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '搜狗号';
                }

            }
            if($platform_id == 12) {
                try{
                    ArticlePublishServices::publishDazhong($account,$account_info);
                }catch (\Exception $e){
                    \Log::error('dazhong publish error:'.$e->getMessage());
                    $publish_error_msgs[] = '大众点评';
                }

            }

        }
        if(count($publish_error_msgs)>0) {
            return ['code'=>10000,'msg'=>implode(',',$publish_error_msgs).'账号发布失败!'];
        }
        return response()->success();
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function upload_img(Request $request) {

        $path = $request->file('img')->store(date('Ymd'));
        $sub_path = '/upload/image/'.$path;
        $final_path = 'http://'.env('STATIC_DOMAIN').$sub_path;
        $account_id = $request->input('account_id','');
        $account_info = PlatformAccounts::getById($account_id);
        if($account_info && $account_info->platform_id==11) {
            $client = new Client();
            Redis::set('account_cookie:'.$account_id,$account_info->cookie);
            $response_z = $client->request('POST',env('PYTHON_DOMAIN').':8000'.'/up',['form_params' => [
                'path' => $final_path,
                'account_id' => $account_id,
                'domain'=>env('STATIC_DOMAIN')
            ]]);
            $res = $response_z->getBody()->getContents();
            if($response_z->getStatusCode() == 200) {
                return response()->success($res);
            }

            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id==12) {
            $client = new Client();
            Redis::set('account_cookie:'.$account_id,$account_info->cookie);
            $response_z = $client->request('POST',env('PYTHON_DOMAIN').':8000'.'/up_dz',['form_params' => [
                'path' => $final_path,
                'account_id' => $account_id,
                'domain'=>env('STATIC_DOMAIN')
            ]]);
            $res = $response_z->getBody()->getContents();
            if($response_z->getStatusCode() == 200) {
                return response()->success($res);
            }

            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id==1) {
            $cookie = $account_info->cookie;
            $token = $account_info->token;
            $extra = json_decode($account_info->extra_info,true);
            $appid = $extra['app_id'];
            $client = new Client();
            $response = $client->request('POST','https://baijiahao.baidu.com/builderinner/api/content/file/upload',[
                'headers'=>['Cookie'=>$cookie,'Token'=>$token],
                'multipart'=>[
                    [
                        'name'     => 'type',
                        'contents' => 'image'
                    ],
                    [
                        'name'     => 'app_id',
                        'contents' => $appid
                    ],
                    [
                        'name'     => 'is_waterlog',
                        'contents' => 1
                    ],
                    [
                        'name'     => 'save_material',
                        'contents' => 1
                    ],
                    [
                        'name'     => 'no_compress',
                        'contents' => 0
                    ],
                    [
                        'name'     => 'is_events',
                        'contents' => ''
                    ],
                    [
                        'name'     => 'media',
                        'contents' => fopen(public_path('/upload/image/'.$path), 'r') //file_get_contents(public_path('/upload/image/'.$path))
                    ],

                ]

            ]);
            \Log::info(public_path('/upload/image/'.$path));
            $res = $response->getBody()->getContents();
            \Log::info($res);
            $token = $response->getHeader('Token')[0];
//            var_dump($token);
            PlatformAccounts::where('id', $account_id)->update(['token'=>$token]);
            $res_arr = json_decode($res,true);
            if($res_arr['errno'] == 0) {

                Redis::hMset('baijiahao_img:'.$res_arr['ret']['org_url'],$res_arr['ret']);
                return response()->success($res_arr['ret']['org_url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 3) {
            $client = new Client();
            $cookie = $account_info->cookie;
            $response = $client->request('POST','http://kuaichuan.360.cn/upload/img?source=post',[
                'headers'=>['Cookie'=>$cookie,'Referer'=>'http://kuaichuan.360.cn/','User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'],
                'multipart'=>[
                    [
                        'name'=>'img',
                        'contents'=>fopen(public_path('/upload/image/'.$path), 'r')
                    ]
                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            if($res_arr['errno'] ==0) {
                return response()->success($res_arr['data']['url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 6) {
            $client = new Client();
            $extra_info = json_decode($account_info->extra_info,true);
            $wmid =$extra_info['wmid'];
            $wxname = urlencode($extra_info['weMediaName']);
            $sign = $extra_info['nsImageUploadSign'];
            $response = $client->request('POST','https://ns.dayu.com/article/imageUpload?appid=website&fromMaterial=0&wmid='.$wmid.'&wmname='.$wxname.'&sign='.$sign,[
                'headers'=>['Cookie'=>$account_info['cookie'],'Referer'=>'https://mp.dayu.com/dashboard/article/write?spm=a2s0i.db_index.menu.4.15fb3caa7yK7EW','Origin'=>'https://mp.dayu.com'],
                'multipart'=>[
                    [
                        'name'=>'upfile',
                        'contents'=>fopen(public_path('/upload/image/'.$path), 'r')
                    ],

                ]
            ]);
            $res = $response->getBody()->getContents();//{"code":0,"message":"","data":{"status":1,"imgInfo":{"format":"JPEG","size":"4627023","status":200,"org_url":"http://image.uc.cn/s/wemedia/s/upload/2019/bd6e9b72b82565c4dbf968bd6a72626d.jpg","width":"4032","height":"3024","phash":"3E69E3B24095B53C","url":"https://mp.dayu.com/dayu/image?t=1577115134062&s=6040a1ab17f73eaf6893d29d8e35df9a&p=95a1f4676e59df62512298bbc5fd7f4ed0372e3c8bffc14f54833e09b46de4ef6cc71dcd1ac61f4d74196d356350e94b0cba8091396a8fbee20e91fb675827aacad0480fc4ba12851e5ca0eb369cb2fa"}}}
            $res_arr = json_decode($res,true);
            if($res_arr['code']==0) {
                return response()->success($res_arr['data']['imgInfo']['org_url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 7) {
            $client = new Client();

            $response = $client->request('POST','https://mp.toutiao.com/tools/upload_picture/?type=ueditor&pgc_watermark=1&action=uploadimage&encode=utf-8',[
                'headers'=>['Cookie'=>$account_info['cookie']],
                'multipart'=>[
                    [
                        'name'=>'upfile',
                        'contents'=>fopen(public_path('/upload/image/'.$path), 'r')
                    ],

                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            if(isset($res_arr['url'])) {
                Redis::set('toutiao_img:'.$res_arr['url'],$res);
                return response()->success($res_arr['url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 4) {
            $client = new Client();

            $response = $client->request('POST','https://mp.sohu.com/commons/upload/file',[
                'headers'=>['Cookie'=>$account_info['cookie']],
                'multipart'=>[
                    [
                        'name'=>'file',
                        'contents'=>fopen(public_path('/upload/image/'.$path), 'r')
                    ],

                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            \Log::info('res:'.$res);
            if($res_arr && isset($res_arr['url'])) {
                Redis::hMset('souhu_img:'.$res_arr['url'],$res_arr);
                return response()->success($res_arr['url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 2) {
            $client = new Client();

            $response = $client->request('POST','http://mp.sogou.com/api/articles/temp-image',[
                'headers'=>['Cookie'=>$account_info['cookie']],
                'multipart'=>[
                    [
                        'name'=>'file',
                        'contents'=>fopen(public_path('/upload/image/'.$path), 'r')
                    ],

                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            \Log::info('res:'.$res);
            if($res_arr && isset($res_arr['url'])) {
                Redis::hMset('sougou_img:'.$res_arr['url'],$res_arr);
                return response()->success($res_arr['url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 8) {
            $client = new Client();
            $url = 'https://mp.yidianzixun.com/upload?action=uploadimage&_rk=1575601398416';
            $response = $client->request('POST', $url, [
                'headers'=>[
                    'Cookie'=>$account_info['cookie']
                ],
                'multipart' => [
                    [
                        'name'     => 'upfile',
                        'contents' => fopen(public_path('/upload/image/'.$path), 'r')
                    ]
                ]
            ]);
            $res = $response->getBody()->getContents();
            $json = json_decode($res,true);
            //$ret = [];
            if(is_array($json)) {
                return response()->success($json['url']);//['key'=>$img_path,'res'=>$json['url']];
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        return response()->success($final_path);
    }


    public function uploadImgMultiple(Request $request) {
        $platform_account_lists = $request->input('platform_accounts_list','');
        $image_lists = $request->input('images_list','');
        //$uid = 1;
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $uid = auth('api')->user()->id;

        if(empty($platform_account_lists)||empty($image_lists)) {
            return response()->error('PARAM_EMPTY');
        }
        $platform_account_ids = explode(',',$platform_account_lists);
        $image_links = explode(',',$image_lists);
        $img_contents = $this->imagesContentsFromRemote($image_links,$uid);
        $platform_account_infos = PlatformAccounts::getPlatformByIds($platform_account_ids);
        /*$platform_account_id_info_hash = [];
        foreach ($platform_account_infos as $k=>$platform_account_each) {
            $platform_account_id_info_hash[$platform_account_each->id] = $platform_account_each;
        }*/
        $client = new Client();
        $final_data = [];
        foreach ($image_links as $key=>$image_link) {
            $final_data[$key] = array();
            $promises = [];
            foreach ($platform_account_infos as $k=>$platform_account_info) {

                if($platform_account_info->platform_id == 1) {
                    $cookie = $platform_account_info->cookie;
                    $token = $platform_account_info->token;
                    $extra = json_decode($platform_account_info->extra_info,true);
                    $appid = $extra['app_id'];

                    $promises[$platform_account_info->id] = $client->postAsync('https://baijiahao.baidu.com/builderinner/api/content/file/upload',[
                        'headers'=>['Cookie'=>$cookie,'Token'=>$token],
                        'multipart'=>[
                            [
                                'name'     => 'type',
                                'contents' => 'image'
                            ],
                            [
                                'name'     => 'app_id',
                                'contents' => $appid
                            ],
                            [
                                'name'     => 'is_waterlog',
                                'contents' => 1
                            ],
                            [
                                'name'     => 'save_material',
                                'contents' => 1
                            ],
                            [
                                'name'     => 'no_compress',
                                'contents' => 0
                            ],
                            [
                                'name'     => 'is_events',
                                'contents' => ''
                            ],
                            [
                                'name'     => 'media',
                                'contents' => fopen(storage_path($key.'_'.$uid.'.jpg'),'r') //file_get_contents(public_path('/upload/image/'.$path))
                            ],

                        ]

                    ]);

                }
                if($platform_account_info->platform_id == 2) {
                    $promises[$platform_account_info->id] = $client->postAsync('http://mp.sogou.com/api/articles/temp-image',[
                        'headers'=>['Cookie'=>$platform_account_info->cookie],
                        'multipart'=>[
                            [
                                'name'=>'file',
                                'contents'=>fopen(storage_path($key.'_'.$uid.'.jpg'),'r')
                            ],

                        ]
                    ]);
                }
                if($platform_account_info->platform_id == 3) {
                    $promises[$platform_account_info->id] = $client->postAsync('http://kuaichuan.360.cn/upload/img?source=post',[
                        'headers'=>['Cookie'=>$platform_account_info->cookie,'Referer'=>'http://kuaichuan.360.cn/','User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'],
                        'multipart'=>[
                            [
                                'name'=>'img',
                                'contents'=>fopen(storage_path($key.'_'.$uid.'.jpg'),'r')
                            ]
                        ]
                    ]);
                }
                if($platform_account_info->platform_id == 4) {
                    $promises[$platform_account_info->id] = $client->postAsync('https://mp.sohu.com/commons/upload/file',[
                        'headers'=>['Cookie'=>$platform_account_info->cookie],
                        'multipart'=>[
                            [
                                'name'=>'file',
                                'contents'=>fopen(storage_path($key.'_'.$uid.'.jpg'),'r')
                            ],

                        ]
                    ]);
                }
                if($platform_account_info->platform_id == 6) {
                    $extra_info = json_decode($platform_account_info->extra_info,true);
                    $wmid =$extra_info['wmid'];
                    $wxname = urlencode($extra_info['weMediaName']);
                    $sign = $extra_info['nsImageUploadSign'];
                    $promises[$platform_account_info->id] = $client->postAsync('https://ns.dayu.com/article/imageUpload?appid=website&fromMaterial=0&wmid='.$wmid.'&wmname='.$wxname.'&sign='.$sign,[
                        'headers'=>['Cookie'=>$platform_account_info->cookie,'Referer'=>'https://mp.dayu.com/dashboard/article/write?spm=a2s0i.db_index.menu.4.15fb3caa7yK7EW','Origin'=>'https://mp.dayu.com'],
                        'multipart'=>[
                            [
                                'name'=>'upfile',
                                'contents'=>fopen(storage_path($key.'_'.$uid.'.jpg'),'r')
                            ],

                        ]
                    ]);

                }
                if($platform_account_info->platform_id == 7) {
                    $promises[$platform_account_info->id] = $client->postAsync('https://mp.toutiao.com/tools/upload_picture/?type=ueditor&pgc_watermark=1&action=uploadimage&encode=utf-8',[
                        'headers'=>['Cookie'=>$platform_account_info->cookie],
                        'multipart'=>[
                            [
                                'name'=>'upfile',
                                'contents'=>fopen(storage_path($key.'_'.$uid.'.jpg'),'r')
                            ],

                        ]
                    ]);

                }
                if($platform_account_info->platform_id == 8) {
                     $url = 'https://mp.yidianzixun.com/upload?action=uploadimage&_rk=1575601398416';
                    $promises[$platform_account_info->id]  = $client->postAsync( $url, [
                        'headers'=>[
                            'Cookie'=>$platform_account_info->cookie
                        ],
                        'multipart' => [
                            [
                                'name'     => 'upfile',
                                'contents' => fopen(storage_path($key.'_'.$uid.'.jpg'),'r')//$img_contents[$image_link]
                            ]
                        ]
                    ]);

                }
                $img_host_info = parse_url($image_link);
                if($platform_account_info->platform_id == 12) {
                    Redis::set('account_cookie:'.$platform_account_info->id,$platform_account_info->cookie);
                    $promises[$platform_account_info->id] = $client->postAsync(env('PYTHON_DOMAIN').':8000'.'/up_dz',['form_params' => [
                        'path' => $img_host_info['path'],//$image_link,
                        'account_id' => $platform_account_info->id,
                        'domain'=>$img_host_info['host']
                    ]]);

                }
                if($platform_account_info->platform_id == 11) {
                    Redis::set('account_cookie:'.$platform_account_info->id,$platform_account_info->cookie);
                    $promises[$platform_account_info->id] = $client->postAsync(env('PYTHON_DOMAIN').':8000'.'/up',['form_params' => [
                        'path' => $img_host_info['path'],
                        'account_id' => $platform_account_info->id,
                        'domain'=>$img_host_info['host']
                    ]]);

                }
            }
            $results = Promise\unwrap($promises);

            foreach ($platform_account_infos as $k=>$platform_account_info) {
                $des_img_link = $image_link;
                if($platform_account_info->platform_id == 1) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();
                    \Log::info($res);
                    $token = $response->getHeader('Token')[0];

                    PlatformAccounts::where('id', $platform_account_info->id)->update(['token'=>$token]);
                    $res_arr = json_decode($res,true);
                    if($res_arr['errno'] == 0) {
                        Redis::hMset('baijiahao_img:'.$res_arr['ret']['org_url'],$res_arr['ret']);
                        $des_img_link=$res_arr['ret']['org_url'];
                    }

                }
                if($platform_account_info->platform_id == 2) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();
                    $res_arr = json_decode($res,true);
                    \Log::info('res:'.$res);
                    if($res_arr && isset($res_arr['url'])) {
                        Redis::hMset('sougou_img:'.$res_arr['url'],$res_arr);
                        $des_img_link=$res_arr['url'];
                    }

                }
                if($platform_account_info->platform_id == 3) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();
                    $res_arr = json_decode($res,true);
                    if($res_arr['errno'] ==0) {
                        $des_img_link=$res_arr['data']['url'];
                    }

                }
                if($platform_account_info->platform_id == 4) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();
                    $res_arr = json_decode($res,true);
                    \Log::info('res:'.$res);
                    if($res_arr && isset($res_arr['url'])) {
                        Redis::hMset('souhu_img:'.$res_arr['url'],$res_arr);
                        $des_img_link=$res_arr['url'];
                    }

                }
                if($platform_account_info->platform_id == 6) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();//{"code":0,"message":"","data":{"status":1,"imgInfo":{"format":"JPEG","size":"4627023","status":200,"org_url":"http://image.uc.cn/s/wemedia/s/upload/2019/bd6e9b72b82565c4dbf968bd6a72626d.jpg","width":"4032","height":"3024","phash":"3E69E3B24095B53C","url":"https://mp.dayu.com/dayu/image?t=1577115134062&s=6040a1ab17f73eaf6893d29d8e35df9a&p=95a1f4676e59df62512298bbc5fd7f4ed0372e3c8bffc14f54833e09b46de4ef6cc71dcd1ac61f4d74196d356350e94b0cba8091396a8fbee20e91fb675827aacad0480fc4ba12851e5ca0eb369cb2fa"}}}
                    $res_arr = json_decode($res,true);
                    \Log::info('6-res'.$res);
                    if($res_arr['code']==0) {

                        $des_img_link=$res_arr['data']['imgInfo']['org_url'];
                        \Log::info($des_img_link);
                    }

                }
                if($platform_account_info->platform_id == 7) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();
                    $res_arr = json_decode($res,true);
                    if(isset($res_arr['url'])) {
                        Redis::set('toutiao_img:'.$res_arr['url'],$res);
                        $des_img_link=$res_arr['url'];
                    }

                }
                if($platform_account_info->platform_id == 8) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();
                    $json = json_decode($res,true);
                    //$ret = [];
                    \Log::info('8'.$res);
                    if(is_array($json)) {
                        $des_img_link=$json['url'];//['key'=>$img_path,'res'=>$json['url']];
                    }

                }
                if($platform_account_info->platform_id == 11) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();
                    if($response->getStatusCode() == 200) {
                        $des_img_link=$res;
                    }

                }
                if($platform_account_info->platform_id == 12) {
                    $response = $results[$platform_account_info->id];
                    $res = $response->getBody()->getContents();
                    if($response->getStatusCode() == 200) {
                        $des_img_link=$res;
                    }

                }
                $final_data[$key][$platform_account_info->id] = $des_img_link;
            }
        }
        $ret_data = [];
        foreach ($final_data as $key=>$platform_account_id_i) {
            foreach ($platform_account_id_i as $k=>$item) {
                if(!array_key_exists($k,$ret_data)) {
                    $ret_data[$k] =array('image_list'=>[]);
                }
                $ret_data[$k]['image_list'][] = $item;

            }
        }
        return response()->success($ret_data);
    }

    private function imagesContentsFromRemote($image_links,$uid) {
        $client = new Client();

        $promises = [];
        foreach ($image_links as $key=>$image_link) {
            $promises[$key] = $client->getAsync($image_link);
        }
        $results = Promise\unwrap($promises);
        $img_contents = [];
        foreach ($results as $key=>$result) {
            $res = $result->getBody()->getContents();
            $img_contents[$key] = $res;
            $myfile = fopen(storage_path($key.'_'.$uid.'.jpg'), "w");
            //file_put_contents(storage_path($index.'.jpg'),$res);
            fwrite($myfile,$res);
            //$index++;
        }
        //var_dump($img_contents);

        return $img_contents;
    }

    public function upload_img_by_link(Request $request) {
        //$path = $request->file('img')->store(date('Ymd'));
        $url = $request->input('url','');
        $sub_path = '/upload/image/'.date('Ymd');
        $path = public_path().$sub_path;
        $file_name_explode = explode('/',$url);
        $file_name = end($file_name_explode);
        if(!file_exists($path)) {
            @mkdir($path,0777,true);
            @chmod($path, 0777);

        }
        $file_name_final = $path.'/'.$file_name;
        curl_download($url,$file_name_final);
        $final_path = 'http://'.env('STATIC_DOMAIN').$sub_path.'/'.$file_name;
        $account_id = $request->input('account_id','');
        \Log::info($account_id);
        $account_info = PlatformAccounts::getById($account_id);
        \Log::info($account_info);
        if($account_info && $account_info->platform_id==11) {
            $client = new Client();
            Redis::set('account_cookie:'.$account_id,$account_info->cookie);
            $response_z = $client->request('POST',env('PYTHON_DOMAIN').':8000'.'/up',['form_params' => [
                'path' => $final_path,
                'account_id' => $account_id,
                'domain'=>env('STATIC_DOMAIN')
            ]]);
            $res = $response_z->getBody()->getContents();
            if($response_z->getStatusCode() == 200) {
                return response()->success($res);
            }

            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id==12) {
            $client = new Client();
            Redis::set('account_cookie:'.$account_id,$account_info->cookie);
            $response_z = $client->request('POST',env('PYTHON_DOMAIN').':8000'.'/up_dz',['form_params' => [
                'path' => $final_path,
                'account_id' => $account_id,
                'domain'=>env('STATIC_DOMAIN')
            ]]);
            $res = $response_z->getBody()->getContents();
            if($response_z->getStatusCode() == 200) {
                return response()->success($res);
            }

            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id==1) {
            $cookie = $account_info->cookie;
            $token = $account_info->token;
            $extra = json_decode($account_info->extra_info,true);
            $appid = $extra['app_id'];
            $client = new Client();
            $response = $client->request('POST','https://baijiahao.baidu.com/builderinner/api/content/file/upload',[
                'headers'=>['Cookie'=>$cookie,'Token'=>$token],
                'multipart'=>[
                    [
                        'name'     => 'type',
                        'contents' => 'image'
                    ],
                    [
                        'name'     => 'app_id',
                        'contents' => $appid
                    ],
                    [
                        'name'     => 'is_waterlog',
                        'contents' => 1
                    ],
                    [
                        'name'     => 'save_material',
                        'contents' => 1
                    ],
                    [
                        'name'     => 'no_compress',
                        'contents' => 0
                    ],
                    [
                        'name'     => 'is_events',
                        'contents' => ''
                    ],
                    [
                        'name'     => 'media',
                        'contents' => fopen($file_name_final, 'r') //file_get_contents(public_path('/upload/image/'.$path))
                    ],

                ]

            ]);
            //\Log::info(public_path('/upload/image/'.$path));
            $res = $response->getBody()->getContents();
            //\Log::info($res);
            $token = $response->getHeader('Token')[0];
//            var_dump($token);
            PlatformAccounts::where('id', $account_id)->update(['token'=>$token]);
            $res_arr = json_decode($res,true);
            if($res_arr['errno'] == 0) {

                Redis::hMset('baijiahao_img:'.$res_arr['ret']['org_url'],$res_arr['ret']);
                return response()->success($res_arr['ret']['org_url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 3) {
            $client = new Client();
            $cookie = $account_info->cookie;
            $response = $client->request('POST','http://kuaichuan.360.cn/upload/img?source=post',[
                'headers'=>['Cookie'=>$cookie,'Referer'=>'http://kuaichuan.360.cn/','User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'],
                'multipart'=>[
                    [
                        'name'=>'img',
                        'contents'=>fopen($file_name_final, 'r')
                    ]
                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            if($res_arr['errno'] ==0) {
                return response()->success($res_arr['data']['url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 6) {
            $client = new Client();
            $extra_info = json_decode($account_info->extra_info,true);
            $wmid =$extra_info['wmid'];
            $wxname = urlencode($extra_info['weMediaName']);
            $sign = $extra_info['nsImageUploadSign'];
            $response = $client->request('POST','https://ns.dayu.com/article/imageUpload?appid=website&fromMaterial=0&wmid='.$wmid.'&wmname='.$wxname.'&sign='.$sign,[
                'headers'=>['Cookie'=>$account_info['cookie'],'Referer'=>'https://mp.dayu.com/dashboard/article/write?spm=a2s0i.db_index.menu.4.15fb3caa7yK7EW','Origin'=>'https://mp.dayu.com'],
                'multipart'=>[
                    [
                        'name'=>'upfile',
                        'contents'=>fopen($file_name_final, 'r')
                    ],

                ]
            ]);
            $res = $response->getBody()->getContents();//{"code":0,"message":"","data":{"status":1,"imgInfo":{"format":"JPEG","size":"4627023","status":200,"org_url":"http://image.uc.cn/s/wemedia/s/upload/2019/bd6e9b72b82565c4dbf968bd6a72626d.jpg","width":"4032","height":"3024","phash":"3E69E3B24095B53C","url":"https://mp.dayu.com/dayu/image?t=1577115134062&s=6040a1ab17f73eaf6893d29d8e35df9a&p=95a1f4676e59df62512298bbc5fd7f4ed0372e3c8bffc14f54833e09b46de4ef6cc71dcd1ac61f4d74196d356350e94b0cba8091396a8fbee20e91fb675827aacad0480fc4ba12851e5ca0eb369cb2fa"}}}
            $res_arr = json_decode($res,true);
            if($res_arr['code']==0) {
                return response()->success($res_arr['data']['imgInfo']['org_url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 7) {
            $client = new Client();

            $response = $client->request('POST','https://mp.toutiao.com/tools/upload_picture/?type=ueditor&pgc_watermark=1&action=uploadimage&encode=utf-8',[
                'headers'=>['Cookie'=>$account_info['cookie']],
                'multipart'=>[
                    [
                        'name'=>'upfile',
                        'contents'=>fopen($file_name_final, 'r')
                    ],

                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            if(isset($res_arr['url'])) {
                Redis::set('toutiao_img:'.$res_arr['url'],$res);
                return response()->success($res_arr['url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 4) {
            $client = new Client();

            $response = $client->request('POST','https://mp.sohu.com/commons/upload/file',[
                'headers'=>['Cookie'=>$account_info['cookie']],
                'multipart'=>[
                    [
                        'name'=>'file',
                        'contents'=>fopen($file_name_final, 'r')
                    ],

                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            \Log::info('res:'.$res);
            if($res_arr && isset($res_arr['url'])) {
                Redis::hMset('souhu_img:'.$res_arr['url'],$res_arr);
                return response()->success($res_arr['url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 2) {
            $client = new Client();

            $response = $client->request('POST','http://mp.sogou.com/api/articles/temp-image',[
                'headers'=>['Cookie'=>$account_info['cookie']],
                'multipart'=>[
                    [
                        'name'=>'file',
                        'contents'=>fopen($file_name_final, 'r')
                    ],

                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            //\Log::info('res:'.$res);
            if($res_arr && isset($res_arr['url'])) {
                Redis::hMset('sougou_img:'.$res_arr['url'],$res_arr);
                return response()->success($res_arr['url']);
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        if($account_info && $account_info->platform_id == 8) {
            $client = new Client();
            $url = 'https://mp.yidianzixun.com/upload?action=uploadimage&_rk=1575601398416';
            $response = $client->request('POST', $url, [
                'headers'=>[
                    'Cookie'=>$account_info['cookie']
                ],
                'multipart' => [
                    [
                        'name'     => 'upfile',
                        'contents' => fopen($file_name_final, 'r')
                    ]
                ]
            ]);
            $res = $response->getBody()->getContents();
            $json = json_decode($res,true);
            //$ret = [];
            if(is_array($json)) {
                return response()->success($json['url']);//['key'=>$img_path,'res'=>$json['url']];
            }
            return response()->error('UPLOAD_IMG_ERROR');
        }
        return response()->success($final_path);
    }
}