<?php
namespace App\Http\Controllers\Video\Release;

use App\Http\Controllers\Controller;
use App\Jobs\PublishVideos;
use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Publishes;
use App\Modules\Platform\Models\UploadVideo;
use App\Modules\Platform\Models\UploadVideoCover;
use App\Modules\Platform\Services\ReleaseDouyinServices;
use App\Modules\Platform\Services\ReleaseKuaishouServices;
use App\Modules\Platform\Services\ReleaseVideoServices;
use App\Modules\Platform\Services\ReleaseYoukuServices;
use App\Modules\Publish\Services\VideoPublishServices;
use Illuminate\Http\Request;
use DB;

class ReleaseVideoController extends Controller
{
    //上传视频
    public function uploadVideo(Request $request)
    {
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $user = auth('api')->user();
        if(!$user){
            $user_id = 1;
        }else{
            $user_id = $user->id;
        }

        if (!$request->hasFile('video')) {

            return response()->error('VIDEO_EXIT');
        }
        $file = $request->file('video');
        //获取上传属性
        $allowedExtensions = ['mp4','avi','divx','dat','mpeg4','vcd','mpeg1','mpeg2','dvd','vob','rm','rmvb','wmv','asf','mov'];

        \Log::error([$file->getClientOriginalExtension()]);
        if (!$file->getClientOriginalExtension() ||
            !collect($allowedExtensions)->flatten()->contains(strtolower($file->getClientOriginalExtension()))
        ) {
            return response()->error('EXTENSION_ERROR');
        }

        //获取格式
        if (!$file->isValid()) {
            return response()->error('ISVALID_ERROR');
        }
        $fileUploadName = 'upload';
        $desPath = public_path($fileUploadName);
        \Log::info('des_path:'.$desPath);
        \Log::info('des_path1:'.public_path($fileUploadName));
        if (!file_exists($desPath)) {
            return response()->error('FILE_EXISTS');
        }
        $module = 'video';
        $prefixPath = $fileUploadName.'/'.$module;
        $desPath = public_path($prefixPath);

        if (!file_exists($desPath)) {
            mkdir($desPath, 0777, true);
        }

        //图片命名
        $fileName = date('YmdHis', time()) . '_' . rand(100000, 999999) . '.' . $file->getClientOriginalExtension();
        if (!$file->move($desPath, $fileName)) {
            return response()->error('MOVE_FAIL');
        }
        $file = $file->getClientOriginalName();
        unset($file);
        $desPath = '/'.$prefixPath."/".$fileName;


        $res = UploadVideo::create([
            'user_id' => $user_id,
            'relative_path' => $desPath,
            'file_name' => $fileName,
            'slice' => date('YmdHis', time())
        ]);
        return response()->success($res);

    }
    //上传视频封面
    public function uploadVideoCover(Request $request)
    {
        $user = auth('api')->user();
        if(!$user){
            $user_id = 1;
        }else{
            $user_id = $user->id;
        }
        $slice = date('Ymd');
        $path = $request->file('videoCover')->store(
            $slice, 'videoCover'
        );
        $path_arr = explode('/',$path);
        $path = '/upload/videoCover/'.$path;
        $res = UploadVideoCover::create([
            'user_id' => $user_id,
            'relative_path' => $path,
            'file_name' => array_pop($path_arr),
            'slice' => $slice
        ]);
        $res->abslute_path = ''.'';
        return response()->success($res);
    }
    //发布
    public function releaseVideo(Request $request)
    {
        if(empty(auth('api')->user())){
            return response()->error('NOT_LOGIN');
        }
        $user_id = auth('api')->user()->id;
        $data = $request->accounts;
        \Log::info($data);
        /*$formData  = $data[0]['formData'];
        $accountId = $data[0]['accountId'];
        $video_id  = $data[0]['video_id'];*/
        $error_msgs = [];
        foreach ($data as $key=>$each) {
            $formData  = $each['formData'];
            $accountId = $each['accountId'];
            $video_id  = $each['video_id'];
            //验证账号
            if(!$accountInfo = PlatformAccounts::where('id',$accountId)->first()){
                return response()->error('ACCOUNT_NOT_EXIST');
            }

            //验证视频
            if(!$videoInfo = UploadVideo::where('id',$video_id)->first()){
                return response()->error('VIDEO_NOT_EXIST');
            }
            $coverInfo = [];
            if(isset($formData['video_cover_id'])) {
                $coverInfo = UploadVideoCover::where('id',$formData['video_cover_id'])->first();
            }
            $to_insert_data = [
                'user_id'=>$user_id,
                'title'=>$formData['title'],
                'platform_id'=>$accountInfo->platform_id,
                'platform_account_id'=>$accountInfo->id,
                'status'=>'WAITING_UPLOAD'
            ];
            $publishes = Publishes::create($to_insert_data);
            $delay=0;
            $job = (new PublishVideos($publishes,$accountInfo,$videoInfo,$formData,$coverInfo))->onConnection('redis')->delay($delay);
            dispatch($job);

            /*$to_insert_data = [
                'uid'=>$user_id,
                'platform_id'=>$accountInfo->platform_id ,
                'account_info'=>json_encode($accountInfo),
                'video_info'=>json_encode($videoInfo),
                'form_data'=>json_encode($formData),
                'cover_info'=>json_encode($coverInfo),
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                'status'=>0
            ];
            DB::table('video_tmp_tasks')->insert($to_insert_data);*/
            /*if($accountInfo->platform_id == 19){//发布抖音视频
                try{
                    ReleaseDouyinServices::release($accountInfo,$videoInfo,$formData);
                }catch (\Exception $e){
                    \Log::error('douyin_publish error:'.$e->getMessage());
                    $error_msgs[] = '抖音';
                }
            }
            if($accountInfo->platform_id == 21){//发布快手
                try{
                    ReleaseKuaishouServices::release($accountInfo,$videoInfo,$formData,$coverInfo);
                }catch (\Exception $e){
                    \Log::error('kuaishou_publish error:'.$e->getMessage());
                    $error_msgs[] = '快手';
                }
            }


            if($accountInfo->platform_id == 14){
                try{
                    VideoPublishServices::publishAiqiyi($accountInfo,$videoInfo,$formData);
                }catch (\Exception $e){
                    \Log::error('aiqiyi publish error:'.$e->getMessage());
                    $error_msgs[] = '爱奇艺';
                }
            }elseif($accountInfo->platform_id == 18){
                try{
                    VideoPublishServices::publishHaokan($accountInfo,$videoInfo,$formData,$coverInfo);
                }catch (\Exception $e){
                    \Log::error('haokan publish error:'.$e->getMessage());
                    $error_msgs[] = '好看';
                }
            }elseif($accountInfo->platform_id == 16){//发布优酷视频
                try{
                    VideoPublishServices::publishYouku($accountInfo,$videoInfo,$formData);
                }catch (\Exception $e){
                    \Log::error('youku publish error:'.$e->getMessage());
                    $error_msgs[] = '优酷';
                }
            }*/
        }
        /*if(count($error_msgs)>0){
            return ['code'=>100000,'msg'=>implode(',',$error_msgs).'账号发布失败！'];
        }*/
        return response()->success();
        //$res =ReleaseYoukuServices::release($request);
        //$res = ReleaseDouyinServices::release($request);
        //$res = ReleaseKuaishouServices::release($request);

    }
}
