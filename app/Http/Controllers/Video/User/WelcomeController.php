<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/11/26
 * Time: 18:22
 */
namespace App\Http\Controllers\Video\User;

use App\Http\Controllers\Controller;
use App\Modules\Manage\Services\UserService;
use Illuminate\Http\Request;

use App\Modules\Manage\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWTAuth;
use Validator;


class WelcomeController extends Controller
{
    public function welcome() {
        return view('video.index');
    }
}