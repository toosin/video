<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/11/26
 * Time: 18:22
 */
namespace App\Http\Controllers\Video\User;

use App\Http\Controllers\Controller;
use App\Modules\Manage\Services\UserService;
use Illuminate\Http\Request;

use App\Modules\Manage\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWTAuth;
use Validator;


class LoginController extends Controller
{
    public $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 1,
                'msg' => $validator->errors()->first()
            ]);
        }
        $user = User::where('phone',$request->phone)->first();
        if($user){
            return response()->json([
                'code' => 1,
                'msg' => 'Account already exists'
            ]);
        }
        $user = new User();
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->save();

        return $this->login($request);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 1,
                'msg' => $validator->errors()->first()
            ]);
        }
        $credentials = request(['phone', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['code' => 1,'msg' => 'Unauthorized']);
        }

        $userInfo = auth('api')->user()->toArray();

        return $this->respondWithToken($token,$userInfo);
    }

    public function logout()
    {
        auth('api')->logout();
        return response()->json([
            'code' => 0,
            'msg' => 'Successfully logged out'
        ]);
    }

    public function getUser()
    {
        $user = auth('api')->user();
        return response()->json([
            'code' => 0,
            'data' => $user ?: []
        ]);
    }

    public function resetPassword(Request $request)
    {
        $user = auth('api')->user();
        if(!$user){
            return response()->json([
                'code' => 1,
                'msg' => 'Token invalide',
            ]);
        }
        $userInfo = User::find($user['id'])->first();
        if(!$userInfo){
            return response()->json([
                'code' => 1,
                'msg' => 'User not find',
            ]);
        }
        if(!Hash::check($request->old_password,$userInfo->password)){
            return response()->json([
                'code' => 1,
                'msg' => 'The old password is not correct',
            ]);
        }
        if($request->new_password != $request->new_password_confirm){
            return response()->json([
                'code' => 1,
                'msg' => 'Two password entries are inconsistent',
            ]);
        }
        if($request->new_password == $request->old_password){
            return response()->json([
                'code' => 1,
                'msg' => 'The new password is the same as the old password',
            ]);
        }

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'new_password_confirm' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 1,
                'msg' => $validator->errors()->first()
            ]);
        }
        $userInfo->password = bcrypt($request->new_password);
        $userInfo->save();
        auth('api')->invalidate();
        return response()->json([
            'code' => 0,
            'msg' => 'Success'
        ]);
    }

    protected function respondWithToken($token,$userInfo)
    {
        return response()->json([
            'code' => 0,
            'msg' => 'Success',
            'data' => $userInfo
        ])->cookie('token',$token,auth('api')->factory()->getTTL());
    }
}