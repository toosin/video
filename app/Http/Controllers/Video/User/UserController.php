<?php

namespace App\Http\Controllers\Agents\User;

use App\Modules\Agents\Services\AgentsManageService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class UserController extends Controller {

    public function login() {
        return view('agents.login',['page'=>'daily_statistic']);
    }

    public function doLogin(Request $request) {
        $account = $request->has('username') ? trim($request->input('username')) : '';
        $password = $request->has('password') ? trim($request->input('password')) : '';
//         var_dump('$password:'.$password.' $account:'.$account);die();

        if(!$account || !$password) return back()->withErrors(['username'=>'用户名或者密码错误']);

        $manage = AgentsManageService::getByAccount($account);
//         var_dump($manage);
        \Log::info(md5($password."^-^zhuishuyun^_^"));
        if($manage && $manage->password == md5($password."^-^zhuishuyun^_^"))
        {
            //if($manage->is_enabled == 0) return response()->error('MANAGE_NOT_ENABLED');
            $request->session()->put('agent_auth', $manage->id);
            $request->session()->put('agent_user', serialize($manage));

            return redirect('/');
            //return response()->success(compact('options'));
        }
        return back()->withErrors(['username'=>'用户名或者密码错误']);
        //return response()->error('LOGIN_VERIFY_ERROR');
    }

    public function welcome(Request $request) {
        return view('agents.welcome',['page'=>'welcome']);
    }
    public function sendOrders(Request $request) {
        return view('agents.send_orders',['page'=>'send_orders']);
    }
    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/login');
    }
}
