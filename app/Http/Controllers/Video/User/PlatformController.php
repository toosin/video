<?php

namespace App\Http\Controllers\Agents\User;

use App\Http\Controllers\Agents\User\Transformers\PlatformStatsTransformer;
use App\Modules\Agents\Models\AgentPlatformStats;
use App\Modules\Agents\Services\AgentsManageService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;

class PlatformController extends Controller {

    public function getCaptcha(Request $request) {
        $ch = curl_init('https://vip.youshuge.com/captcha');
        $cookie_file = storage_path('cookie.txt');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        $response = curl_exec($ch);
        //dd($response);
        $re = json_decode($response,1);
        $url = $re['data']['url'];


        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);
        file_put_contents(public_path('youshuge_captcha.jpg'),$response);
        return response()->success();
    }

    public function loginYSG(Request $request) {

        $captcha = $request->input('captcha','');
        if(empty($captcha)){
            return response()->error('PARAM_EMPTY');
        }
        $user_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36";
        $url = 'https://vip.youshuge.com/check_login';
        $ch = curl_init($url);
        $cookie_file = storage_path('cookie.txt');
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        curl_setopt($ch, CURLOPT_POST, 1);
        $headers = [
            'User-Agent'=>$user_agent,
            'Referer'=>'https://vip.youshuge.com/login',
            'Content-Type'=>'application/x-www-form-urlencoded',
        ];
        $post_fields = [
            'user_name' => '小方先生',
            'password' => 'wzyy2018',
            'captcha'=>$captcha
        ];
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $response = curl_exec($ch);
        $now_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        \Log::info('$now_url:'.$now_url);
        //print_r('$now_url:'.$now_url);
        $cookie = file_get_contents($cookie_file);
        \Log::info($cookie);
        if($now_url == 'https://vip.youshuge.com/'){
            $st = DB::table('agent_platforms')
                ->where('platform','youshuge')
                ->update(['cookie'=>$cookie,'cookie_status'=>1]);
            return response()->success();
        }else{
            return ['code'=>3005,'msg'=>'登录失败'];
        }

    }

    //获取麦子云验证码
    public function getMZYCaptcha(Request $request) {
        $ch = curl_init('http://xs.maizibook.com/Home/ShowVCode?r=10.');
        $cookie_file = storage_path('mzy_cookie.txt');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
        curl_setopt($ch, CURLOPT_REFERER, 'http://xs.maizibook.com/Home/Login');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        $response = curl_exec($ch);

        file_put_contents(public_path('mzy_captcha.jpg'),$response);
        return response()->success();
    }

    //麦子云登陆
    public function loginMZY(Request $request) {

        $captcha = $request->input('captcha','');
        if(empty($captcha)){
            return response()->error('PARAM_EMPTY');
        }
        $user_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36";
        $url = 'http://xs.maizibook.com/Home/UserLogin';
        $ch = curl_init($url);
        $cookie_file = storage_path('mzy_cookie.txt');
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        curl_setopt($ch, CURLOPT_POST, 1);
        $headers = [
            'User-Agent'=>$user_agent,
            'Referer'=>'http://xs.maizibook.com/Home/Login',
            'Content-Type'=>'application/x-www-form-urlencoded',
        ];
        $post_fields = [
            'username' => 'beidao1',
            'password' => 'wzyy20190711',
            'vcode'=>$captcha
        ];
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $response = curl_exec($ch);

        if(strstr($response, 'success'))
        {
            $cookie = file_get_contents($cookie_file);
            $st = DB::table('agent_platforms')
                ->where('platform','mzy')
                ->update(['cookie'=>$cookie,'cookie_status'=>1,'updated_at'=>date('Y-m-d H:i:s')]);
            return response()->success();
        }else{
            return ['code'=>3005,'msg'=>'登录失败'];
        }

    }

    public function showStats(Request $request) {
        return view('agents.platform_stats',['page'=>'platform']);
    }

    public function showMZYStats(Request $request) {
        return view('agents.mzy_platform_stats',['page'=>'platform_mzy']);
    }

    public function getStats(Request $request) {
        //检查cookie状态
        $st = DB::table('agent_platforms')
            ->where('platform','youshuge')
            ->first();
        $status = (int)($st->cookie_status);
        if($status==0) {
            return ['code'=>304,'msg'=>'登录cookie失效'];
        }
        $agentId = $request->session()->get('agent_auth');
        $data = AgentPlatformStats::where('agent_id',$agentId)->where('platform_id',1)->paginate();

        return response()->pagination(new PlatformStatsTransformer(),$data);
    }

    public function getMZYStats(Request $request) {
        //检查cookie状态
        $st = DB::table('agent_platforms')
            ->where('platform','mzy')
            ->first();
        $status = (int)($st->cookie_status);
        if($status==0) {
            return ['code'=>304,'msg'=>'登录cookie失效'];
        }
        $agentId = $request->session()->get('agent_auth');
        $data = AgentPlatformStats::where('agent_id',$agentId)->where('platform_id',2)->paginate();

        return response()->pagination(new PlatformStatsTransformer(),$data);
    }


}
