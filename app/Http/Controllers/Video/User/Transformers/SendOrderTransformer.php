<?php
/**
 * Created by PhpStorm.
 * Date: 2017/3/31
 * Time: 14:02
 */

namespace App\Http\Controllers\Agents\User\Transformers;

use Hashids;

class SendOrderTransformer
{
    public function transform($sendOrderItem)
    {
        return [
            'id' => $sendOrderItem->id,
            'distribution_channel_id' => $sendOrderItem->distribution_channel_id,
            'entrance' => $sendOrderItem->entrance,
            'name' => $sendOrderItem->name,
            'book_id' => Hashids::encode($sendOrderItem->book_id),
            'channel_type' => $sendOrderItem->channel_type,
            'chapter_id' => $sendOrderItem->chapter_id,
            'document_cover_id' => $sendOrderItem->document_cover_id,
            'headline_id' => $sendOrderItem->headline_id,
            'body_template_id' => $sendOrderItem->body_template_id,
            'original_guide_id' => $sendOrderItem->original_guide_id,
            'subscribe_chapter_id' => $sendOrderItem->subscribe_chapter_id,
            'book_name' => $sendOrderItem->book_name,
            'registerNum' => $sendOrderItem->registerNum,
            'domain' => $sendOrderItem->domain,
            'chapter_name' => $sendOrderItem->chapter_name,
            'subscribe_chapter_name' => $sendOrderItem->subscribe_chapter_name,
            'subscribe_chapter_seq' => $sendOrderItem->subscribe_chapter_seq,
            'remark' => $sendOrderItem->remark,
            'star_num' => $sendOrderItem->star_num,
            'updated_at' => date("Y-m-d H:i:s", strtotime($sendOrderItem->updated_at)),
            'created_at' => date("Y-m-d H:i:s", strtotime($sendOrderItem->created_at)),
            'promotion_page_id' => $sendOrderItem->promotion_page_id,
            'redirect_url' => $sendOrderItem->redirect_url,
            'totalChargeAmount' => $sendOrderItem->totalChargeAmount,
            'fansNum' => $sendOrderItem->fansNum,
            'cost' => $sendOrderItem->cost,
            'payUserNum' => $sendOrderItem->payUserNum,
            'document_end_chapter_seq' => $sendOrderItem->document_end_chapter_seq,
            'sex_preference' => $sendOrderItem->sex_preference,
            'clickNum' => $sendOrderItem->clickNum,
            'qrcode' => $sendOrderItem->qrcode,
            'qr_code_id' => $sendOrderItem->qr_code_id,
            'clickNumUV' => $sendOrderItem->clickNumUV,
            'continue_read_uv' => $sendOrderItem->continue_read_uv,
            'consumeTotalFee' => $sendOrderItem->consumeTotalFee,
            'consumeChargeFee' => $sendOrderItem->consumeChargeFee,
            'consumeRewardFee' => $sendOrderItem->consumeRewardFee,
            'promotion_url' => $sendOrderItem->promotion_url,
            'promotion_type' => $sendOrderItem->promotion_type,
            'company_name' => $sendOrderItem->import_company_name,
            'browserClickNum' => $sendOrderItem->browserClickNum,
            'browserClickNumUV' => $sendOrderItem->browserClickNumUV,
            'fans_percent'=> $sendOrderItem->fans_percent,
            'charge_cost_percent'=> $sendOrderItem->charge_cost_percent,
            'pre_send_date' => empty($sendOrderItem->pre_send_date) ? '' : date("Y/m/d", strtotime($sendOrderItem->pre_send_date))
        ];
    }
}