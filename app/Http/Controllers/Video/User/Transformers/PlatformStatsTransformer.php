<?php
/**
 * Created by PhpStorm.
 * Date: 2017/3/31
 * Time: 14:02
 */

namespace App\Http\Controllers\Agents\User\Transformers;

use Hashids;

class PlatformStatsTransformer
{
    public function transform($sendOrderItem)
    {
        return [
            'id' => $sendOrderItem->id,
            'platform_id' => $sendOrderItem->platform_id,
            'send_order_id' => $sendOrderItem->send_order_id,
            'date' => $sendOrderItem->date,
            'link' => $sendOrderItem->link,
            'link_create_time' => $sendOrderItem->link_create_time,
            'tag' => $sendOrderItem->tag,
            'enter_chapter' => $sendOrderItem->enter_chapter,
            'click_num' => $sendOrderItem->click_num,
            'click_pv' => $sendOrderItem->click_pv,
            'inc_num' => $sendOrderItem->inc_num,
            'recharge_amount' => $sendOrderItem->recharge_amount,
            'recharge_num' => $sendOrderItem->recharge_num,
            /*'cost' => $sendOrderItem->cost,
            'hb_rate'=>$sendOrderItem->cost>0?(round($sendOrderItem->recharge_amount/$sendOrderItem->cost,4)*100).'%':'0%'*/
        ];
    }
}