<?php

namespace App\Http\Controllers\Agents\User;

use App\Http\Controllers\Agents\User\Transformers\SendOrderTransformer;
use App\Modules\Agents\Models\AgentsManage;
use App\Modules\Agents\Services\AgentsManageService;
use App\Modules\Book\Services\BookConfigService;
use App\Modules\OfficialAccount\Services\ForceSubscribeService;
use App\Modules\SendOrder\Services\SendOrderService;
use App\Modules\Statistic\Services\WapVisitStatService;
use App\Modules\Trade\Services\OrderService;
use App\Modules\User\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class SendOrdersController extends Controller
{

    public function getSendorderList()
    {

    }

    function getSendOrderListByAgentName(Request $request)
    {
        $agentId = $request->session()->get('agent_auth');
        $agent = AgentsManage::where('id', $agentId)->first();

        \Log::info('getSendOrderListByAgentName');
        if ($agent) {
            $agentName = $agent->agent_name;
            $is_yunqi = $request->has('is_yunqi') ? $request->input('is_yunqi') : '2';
            //获取云栖的派单数据
            if (1 == $is_yunqi) {
                $sendOrders = AgentsManageService::getYunQiSendOrdersByAgentName($agentName, false);
            } else {
                $sendOrders = AgentsManageService::getSendOrdersByAgentName($agentName, false);
            }

            foreach ($sendOrders as $item) {
                $sendOrderId = $item->id;
                $item->name = mb_convert_encoding($item->name, 'UTF-8', 'UTF-8');
                $item->book_name = mb_convert_encoding($item->book_name, 'UTF-8', 'UTF-8');
                $item->chapter_name = mb_convert_encoding($item->chapter_name, 'UTF-8', 'UTF-8');
                $item->consumeChargeFee = 0;//$bookOrderInfo['charge_balance'] + $chapterOrderInfo['charge_balance'];
                $item->consumeRewardFee = 0;//$bookOrderInfo['reward_balance'] + $chapterOrderInfo['reward_balance'];;
                $item->consumeTotalFee = 0;//$item->consumeChargeFee + $item->consumeRewardFee;
                $item->qrcode = "";//"data:image/png;base64," . base64_encode(QrCode::format('png')->size(200)->generate($promotion_url));
                $item->promotion_url = 'https://site' . encodeDistributionChannelId($item->distribution_channel_id) . '.' . $this->getDomainByBid($item->book_id) . '/yun/' . $item->id;

                if (1 == $is_yunqi) {
                    $item->fansNum = ForceSubscribeService::getNewFansNumYunQi($sendOrderId);
                    $item->payUserNum = OrderService::getPayUserNumYunQi($sendOrderId);
                    $item->registerNum = UserService::getPromotionTotalYunQi($sendOrderId);
                    $item->totalChargeAmount = (float)OrderService::getAmountYunQi(['send_order_id' => $sendOrderId]);
                    $browser_visit_info = SendOrderService::getBrowserUvAndPvYunQi($sendOrderId);
                    $item->browserClickNum = $browser_visit_info['pv'];
                    $item->browserClickNumUV = $browser_visit_info['uv'];
                    $item->continue_read_uv = SendOrderService::getContinueTotalReadUvYunQi($item->id);
                    $item->clickNumUV = WapVisitStatService::getSendOrderTotalUvYunQi($sendOrderId);//uv
                    $item->clickNum = WapVisitStatService::getSendOrderTotalPvYunQi($sendOrderId);

                    $item->fans_percent=self::getPersentAmount($item->fansNum,$item->registerNum);
                    $item->charge_cost_percent=self::getPersentAmountInteger($item->totalChargeAmount,$item->cost);
                }else{
                    $item->fansNum = ForceSubscribeService::getNewFansNum($sendOrderId);
                    $item->payUserNum = OrderService::getPayUserNum($sendOrderId);
                    $item->registerNum = UserService::getPromotionTotal($sendOrderId);
                    $item->totalChargeAmount = (float)OrderService::getAmount(['send_order_id' => $sendOrderId]);
                    $browser_visit_info = SendOrderService::getBrowserUvAndPv($sendOrderId);
                    $item->browserClickNum = $browser_visit_info['pv'];
                    $item->browserClickNumUV = $browser_visit_info['uv'];
                    $item->continue_read_uv = SendOrderService::getContinueTotalReadUv($item->id);

                    $item->clickNum = SendOrderService::getPv($sendOrderId);
                    $item->clickNumUV = SendOrderService::getUv($sendOrderId);//uv

                    $item->fans_percent=self::getPersentAmount($item->fansNum,$item->registerNum);
                    $item->charge_cost_percent=self::getPersentAmountInteger($item->totalChargeAmount,$item->cost);
                }
            }
            return response()->pagination(new SendOrderTransformer, $sendOrders);
        }
    }

    /**
     * 根据图书id获取域名
     * @param string $bid 图书id
     * @return string 域名
     */
    function getDomainByBid($bid = '')
    {
        $domain = 'leyuee.com';
        //如果图书id为空，则返回默认的域名
        if (!empty($bid)) {
            $bookConfig = BookConfigService::getBookById($bid);
            if ($bookConfig) {
                $domain = $bookConfig->promotion_domain;
            }
        }
        return $domain;
    }

    /**
     * 计算成本率
     * @param $amount 充值金额
     * @param $cost 成本
     * @return int|string
     */
    static function getPersentAmount($amount, $cost)
    {
        $percentResult = '0%';
        if (is_numeric($amount) && $amount > 0) {
            if (abs($cost) < 0.01) {
                $percentResult = '100%';
            } else {
                $percentResult = round(($amount / $cost) * 100, 2) . '%';
            }
        }
        return $percentResult;
    }

    static function getPersentAmountInteger($amount, $cost)
    {
        $percentResult = '0';
        if (is_numeric($amount) && $amount > 0) {
            if (abs($cost) < 0.01) {
                $percentResult = '1';
            } else {
                $percentResult = round(($amount / $cost), 4);
            }
        }
        return $percentResult;
    }

}
