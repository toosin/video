<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/11/26
 * Time: 18:22
 */
namespace App\Http\Controllers\Python\Image;

use App\Http\Controllers\Controller;
use App\Modules\Manage\Services\UserService;
use Illuminate\Http\Request;

use App\Modules\Manage\Models\User;

class ImageUploadController extends Controller
{
    public function upload_zhihu(Request $request) {
        $account_id = $request->input('account_id','');
        $path = $request->input('path','');
        $save_path = storage_path('download\\'.date('Ymd'));
        if (!file_exists($save_path)){
            mkdir ($save_path,0777,true);
        }
        $save_name = $save_path.'\\'.time().'_'.mt_rand(1,20000).'.jpg';
        $this->download($path,$save_name);
        $command ='python C://project/video/test_selenium.py '.$account_id." ".$save_name." 2>&1";
        exec($command,$ret,$status);
        \Log::info('command:'.$command);
        //\Log::info('--ret:'.var_export($ret,true));
        foreach ($ret as $each) {
            \Log::info('--ret:'.iconv("GB2312","UTF-8",$each));
        }
        \Log::info('--status:'.$status);
        if($status == 0) {
            $length = count($ret);
            if($length>0){
                $img_cdn = $ret[$length-1];
                return response()->success($img_cdn);
            }
        }
        return response('UPLOAD_IMG_ERROR');
    }


    /**
     *@通过curl方式获取制定的图片到本地
     *@ 完整的图片地址
     *@ 要存储的文件名
    **/
    public function download($url, $filename)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);

        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();

        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        file_put_contents($filename,$return_content);

        return true;


    }

    public function test_exec() {
        exec('php C:\\php7\test_exec.php 2>&1',$ret,$status);
        foreach ($ret as $each) {
            echo iconv("GB2312","UTF-8",$each);
        }
        var_dump($status);
    }
}