<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 2017/11/21
 * Time: 10:42
 */

namespace App\Modules\User\Services;

use App\Modules\User\Models\User;

class UserService
{
    /**
     * 查询推广注册用户总数
     * @param $send_order_id
     * @return mixed
     */
    static function getPromotionTotal($send_order_id)
    {
        return User::getPromotionTotal($send_order_id);
    }
}