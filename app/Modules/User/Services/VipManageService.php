<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 2017/11/21
 * Time: 10:42
 */

namespace App\Modules\User\Services;

use App\Modules\User\Models\VipManage;

class VipManageService
{
    public static function judgeLogin($name, $pwd)
    {
        return VipManage::judgeLogin($name, $pwd);
    }

    public static function getChannelusers($id)
    {
        return VipManage::getChannelusers($id);
    }
}