<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 2017/11/21
 * Time: 10:42
 */

namespace App\Modules\User\Services;

use App\Modules\User\Models\Manage;

class ManageService
{
    public static function judgeLogin($name, $pwd)
    {
        return Manage::judgeLogin($name, $pwd);
    }
}