<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 2017/11/21
 * Time: 10:42
 */

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserSubscribeBehaviorStats extends Model
{
    protected $table = 'user_subscribe_statistics';

    protected $fillable = ['id', 'date', 'appid', 'new_subscribe_num', 'recharge_sum', 'one_day_recharge',
        'three_days_recharge', 'actual_subscibe_num', 'seven_days_recharge', 'thirty_days_recharge',
        'sixty_days_recharge', 'new_sub_recharge_num', 'sub_user_orders_num'
    ];

}