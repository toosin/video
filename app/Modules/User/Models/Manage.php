<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/1/23
 * Time: 9:25
 */

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Manage extends Model
{
    protected $table = 'promotion_groups_manages';
    protected $fillable = ['name', 'role', 'password', 'state'];

    public static function judgeLogin($name, $pwd)
    {
        $res = self::where(['name' => $name, 'password' => $pwd])->first();
        return $res;
    }
}