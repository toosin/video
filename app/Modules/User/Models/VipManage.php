<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/1/23
 * Time: 9:25
 */

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class VipManage extends Model
{

    protected $table = 'vip_manages';
    protected $fillable = ['username', 'password', 'is_enabled', 'id'];

    public static function judgeLogin($name, $pwd)
    {
        $res = self::leftjoin('vip_channel_users', 'vip_channel_users.vip_manager_id', '=', 'vip_manages.id')
            ->select(['vip_manages.id', 'vip_channel_users.channel_user_id', 'username'])
            ->where(['username' => $name, 'password' => $pwd])
            ->get();
        return $res;
    }

    static function getChannelusers($id)
    {
        return self::join('vip_channel_users', 'vip_channel_users.vip_manager_id', '=', 'vip_manages.id')
            ->select(['vip_channel_users.channel_user_id'])
            ->where('vip_manages.id', $id)
            ->get();
    }
}