<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 2017/11/21
 * Time: 10:42
 */

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable =
        ['id', 'openid', 'unionid', 'distribution_channel_id', 'head_img',
            'register_ip', 'send_order_id', 'balance', 'sex', 'country', 'city',
            'province', 'nickname', 'charge_balance', 'reward_balance', 'is_new'
        ];

    //获取推广用户总数
    static function getPromotionTotal($send_order_id)
    {
        return self::where('send_order_id', $send_order_id)->count();
    }

    static function getTodayRegisterUserCount($distribution_channel_ids)
    {
        $begin_time = date('Y-m-d');
        $search_obj = self::orderBy('id', 'desc');
        $search_obj->whereIn('distribution_channel_id', $distribution_channel_ids);
        $search_obj->where('created_at', '>=', $begin_time);
        return $search_obj->count();
    }
}