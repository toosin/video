<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/1/23
 * Time: 9:25
 */

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class VipChannelUsers extends Model
{
    protected $table = 'vip_channel_users';
    protected $fillable = ['vip_manager_id', 'channel_user_id', 'is_enabled', 'id'];

    public static function addItem($data)
    {
        return self::create($data);
    }

    public static function updateItem($id, $data)
    {
        return self::where('id', $id)->update($data);
    }

    static function judgeIsExist($channel_user_id, $vip_manage_id)
    {
        return self::where('vip_manager_id', $vip_manage_id)->where('channel_user_id', $channel_user_id)->first();
    }
}