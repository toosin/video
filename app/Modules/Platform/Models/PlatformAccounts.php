<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/12
 * Time: 16:49
 */
namespace App\Modules\Platform\Models;
use Illuminate\Database\Eloquent\Model;

class PlatformAccounts extends Model {
    protected $table = 'platform_accounts';

    protected $fillable = ['id','user_id','platform','platform_id','account','password','cookie','avatar','nickname','status','platform_user_id','openid','token','extra_info','access_token','access_token_expire','binded_time','due_time','valid_time'];

    public static function getBindedAccountsList($user_id,$params,$paginate) {
        $obj = self::where('platform_accounts.status','<',2)
            ->where('platform_accounts.user_id',$user_id)
            ->join('platforms','platforms.id','=','platform_accounts.platform_id')
            ->select([
                'platform_accounts.id',
                'platform_accounts.platform_id',
                'platform_accounts.nickname',
                'platform_accounts.avatar',
                'platforms.auth_url',
                \DB::raw('platforms.icon as platform_icon'),
                \DB::raw('platforms.desc as platform_name'),
                'platform_accounts.status',
                'platforms.extral_fields'
            ])->orderBy('platform_accounts.id','desc');
        if(isset($params['type'])&&$params['type']) {
            $obj->where('platforms.type',$params['type']);
        }
        return $obj->paginate($paginate);
    }

    public static function getById($id) {
        return self::where('id',$id)->where('status','<',2)->first();
    }

    public static function getByOpenIdAndPlatform($openId,$platform) {
        return self::where('openid',$openId)->where('platform',$platform)->first();
    }

    /**
     * 通过id获取数据.
     *
     * @param array $userIds
     * @return array
     * @author 肖俊明<2284876299@qq.com>
     * @since 2019年12月28日
     */
    public static function getPlatformKeyId (array $ids)
    {
        $platforms = self::getPlatformByIds($ids);
        return empty($platforms) ? [] : $platforms->keyBy('id');
    }

    /**
     * 通过id获取数据.
     *
     * @param array $ids
     * @return array
     * @author 肖俊明<2284876299@qq.com>
     * @since 2019年12月28日
     */
    public static function getPlatformByIds (array $ids)
    {
        return self::whereIn('id', $ids)->get();
    }
}
