<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/12
 * Time: 16:49
 */
namespace App\Modules\Platform\Models;
use Illuminate\Database\Eloquent\Model;

class ColumnAccounts extends Model {
    protected $table = 'column_accounts';

    protected $fillable = ['id','platform_account_id','column_id','status'];

    public static function getByAccountColumn($account_id,$column_id) {
        return self::where('column_id',$column_id)->where('platform_account_id',$account_id)->first();
    }

    public static function countColumnAccounts($column_id) {
        return self::where('column_id',$column_id)->where('status',1)->count('id');
    }

    public static function getColumnAccounts($column_id) {
        $obj = self::join('platform_accounts','platform_accounts.id','=','column_accounts.platform_account_id')
            ->join('platforms','platforms.id','=','platform_accounts.platform_id')
            ->where('column_accounts.column_id',$column_id)
            ->where('column_accounts.status',1)
            ->where('platform_accounts.status','<',2)
            ->select([
                'platform_account_id','platform_id','avatar','nickname',
                'platforms.auth_url',
                \DB::raw('platforms.icon as platform_icon'),
                \DB::raw('platforms.desc as platform_name'),
                'platform_accounts.status',
                'platforms.extral_fields'
            ])
            ->get();
        return $obj;
    }
}