<?php
namespace App\Modules\Platform\Models;
use Illuminate\Database\Eloquent\Model;

class UploadVideo extends Model
{
    protected $table = 'upload_video';
    protected $fillable = ['id','user_id','relative_path','file_name','slice','status'];

    //文件地址访问器
    public function getRelativePathAttribute($value)
    {
        return config('path.static_domain') . $value;
    }
}