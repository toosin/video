<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/12
 * Time: 16:49
 */
namespace App\Modules\Platform\Models;
use Illuminate\Database\Eloquent\Model;

class Platforms extends Model {
    protected $table = 'platforms';

    protected $fillable = ['id','platform','desc','type','status','login_type','icon','cookie_keys'];

    public static function getAll($params) {
        $obj = self::where('status',1);
        if(isset($params['type']) && $params['type']) {
            $obj->where('type',$params['type']);
        }
        return $obj->get();
    }

    public static function getByid($id){
        return self::where('id',$id)->first();
    }
}