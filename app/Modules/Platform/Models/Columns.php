<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/12
 * Time: 16:49
 */
namespace App\Modules\Platform\Models;
use Illuminate\Database\Eloquent\Model;

class Columns extends Model {
    protected $table = 'columns';

    protected $fillable = ['id','user_id','column_name','status','type'];

    public static function getByName($name,$uid,$type) {
        return self::where('column_name',$name)
            ->where('type',$type)
            ->where('user_id',$uid)
            ->first();
    }

    public static function getById($id) {
        return self::where('id',$id)->first();
    }

    public static function getUserColumns($uid,$type) {
        return self::where('user_id',$uid)
            ->where('status',1)
            ->where('type',$type)
            ->get();
    }
}