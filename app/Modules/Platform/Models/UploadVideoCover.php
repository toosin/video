<?php
namespace App\Modules\Platform\Models;

use Illuminate\Database\Eloquent\Model;

class UploadVideoCover extends Model
{
    protected $table = 'upload_video_cover';
    protected $fillable = ['id','user_id','relative_path','slice','file_name'];

    //文件地址访问器
    public function getRelativePathAttribute($value)
    {
        return config('path.static_domain') . $value;
    }
}