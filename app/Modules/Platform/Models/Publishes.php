<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/12
 * Time: 16:49
 */
namespace App\Modules\Platform\Models;
use Illuminate\Database\Eloquent\Model;

class Publishes extends Model {
    protected $table = 'publishes';

    protected $fillable = ['id','title','platform_account_id','platform_id','user_id','status','reason','contents_info','response','content_id','publish_time','timer'];

    public static function getPublishedList($user_id,$params = []) {
        $obj = self::join('platform_accounts','platform_accounts.id','=','publishes.platform_account_id')
            ->join('platforms','platforms.id','=','platform_accounts.platform_id')
            ->select([
                'publishes.id',
                'publishes.title',
                'publishes.publish_time',
                'platforms.desc as platform_name',
                'platform_accounts.nickname',
                'publishes.reason',
                'publishes.status'
            ])
            ->where('publishes.user_id',$user_id)
            ->orderBy('publishes.id','desc');
        if(isset($params['status'])&&$params['status']) {
            if($params['status'] == 1) {
                $obj->whereIn('publishes.status',['UPLOAD_SUCCESS','UPLOAD_FAILED','REVIEWING','REVIEW_FAILED']);
            }
            if($params['status'] == 2) {
                $obj->where('publishes.status','REVIEW_SUCCESS');
            }
            if($params['status'] == 3) {
                $obj->where('publishes.status','WAIT_UP');
            }
            //$obj->where('');
        }
        if(isset($params['type'])&&$params['type']) {
            $obj->where('platforms.type',$params['type']);
        }
        return $obj->paginate();
    }
}