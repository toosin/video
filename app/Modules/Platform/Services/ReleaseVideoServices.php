<?php
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */


namespace App\Modules\Platform\Services;

use App\Modules\Platform\Models\UploadVideo;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use App\Modules\Platform\Models\PlatformAccounts;

class ReleaseVideoServices
{
    public static $releaseVideoPlate = [
        15 => \App\Modules\Platform\Services\ReleaseQqServices::class,
        18 => \App\Modules\Platform\Services\ReleaseHaoKanServices::class
    ];

    /**
     * Release.
     *
     * @param $request
     * @return mixed
     * @since 2019年12月28日
     */
    public static function release($request)
    {
        $accounts = $request->accounts;
        if (empty($accounts)) {
            return response()->error('MOVE_FAIL');
        }
        #只获取第一个视频Id就可以了
        $video_id = $accounts[0]['video_id'];
        $video = UploadVideo::find($video_id);
        $accountIds = array_column($accounts, 'accountId');

        $platformAccounts = PlatformAccounts::getPlatformKeyId($accountIds);

        $accounts = collect($accounts)->keyBy('accountId');

        foreach ($accounts as $accountId => $account) {
            $platformAccount = $platformAccounts->get($accountId);
            if (empty($platformAccount)) {
                continue;
            }
            $platformId = $platformAccount->platform_id;

            $class = self::$releaseVideoPlate[$platformId];
            $class::pushVideo($video,$platformAccount,$account['formData']);

        }
        return $platformId;
        #$videoFile = Storage::disk('video')->get($video->slice . '/' . $video->file_name);
    }
}
