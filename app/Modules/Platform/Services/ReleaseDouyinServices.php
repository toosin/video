<?php
namespace App\Modules\Platform\Services;

use App\Libs\Douyin\Open\Lib\Model\VideoCreateBody;
use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Platforms;
use App\Modules\Platform\Models\Publishes;
use App\Libs\Douyin\Open\Lib\Api\VideoPublishApi;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use App\Modules\Platform\Models\UploadVideo;

class ReleaseDouyinServices
{
    public static function release1($accountInfo,$videoInfo,$formData,$publishes)
    {
        $user_id=$accountInfo->user_id ;// = auth('api')->user()->id;
        $platform_id = 19;
//        if(!$user){
//            $user_id = 1;
//        }else{
//            $user_id = $user->id;
//        }

        //$video_id = $request->video_id;

        $video =$videoInfo;
        //$videoFile = Storage::disk('video')->get($video->slice . '/' . $video->file_name);

        $platform = PlatformAccounts::where('user_id',$user_id)->where('platform_id',$platform_id)->first();
        $config = config('key.douyin');
       /* $token = Redis::hMGet('access_token_info:' . $platform_id . ':' . $user_id.':'.$accountInfo['id'],['access_token']);
        \Log::info($token);
        $access_token = $token[0];*/
        $access_token = $platform->access_token;
        $client = new Client();
        $file_name_expload = explode('.',$video->file_name);
        $ext = end($file_name_expload);
        \Log::info('video/'.$ext);
        //\Log::info(fopen(public_path().$videoInfo->relative_path,'r'));
        $response = $client->request('POST',$config['uri']. $config['upload_video_api'].'?open_id='.$platform->openid.'&access_token='.$access_token,[
           /* 'form_params' => [
                'open_id' => $platform->openid,
                'access_token ' => $access_token,
            ],*/
            'multipart' => [
                ['name'=>'Content-Type','contents'=>'video/'.$ext],
//                ['name'=>'open_id','contents'=>$platform->openid],
//                ['name'=>'access_token','contents'=> $access_token],
                [
                    'name'     => 'video',
                    'contents' => fopen(public_path().$videoInfo->relative_path,'r'),
                    'filename'=>$video->file_name,
                    'headers'  => [
                        'Content-Type' => 'video/'.$ext
                    ]
                ]
            ],
        ]);
        $content = $response->getBody()->getContents();
        \Log::info('content:1'.$content);
        $content = json_decode($content,true);
        $formData['description'] = isset($formData['description'])?$formData['description']:'';
        if(isset($content['data']['error_code']) && $content['data']['error_code'] == 0){
            $douyin_video_id = $content['data']['video']['video_id'];
            $response = $client->request('POST',$config['uri'] . $config['create_video_api'].'?open_id='.$platform->openid.'&access_token='.$access_token,[
//                'form_params' => [
//                    'open_id' => $platform->openid,
//                    'access_token ' => $access_token,
//                ],
                'json' => [
                    "video_id" => $douyin_video_id,
                    "text" => $formData['title'].$formData['description'],
//                    "poi_id" => "string",
//                    "poi_name" => "string",
//                    "micro_app_id" => "ttef9b112671b152ba",
//                    "micro_app_title" => "小程序标题",
//                    "micro_app_url" => "string",
                    "cover_tsp" => 2.3,
//                    "at_users" => [
//                        "1ad4e099-4a0c-47d1-a410-bffb4f2f64a4"
//                    ]
                ],
            ]);
            $content = $response->getBody()->getContents();
            \Log::info('content:2:'.$content);
            $content = json_decode($content,true);
            if(isset($content['data']['error_code']) && $content['data']['error_code'] == 0){
                //抖音视频id
                $item_id = $content['data']['item_id'];
                $publishes->content_id = $item_id;
                $publishes->contents_info = json_encode([
                    "video_id" => $douyin_video_id,
                    "text" => $formData['title'],
                    "cover_tsp" => 2.3,

                ],JSON_UNESCAPED_UNICODE);
                $publishes->response = json_encode($content);
                $publishes->status = 'REVIEW_SUCCESS';
                $publishes->save();
                /*$publish_data = [
                    'title'=>$formData['title'],
                    'platform_id'=>$accountInfo->platform_id,
                    'platform_account_id'=>$accountInfo->id,
                    'user_id'=>$accountInfo->user_id,
                    'content_id'=>$item_id,
                    'contents_info'=>json_encode([
                        "video_id" => $douyin_video_id,
                        "text" => $formData['title'],
                        "cover_tsp" => 2.3,

                    ],JSON_UNESCAPED_UNICODE),
                    'response'=>json_encode($content),
                    'status'=>'REVIEW_SUCCESS'
                ];
                Publishes::create($publish_data);*/
                //todo 存入发布视频表
                return response()->success();
            }else{
                $publishes->content_id = 0;
                $publishes->contents_info = json_encode([
                    "video_id" => $douyin_video_id,
                    "text" => $formData['title'],
                    "cover_tsp" => 2.3,

                ],JSON_UNESCAPED_UNICODE);
                $publishes->response = json_encode($content);
                $publishes->status = 'UPLOAD_FAILED';
                $publishes->reason = $content['data']['description'];
                $publishes->save();
                /*$publish_data = [
                    'title'=>$formData['title'],
                    'platform_id'=>$accountInfo->platform_id,
                    'platform_account_id'=>$accountInfo->id,
                    'user_id'=>$accountInfo->user_id,
                    'content_id'=>0,
                    'contents_info'=>json_encode([
                        "video_id" => $douyin_video_id,
                        "text" => $formData['title'],
                        "cover_tsp" => 2.3,

                    ],JSON_UNESCAPED_UNICODE),
                    'response'=>json_encode($content),
                    'status'=>'UPLOAD_FAILED',
                    'reason'=>$content['data']['description']
                ];
                Publishes::create($publish_data);
                return response()->error('UPLOAD_FAILED');*/
            }
        }else{
            $publishes->content_id = 0;
            $publishes->contents_info = json_encode([
                "video_id" => 0,
                "text" => $formData['title'],
                "cover_tsp" => 2.3,

            ],JSON_UNESCAPED_UNICODE);
            $publishes->response = json_encode($content);
            $publishes->status = 'UPLOAD_FAILED';
            $publishes->reason = $content['data']['description'];
            $publishes->save();
            /*$publish_data = [
                'title'=>$formData['title'],
                'platform_id'=>$accountInfo->platform_id,
                'platform_account_id'=>$accountInfo->id,
                'user_id'=>$accountInfo->user_id,
                'content_id'=>0,
                'contents_info'=>json_encode([
                    "video_id" => 0,
                    "text" => $formData['title'],
                    "cover_tsp" => 2.3,

                ],JSON_UNESCAPED_UNICODE),
                'response'=>json_encode($content),
                'status'=>'UPLOAD_FAILED',
                'reason'=>$content['data']['description']
            ];
            Publishes::create($publish_data);*/
        }
        return response()->error('UPLOAD_FAILED');
        //return 1;
    }

    public static function release($accountInfo,$videoInfo,$formData,$publishes){
        $pub_api = new VideoPublishApi();
        $open_id = $accountInfo->openid;
        $access_token = $accountInfo->access_token;
        $video_path = public_path().$videoInfo->relative_path;
        $res = $pub_api->videoUploadPost($video_path,$open_id,$access_token);
        $data = $res->getData();
        $error_msg = $data->getErrorCode()->valid();
        //var_dump($error_msg);
        $video = $data->getVideo();
        $video_id = $video->getVideoId();
        \Log::info('douyin_upload_res:'.json_encode($res));
        //var_dump($video->getVideoId());
        //var_dump($video->getHeight());
        //var_dump($video->getWidth());

        $video_create_body = new VideoCreateBody();
        $video_create_body->setCoverTsp(2.3);
        $video_create_body->setVideoId($video_id);
        $video_create_body->setText($formData['title'].$formData['description']);
        $result = $pub_api->videoCreatePost($open_id, $access_token, $video_create_body);
        $create_data = $result->getData();
        $error_code = $create_data->getErrorCode()->valid();
        $item_id = $create_data->getItemId()->valid();
        \Log::info('douyin_create_res:'.json_encode($result));
        if($error_code){
            $publishes->content_id = $item_id;
            $publishes->contents_info = json_encode([
                "video_id" => $video_id,
                "text" => $formData['title'],
                "cover_tsp" => 2.3,

            ],JSON_UNESCAPED_UNICODE);
            $publishes->response = json_encode($result);
            $publishes->publish_time = date('Y-m-d H:i:s');
            $publishes->status = 'REVIEW_SUCCESS';
            $publishes->save();
        }

    }
}