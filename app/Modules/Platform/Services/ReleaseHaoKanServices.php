<?php
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */


namespace App\Modules\Platform\Services;

use GuzzleHttp\Client;

class ReleaseHaoKanServices
{
    /**
     * 上传好看视频内容.
     *
     * @param $video
     * @param $platformAccount
     * @param $formData
     * @return array|bool
     * @since 2019年12月28日
     */
    public static function pushVideo($video, $platformAccount, $formData)
    {
        $is_original = 0;
        $title = $formData['title'];
        $app_id = $platformAccount['openid'];
        $app_token = $platformAccount['token'];
        $video_url = 'http://'.$video->relative_path;
        $cover_images = $formData['cover'];

        return self::publishHaoKan($app_id, $app_token, $title, $video_url, $cover_images,
            $is_original, $use_auto_cover=1, $tag = '');
    }

    /**
     * 发布好看视频.
     *
     * @param $app_id
     * @param $app_token
     * @param $title
     * @param $video_url
     * @param string $cover_images
     * @param int $is_original
     * @param int $use_auto_cover
     * @param string $tag
     * @return array|bool
     * @since 2019年12月25日
     */
    public static function publishHaoKan($app_id, $app_token, $title, $video_url, $cover_images= '',
                                         $is_original = 0, $use_auto_cover=1, $tag = '')
    {

        $args = [
            'app_id' => $app_id,
            'app_token' => $app_token,
            'title' => $title,
            'video_url' => $video_url,
            'cover_images' => $cover_images,
            'is_original' => $is_original,
            'use_auto_cover' => $use_auto_cover,
            'tag' => $tag
        ];


        $client = new Client();
        $url = 'http://baijiahao.baidu.com/builderinner/open/resource/video/publish';
        $options['form_params'] = $args;
        try {
            $response = $client->request('POST', $url, $options);
        } catch (\Exception $exception) {
            return false;
        }
        $result = json_decode($response->getBody()->getContents(),true);



        //$ret = [];
        if(is_array($result) && (int)$result['errno'] == 0) {
            return $result['data'];
        }
        return [];
    }

}
