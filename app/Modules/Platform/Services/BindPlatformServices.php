<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/1/23
 * Time: 9:51
 */

namespace App\Modules\Platform\Services;

use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Platforms;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Redis;
use Sunra\PhpSimple\HtmlDomParser;

class BindPlatformServices
{
    public static function getAiqiyiQrcode($platform_id){
        $uid = auth('api')->user()->id;
        $client     = new Client();
        $response   = $client->request('POST','https://passport.iqiyi.com/apis/qrcode/gen_login_token.action?agenttype=1&device_name=%E7%BD%91%E9%A1%B5%E7%AB%AF&fromSDK=1&ptid=01010021010000000000&sdk_version=1.0.0&surl=1');
        $result     = json_decode((string) $response->getBody(),true);
        $cookie_str = self::extractCookie($response->getHeader('Set-Cookie'));
        $url        =  urlencode($result['data']['url']);
        $salt       = md5('35f4223bb8f6c8638dc91d94e9b16f5'.$url);
        $img_url    = "https://qrcode.iqiyipic.com/login/?data={$url}&salt={$salt}&width=162&property=0";
        $flag = ['user_id'=>1,'platform_id'=>$platform_id];
        /*$data = [
            'user_id'       =>1,
            'platform_id'   =>$platform_id,
            'token'          =>$result['data']['token'],
            'cookie'        => $cookie_str,
            //'extra_info'     => json_encode($result['data'])
        ];*/
        Redis::hMset('aqiyi_qrcode_info:'.$uid.':'.$platform_id.':',['token'=>$result['data']['token'],'cookie'=>$cookie_str]);
        \Log::info(json_encode($result));
        //PlatformAccounts::updateOrCreate($flag,$data);
        return file_get_contents($img_url);
    }

    public  static  function getYoukuQrcode($platform_id){
        $uid = auth('api')->user()->id;
        $client = new client();
        $response = $client->request('GET',"https://cnpassport.youku.com/newlogin/qrcode/generate.do?appName=youku");
        $info = json_decode($response->getBody()->getContents(),true);
        \Log::info(json_encode($info));
        $cookie_str = self::extractCookie($response->getHeader('Set-Cookie'));
        if(!isset($info['content'])){
            return response()->error('PARAM_EMPTY');
        }
        if(isset($info['content']['data']['imgUrl'])){
            $flag = ['user_id'=>1,'platform_id'=>$platform_id];
            /*$data = [
                'user_id'        => 1,
                'platform_id'   => $platform_id,
                'cookie'         => $cookie_str,
                'extra_info'   =>  json_encode(['t'=>$info['content']['data']['t'],'ck'=>$info['content']['data']['ck']])
            ];
            \Log::info(json_encode($data));
            PlatformAccounts::updateOrCreate($flag,$data);*/
            Redis::hMset('youku_qrcode_info:'.$uid.':'.$platform_id.':',['cookie'=>$cookie_str,'extra_info'=>json_encode(['t'=>$info['content']['data']['t'],'ck'=>$info['content']['data']['ck']])]);
            return file_get_contents($info['content']['data']['imgUrl']);
        }else{
            return response()->error('PARAM_EMPTY');
        }

    }

    public static function getYoukuScanResult($request){
        $client = new client();
        $platform_id= $request->platform_id;
        $user_id    = auth('api')->user()->id;////auth('api')->user()->id;
        //\Log::info('platform_id:'.$platform_id);
        //$platform = PlatformAccounts::where('user_id',$user_id)->where('platform_id',$platform_id)->first();
        $qr_info = Redis::hMGet('youku_qrcode_info:'.$user_id.':'.$platform_id.':',['cookie','extra_info']);
        //\Log::info(json_encode($platform));
        if(!$qr_info){
            return response()->error('NO_SCAN');
        }
        $info = json_decode($qr_info[1],true);
        //获取扫码结果并获取cookie
        $url = "https://cnpassport.youku.com/newlogin/qrcode/query.do?t={$info['t']}&ck={$info['ck']}&appName=youku";
        $response = $client->request('POST',$url,[
            'headers'=>['Cookie'=>$qr_info[0]],
            'form_params'=>[
                't'=>$info['t'],
                'ck'=>$info['ck']
            ]
        ]);
        $resp = $response->getBody()->getContents();
        \Log::info($resp);
        $result = json_decode($resp,true);
        \Log::info('-------扫码结果--'.json_encode($result)."\n\n");
        if(!isset($result['content']['data']['loginResult'])){
            return response()->error('NO_SCAN');
        }

        if($result['content']['data']['loginResult']!='success'){
            return response()->error('AUTH_ERROR');
        }
        //获取扫码的cookie
        $headers = self::extractCookie($response->getHeader('Set-Cookie'));
        //获取用户的优酷信息
        $youKuUserInfo = self::getYoukuUserInfo($headers);
        \Log::info('-------获取用户信息--'.json_encode($youKuUserInfo)."\n\n");
        if(!isset($youKuUserInfo['result']['ret'])){
            \Log::info('-------获取用户信息--'.json_encode($youKuUserInfo['ret'])."\n\n");
            return response()->error('AUTH_ERROR');
        }
        //保存用户信息到数据库
        $platform_user_id = $youKuUserInfo['result']['data']['uid'];
        $platform     ='youkuvideo';
        $account      =$youKuUserInfo['result']['data']['login_mobile'];
        $avatar       =$youKuUserInfo['result']['data']['user_icon'];
        $nickname     =$youKuUserInfo['result']['data']['uname'];
        $updated_at   =date("Y-m-d H:i:s",time());
        $extra_info   =json_encode($youKuUserInfo);
        $cookie       =$youKuUserInfo['cookie'];
        $status       =1;
        $res = PlatformAccounts::updateOrCreate(compact('platform_id','platform_user_id'),compact('account','avatar','nickname','extra_info','cookie','status'));
        //$res = $platform->save();
        //\Log::info('-------保存用户信息--'.json_encode($res)."\n\n");
        if($res){
            return response()->success($platform);
        }else{
            return response()->error('AUTH_ERROR');
        }

    }
    public static function youkuUserInfo($request){
        $cookie = $request->input('cookie','');
        $user_id = auth('api')->user()->id;
        if(empty($cookie)) {
            return response()->error('PARAM_EMPTY');
        }
        try{
            $client = new client();
            $sign = '';
            $appKey = '24679788';
            $data = '{"req":"{\"deviceType\":\"pc\",\"show\":1}"}';
            $t = timeStamp();
            $urlData = [
                'jsv'   =>'2.4.2',
                'appKey'=> $appKey,
                't'     => $t,
                'sign'  => $sign,
                'data'  => $data
            ];
            $url = "https://acs.youku.com/h5/mtop.youku.vip.xtop.member.profile.get/1.0/?".http_build_query($urlData);
            $response = $client->request('GET',$url,['headers'=>['cookie'=>$cookie]]);
            $result = json_decode($response->getBody()->getContents(),true);
            \Log::info("---RESULT111---".json_encode($result,JSON_UNESCAPED_UNICODE)."\n");
            if($result['ret'][0] == 'FAIL_SYS_ILLEGAL_ACCESS::非法请求'){
                $cookie_arr = $response->getHeader('Set-Cookie');
                foreach ($cookie_arr as $k=>$v){
                    $each_cookie = explode(';',$v);
                    if(strstr($each_cookie[0],'_m_h5_tk=')){
                        $token = substr(explode('=',$each_cookie[0])[1],0,32);
                    }
                    $arr[] = $each_cookie[0];
                }
                $cookie = explode(';',$cookie);
                foreach ($cookie as $k=>$v){
                    if(strstr($v,'_m_h5_tk=')){
                        unset($cookie[$k]);
                    }
                    if(strstr($v,'_m_h5_tk_enc=')){
                        unset($cookie[$k]);
                    }
                }
                $cookie = implode(";",$cookie).";".implode(';',$arr);
                $t = timeStamp();
                $sign =md5($token."&".$t."&".$appKey."&".$data);
                $urlData = [
                    'jsv'   =>'2.4.2',
                    'appKey'=> $appKey,
                    't'     => $t,
                    'sign'  => $sign,
                    'data'  => $data
                ];
                \Log::info("---Sign---".json_encode($urlData)."\n");
                \Log::info("------cookie----".$cookie);
                $url = "https://acs.youku.com/h5/mtop.youku.vip.xtop.member.profile.get/1.0/?".http_build_query($urlData);
                $response = $client->request('GET',$url,['headers'=>['cookie'=>$cookie]]);
                $result = json_decode($response->getBody()->getContents(),true);
                \Log::info("---RESULT22---".json_encode($result,JSON_UNESCAPED_UNICODE)."\n");
                if($result['ret'][0] == 'SUCCESS::调用成功'){
                    $sign = ['user_id'=>$user_id,'platform_id'=>16,'platform_user_id'  => $result['data']['uid']];
                    $up = [

                        'platform'          => 'youkuvideo',
                        'account'           => $result['data']['login_mobile'],
                        'avatar'            => $result['data']['user_icon'],
                        'nickname'          => $result['data']['uname'],
                        'updated_at'        => date("Y-m-d H:i:s",time()),
                        'cookie'            => $cookie,
                        'extra_info'        => json_encode($result),
                        'binded_time'=>date('Y-m-d H:i:s'),
                        'status'=>1
                    ];
                    $account_info = PlatformAccounts::updateOrCreate($sign,$up);
                    if($account_info){
                        return response()->success($account_info);
                    }else{
                        return response()->error('AUTH_ERROR');
                    }
                }else{
                    return response()->error('AUTH_ERROR');
                }

            }else{
                return response()->error('AUTH_ERROR');
            }

        }catch (\Exception $e){
            return response()->error('AUTH_ERROR');
        }

    }

    //获取优酷的用户信息，需要先找到到生产sign的token
    public static function getYoukuUserInfo($headers){
        $client = new client();
        $sign = '';
        $appKey = '24679788';
        $t = timeStamp();
        $data = '{"req":"{\"deviceType\":\"pc\",\"show\":1}"}';
        $urlData = [
            'jsv'   =>'2.4.2',
            'appKey'=> $appKey,
            't'     => $t,
            'sign'  => $sign,
            'data'  => $data
        ];
        $url = "https://acs.youku.com/h5/mtop.youku.vip.xtop.member.profile.get/1.0/?".http_build_query($urlData);
        $response = $client->request('GET',$url,['headers'=>['cookie'=>$headers]]);
        $cookie_arr = $response->getHeader('Set-Cookie');
        foreach ($cookie_arr as $k=>$v){
            $each_cookie = explode(';',$v);
            if(strstr($each_cookie[0],'_m_h5_tk=')){
                $token = substr(explode('=',$each_cookie[0])[1],0,32);
            }
            $arr[] = $each_cookie[0];
        }
        $cookie_str = $headers.';'.implode(';',$arr);
        $sign =md5($token."&".$t."&".$appKey."&".$data);
        $urlData['sign'] = $sign;
        $url = "https://acs.youku.com/h5/mtop.youku.vip.xtop.member.profile.get/1.0/?".http_build_query($urlData);
        $response = $client->request('GET',$url,['headers'=>['cookie'=>$cookie_str]]);
        $result =  json_decode($response->getBody()->getContents(),true);
        return ['cookie'=>$cookie_str,'result'=>$result];
    }

    public static function getSouhuQrcode($platform_id){
        $client     = new Client();
        $response   = $client->request('POST','http://usr.mb.hd.sohu.com/pc/genqrtoken.json');
        $result     = json_decode($response->getBody()->getContents(),true);
//        $url        =  urlencode($result['data']['url']);
//        $salt       = md5('35f4223bb8f6c8638dc91d94e9b16f5'.$url);
//        $img_url    = "https://qrcode.iqiyipic.com/login/?data={$url}&salt={$salt}&width=162&property=0";
//        $flag = ['user_id'=>1,'platform_id'=>$platform_id];
//        $data = [
//            'user_id'       =>1,
//            'platform_id'   =>$platform_id,
//            'token'          =>$result['data']['token'],
//            //'extra_info'     => json_encode($result['data'])
//        ];
//        PlatformAccounts::updateOrCreate($flag,$data);
        return file_get_contents($result['qrurl']);
    }

    public static function getAiqiyiScanResult($request)
    {

        $client     = new Client();
        $platform_id= $request->platform_id;
        $user_id    = auth('api')->user()->id;////auth('api')->user()->id;
        //$platform = PlatformAccounts::where('user_id',$user_id)->where('platform_id',$platform_id)->first();
        /*if(!$platform){
            return response()->error('NO_SCAN');
        }*/
        $qr_info = Redis::hMGet('aqiyi_qrcode_info:'.$user_id.':'.$platform_id.':',['token','cookie']);
        if(empty($qr_info)) {
            return response()->error('NO_SCAN');
        }
        $url        = "https://passport.iqiyi.com/apis/qrcode/is_token_login.action?agenttype=1&fromSDK=1&ptid=01010021010000000000&sdk_version=1.0.0&token={$qr_info[0]}";
        $response   = $client->request('POST',$url);
        $data       = json_decode( (string) $response->getBody(),true);

        if(($data['code'] == 'A00000') && isset($data['data']['authcookie'])){
            $tokenInfo = file_get_contents("http://openapi.iqiyi.com/api/person/authorize?uid={$data['data']['userinfo']['uid']}&authtoken={$data['data']['authcookie']}&client_id=404461fb9cf041ba94a8dfdc181fe23b&client_secret=dac197defc53d6252f8d7c7d6ecc8508&cb=vcop__1577152469699person");
            $cookie_str = self::extractCookie($response->getHeader('Set-Cookie'));
            if(!$tokenInfo = strstr($tokenInfo,"{\"")){
                return response()->error('BIND_ERROR');
            }
            $tokenInfo = json_decode($tokenInfo,true);
            if($tokenInfo['code'] != "A00000"){
                return response()->error('BIND_ERROR');
            }
            //$to_add = [];
            $access_token = $tokenInfo['data']['access_token'];
            $access_token_expire = date('Y-m-d H:i:s',time()+$tokenInfo['data']['expires_in']);
            $auth_cookie   = $data['data']['authcookie'];
            $cookie   = $qr_info[0].';'.$cookie_str;
            $openid   = $data['data']['userinfo']['uid'];
            $nickname = $data['data']['qiyi_vip_info']['name'];
            $avatar   = $data['data']['userinfo']['icon'];
            $extra_info = json_encode($tokenInfo['data']);
            $binded_time=date('Y-m-d H:i:s');
            $status=1;
            $res = PlatformAccounts::updateOrCreate(compact('platform_id','openid','user_id'),compact('access_token','access_token_expire','auth_cookie','cookie','nickname','avatar','extra_info','binded_time','status'));
            //$res = $platform->save();
            if($res){
                return response()->success();
            }else{
                return response()->error('NEED_REFRESH_QRCODE');
            }
        }else{
            return response()->error('NO_SCAN');
        }
    }


    public static function getDazhongQrcode($platform_id)
    {
        $user_id = auth('api')->user()->id;
        $platform_id = 12;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://account.dianping.com/account/getqrcodeimg");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, storage_path('dazhong_lgtoken_'.$platform_id.'_'.$user_id));
        $result=curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public static function getDazhongScanResult($request)
    {
        //$client = new Client();

        $user_id = auth('api')->user()->id;
        $platform_id = $request->platform_id;

        $res = file_get_contents(storage_path('dazhong_lgtoken_'.$platform_id.'_'.$user_id));
        $pos = strpos($res,'lgtoken');
        $token = substr($res,$pos + strlen('lgtoken'));
        $token = trim($token);

        $url = "https://account.dianping.com/account/ajax/queryqrcodestatus?lgtoken=" . $token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($url));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $result=curl_exec($ch);
        \Log::info($url);
        \Log::info($result);
        curl_close($ch);
        $result = json_decode($result,true);

        if(!isset($result['msg']['status'])){
            return response()->error('BIND_ERROR');
        }
        if($result['msg']['status'] == -1){
            //已失效
            return response()->error('NEED_REFRESH_QRCODE');
        }
        if($result['msg']['status'] == 0){
            //未扫码
            return response()->error('NO_SCAN');
        }
        if($result['msg']['status'] == 3){
            //已取消授权
            return response()->error('USER_CANCL_SCAN');
        }
        if($result['msg']['status'] == 1){
            //扫码成功
            return response()->error('BIND_ERROR');
        }
        if($result['msg']['status'] == 2){
            //确认授权
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        //curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, storage_path('dazhong_cookie_'.$platform_id.'_'.$user_id));
        $result=curl_exec($ch);

        curl_setopt($ch,CURLOPT_URL,'http://www.dianping.com/dpnav/userCardData');
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
        curl_setopt($ch, CURLOPT_POST, 0);
        $response = curl_exec($ch);
        $a=curl_getinfo($ch);

        $header = $a['request_header'];
        \Log::info('aaaaaa:'.json_encode($a));
        \Log::info('response:'.$response);
        $cookie_match = preg_match('/Cookie:(.*)\r\n/',$header,$matches);
        $cookie = $matches[1];
        $user_info = json_decode($response,true);
        if(is_array($user_info)) {
            $avatar = $user_info['msg']['userCard']['userFace'];
            $nickname = $user_info['msg']['userCard']['userNickName'];
            $platform_uid = $user_info['msg']['userCard']['userId'];
            //$platform_info = Platforms::getByid($platform_id);
            $platform_info = Platforms::getByid($platform_id);
            $flag = ['platform_user_id'=>$platform_uid];
            $data = [
                'cookie'=>$cookie,
                'platform_id'=>$platform_id,
                'nickname'=>$nickname,
                'user_id'=>$user_id,
                'platform'=>$platform_info->platform,
                'avatar'=>$avatar,
                'binded_time'=>date('Y-m-d H:i:s'),
                'status'=>1
            ];
            $account_info = PlatformAccounts::updateOrCreate($flag,$data);
        }
        curl_close($ch);

        return response()->success();
    }

    public static function getKuaishouQrcode($platform_id)
    {
        $client = new Client();

        $user_id = auth('api')->user()->id;//auth('api')->user()->id;

        //获取二维码
        $response = $client->request('POST','https://id.kuaishou.com/rest/c/infra/ks/qr/start',[
            'form_params' => [
                'sid' => 'kuaishou.live.web',
            ]
        ]);

        $body = $response->getBody()->getContents();
        $res = json_decode($body,true);
        $qrcodeInfo = [
            'qrLoginSignature' => $res['qrLoginSignature'],
            'qrLoginToken' => $res['qrLoginToken'],
            'imageData' => 'data:image/png;base64,' . $res['imageData'],
            'sid' => $res['sid'],
            'expireTime' => $res['expireTime']
        ];
        Redis::hMset('kuaishou_qrcode_info:'.$platform_id.':'.$user_id,['qrLoginSignature'=>$res['qrLoginSignature'],'qrLoginToken'=>$res['qrLoginToken'],'sid'=>$res['sid']]);
        return $res['imageData'];//$qrcodeInfo['imageData'];
    }

    public static function getKuaishouScanResult($request)
    {
        $platform_id = $request->platform_id;

        $user_id = auth('api')->user()->id;
        //$user_id =1;
        $client = new Client();

        $qrcode_info = Redis::hMGet('kuaishou_qrcode_info:'.$platform_id.':'.$user_id,['qrLoginSignature','qrLoginToken','sid']);
        $qrLoginSignature = $qrcode_info[0];
        $qrLoginToken = $qrcode_info[1];
        $sid = $qrcode_info[2];

        /*$platform = PlatformAccounts::where('user_id',$user_id)->where('platform_id',$platform_id)->first();
        if(!$platform){
            $platform = PlatformAccounts::create([
                'user_id' => $user_id,
                'platform_id' => $platform_id,
            ]);
        }*/

        //获取扫一扫结果
        $response = $client->request('POST','https://id.kuaishou.com/rest/c/infra/ks/qr/scanResult',[
            'form_params' => [
                'qrLoginToken' => $qrLoginToken,
                'qrLoginSignature' => $qrLoginSignature,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $res = json_decode($body,true);

        if(isset($res['result']) && $res['result'] ==1){
            $platform_info = Platforms::getByid($platform_id);
            $platform = $platform_info->platform;
            $avatar = $res['user']['headurl'];
            $nickname = $res['user']['user_name'];
            $status = 1;
            $platform_user_id = $res['user']['user_id'];
            //PlatformAccounts::updateOrCreate(['platform']);

            //$platform->save();

            //获取确认登录结果
            $response = $client->request('POST','https://id.kuaishou.com/rest/c/infra/ks/qr/acceptResult',[
                'form_params' => [
                    'qrLoginToken' => $qrLoginToken,
                    'qrLoginSignature' => $qrLoginSignature,
                    'sid' => $sid,
                ]
            ]);

            $body = $response->getBody()->getContents();
            $res = json_decode($body,true);

            $response = $client->request('GET','https://live.kuaishou.com',[]);
            $header = $response->getHeader('Set-Cookie');
            $cookie_str = static::getCookie($header);

            //$platform->cookie = $cookie_str;
            $cookie_final = $cookie_str;

            $cookie_arr = static::getBase($cookie_str);
            $cookieJar = CookieJar::fromArray($cookie_arr, 'id.kuaishou.com');

            $response = $client->request('POST','https://id.kuaishou.com/pass/kuaishou/login/qr/callback',[
                'form_params' => [
                    'qrToken' => $res['qrToken'],
                    'sid' => $sid,
                ],
                'cookies' => $cookieJar
            ]);
            $header = $response->getHeader('Set-Cookie');
            $body = $response->getBody()->getContents();
            $res = json_decode($body,true);
            $cookie_str = static::getCookie($header);

            $cookie_final .=  ';' . $cookie_str;
            $response = $client->request('POST','https://live.kuaishou.com/graphql',[
                'headers'=>['Cookie'=>$cookie_final],
                'json'=>[
                    'operationName'=>"UserLogin",
                    'variables'=>[
                        'userLoginInfo'=>[
                            'authToken'=>$res['kuaishou.live.web.at'],
                            'sid'=> "kuaishou.live.web"
                        ],

                    ],
                    'query'=>  "mutation UserLogin(\$userLoginInfo: FlatObject) {
  login(userLoginInfo: \$userLoginInfo) {
    result
    __typename
  }
}
"
                ]
            ]);
            $headers = $response->getHeader('Set-Cookie');
            $cookie_str2 = static::getCookie($headers);
            $cookie_final .= ';'.$cookie_str2;
            /*$platform->cookie .= ';'.$cookie_str2;
            $res = $platform->save();*/
            $old_platform_info = PlatformAccounts::where('platform_user_id',$platform_user_id)->where('user_id',$user_id)->first();
            $binded_time=date('Y-m-d H:i:s');
            if($old_platform_info) {
                $old_platform_info->platform = $platform_info->platform;
                $old_platform_info->avatar = $avatar;
                $old_platform_info->status = $status;
                $old_platform_info->nickname = $nickname;
                $old_platform_info->cookie = $cookie_final;
                $old_platform_info->binded_time = $binded_time;
                $old_platform_info->save();
            }else{
                $cookie = $cookie_final;
                PlatformAccounts::create(compact('platform','avatar','nickname','platform_user_id','status','user_id','platform_id','cookie','binded_time'));
            }
            if($res){
                return response()->success($platform);
            }else{
                return response()->error('NEED_REFRESH_QRCODE');
            }
        }else{
            return response()->error('NO_SCAN');
        }
    }

    public static function bindZhihu($request) {
        $platform_id = $request->input('platform_id');
        $stage = $request->input('stage');
        //$uid = 1;
        $uid = auth('api')->user()->id;
        if($stage == 1) {
            $client = new Client(['base_uri' => 'https://www.zhihu.com', 'timeout' => 2.0]);
            //$jar = new \GuzzleHttp\Cookie\CookieJar();
            $response = $client->request('GET', '/signin');
            $header = $response->getHeader('Set-Cookie');
            $cookie_str = self::extractCookie($header);
            $response_ = $client->request('POST', '/udid',
                ['headers' => [
                    'Cookie' => $cookie_str
                ]]
            );
            $header2 = $response_->getHeader('Set-Cookie');
            $cookie_str2 = self::extractCookie($header2);
            $cookie_str3 = $cookie_str . ';' . $cookie_str2;
            $response2 = $client->request('POST', '/api/v3/account/api/login/qrcode',
                ['headers' => [
                    'Cookie' => $cookie_str3
                ]]
            );
            $token_raw = json_decode($response2->getBody()->getContents(), true);
            $token = $token_raw['token'];
            $response3 = $client->request('GET', '/api/v3/account/api/login/qrcode/' . $token . '/image', ['headers' => [
                'Cookie' => $cookie_str3
            ]]);
            $contents = $response3->getBody()->getContents();
            file_put_contents(storage_path('qrcode.png'),$contents);
            header('Content-Type', 'image/png');
            Redis::hMset('qrcode_info:' . $platform_id . ':' . $uid, ['cookie' => $cookie_str3, 'token' => $token]);
            return $contents;
        }elseif ($stage == 2) {
            $client = new Client(['base_uri'=>'https://www.zhihu.com','timeout'=>2.0]);
            $qrcode_info = Redis::hMGet('qrcode_info:'.$platform_id.':'.$uid,['cookie','token']);

            $cookie_str = $qrcode_info[0];
            $token = $qrcode_info[1];
            $response1 = $client->request('GET','/api/v3/account/api/login/qrcode/'.$token.'/scan_info');
            $scan_info_raw = $response1->getBody()->getContents();
            //var_dump('response1:'.$scan_info_raw);
            if($scan_info_raw) {
                $scan_info = json_decode($scan_info_raw,true);
                if(isset($scan_info['status']) && $scan_info['status'] == 5) {
                    return response()->error('NEED_REFRESH_QRCODE');
                }
                if(isset($scan_info['user_id'])){
                    Redis::set('zhihu_profile:'.$platform_id.':'.$uid,serialize($scan_info));
                    $cookies_header = $response1->getHeader('Set-Cookie');
                    $cookie_str2 = self::extractCookie($cookies_header);
                    $cookie_str3 = $cookie_str.';'.$cookie_str2;
                    $response = $client->request('GET','/',['headers' => ['Cookie'=>$cookie_str3]]);
                    $res = $response->getBody()->getContents();
                    $home_html = HtmlDomParser::str_get_html($res);
                    //var_dump($res);
                    $profile_link_raw = $home_html->find('div[data-zop-usertoken]')[0]->getAttribute('data-zop-usertoken');
                    $profile_link_arr = json_decode($profile_link_raw,true);
                    $profile_link = $profile_link_arr['urlToken'];
                    $profile_link = 'https://www.zhihu.com/people/'.$profile_link;
                    $response_profile = $client->request('GET',$profile_link,['headers' => ['Cookie'=>$cookie_str3]]);
                    $profile_response = $response_profile->getBody()->getContents();
                    $profile_response_html = HtmlDomParser::str_get_html($profile_response);
                    $avatar_src = $profile_response_html->find('.UserAvatar')[0]->find('img')[0]->getAttribute('srcset');
                    $nickname = $profile_response_html->find('.ProfileHeader-name')[0]->innertext;

                    $platform_info = Platforms::getByid($platform_id);
                    $flag = ['nickname'=>$nickname];
                    $data = [
                        'cookie'=>$cookie_str,
                        'platform_id'=>$platform_id,
                        //'password'=>$password,
                        'uid'=>$uid,
                        'platform'=>$platform_info->platform,
                        'avatar'=>$avatar_src,
                        'binded_time'=>date('Y-m-d H:i:s'),
                        'status'=>1
                    ];
                    //var_dump($data);
                    $account_info = PlatformAccounts::updateOrCreate($flag,$data);
                    return $account_info;
                    //var_dump($res);
                }
                return response()->error('NEED_REFRESH_QRCODE');
            }
            return response()->error('NO_SCAN');
        }
    }

    public static function getZhihuScanResult($request) {
        $platform_id = $request->input('platform_id');
        $uid = auth('api')->user()->id;
        $client = new Client(['base_uri'=>'https://www.zhihu.com','timeout'=>2.0]);
        $qrcode_info = Redis::hMGet('qrcode_info:'.$platform_id.':'.$uid,['cookie','token']);
        //var_dump($qrcode_info);
        \Log::info('qrcode_info:'.$platform_id.':'.$uid);
        //\Log::info('qrcode_info:'.$platform_id.':'.$uid);
        $cookie_str = $qrcode_info[0];
        \Log::info('qrcode_info:qrcode:'.$cookie_str);
        $token = $qrcode_info[1];
        try {
            $response1 = $client->request('GET','/api/v3/account/api/login/qrcode/'.$token.'/scan_info');
            $scan_info_raw = $response1->getBody()->getContents();
            \Log::info('response1:'.$scan_info_raw);

            if($scan_info_raw) {
                $scan_info = json_decode($scan_info_raw,true);
                if(isset($scan_info['status']) && $scan_info['status'] == 5) {
                    return response()->error('NEED_REFRESH_QRCODE');
                }
                if(isset($scan_info['status']) && $scan_info['status'] == 6) {
                    return response()->error('USER_CANCL_SCAN');
                }
                if(isset($scan_info['status'])&& in_array($scan_info['status'],[0,'0','1',1])){
                    return response()->error('NO_SCAN');
                }
                if(isset($scan_info['user_id'])){
                    Redis::set('zhihu_profile:'.$platform_id.':'.$uid,serialize($scan_info));
                    $cookies_header = $response1->getHeader('Set-Cookie');
                    $cookie_str2 = self::extractCookie($cookies_header);
                    $cookie_str3 = $cookie_str.';'.$cookie_str2;
                    $response = $client->request('GET','/api/v4/me?include=ad_type,available_message_types,default_notifications_count,follow_notifications_count,vote_thank_notifications_count,messages_count,draft_count,account_status,email,is_bind_phone,following_question_count,is_force_renamed,renamed_fullname',['headers' => ['Cookie'=>$cookie_str3]]);
                    $res = $response->getBody()->getContents();
                    \Log::info('cookie:------'.$cookie_str);
                    //$home_html = HtmlDomParser::str_get_html($res);
                    //var_dump($res);
                    //$profile_link_raw = $home_html->find('div[data-zop-usertoken]')[0]->getAttribute('data-zop-usertoken');
                    //var_dump($res);
                    $me_info = json_decode($res,true);
                    //$profile_link_arr = json_decode($profile_link_raw,true);
                    /*$profile_link = $profile_link_arr['urlToken'];
                    $profile_link = 'https://www.zhihu.com/people/'.$profile_link;
                    $response_profile = $client->request('GET',$profile_link,['headers' => ['Cookie'=>$cookie_str3]]);
                    $profile_response = $response_profile->getBody()->getContents();
                    $profile_response_html = HtmlDomParser::str_get_html($profile_response);
                    $avatar_src = $profile_response_html->find('.UserAvatar')[0]->find('img')[0]->getAttribute('srcset');
                    $nickname = $profile_response_html->find('.ProfileHeader-name')[0]->innertext;*/
                    $nickname = $me_info['name'];
                    $avatar_src= str_replace('{size}','xll',$me_info['avatar_url_template']);
                    $platform_info = Platforms::getByid($platform_id);
                    $flag = ['nickname'=>$nickname];
                    $data = [
                        'cookie'=>$cookie_str3,
                        'platform_id'=>$platform_id,
                        //'password'=>$password,
                        'user_id'=>$uid,
                        'platform'=>$platform_info->platform,
                        'avatar'=>$avatar_src,
                        'binded_time'=>date('Y-m-d H:i:s'),
                        'status'=>1
                    ];
                    //var_dump($data);
                    $account_info = PlatformAccounts::updateOrCreate($flag,$data);
                    return response()->success($account_info);
                }

                return response()->error('NEED_REFRESH_QRCODE');
            }
        }catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('NEED_REFRESH_QRCODE');
        }

        return response()->error('NO_SCAN');
    }

    public static function getCookie($header)
    {
        foreach($header as $key=>$cookie) {
            $each_cookie = explode(';',$cookie);
            $cookie_filter_arr[] = $each_cookie[0];
        }
        if(!empty($cookie_filter_arr)){
            $cookie_str = implode(';',$cookie_filter_arr);
            return $cookie_str;
        }else{
            return '';
        }
    }

    public static function getBase($cookie_str)
    {
        $cookie_str = explode(';',$cookie_str);
        $arr = [];
        foreach($cookie_str as $v){
            $temp = explode('=',$v);
            $arr[$temp[0]] = $temp[1];
        }
        return $arr;
    }

    public static function bindBaiJh($request) {
        $user_id = auth('api')->user()->id;
        $cookie = $request->input('cookie','');
        if(empty($cookie)) {
            return response()->error('PARAM_EMPTY');
        }
        $client = new Client();
        $user_info_response = $client->request('GET','https://baijiahao.baidu.com/builder/app/appinfo?',['headers'=>['Sec-Fetch-Mode'=> 'cors',
            'Sec-Fetch-Site'=> 'same-origin','Cookie'=>$cookie]]);
        $token = $user_info_response->getHeader('Token')[0];
        $response_in = $user_info_response->getBody()->getContents();
        $user_info = json_decode($response_in,true);
        if($user_info['errno']==0 ) {
            $nick_name = $user_info['data']['user']['name'];
            $avatar = $user_info['data']['user']['avatar'];
            $platform_user_id = $user_info['data']['user']['userid'];
            $extra_info = json_encode($user_info['data']['user']);
            $platform_id = $request->input('platform_id','');
            $platform_info = Platforms::getByid($platform_id);
            $sign = ['user_id'=>$user_id,'platform_user_id'=>$platform_user_id];
            $up = ['nickname'=>$nick_name,'avatar'=>$avatar,'cookie'=>$cookie,'platform_id'=>$platform_id,'platform'=>$platform_info->platform,'extra_info'=>$extra_info,'token'=>$token,'binded_time'=>date('Y-m-d H:i:s'),'status'=>1];
            #var_dump($up);
            $account_info = PlatformAccounts::updateOrCreate($sign,$up);
            return response()->success($account_info);
        }
        return response()->error('BIND_ERROR');
    }

    /**
     * bindBaiHaokan.
     *
     * @param $user_name
     * @param $password
     * @param $platform_id
     * @param $cookie
     * @return mixed
     * @since 2019年12月25日
     */
    public static function bindBaiHaokan($user_name, $password, $platform_id, $cookie)
    {
        $user_id = auth('api')->user()->id;
        $client = new Client();
        $user_info_response = $client->request('GET','https://baijiahao.baidu.com/builder/app/appinfo?',['headers'=>['Sec-Fetch-Mode'=> 'cors',
            'Sec-Fetch-Site'=> 'same-origin','Cookie'=>$cookie]]);
        $token = $user_info_response->getHeader('Token')[0];
        $response_in = $user_info_response->getBody()->getContents();
        $user_info = json_decode($response_in,true);
        if($user_info['errno']==0 ) {
            $nick_name = $user_info['data']['user']['name'];
            $avatar = $user_info['data']['user']['avatar'];
            $platform_user_id = $user_info['data']['user']['userid'];
            $extra_info = $user_info['data']['user'];
            $extra_info = json_encode($extra_info);
            $platform_info = Platforms::getByid($platform_id);
            $sign = ['user_id'=>$user_id,'platform_user_id'=>$platform_user_id];
            $up = ['nickname'=>$nick_name,'avatar'=>$avatar,'cookie'=>$cookie,'platform_id'=>$platform_id,'platform'=>$platform_info->platform,'extra_info'=>$extra_info,'token'=>$token,'status'=>1];

            $up['openid'] = $user_name;
            $up['token'] = $password;

            #var_dump($up);
            $account_info = PlatformAccounts::updateOrCreate($sign,$up);
            return response()->success($account_info);
        }
        return response()->error('BIND_ERROR');
    }

    public static function bindDayuhao($request) {
        $cookie = $request->input('cookie','');
        $user_id = auth('api')->user()->id;
        if(empty($cookie)) {
            return response()->error('PARAM_EMPTY');
        }
        $client = new Client();
        $user_info_response = $client->request('GET','https://mp.dayu.com/dashboard/index',[
            'headers'=>['Cookie'=>$cookie]
        ]);
        try{
            $response_in = $user_info_response->getBody()->getContents();
            /*$match = preg_match('/\"weMediaName\":\"(.*)\"/',$response_in,$nickname_matches);
            $match = preg_match('/\"yt_id\":\"(.*)\"/',$response_in,$platform_id_matches);
            $match = preg_match('/\"wmAvator\":\"(.*)\"/',$response_in,$avatar_matches);
            */
            $match = preg_match('/var[\s]*globalConfig[\s]*=[\s]*(\{.+\})/',$response_in,$matches);
            $glob_info = json_decode($matches[1],true);
            $nickname = $glob_info['weMediaName'];
            $platform_user_id = $glob_info['yt_id'];
            $avatar = $glob_info['wmAvator'];
            $nick_name = $nickname;
            $extra_info = $matches[1];
            $platform_id = $request->input('platform_id','');
            $platform_info = Platforms::getByid($platform_id);
            $sign = ['user_id'=>$user_id,'platform_user_id'=>$platform_user_id];
            $up = ['nickname'=>$nick_name,'avatar'=>$avatar,'cookie'=>$cookie,'platform_id'=>$platform_id,'platform'=>$platform_info->platform,'extra_info'=>$extra_info,'binded_time'=>date('Y-m-d H:i:s'),'status'=>1];
            $account_info = PlatformAccounts::updateOrCreate($sign,$up);
            return response()->success($account_info);
        }catch (\Exception $e){
            \Log::error($e->getMessage());
            return response()->error('BIND_ERROR');
        }

    }

    public static function bindKuaichuan($request) {
        $cookie = $request->input('cookie','');
        $user_id = auth('api')->user()->id;
        if(empty($cookie)) {
            return response()->error('PARAM_EMPTY');
        }
        $client = new Client();
        $user_info_response = $client->request('GET','http://kuaichuan.360.cn/user/detail',[
            'headers'=>['Cookie'=>$cookie,'Referer'=>'http://kuaichuan.360.cn/','User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36']
        ]);

        $response_in = $user_info_response->getBody()->getContents();
        \Log::info('jj:'.$response_in);
        $res_arr = json_decode($response_in,true);
        if($res_arr['errno']==0) {
            $nick_name = $res_arr['data']['user_info']['name'];
            $avatar = $res_arr['data']['user_info']['image'];
            $platform_user_id = $res_arr['data']['user_info']['id'];
            $extra_info = json_encode($res_arr['data']['user_info']);
            $platform_id = $request->input('platform_id','');
            $platform_info = Platforms::getByid($platform_id);
            $sign = ['user_id'=>$user_id,'platform_user_id'=>$platform_user_id];
            $up = ['nickname'=>$nick_name,'avatar'=>$avatar,'cookie'=>$cookie,'platform_id'=>$platform_id,'platform'=>$platform_info->platform,'extra_info'=>$extra_info,'binded_time'=>date('Y-m-d H:i:s'),'status'=>1];
            $account_info = PlatformAccounts::updateOrCreate($sign,$up);
            return response()->success($account_info);
        }
        return response()->error('BIND_ERROR');
    }

    public static function bindToutiao($request) {
        $cookie = $request->input('cookie','');
        $user_id = auth('api')->user()->id;
        if(empty($cookie)) {
            return response()->error('PARAM_EMPTY');
        }
        $client = new Client();
        $response = $client->request('GET','https://mp.toutiao.com/mp/agw/media/get_media_info',[
            'headers'=>['Cookie'=>$cookie]
        ]);
        $res = $response->getBody()->getContents();
        $res_arr = json_decode($res,true);
        if($res_arr['code'] == 0) {
            $user_info = $res_arr['data']['user'];
            $nick_name = $user_info['screen_name'];
            $avatar = $user_info['https_avatar_url'];
            $platform_user_id = (int)$user_info['id'];
            $sign = ['user_id'=>(int)$user_id,'platform_user_id'=>(int)$platform_user_id];
            $platform_id = $request->input('platform_id','');
            $platform_info = Platforms::getByid($platform_id);
            $account_info = PlatformAccounts::where('platform_user_id',$platform_user_id)->where('user_id',$user_id)->first();
            \Log::info('jinritoutiao:'.json_encode($account_info));
            if($account_info) {
                $account_info->nickname = $nick_name;
                $account_info->cookie = $cookie;
                $account_info->platform_id = $platform_id;
                $account_info->platform = $platform_info->platform;
                $account_info->binded_time=date('Y-m-d H:i:s');
                $account_info->status = 1;
                $account_info->save();
            }else{
                $data = ['nickname'=>$nick_name,'avatar'=>$avatar,'cookie'=>$cookie,'platform_id'=>$platform_id,'platform'=>$platform_info->platform,'binded_time'=>date('Y-m-d H:i:s'),'status'=>1,'user_id'=>$user_id,'platform_user_id'=>$platform_user_id];
                $account_info = PlatformAccounts::create($data);
            }
            return response()->success($account_info);
        }
        return response()->error('BIND_ERROR');
    }

    public static function bindSouhu($request) {
        $cookie = $request->input('cookie','');
        $user_id = auth('api')->user()->id;
        if(empty($cookie)) {
            return response()->error('PARAM_EMPTY');
        }
        $client = new Client();
        $response = $client->request('GET','https://mp.sohu.com/mp-accounts/accounts/list?_=1577370253777',[
            'headers'=>['Cookie'=>$cookie]
        ]);
        $res = $response->getBody()->getContents();
        \Log::info($res);
        $res_arr = json_decode($res,true);
        if(isset($res_arr['totalAccount']) && $res_arr['totalAccount']>0) {
            $user_info = $res_arr['list'][0]['accountslist'][0];
            $platform_user_id = $user_info['id'];
            $nick_name = $user_info['nickName'];
            $avatar = $user_info['avatar'];
            $extra_info = '';
            $response2 = $client->request('GET','https://mp.sohu.com/mpbp/bp/account/info?accountId='.$platform_user_id.'&_=1577370253780',['headers'=>['Cookie'=>$cookie]]);
            $res2 = $response2->getBody()->getContents();
            \Log::info('$res2:'.$res2);
            $res2_arr = json_decode($res2,true);
            if($res2_arr && $res2_arr['code']==2000000) {
                $nick_name = $res2_arr['data']['nickName'];
                $avatar = $res2_arr['data']['avatar'];
                $extra_info = json_encode($res2_arr['data']);
            }
            $sign = ['user_id'=>$user_id,'platform_user_id'=>$platform_user_id];
            $platform_id = $request->input('platform_id','');
            $platform_info = Platforms::getByid($platform_id);
            $up = ['nickname'=>$nick_name,'avatar'=>$avatar,'cookie'=>$cookie,'platform_id'=>$platform_id,'extra_info'=>$extra_info,'platform'=>$platform_info->platform,'binded_time'=>date('Y-m-d H:i:s'),'status'=>1];
            $account_info = PlatformAccounts::updateOrCreate($sign,$up);
            return response()->success($account_info);
        }
        return response()->error('BIND_ERROR');
    }

    public static function extractCookie($header) {
        $cookie_filter_arr = [];
        foreach($header as $key=>$cookie) {
            $each_cookie = explode(';',$cookie);
            $cookie_filter_arr[] = $each_cookie[0];
        }
        $cookie_str = implode(';',$cookie_filter_arr);
        return $cookie_str;
    }
}
