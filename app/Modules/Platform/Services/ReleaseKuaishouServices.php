<?php
namespace App\Modules\Platform\Services;

use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Publishes;
use App\Modules\Platform\Models\UploadVideo;
use App\Modules\Platform\Models\UploadVideoCover;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Facades\Storage;

class ReleaseKuaishouServices
{
    public static function release($accountInfo,$videoInfo,$formData,$cover_info,$publishes)
    {
        $user_id = $accountInfo->user_id;//auth('api')->user();

        $video_id = $videoInfo->video_id ?: 1;
        //$video_cover_id = $request->video_cover_id ?: 1;
        $client = new Client();

        $video =$videoInfo;
        //$videoFile = Storage::disk('video')->get($video->slice . '/' . $video->file_name);

        //$videoCover = UploadVideoCover::find($video_cover_id);
        //$videoCoverFile = Storage::disk('videoCover')->get($videoCover->slice . '/' . $videoCover->file_name);
//        $cookie_arr = [
//            //'didv' => '1575463349000',
//            'kuaishou.live.web_st' => 'ChRrdWFpc2hvdS5saXZlLndlYi5zdBKgAfghOJQ8sNvE9BUxa0uDVqfQmb9QYcRYD0v9TsykBw4aNkAOTFqseqMOQkpgpEUbRM2ukL-yVt2eVBkhSXhnXRoGwqdOw_nngtSDaHzQq-t6np2ykDPRenNGLdKRN36Wrtwasb602S5igXl2mUod--5hbTz27sDsJlpmki4hYjlZpbvLSCd0b9zZUXN-2X9p3K2m-fEg4YrC9Xy8d770DbIaEk2hY_LIikBot7IUVtJ3ydB6KCIguMeF_t1jB_tnSkW1bvktk34vV21JCcTAiI28CMxbksQoBTAB',
//            'clientid' => '3',
//            'userId' => '1586492813',
//            'did' => 'web_b628d545f2ce499594834971a40d5036',
//            //'kuaishou.live.web_ph' => '64586e9a857b5b9fb2a58f63e608e2be7427'
//        ];
        $cookie = $accountInfo['cookie'];

        /*$cookie_arr = static::CookieStrToArray($cookie);
        if(isset($cookie_arr['passToken'])){
            $cookie_arr['kuaishou.live.web_st'] = $cookie_arr['passToken'];
            unset($cookie_arr['passToken']);
        }*/

        //$cookieJar = CookieJar::fromArray($cookie_arr, 'live.kuaishou.com');

        //上传封面
        $response = $client->request('POST','https://live.kuaishou.com/rest/wd/photo/upload/cover',[
            'headers' => ['Cookie'=>$cookie],
            'multipart' => [
                [
                    'name'     => 'cover',
                    'contents' => fopen(public_path().$cover_info->relative_path,'r'),
                    'filename' => $cover_info->file_name
                ],
            ]
        ]);
        //"{"result":1,"coverKey":"1575466626520_BIIqkqhGhdAD8rzZ8camVQJd1","host-name":"bjpg-rs8702.yz02"}"
        $content = $response->getBody()->getContents();
        \Log::info('content1:'.$content);
        $content = json_decode($content,true);
        $coverKey = $content['coverKey'];

        //上传视频
        $response = $client->request('POST','https://live.kuaishou.com/rest/wd/photo/upload',[
            'headers' => ['Cookie'=>$cookie],
            'multipart' => [
                [
                    'name'     => 'video',
                    'contents' => fopen(public_path().$video->relative_path,'r'),
                    'filename' => $video->file_name
                ],
                [
                    'name'     => 'caption', //作品描述
                    'contents' => $formData['caption'],
                ],
                [
                    'name'     => 'status', //公开 此刻 私密
                    'contents' => $formData['status'], //PUBLIC SNAP_SHOW
                ],
                [
                    'name'     => 'coverKey',
                    'contents' => $coverKey,
                ],
            ]
        ]);
        $content_raw = $response->getBody()->getContents();
        \Log::info('content2:'.$content_raw);
        $content = json_decode($content_raw,true);
        if(isset($content['result'])&&$content['result']==1) {
            $publishes->content_id = $content['photo_id'];
            $publishes->contents_info = json_encode([
                [
                    'name'     => 'video',
                    'contents' => fopen(public_path().$video->relative_path,'r'),
                    'filename' => $video->file_name
                ],
                [
                    'name'     => 'caption', //作品描述
                    'contents' => $formData['caption'],
                ],
                [
                    'name'     => 'status', //公开 此刻 私密
                    'contents' => $formData['status'], //PUBLIC SNAP_SHOW
                ],
                [
                    'name'     => 'coverKey',
                    'contents' => $coverKey,
                ],
            ],JSON_UNESCAPED_UNICODE);
            $publishes->response = $content_raw;
            $publishes->status = 'REVIEW_SUCCESS';
            $publishes->save();
            /*$publish_data = [
                'title'=>$formData['title'],
                'platform_id'=>$accountInfo->platform_id,
                'platform_account_id'=>$accountInfo->id,
                'user_id'=>$accountInfo->user_id,
                'content_id'=>$content['photo_id'],
                'contents_info'=>json_encode([
                    [
                        'name'     => 'video',
                        'contents' => fopen(public_path().$video->relative_path,'r'),
                        'filename' => $video->file_name
                    ],
                    [
                        'name'     => 'caption', //作品描述
                        'contents' => $formData['caption'],
                    ],
                    [
                        'name'     => 'status', //公开 此刻 私密
                        'contents' => $formData['status'], //PUBLIC SNAP_SHOW
                    ],
                    [
                        'name'     => 'coverKey',
                        'contents' => $coverKey,
                    ],
                ],JSON_UNESCAPED_UNICODE),
                'response'=>$content_raw,
                'status'=>'REVIEW_SUCCESS'
            ];
            Publishes::create($publish_data);*/
            return response()->success();
        }else{
            $publishes->content_id = $content['photo_id'];
            $publishes->contents_info = json_encode([
                [
                    'name'     => 'video',
                    'contents' => fopen(public_path().$video->relative_path,'r'),
                    'filename' => $video->file_name
                ],
                [
                    'name'     => 'caption', //作品描述
                    'contents' => $formData['caption'],
                ],
                [
                    'name'     => 'status', //公开 此刻 私密
                    'contents' => $formData['status'], //PUBLIC SNAP_SHOW
                ],
                [
                    'name'     => 'coverKey',
                    'contents' => $coverKey,
                ],
            ],JSON_UNESCAPED_UNICODE);
            $publishes->response = $response;
            $publishes->status = 'UPLOAD_FAILED';
            $publishes->reason = '未知原因:'.$content_raw;
            $publishes->save();
            /*$publish_data = [
                'title'=>$formData['title'],
                'platform_id'=>$accountInfo->platform_id,
                'platform_account_id'=>$accountInfo->id,
                'user_id'=>$accountInfo->user_id,
                'content_id'=>$content['photo_id'],
                'contents_info'=>json_encode([
                    [
                        'name'     => 'video',
                        'contents' => fopen(public_path().$video->relative_path,'r'),
                        'filename' => $video->file_name
                    ],
                    [
                        'name'     => 'caption', //作品描述
                        'contents' => $formData['caption'],
                    ],
                    [
                        'name'     => 'status', //公开 此刻 私密
                        'contents' => $formData['status'], //PUBLIC SNAP_SHOW
                    ],
                    [
                        'name'     => 'coverKey',
                        'contents' => $coverKey,
                    ],
                ],JSON_UNESCAPED_UNICODE),
                'response'=>$content_raw,
                'status'=>'UPLOAD_FAILED',
                'reason'=>'未知原因:'.$content_raw
            ];
            Publishes::create($publish_data);*/
        }

    }
    public static function CookieStrToArray($cookieStr)
    {
        $cookie = explode(';',$cookieStr);
        $arr = [];
        foreach ($cookie as $v){
            $tmp = explode('=',$v);
            $arr[trim($tmp[0])] = $tmp[1];
        }
        return $arr;
    }
}