<?php
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */


namespace App\Modules\Platform\Services;


class ReleaseDayuServices
{
    /**
     * 上传好看视频内容.
     *
     * @param $video
     * @param $platformAccount
     * @param $formData
     * @return array|bool
     * @author 肖俊明<2284876299@qq.com>
     * @since 2019年12月28日
     */
    public static function pushVideo($video, $platformAccount, $formData)
    {
        #$is_original = 0;
        $videoId = 'b04d479695ade12e';
        $videoFileName = '未命名.mp4';
        $title = $formData['title'];
        $sub_title = $title;
        $cover_images = $formData['cover'];
        $article_type = 1;
        $category = '';
        $curDaySubmit= false;
        $customize_tags = [];
        $is_original =  0;
        $customize_tags = ["测试","测试赛"];


        $app_id = $platformAccount['openid'];
        $app_token = $platformAccount['token'];
        $video_url = 'http://'.$video->relative_path;


        return self::publishHaoKan($app_id, $app_token, $title, $video_url, $cover_images,
            $is_original, $use_auto_cover=1, $tag = '');
    }


    public static function publishDaYu($access_token, $openid, $title,$media,$cover_images, $tags, $cat, $desc,
                                $is_original = 0)
    {
        $client = new Client();

        $form_params = [
            'videoFileName' => $media,
            'title' => $title,
            'sub_title' => $desc,
            'tags' => $tags,
            'cat' => $cat,
            'cover_images' => $cover_images,
            'is_original' => $is_original,
            ''
        ];
        return response()->error('PUBLISH_FAILED');
    }
}
