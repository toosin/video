<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/1/23
 * Time: 9:51
 */

namespace App\Modules\Platform\Services;

use App\Modules\Platform\Models\PlatformAccounts;
use GuzzleHttp\Client;

class PlatformAccountServices
{

    public static function getSougouDashbord($cookie) {
        $client = new Client();
        $url = 'http://mp.sogou.com/api/if-login?status=81';
        $response = $client->request('GET',$url,[
            'headers' => [
                'Cookie' => $cookie,
            ]
        ]);
        $status = $response->getStatusCode();
        $body = $response->getBody()->getContents();
        return json_decode($body,true);
    }

    public static function getBindedAccountsList($user_id,$params,$paginate) {
        return PlatformAccounts::getBindedAccountsList($user_id,$params,$paginate);
    }
}