<?php
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */


namespace App\Modules\Platform\Services;

use App\Modules\Platform\Models\UploadVideo;
use App\Modules\Publish\Services\ArticlePublishServices;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use App\Modules\Platform\Models\PlatformAccounts;

class ReleaseQqServices
{

    public static function pushVideo($video,$platformAccount,$formData)
    {
        $accessToken = self::getQQAccessToken($platformAccount['user_id']);
        if (empty($accessToken)) {
            return [];
        }
        $title = $formData['title'];
        $desc = $title;
        $apply = 0;
        $media = public_path(str_replace(env('STATIC_DOMAIN'), '',$video->relative_path));

        $tags = '歌曲 音乐 视频音乐';
        $cat = 2500;
        $openid = $platformAccount['openid'];
        #$openid = '1637a9dbd3b03faf5c1102f9dd758c7a';
        return self::publishOmQqVideo($accessToken, $openid, $title, $media, $tags, $cat, $desc, $apply);
    }


    /**
     * getQQAccessToken.
     *
     * @param $accountId
     * @return bool
     * @author 肖俊明<2284876299@qq.com>
     * @since 2019年12月29日
     */
    public static function getQQAccessToken($accountId)
    {
        $appid = '1c805ef8c87d1d46e1375d08728fde77';
        $refresh_token = Redis::hGet('qq_om_access_account_token_info:'.$accountId,'refresh_token');
        $url = 'https://auth.om.qq.com/omoauth2/refreshtoken?grant_type=refreshtoken&client_id='.$appid.'&refresh_token='.$refresh_token;
        $client = new Client();
        $response = $client->request('POST',$url);
        $res = $response->getBody()->getContents();
        $res = json_decode($res,true);
        if($res['code']==0) {
            $access_token = $res['data']['access_token'];
            $refresh_token = $res['data']['refresh_token'];
            Redis::hMSet('qq_om_access_account_token_info:'.$accountId,['access_token'=>$access_token,'refresh_token'=>$refresh_token]);
            return $access_token;
        }
        return false;
    }

    /**
     * 发布qq视频.
     *
     * @param $access_token
     * @param $openid
     * @param $title
     * @param $media
     * @param $tags
     * @param $cat
     * @param $desc
     * @param int $apply
     * @return array|bool|mixed
     * @author 肖俊明<2284876299@qq.com>
     * @since 2019年12月25日
     */
    private static function publishOmQqVideo($access_token, $openid, $title, $media, $tags, $cat, $desc,
                                     $apply = 0)
    {
        $md5Value = md5_file($media);

        $client = new Client();
        $url = 'http://api.om.qq.com/articlev2/authpubvid';

        $multipart = [
            [
                'name' => 'access_token',
                'contents' => $access_token
            ],
            [
                'name' => 'openid',
                'contents' => $openid
            ],
            [
                'name' => 'title',
                'contents' => $title
            ],
            [
                'name' => 'media',
                'contents' => fopen($media, "r")
            ],
            [
                'name' => 'tags',
                'contents' => $tags
            ],
            [
                'name' => 'cat',
                'contents' => $cat
            ],
            [
                'name' => 'desc',
                'contents' => $desc
            ],
            [
                'name' => 'md5',
                'contents' => $md5Value
            ],
            [
                'name' => 'apply',
                'contents' => $apply
            ],

        ];
        $options['multipart'] = $multipart;
        try {
            $response = $client->request('POST', $url, $options);
        } catch (\Exception $exception) {
            #\Log::error($exception->getMessage());
            #throw new \Exception($exception->getMessage());
        }
        $result = json_decode($response->getBody()->getContents(),true);
        \Log::error($result);
        //$ret = [];
        if(is_array($result) && (int)$result['code'] == 0) {
            return $result['data'];
        }else{
            return $result;
        }
        return [];
    }
}
