<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/12
 * Time: 16:49
 */
namespace App\Modules\Platform\Models;
use Illuminate\Database\Eloquent\Model;

class ArticlePublish extends Model {
    protected $table = 'platform_accounts';

    protected $fillable = ['id','user_id','platform','platform_id','account','password','cookie','avatar','nickname','status','platform_user_id'];

    public static function getBindedAccountsList($user_id,$params) {
        $obj = self::where('platform_accounts.status','<',2)
            ->where('platform_accounts.user_id',$user_id)
            ->join('platforms','platforms.id','=','platform_accounts.platform_id')
            ->select([
                'platform_accounts.id',
                'platform_accounts.platform_id',
                'platform_accounts.nickname',
                'platform_accounts.avatar',
                \DB::raw('platforms.icon as platform_icon'),
                \DB::raw('platforms.desc as platform_name'),
                'platform_accounts.status',
                'platforms.extral_fields'
            ]);
        if(isset($params['type'])&&$params['type']) {
            $obj->where('platforms.type',$params['type']);
        }
        return $obj->paginate(30);
    }

    public static function getById($id) {
        return self::where('id',$id)->where('status','<',2)->first();
    }
}