<?php
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */
namespace App\Modules\Publish\Services;

use App\Modules\Platform\Models\Publishes;
use GuzzleHttp\Client;
use OSS\OssClient;
use Sunra\PhpSimple\HtmlDomParser;

class VideoPublishServices{


    /**
     * 发布好看视频.
     *
     * @param $app_id
     * @param $app_token
     * @param $title
     * @param $video_url
     * @param string $cover_images
     * @param int $is_original
     * @param int $use_auto_cover
     * @param string $tag
     * @return array|bool
     * @since 2019年12月25日
     */
    public static function publishHaokan_bak($app_id, $app_token, $title, $video_url, $cover_images= '',
                                  $is_original = 0, $use_auto_cover=1, $tag = '')
    {

        $args = [
            'app_id' => $app_id,
            'app_token' => $app_token,
            'title' => $title,
            'video_url' => $video_url,
            'cover_images' => $cover_images,
            'is_original' => $is_original,
            'use_auto_cover' => $use_auto_cover,
            'tag' => $tag
        ];
        $client = new Client();
        $url = 'http://baijiahao.baidu.com/builderinner/open/resource/video/publish';
        $options['form_params'] = $args;
        try {
            $response = $client->request('POST', $url, $options);
        } catch (\Exception $exception) {
            return false;
        }
        $result = json_decode($response->getBody()->getContents(),true);
        //$ret = [];
        if(is_array($result) && (int)$result['errno'] == 0) {
            return $result['data'];
        }
        return [];
    }


    /**
     * 发布qq视频.
     *
     * @param $access_token
     * @param $openid
     * @param $title
     * @param $media
     * @param $tags
     * @param $cat
     * @param $desc
     * @param int $apply
     * @return array|bool|mixed
     * @author 肖俊明<2284876299@qq.com>
     * @since 2019年12月25日
     */
    public function publishOmQqVideo($access_token, $openid, $title, $media, $tags, $cat, $desc,
                                     $apply = 0)
    {
        $md5Value = md5_file($media);
        $args = [
            'access_token' => $access_token,
            'openid' => $openid,
            'title' => $title,
            'media' => $media,
            'tags' => $tags,
            'cat' => $cat,
            'desc' => $desc,
            'md5' => $md5Value,
            'apply' => $apply
        ];
        $client = new Client();
        $url = 'http://baijiahao.baidu.com/builderinner/open/resource/video/publish';
        $options['form_params'] = $args;
        try {
            $response = $client->request('POST', $url, $options);
        } catch (\Exception $exception) {
            return false;
        }
        $result = json_decode($response->getBody()->getContents(),true);
        //$ret = [];
        if(is_array($result) && (int)$result['code'] == 0) {
            return $result['data'];
        }
        return [];

    }

    public static function extractHaokanCookie($getCookie,$oldCookie) {
        if($getCookie){
            $oldCookie = explode(';',$oldCookie);
            foreach($getCookie as $val){
                $each = explode(';',$val);
                if(strstr($each[0],'bjhStoken=')){
                    foreach ($oldCookie as $k=>$v){
                        if(strstr($v,'bjhStoken=')){
                            $oldCookie[$k] = $each[0];
                        }
                    }
                }
                if(strstr($each[0],'devStoken=')){
                    foreach ($oldCookie as $k=>$v){
                        if(strstr($v,'bjhStoken=')){
                            $oldCookie[$k] = $each[0];
                        }
                    }
                }
            }
            return implode(';',$oldCookie);
        }else{
            return $oldCookie;
        }
    }

    public static function extraCookie($header){
        $cookie_filter_arr = [];
        foreach($header as $key=>$cookie) {
            $each_cookie = explode(';',$cookie);
            $cookie_filter_arr[] = $each_cookie[0];
        }
        $cookie_str = implode(';',$cookie_filter_arr);
        return $cookie_str;
    }


    public static function publishYouku($accountInfo,$videoInfo,$formData,$publishes){
        $client = new Client();
        $videoSize = filesize(public_path().$videoInfo->relative_path);
        $response = $client->request('POST',"https://mp.youku.com/video-upload/create.json",[
            'verify' => false,
            'headers'=>[
                'cookie'=>$accountInfo->cookie
            ],
            'form_params'=>[
                'fileSize'=>$videoSize,
                'fileName'=>$videoInfo->file_name,
                'fileType'=>'video'
            ]
        ]);
        $cookie = $accountInfo->cookie.';'.self::extraCookie($response->getHeader('Set-Cookie'));
        $videoUpload = json_decode($response->getBody()->getContents(),true);
        \Log::info('----获取优酷视频上传信息--'.json_encode($videoUpload)."\n\n");
        \Log::info('----securityToken--'.$videoUpload['data']['securityToken']."\n\n");
        if($videoUpload['success'] != 'true'){
            exit(json_encode(['code'=>110004,'msg'=>'cookie已过期，请重新扫码登陆！']));
        }
        try{
            $ossClient = new OssClient($videoUpload['data']['tempAccessId'], $videoUpload['data']['tempAccessSecret'], $videoUpload['data']['endpoint'], false, $videoUpload['data']['securityToken']);
            $res = $ossClient->uploadFile($videoUpload['data']['ossBucket'], $videoUpload['data']['ossObject'],public_path().$videoInfo->relative_path);
            \Log::info('----获取优酷视频上传信息2--'.json_encode($res)."\n\n==================================");
        }catch (\Exception $e){
            \Log::info('----获取优酷视频上传信息2--'.$e->getMessage()."\n\n==================================");
            //return response()->error('VIDEO_UPLOAD_INFO_ERROR');
            return response()->json();
        }
        $sessionId = '9PU2'.$videoUpload['data']['vid'].time().rand(1000000000,9999999999);
        $params = [
            'sessionId'=>$sessionId,
            'share'=>0,
            'syncTudou'=>0,
            'bullet'=>0,
            'publishtime'=>date('Y-m-d H:i:s',time()),
            'title'=>$formData['title'],
            'description'=>'',
            'privacy'=>'anybody',
            'size'=>4.22,
            'uploadToken'=>$videoUpload['data']['uploadToken'],
            'vid'=>$videoUpload['data']['vid'],
            'encodeTime'=>0
        ];
        //$url = http_build_query($params);
        $response = $client->request('POST',"https://mp.youku.com/video-upload/save.json",[
            'headers'=>[
                ':authority'=>'mp.youku.com',
                ':method'=>'GET',
                ':scheme'=>'https',
                'cookie'=>$cookie,
                'user-agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
                'referer'=>'https://mp.youku.com/upload',
                'pragma'=>'no-cache'
            ],
            'form_params'=>$params
        ]);
        $save = json_decode((string) $response->getBody()->getContents(),true);
        \Log::info('----优酷视频保存视频信息--'.json_encode($save,JSON_UNESCAPED_UNICODE)."\n\n==================================");
        if($save['success'] != 'true'){
            return response()->error('VIDEO_UPLOAD_INFO_ERROR');
        }
        $response = $client->request('GET',"https://mp.youku.com/video-upload/complete.json?uploadToken={$videoUpload['data']['uploadToken']}&client=html5&sessionId={$sessionId}",[
            'headers'=>[
                ':authority'=>'mp.youku.com',
                ':method'=>'GET',
                ':scheme'=>'https',
                'cookie'=>$cookie,
                'user-agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
                'referer'=>'https://mp.youku.com/upload',
                'pragma'=>'no-cache'
            ]
        ]);
        $complete = json_decode($response->getBody()->getContents(),true);
        \Log::info('----优酷视频完成视频信息--'.json_encode($complete,JSON_UNESCAPED_UNICODE)."\n\n==================================");
        if(isset($complete['success'])&&$complete['success']==true) {
            $publishes->content_id = $complete['data']['vid'];
            $publishes->contents_info = json_encode($params,JSON_UNESCAPED_UNICODE);
            $publishes->response = json_encode($complete);
            $publishes->status= 'UPLOAD_SUCCESS';
            $publishes->save();

        }else{
            $publishes->content_id = $complete['data']['vid'];
            $publishes->contents_info = json_encode($params,JSON_UNESCAPED_UNICODE);
            $publishes->response = json_encode($complete);
            $publishes->status= 'UPLOAD_FAILED';
            $publishes->reason= json_encode($complete);
            $publishes->save();
            /*$publish_data = [
                'title'=>$formData['title'],
                'platform_id'=>$accountInfo->platform_id,
                'platform_account_id'=>$accountInfo->id,
                'user_id'=>$accountInfo->user_id,
                'content_id'=>$complete['data']['vid'],
                'contents_info'=>json_encode($params,JSON_UNESCAPED_UNICODE),
                'response'=>json_encode($complete),
                'status'=>'UPLOAD_FAILED',
                'reason'=>json_encode($complete)
            ];
            Publishes::create($publish_data);*/
        }
        return response()->success();//echo json_encode(['code'=>0,'msg'=>'ok']);
        exit;

    }

     public static function publishYouku2($accountInfo,$videoInfo,$formData,$publishes){
        $client = new Client();
        $videoSize = filesize(public_path().$videoInfo->relative_path);

        $token_response = $client->request('GET','https://mp.youku.com/platform/token.json?',[
           'headers'=>[ ':authority'=>'mp.youku.com',
                ':method'=>'GET',
                ':path'=>'/platform/token.json?',
                ':scheme'=>'https',
                'Cookie'=>$accountInfo->cookie,
                'user-agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
                'referer'=>'https://mp.youku.com/index',
                'pragma'=>'no-cache',
                'origin'=>'https://mp.youku.com'
           ]
        ]);
        $token_info = json_decode($token_response->getBody()->getContents(),true);
        if(!isset($token_info['code'])||$token_info['code']!='SUCCESS') {
            return response()->error('VIDEO_UPLOAD_INFO_ERROR');
        }
        $x_xsrf_token = $token_info['data'];
        $response = $client->request('POST',"https://mp.youku.com/video-upload/create.json",[
            'verify' => false,
            'headers'=>[
                'Cookie'=>$accountInfo->cookie,
                'x-xsrf-token'=>$x_xsrf_token
            ],
            'form_params'=>[
                'fileSize'=>$videoSize,
                'fileName'=>$videoInfo->file_name,
                'fileMd5'=>md5($videoInfo->file_name)
            ]
        ]);
        $cookie = $accountInfo->cookie.';'.self::extraCookie($response->getHeader('Set-Cookie'));
        $videoUpload = json_decode($response->getBody()->getContents(),true);
        \Log::info('----获取优酷视频上传信息--'.json_encode($videoUpload)."\n\n");
        \Log::info('----securityToken--'.$videoUpload['data']['securityToken']."\n\n");
        if($videoUpload['success'] != 'true'){
            exit(json_encode(['code'=>110004,'msg'=>'cookie已过期，请重新扫码登陆！']));
        }
        try{
            $ossClient = new OssClient($videoUpload['data']['tempAccessId'], $videoUpload['data']['tempAccessSecret'], $videoUpload['data']['endpoint'], false, $videoUpload['data']['securityToken']);
            $res = $ossClient->uploadFile($videoUpload['data']['ossBucket'], $videoUpload['data']['ossObject'],public_path().$videoInfo->relative_path);
            \Log::info('----获取优酷视频上传信息2--'.json_encode($res)."\n\n==================================");
        }catch (\Exception $e){
            \Log::info('----获取优酷视频上传信息2--'.$e->getMessage()."\n\n==================================");
            //return response()->error('VIDEO_UPLOAD_INFO_ERROR');
            return response()->json();
        }
        $sessionId = '9PU2'.$videoUpload['data']['vid'].time().rand(1000000000,9999999999);

        $params = [
            'uploadToken'=>$videoUpload['data']['uploadToken'],
            'sessionId'=>$sessionId
        ];
        \Log::info($params);
        $response = $client->request('POST',"https://mp.youku.com/video-upload/file-complete.json",[
            'headers'=>[
                ':authority'=>'mp.youku.com',
                ':method'=>'POST',
                ':path'=>'/video-upload/file-complete.json',
                ':scheme'=>'https',
                'Cookie'=>$cookie,
                'user-agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
                'referer'=>'https://mp.youku.com/upload',
                'pragma'=>'no-cache',
                'x-xsrf-token'=>$x_xsrf_token,
                'origin'=>'https://mp.youku.com'
            ],
            'form_params'=>$params
        ]);
        $complete = json_decode($response->getBody()->getContents(),true);
        \Log::info('----优酷视频完成视频信息--'.json_encode($complete,JSON_UNESCAPED_UNICODE)."\n\n==================================");
        if(isset($complete['success'])&&$complete['success']==true) {
            /*$publishes->content_id = $complete['data']['vid'];
            $publishes->contents_info = json_encode($params,JSON_UNESCAPED_UNICODE);
            $publishes->response = json_encode($complete);
            $publishes->status= 'UPLOAD_SUCCESS';
            $publishes->save();*/
            $params = [
                'sessionId'=>$sessionId,
                'title'=>$formData['title'],
                'uploadToken'=>$complete['data']['uploadToken'],
                'source'=> 'MZ',
                'categoryId'=>$formData['categoryId'],
                'privacy'=> 'anybody',
                'thumbnail_num'=> 3,
                'vid'=>$complete['data']['vid'],
                //'tags'=> '自驾,马路',
                'encodeTime'=>0
            ];
            if(isset($formData['tags'])&&$formData['tags']) {
                $params['tags'] = $formData['tags'];
            }
            //$url = http_build_query($params);
            $response = $client->request('POST',"https://mp.youku.com/video-upload/save.json",[
                'headers'=>[
                    ':authority'=>'mp.youku.com',
                    ':method'=>'POST',
                    ':scheme'=>'https',
                    ':path'=> '/video-upload/save.json',
                    'cookie'=>$cookie,
                    'user-agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
                    'referer'=>'https://mp.youku.com/upload',
                    'pragma'=>'no-cache',
                    'x-xsrf-token'=> $x_xsrf_token
                ],
                'form_params'=>$params
            ]);

            $save = json_decode((string) $response->getBody()->getContents(),true);
            \Log::info('----优酷视频保存视频信息--'.json_encode($save,JSON_UNESCAPED_UNICODE)."\n\n==================================");
            if($save['success'] != 'true'){
                return response()->error('VIDEO_UPLOAD_INFO_ERROR');
            }
            $params = [
                'uploadToken'=> $complete['data']['uploadToken'],
                'sessionId'=> $sessionId,
                'vidStr'=> $complete['data']['vid']
            ];
            $response = $client->request('POST',"https://mp.youku.com/video-upload/complete.json",[
                'headers'=>[
                    ':authority'=>'mp.youku.com',
                    ':method'=>'GET',
                    ':scheme'=>'https',
                    'cookie'=>$cookie,
                    'user-agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
                    'referer'=>'https://mp.youku.com/upload',
                    'pragma'=>'no-cache',
                    'x-xsrf-token'=> $x_xsrf_token
                ],
                'form_params'=>$params
            ]);

            $complete = json_decode($response->getBody()->getContents(),true);
            \Log::info('----优酷视频完成视频信息--'.json_encode($complete,JSON_UNESCAPED_UNICODE)."\n\n==================================");
            if(isset($complete['success'])&&$complete['success']==true) {
                $publishes->content_id = $complete['data']['vid'];
                $publishes->contents_info = json_encode($params,JSON_UNESCAPED_UNICODE);
                $publishes->response = json_encode($complete);
                $publishes->status= 'UPLOAD_SUCCESS';
                $publishes->save();

            }else{
                $publishes->content_id = $complete['data']['vid'];
                $publishes->contents_info = json_encode($params,JSON_UNESCAPED_UNICODE);
                $publishes->response = json_encode($complete);
                $publishes->status= 'UPLOAD_FAILED';
                $publishes->reason= json_encode($complete);
                $publishes->save();
                /*$publish_data = [
                    'title'=>$formData['title'],
                    'platform_id'=>$accountInfo->platform_id,
                    'platform_account_id'=>$accountInfo->id,
                    'user_id'=>$accountInfo->user_id,
                    'content_id'=>$complete['data']['vid'],
                    'contents_info'=>json_encode($params,JSON_UNESCAPED_UNICODE),
                    'response'=>json_encode($complete),
                    'status'=>'UPLOAD_FAILED',
                    'reason'=>json_encode($complete)
                ];
                Publishes::create($publish_data);*/
            }

        }else{
            /*$publishes->content_id = $complete['data']['vid'];
            $publishes->contents_info = json_encode($params,JSON_UNESCAPED_UNICODE);
            $publishes->response = json_encode($complete);
            $publishes->status= 'UPLOAD_FAILED';
            $publishes->reason= json_encode($complete);
            $publishes->save();
            */
        }
        return response()->success();//echo json_encode(['code'=>0,'msg'=>'ok']);
        exit;

    }

    public static function getYoukuAuthorization($videoUpload,$gmtTime){
        $i="POST

video/mp4
{$gmtTime}
x-oss-date:{$gmtTime}
x-oss-security-token:{$videoUpload['data']['securityToken']}
x-oss-user-agent:aliyun-sdk-js/6.1.1 Chrome 79.0.3945.88 on OS X 10.14.3 64-bit
/{$videoUpload['data']['ossBucket']}/{$videoUpload['data']['ossObject']}";
        $sts = base64_encode(hash_hmac('sha1', $i, $videoUpload['data']['tempAccessSecret'], true));
        return "OSS {$videoUpload['data']['tempAccessId']}:{$sts}";
    }


    public static function publishAiqiyi($accountInfo,$videoInfo,$formData,$publishes){
        $client = new Client();
        $videoSize = filesize(public_path().$videoInfo->relative_path);
        $response = $client->request('GET',"https://upload.iqiyi.com/openupload",[
            'verify' => false,
            'headers'=>[
                'access-token'  => $accountInfo->access_token,
                'filesize'      => $videoSize,
                'filetype'      => substr(strrchr($videoInfo->relative_path, '.'), 1)
            ]
        ]);
        $openupload = json_decode($response->getBody()->getContents(),true);
        \Log::info('aliqiyi---openupload----'.json_encode($openupload,JSON_UNESCAPED_UNICODE)."\n\n");
        if($openupload['code'] != 'A00000'){
            return response()->error('PUBLISH_FAILED');
        }
        $response = $client->request('POST',$openupload['data']['upload_url'],[
            'verify' => false,
            'headers'=>[],
            'multipart'=>[
                [
                    'name'=>'file_id',
                    'contents'=>$openupload['data']['file_id']
                ],
                [
                    'name'=>'file_size',
                    'contents'=> $videoSize
                ],
                [
                    'name'=>'range',
                    'contents'=>'0-'.$videoSize
                ],
                [
                    'name'=>'file',
                    'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                ]
            ]
        ]);
        $upload = json_decode($response->getBody()->getContents(),true);
        \Log::info('aliqiyi---upload----'.json_encode($upload,JSON_UNESCAPED_UNICODE)."\n\n");
        if($upload['code'] != 'A00000'){
            $publishes->content_id = $openupload['data']['file_id'];
            $publishes->contents_info = json_encode([
                [
                    'name'=>'file_id',
                    'contents'=>$openupload['data']['file_id']
                ],
                [
                    'name'=>'file_size',
                    'contents'=> $videoSize
                ],
                [
                    'name'=>'range',
                    'contents'=>'0-'.$videoSize
                ],
                [
                    'name'=>'file',
                    'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                ]
            ],JSON_UNESCAPED_UNICODE);
            $publishes->response = json_encode($upload);
            $publishes->status = 'UPLOAD_FAILED';
            $publishes->reason = $upload['msg'];
            $publishes->save();
            /*$publish_data = [
                'title'=>$formData['title'],
                'platform_id'=>$accountInfo->platform_id,
                'platform_account_id'=>$accountInfo->id,
                'user_id'=>$accountInfo->user_id,
                'content_id'=>$openupload['data']['file_id'],
                'contents_info'=>json_encode([
                    [
                        'name'=>'file_id',
                        'contents'=>$openupload['data']['file_id']
                    ],
                    [
                        'name'=>'file_size',
                        'contents'=> $videoSize
                    ],
                    [
                        'name'=>'range',
                        'contents'=>'0-'.$videoSize
                    ],
                    [
                        'name'=>'file',
                        'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                    ]
                ],JSON_UNESCAPED_UNICODE),
                'response'=>json_encode($upload),
                'status'=>'UPLOAD_FAILED',
                'reason'=>$upload['msg']
            ];
            Publishes::create($publish_data);*/
            return false;//response()->error('PUBLISH_FAILED');
        }
        $response = $client->request('GET',"https://upload.iqiyi.com//uploadfinish?access_token={$accountInfo->account_token}&file_id={$openupload['data']['file_id']}&range_finished=true");
        $uploadfinish = json_decode($response->getBody()->getContents(),true);
        \Log::info('aliqiyi---uploadfinish----'.json_encode($uploadfinish,JSON_UNESCAPED_UNICODE)."\n\n");
        if($uploadfinish['code'] != 'A00000'){
            $publishes->content_id = $openupload['data']['file_id'];
            $publishes->contents_info = json_encode([
                [
                    'name'=>'file_id',
                    'contents'=>$openupload['data']['file_id']
                ],
                [
                    'name'=>'file_size',
                    'contents'=> $videoSize
                ],
                [
                    'name'=>'range',
                    'contents'=>'0-'.$videoSize
                ],
                [
                    'name'=>'file',
                    'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                ]
            ],JSON_UNESCAPED_UNICODE);
            $publishes->response = json_encode($uploadfinish);
            $publishes->status = 'UPLOAD_FAILED';
            $publishes->reason = $uploadfinish['msg'];
            $publishes->save();

            /*$publish_data = [
                'title'=>$formData['title'],
                'platform_id'=>$accountInfo->platform_id,
                'platform_account_id'=>$accountInfo->id,
                'user_id'=>$accountInfo->user_id,
                'content_id'=>$openupload['data']['file_id'],
                'contents_info'=>json_encode([
                    [
                        'name'=>'file_id',
                        'contents'=>$openupload['data']['file_id']
                    ],
                    [
                        'name'=>'file_size',
                        'contents'=> $videoSize
                    ],
                    [
                        'name'=>'range',
                        'contents'=>'0-'.$videoSize
                    ],
                    [
                        'name'=>'file',
                        'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                    ]
                ],JSON_UNESCAPED_UNICODE),
                'response'=>json_encode($uploadfinish),
                'status'=>'UPLOAD_FAILED',
                'reason'=>$uploadfinish['msg']
            ];
            Publishes::create($publish_data);*/
            return false;//response()->error('PUBLISH_FAILED');
        }
        $headers = [
            '：authority'=>'www.iqiyi.com',
            '：method'=>'POST',
            '：path'=>'/u/api/video/save_video_info',
            '：scheme'=>'https',
            'origin'=>'https://www.iqiyi.com',
            'user-agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
            'referer'=>'https://www.iqiyi.com/upload?type=0',
            'sec-fetch-site'=>'same-origin',
            'sec-fetch-mode'=>'cors',
            'cookie'=>$accountInfo->cookie,
        ];
        $form_params =[
            'flag'=>'upload',
            'file_id'=>$openupload['data']['file_id'],
            'tags'=> $formData['title'],
            'video_title'=> $formData['title'],
            'behavior_type'=> 1,
            'channel_id'=>7,
            'video_intro'=>$formData['title'],
            'private_policy'=> 0,
            'video_type'=> 1,
            'authCookie'=> $accountInfo->auth_cookie,
        ];
        $response = $client->request('POST','https://www.iqiyi.com/u/api/video/save_video_info',[
            'verify' => false,
            'headers'=>$headers,
            'form_params' =>$form_params
        ]);
        $r = $response->getBody()->getContents();
        $savevideo = json_decode($r,true);
        \Log::info('aliqiyi---savevideo1----'.json_encode($savevideo,JSON_UNESCAPED_UNICODE)."\n\n");
        if(isset($savevideo['code']) && ($savevideo['code'] == 'A00000')){
            $content_id = 0;
            try{
                $query_url = 'http://www.iqiyi.com/u/video';
                $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$accountInfo->cookie]]);
                $res = $response->getBody()->getContents();
                $response_html = HtmlDomParser::str_get_html($res);
                $lis = $response_html->find('.myVideo-list')[0]->find('li.myVideo-item');
                $li = $lis[0];
                $content_id = $li->getAttribute('tvid');
            }catch (\Exception $e){
                \Log::error('get iqiyi contnet id failed:'.$e->getMessage());
            }
            $publishes->content_id = $openupload['data']['file_id'];
            $publishes->contents_info = json_encode([
                [
                    'name'=>'file_id',
                    'contents'=>$openupload['data']['file_id']
                ],
                [
                    'name'=>'file_size',
                    'contents'=> $videoSize
                ],
                [
                    'name'=>'range',
                    'contents'=>'0-'.$videoSize
                ],
                [
                    'name'=>'file',
                    'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                ]
            ],JSON_UNESCAPED_UNICODE);
            $publishes->response = $r;
            $publishes->status = 'UPLOAD_SUCCESS';
            $publishes->save();
            /*$publish_data = [
                'title'=>$formData['title'],
                'platform_id'=>$accountInfo->platform_id,
                'platform_account_id'=>$accountInfo->id,
                'user_id'=>$accountInfo->user_id,
                'content_id'=>$content_id,
                'contents_info'=>json_encode([
                    [
                        'name'=>'file_id',
                        'contents'=>$openupload['data']['file_id']
                    ],
                    [
                        'name'=>'file_size',
                        'contents'=> $videoSize
                    ],
                    [
                        'name'=>'range',
                        'contents'=>'0-'.$videoSize
                    ],
                    [
                        'name'=>'file',
                        'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                    ]
                ],JSON_UNESCAPED_UNICODE),
                'response'=>$r,
                'status'=>'UPLOAD_SUCCESS'
            ];
            Publishes::create($publish_data);*/
            return response()->success();//echo json_encode(['code'=>0,'msg'=>'']);
        }else{
            $response = $client->request('POST','https://www.iqiyi.com/u/api/video/save_video_info',[
                'verify' => false,
                'headers'=>$headers,
                'multipart' =>[
                    ['name'=>'flag','contents'=>'upload'],
                    ['name'=>'file_id','contents'=>$openupload['data']['file_id']],
                    ['name'=>'tags','contents'=>$formData['title']],
                    ['name'=>'video_title','contents'=>$formData['title']],
                    ['name'=>'behavior_type','contents'=>''],
                    ['name'=>'channel_id','contents'=>7],
                    ['name'=>'video_intro','contents'=>$formData['title']],
                    ['name'=>'private_policy','contents'=>0],
                    ['name'=>'video_type','contents'=>1],
                    ['name'=>'authCookie','contents'=>$accountInfo->auth_cookie],
                ]
            ]);

            $savevideo = json_decode($response->getBody()->getContents(),true);
            \Log::info('aliqiyi---savevideo2----'.json_encode($savevideo,JSON_UNESCAPED_UNICODE)."\n\n");
            if(isset($savevideo['code']) && ($savevideo['code'] == 'A00000')){
                $publishes->content_id = $openupload['data']['file_id'];
                $publishes->contents_info = json_encode([
                    [
                        'name'=>'file_id',
                        'contents'=>$openupload['data']['file_id']
                    ],
                    [
                        'name'=>'file_size',
                        'contents'=> $videoSize
                    ],
                    [
                        'name'=>'range',
                        'contents'=>'0-'.$videoSize
                    ],
                    [
                        'name'=>'file',
                        'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                    ]
                ],JSON_UNESCAPED_UNICODE);
                $publishes->response = $r;
                $publishes->save();

                /*$publish_data = [
                    'title'=>$formData['title'],
                    'platform_id'=>$accountInfo->platform_id,
                    'platform_account_id'=>$accountInfo->id,
                    'user_id'=>$accountInfo->user_id,
                    'content_id'=>$openupload['data']['file_id'],
                    'contents_info'=>json_encode([
                        [
                            'name'=>'file_id',
                            'contents'=>$openupload['data']['file_id']
                        ],
                        [
                            'name'=>'file_size',
                            'contents'=> $videoSize
                        ],
                        [
                            'name'=>'range',
                            'contents'=>'0-'.$videoSize
                        ],
                        [
                            'name'=>'file',
                            'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                        ]
                    ],JSON_UNESCAPED_UNICODE),
                    'response'=>$r,
                    //'status'=>'REVIEW_SUCCESS'
                ];
                Publishes::create($publish_data);*/
                return response()->success(); //json_encode(['code'=>0,'msg'=>'']);
            }else{
                $publishes->content_id = $openupload['data']['file_id'];
                $publishes->contents_info = json_encode([
                    [
                        'name'=>'file_id',
                        'contents'=>$openupload['data']['file_id']
                    ],
                    [
                        'name'=>'file_size',
                        'contents'=> $videoSize
                    ],
                    [
                        'name'=>'range',
                        'contents'=>'0-'.$videoSize
                    ],
                    [
                        'name'=>'file',
                        'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                    ]
                ],JSON_UNESCAPED_UNICODE);
                $publishes->response = $r;
                $publishes->reason = $savevideo['msg'];
                $publishes->status = 'UPLOAD_FAILED';
                $publishes->save();

                /*$publish_data = [
                    'title'=>$formData['title'],
                    'platform_id'=>$accountInfo->platform_id,
                    'platform_account_id'=>$accountInfo->id,
                    'user_id'=>$accountInfo->user_id,
                    'content_id'=>$openupload['data']['file_id'],
                    'contents_info'=>json_encode([
                        [
                            'name'=>'file_id',
                            'contents'=>$openupload['data']['file_id']
                        ],
                        [
                            'name'=>'file_size',
                            'contents'=> $videoSize
                        ],
                        [
                            'name'=>'range',
                            'contents'=>'0-'.$videoSize
                        ],
                        [
                            'name'=>'file',
                            'contents'=>@fopen(public_path().$videoInfo->relative_path,'r')
                        ]
                    ],JSON_UNESCAPED_UNICODE),
                    'response'=>$r,
                    'status'=>'UPLOAD_FAILED',
                    'reason'=>$savevideo['msg']
                ];
                Publishes::create($publish_data);*/
                //return response()->error('PUBLISH_FAILED');
                return false;
            }
        }


    }


    public static function publishHaokan($accountInfo,$videoInfo,$formData,$coverInfo,$publishes){
        \Log::info('haokan--start--'."\n");
        $client = new client();
        try{
            //获取视频分类下发的token
            $response=$client->request('GET','https://baijiahao.baidu.com/builder/author/video/videoCategory?',[
                'headers'=>[
                    'Cookie'=>$accountInfo->cookie
                ]

            ]);
            $token = $response->getHeader('Token');
            \Log::info('haokan--token--'.json_encode($token[0])."\n");
            //\Log::info('haokan--videocategory--'.json_encode($response->getBody()->getContents())."\n");
            if(!$token){
                return response()->error('PUBLISH_FAILED');
            }

            //先获取appid
            $response = $client->request('GET',"https://baijiahao.baidu.com/builder/app/appinfo",[
                'headers'=>[
                    'Cookie'=>$accountInfo->cookie,
                ]
            ]);
            $userInfo = json_decode($response->getBody()->getContents(),true);
            \Log::info('haokan--userinfo--'.json_encode($userInfo,JSON_UNESCAPED_UNICODE));
            if($userInfo['errno'] !== 0){
                return response()->error('PUBLISH_FAILED');
            }
            $appid = $userInfo['data']['user']['app_id'];
            //获取预上传信息
            $videoSize = @filesize(public_path().$videoInfo->relative_path);
            $videoMd5 =  md5(public_path().$videoInfo->relative_path);
            $response = $client->request('POST',"https://baijiahao.baidu.com/builder/author/video/preuploadVideo?app_id={$appid}&md5={$videoMd5}&is_pay_column=0",[
                'headers'=>[
                    'token' =>$token,
                    'Cookie'=>$accountInfo->cookie,
                    'X-Requested-With'=>'XMLHttpRequest',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ]
            ]);
            $preuploadVideo = json_decode($response->getBody()->getContents(),true);
            \Log::info('haokan--preupload---'.json_encode($preuploadVideo,JSON_UNESCAPED_UNICODE));
            if(!isset($preuploadVideo['error_code']) || $preuploadVideo['error_code'] !== 20000){
                return response()->error('PUBLISH_FAILED');
            }

            //$uploadCookie = $response->getHeader('')
            $mediaId    = $preuploadVideo['mediaId'];
            \Log::info('-----mediaId---'.$mediaId."\n\n");
            $upload_key = $preuploadVideo['upload_key'];
            \Log::info('-----upload_key---'.$upload_key."\n\n");
            $last_chunk = 1;
            \Log::info('file_size:'.$videoSize);
            if($videoSize>1024*2*1024) {//分片上传
                $cookie = $accountInfo->cookie;
                $chunk_nums = ceil($videoSize/(1024*2*1024));
                $read_buffer = 1024*2*1024;
                $sum_buffer = 0;
                $handle = fopen(public_path().$videoInfo->relative_path, 'rb');
                $file_exp = explode('.',basename(public_path().$videoInfo->relative_path));
                $file_ext = array_pop($file_exp);
                $file_base_name = implode('.',$file_exp);
                $file_index = 0;
                \Log::info($chunk_nums.':'.$chunk_nums."\r\n");
                //先分片存储
                while(!feof($handle) && $sum_buffer<$videoSize) {
                    $file_content = fread($handle, $read_buffer);
                    $sum_buffer += $read_buffer;
                    $file_save_name = public_path().'/upload/image/'.$file_base_name.'_'.$file_index;
                    file_put_contents($file_save_name,$file_content);
                    $headers = [
                        'Cookie'    =>$cookie,
                        'Referer'   =>"https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id={$appid}",
                        'Sec-Fetch-Mode'=>'cors',
                        'Sec-Fetch-Site'=>'same-site',
                        'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                    ];
                    $videoData = [
                        [
                            'name'      => 'timestamp',
                            'contents'  => time()
                        ], [
                            'name'      =>'md5',
                            'contents'  =>$videoMd5
                        ], [
                            'name'      =>'id',
                            'contents'  =>'WU_FILE_1'
                        ], [
                            'name'      =>'name',
                            'contents'  =>$videoInfo->file_name
                        ], [
                            'name'      =>'type',
                            'contents'  =>'video/mp4'//substr(strrchr($videoInfo->relative_path, '.'), 1)
                        ],
                        [
                            'name'      =>'size',
                            'contents'  =>$videoSize
                        ],
                        [
                            'name'      =>'chunks',
                            'contents'  =>$chunk_nums
                        ],
                        [
                            'name'      =>'chunk',
                            'contents'  =>$file_index
                        ],
                        [
                            'name'      =>'upload_key',
                            'contents'  =>$upload_key
                        ],
                        [
                            'name'      =>'file',
                            'contents'  =>@fopen($file_save_name,'r'),
                        ]
                    ];
                    $response = $client->request('POST',"https://rsbjh.baidu.com/builder/author/video/uploadVideo?app_id={$appid}",[
                        'verify' => false,
                        'headers'=>$headers,
                        'multipart'=>$videoData
                    ]);
                    $uploadVideo = json_decode($response->getBody()->getContents(),true);
                    \Log::info('$uploadVideo:'.$file_index.":".$response->getBody()->getContents()."\r\n");
                    $file_index++;
                }
                \Log::info($chunk_nums.':'.$chunk_nums."\r\n");
                $last_chunk = $chunk_nums;
            }else{
                //上传视频
                $headers = [
                    'Cookie'    =>$accountInfo->cookie,
                    'Referer'   =>"https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id={$appid}",
                    'Sec-Fetch-Mode'=>'cors',
                    'Sec-Fetch-Site'=>'same-site',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ];
                $videoData = [
                    [
                        'name'      => 'timestamp',
                        'contents'  => time()
                    ], [
                        'name'      =>'md5',
                        'contents'  =>$videoMd5
                    ], [
                        'name'      =>'id',
                        'contents'  =>'WU_FILE_0'
                    ], [
                        'name'      =>'name',
                        'contents'  =>$videoInfo->file_name
                    ], [
                        'name'      =>'type',
                        'contents'  =>'video/mp4'//substr(strrchr($videoInfo->relative_path, '.'), 1)
                    ],
                    [
                        'name'      =>'size',
                        'contents'  =>$videoSize
                    ],
                    [
                        'name'      =>'upload_key',
                        'contents'  =>$upload_key
                    ],
                    [
                        'name'      =>'file',
                        'contents'  =>@fopen(public_path().$videoInfo->relative_path,'r'),
                    ]
                ];
                \Log::info("video-info-data".json_encode($videoData)."\n\n");
                \Log::info("video-info-header".json_encode($headers)."\n\n");
                $response = $client->request('POST',"https://rsbjh.baidu.com/builder/author/video/uploadVideo?app_id={$appid}",[
                    'verify' => false,
                    'headers'=>$headers,
                    'multipart'=>$videoData
                ]);
                $uploadVideo = json_decode($response->getBody()->getContents(),true);
                \Log::info('haokan--uploadvideo---'.json_encode($uploadVideo,JSON_UNESCAPED_UNICODE));
                if(!isset($uploadVideo['error_code']) || $uploadVideo['error_code'] !==20000){
                    $publishes->content_id=0;
                    $publishes->contents_info=json_encode([
                        [
                            'name'      =>'upload_key',
                            'contents'  =>$upload_key
                        ],
                        [
                            'name'      =>'name',
                            'contents'  =>$videoInfo->file_name,
                        ],
                        [
                            'name'      =>'chunks',
                            'contents'  =>1
                        ]
                    ],JSON_UNESCAPED_UNICODE);
                    $publishes->response = json_encode($uploadVideo);
                    $publishes->status = 'UPLOAD_FAILED';
                    $publishes->reason = '未知错误';
                    $publishes->save();
                    return response()->error('PUBLISH_FAILED');
                }
            }

            //告诉他上传完了
            $response = $client->request('POST',"https://baijiahao.baidu.com/builder/author/video/compuploadVideo?uploadTime=1397&app_id={$appid}",[
                'headers'=>[
                    'Referer'=>"https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id={$appid}",
                    'Sec-Fetch-Mode'=>'cors',
                    'Sec-Fetch-Site'=>'same-origin',
                    'token' =>$token,
                    'Cookie'=>$accountInfo->cookie,
                    'X-Requested-With'=>'XMLHttpRequest',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ],
                'multipart'=>[
                    [
                        'name'      =>'upload_key',
                        'contents'  =>$upload_key
                    ],
                    [
                        'name'      =>'name',
                        'contents'  =>$videoInfo->file_name,
                    ],
                    [
                        'name'      =>'chunks',
                        'contents'  =>$last_chunk
                    ]
                ]
            ]);
            $compupload = json_decode($response->getBody()->getContents(),true);
            \Log::info('haokan--compupload---'.json_encode($compupload,JSON_UNESCAPED_UNICODE));
            if(!isset($compupload['error_code']) || ($compupload['error_code'] !== 0)){
                $publishes->content_id=0;
                $publishes->contents_info=json_encode([
                    [
                        'name'      =>'upload_key',
                        'contents'  =>$upload_key
                    ],
                    [
                        'name'      =>'name',
                        'contents'  =>$videoInfo->file_name,
                    ],
                    [
                        'name'      =>'chunks',
                        'contents'  =>1
                    ]
                ],JSON_UNESCAPED_UNICODE);
                $publishes->response = json_encode($compupload);
                $publishes->status = 'UPLOAD_FAILED';
                $publishes->reason = $compupload['errmsg'];
                $publishes->save();

                /*$publish_data = [
                    'title'=>$formData['title'],
                    'platform_id'=>$accountInfo->platform_id,
                    'platform_account_id'=>$accountInfo->id,
                    'user_id'=>$accountInfo->user_id,
                    'content_id'=>0,
                    'contents_info'=>json_encode([
                        [
                            'name'      =>'upload_key',
                            'contents'  =>$upload_key
                        ],
                        [
                            'name'      =>'name',
                            'contents'  =>$videoInfo->file_name,
                        ],
                        [
                            'name'      =>'chunks',
                            'contents'  =>1
                        ]
                    ],JSON_UNESCAPED_UNICODE),
                    'response'=>json_encode($compupload),
                    'status'=>'UPLOAD_FAILED',
                    'reason'=>$compupload['errmsg']
                ];
                Publishes::create($publish_data);*/
                return response()->error('PUBLISH_FAILED');
            }

            //上传封面图片
            $response = $client->request('POST','https://baijiahao.baidu.com/builderinner/api/content/file/upload',[
                'headers'=>[
                    'Cookie'=>$accountInfo->cookie,
                    'X-Requested-With'=>'XMLHttpRequest',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ],
                'multipart'=>[
                    ['name'=>'type','contents'=>'image'],
                    ['name'=>'app_id','contents'=>''],
                    ['name'=>'media','contents'=>@fopen(public_path().$coverInfo->relative_path,'r')],
                ]
            ]);
            $uploadImg = json_decode($response->getBody()->getContents(),true);
            \Log::info('haokan--uploadimg---'.json_encode($uploadImg,JSON_UNESCAPED_UNICODE));
            if(!isset($uploadImg['errno'])){
                if($uploadImg['errno'] !== 20040706){
                    $publishes->content_id=0;
                    $publishes->contents_info=json_encode([
                        ['name'=>'type','contents'=>'image'],
                        ['name'=>'app_id','contents'=>''],
                        ['name'=>'media','contents'=>@fopen(public_path().$coverInfo->relative_path,'r')],
                    ],JSON_UNESCAPED_UNICODE);
                    $publishes->response = json_encode($uploadImg);
                    $publishes->status = 'UPLOAD_FAILED';
                    $publishes->reason = $uploadImg['errmsg'];
                    $publishes->save();

                    /*$publish_data = [
                        'title'=>$formData['title'],
                        'platform_id'=>$accountInfo->platform_id,
                        'platform_account_id'=>$accountInfo->id,
                        'user_id'=>$accountInfo->user_id,
                        'content_id'=>0,
                        'contents_info'=>json_encode([
                            ['name'=>'type','contents'=>'image'],
                            ['name'=>'app_id','contents'=>''],
                            ['name'=>'media','contents'=>@fopen(public_path().$coverInfo->relative_path,'r')],
                        ],JSON_UNESCAPED_UNICODE),
                        'response'=>json_encode($uploadImg),
                        'status'=>'UPLOAD_FAILED',
                        'reason'=>$uploadImg['errmsg']
                    ];
                    Publishes::create($publish_data);*/
                    return response()->error('VIDEO_COVER_NOT_MATCH');
                }else{
                    $publishes->content_id=0;
                    $publishes->contents_info=json_encode([
                        ['name'=>'type','contents'=>'image'],
                        ['name'=>'app_id','contents'=>''],
                        ['name'=>'media','contents'=>@fopen(public_path().$coverInfo->relative_path,'r')],
                    ],JSON_UNESCAPED_UNICODE);
                    $publishes->response = json_encode($uploadImg);
                    $publishes->status = 'UPLOAD_FAILED';
                    $publishes->reason = $uploadImg['errmsg'];
                    $publishes->save();
                    /*$publish_data = [
                        'title'=>$formData['title'],
                        'platform_id'=>$accountInfo->platform_id,
                        'platform_account_id'=>$accountInfo->id,
                        'user_id'=>$accountInfo->user_id,
                        'content_id'=>0,
                        'contents_info'=>json_encode([
                            ['name'=>'type','contents'=>'image'],
                            ['name'=>'app_id','contents'=>''],
                            ['name'=>'media','contents'=>@fopen(public_path().$coverInfo->relative_path,'r')],
                        ],JSON_UNESCAPED_UNICODE),
                        'response'=>json_encode($uploadImg),
                        'status'=>'UPLOAD_FAILED',
                        'reason'=>$uploadImg['errmsg']
                    ];
                    Publishes::create($publish_data);*/
                    return response()->error('PUBLISH_FAILED');
                }
            }
            $token = $response->getHeader('Token');
            //图片自动裁剪
            $response= $client->request('POST','https://baijiahao.baidu.com/builder/author/article/autocutpic',[
                'headers'=>[
                    'Cookie'=>$accountInfo->cookie,
                    'token' =>$token[0]
                ],
                'multipart'=>[
                    ['name'=>'width','contents'=>1200],
                    ['name'=>'height','contents'=>791],
                    ['name'=>'image','contents'=>$uploadImg['ret']['https_url']],
                    ['name'=>'article_type','contents'=>'video'],
                ]
            ]);
            $result = $response->getBody()->getContents();
            \Log::info('---图片自动裁剪----'.$result."\n\n");
            $token = $response->getHeader('Token');
            \Log::info('---图片裁剪2--TOKEN----'.$token[0]."\n\n");

            //图片裁剪2
            $response= $client->request('POST','https://baijiahao.baidu.com/builderinner/api/content/file/cuttingPic',[
                'headers'=>[
                    'Cookie'=>$accountInfo->cookie,
                    'token' =>$token[0]
                ],
                'multipart'=>[
                    ['name'=>'auto','contents'=>false],
                    ['name'=>'x','contents'=>0],
                    ['name'=>'y','contents'=>0],
                    ['name'=>'w','contents'=>1200],
                    ['name'=>'h','contents'=>673],
                    ['name'=>'src','contents'=>$uploadImg['ret']['https_url']],
                    ['name'=>'type','contents'=>'video'],
                ]
            ]);
            $result = json_decode($response->getBody()->getContents(),true);
            \Log::info('---图片裁剪2-result----'.json_encode($result,JSON_UNESCAPED_UNICODE)."\n\n");
            $token = $response->getHeader('Token');
            \Log::info('---图片裁剪2--TOKEN----'.$token[0]."\n\n");
            $headers = [
                'Sec-Fetch-Mode'=>'cors',
                'Sec-Fetch-Site'=>'same-origin',
                'token' =>$token[0],
                'Cookie'=>$accountInfo->cookie,
                'Referer'=>"https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id={$appid}",
                'X-Requested-With'=>'XMLHttpRequest',
                'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
            ];
            \Log::info('---保存请求header------'.json_encode($headers)."\n\n");
            $data = [
                ['name' =>'title', 'contents'=> $formData['title']],
                ['name' =>'content','contents'=> '[{"title":"'.$formData['title'].'","desc":"","mediaId":"'.$mediaId.'","videoName":"'.$videoInfo->file_name.'","local":1}]'],
                ['name'      =>'type', 'contents'  =>'video'],
                ['name'=>'cover_images','contents'=>'[{"source":"local","src":"'.$result['data']['src'].'","cropData":{"x":0,"y":0,"width":1200,"height":673},"isLegal":0}]'],
                ['name'=>'_cover_images_map', 'contents'  =>'[{"src":"'.$result['data']['src'].'","origin_src":"'.$result['data']['origin_src'].'"}]'],
                ['name'=>'cover_layout', 'contents'  =>'one'],
                ['name'=>'usingImgFilter','contents' =>false],
                ['name'=>'source_reprinted_allow', 'contents'  =>0],
                ['name'=>'original_status', 'contents'  =>0],
                ['name'=>'announce_id', 'contents'  =>1],
            ];
            \Log::info('---保存请求data------'.json_encode($data)."\n\n");
            //获取一个发布需要的cookie
            $response = $client->request('POST',"https://baijiahao.baidu.com/builder/author/home/dealToken",[
                'headers'=>[
                    'bjh'=>'b07307529a6ec5d84569725a51ad2d854c3ac2b6fd25d7476dcc6cd573b57082',
                    'dev'=>'a6e03da61922855b2e7cf1d30f983eec4c3ac2b6fd25d7476dcc6cd573b57082',
                    'Cookie'=>$accountInfo->cookie,
                    'token' =>$token[0],
                    'Referer'=>'https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id=1616440569727005',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ]
            ]);
            $cookie_str = self::extractHaokanCookie($response->getHeader('Set-Cookie'),$accountInfo->cookie);
            \Log::info('haokan----cookie---'.$cookie_str);
            //发布视频
            $response = $client->request('POST',"https://baijiahao.baidu.com/builder/author/article/publish",[
                'headers'   =>$headers,
                'multipart' =>$data
            ]);
            $saveInfo = json_decode($response->getBody()->getContents(),true);
            \Log::info('haokan--save_info1---'.json_encode($saveInfo,JSON_UNESCAPED_UNICODE));

            if($saveInfo['errno'] === 0 && $saveInfo['errmsg'] == 'success'){
                $publishes->content_id=$saveInfo['ret']['article_id'];
                $publishes->contents_info=json_encode($data,JSON_UNESCAPED_UNICODE);
                $publishes->response = json_encode($saveInfo);
                $publishes->status = 'UPLOAD_SUCCESS';
                //$publishes->reason = $uploadImg['errmsg'];
                $publishes->save();

                return response()->success();//echo json_encode(['code'=>0,'msg'=>'']);
            }else{
                $publishes->content_id=$saveInfo['ret']['article_id'];
                $publishes->contents_info=json_encode($data,JSON_UNESCAPED_UNICODE);
                $publishes->response = json_encode($saveInfo);
                $publishes->status = 'UPLOAD_FAILED';
                $publishes->reason = $saveInfo['errmsg'];
                $publishes->save();
                /*$publish_data = [
                    'title'=>$formData['title'],
                    'platform_id'=>$accountInfo->platform_id,
                    'platform_account_id'=>$accountInfo->id,
                    'user_id'=>$accountInfo->user_id,
                    'content_id'=>$saveInfo['ret']['article_id'],
                    'contents_info'=>json_encode($data,JSON_UNESCAPED_UNICODE),
                    'response'=>json_encode($saveInfo),
                    'status'=>'UPLOAD_FAILED',
                    'reason'=>$saveInfo['errmsg']
                ];
                Publishes::create($publish_data);*/
                return response()->success();//echo json_encode(['code'=>$saveInfo['errno'],'msg'=>$saveInfo['errmsg']]);
            }

        }catch (\Exception $e){
            \Log::info('haokan--over--'.json_encode($e->getMessage())."\n");
            return response()->error('PUBLISH_FAILED');
        }

    }

    public function publish_aiqiyihao($accountInfo,$videoInfo,$formData,$publishes) {
        $videoSize = @filesize(public_path().$videoInfo->relative_path);
        $split_up_url = 'https://upload.iqiyi.com/split_upload_request?access_token='.$accountInfo->access_token.'&file_type=mp4&file_size='.$videoSize;
        $client = new Client();
        $response = $client->request('GET',$split_up_url);
        $res = json_decode($response->getBody()->getContents(),true);
        $upload_url = $res['data']['upload_url'];
        $file_id = $res['data']['file_id'];
        $chunk_nums = ceil($videoSize/(1024*3*1024));
        $read_buffer = 1024*3*1024;
        $sum_buffer = 0;
        $handle = fopen(public_path().$videoInfo->relative_path, 'rb');
        $file_exp = explode('.',basename(public_path().$videoInfo->relative_path));
        $file_ext = array_pop($file_exp);
        $file_base_name = implode('.',$file_exp);
        $file_index = 0;
        //\Log::info($chunk_nums.':'.$chunk_nums."\r\n");
        //先分片存储
        while(!feof($handle) && $sum_buffer<$videoSize) {
            $file_content = fread($handle, $read_buffer);
            $sum_buffer_before = $sum_buffer;
            $sum_buffer += $read_buffer;
            $start_size = $sum_buffer_before-1;
            $end_size = $sum_buffer>$videoSize?$videoSize-1:$sum_buffer-1;
            $file_save_name = public_path() . '/upload/image/' . $file_base_name . '_iqyi_' . $file_index;
            file_put_contents($file_save_name, $file_content);
            $form_params = [

                [
                    'name'      =>'name',
                    'contents'  =>$videoInfo->file_name
                ],
                [
                    'name'      =>'chunk',
                    'contents'  =>$file_index
                ],
                [
                    'name'      =>'chunks',
                    'contents'  =>$chunk_nums
                ],
                [
                    'name'      =>'file_size',
                    'contents'  =>$videoSize
                ],
                [
                    'name'      =>'file_id',
                    'contents'  =>$file_id
                ],
                [
                    'name'      =>'range',
                    'contents'  =>$start_size.'-'.$end_size
                ],
                [
                    'name'      =>'part_number',
                    'contents'  =>$file_index+1
                ],
                [
                    'name'      =>'part_number',
                    'contents'  =>$file_index+1
                ],
                ['name'=>'file','contents'=>@fopen($file_save_name,'r')],
            ];
            $response_up = $client->request('POST',$upload_url,[
                'multipart'=>$form_params
            ]);
            $response_up_format = json_decode($response_up->getBody()->getContents(),true);
            if($response_up_format['code'] != 'A00000') {
                \Log::error('publish_aiqiyihao upload video error:'.$response_up);
            }
            $file_index++;
        }
        $response = $client->request('GET','https://upload.iqiyi.com/split_upload_finish?file_id='.$file_id);
        $res = json_decode($response->getBody()->getContents(),true);
        if($res['code'] != 'A00000') {
            \Log::error('publish_aiqiyihao upload video finish error:'.$response_up);
        }
    }

}
