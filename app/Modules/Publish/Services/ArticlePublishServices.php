<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/1/23
 * Time: 9:51
 */

namespace App\Modules\Publish\Services;

use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Platforms;
use App\Modules\Platform\Models\Publishes;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Redis;
use Sunra\PhpSimple\HtmlDomParser;

class ArticlePublishServices
{
    public static function uploadYidianImage($cookie,$link) {
        $img_path = str_replace('http://'.env('STATIC_DOMAIN'),'',$link);
        $url = 'https://mp.yidianzixun.com/upload?action=uploadimage&_rk=1575601398416';
        $client = new Client();
        $response = $client->request('POST', $url, [
            'headers'=>[
                'Cookie'=>$cookie
            ],
            'multipart' => [
                [
                    'name'     => 'upfile',
                    'contents' => fopen(public_path().$img_path, 'r')
                ]
            ]
        ]);
        $res = $response->getBody()->getContents();
        $json = json_decode($res,true);
        //$ret = [];
        if(is_array($json)) {
            return ['key'=>$img_path,'res'=>$json['url']];
        }
        return false;
    }

    public static function publishQQ($account,$account_info) {
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $cover_pic = $account['formData']['cover_pic'];
        $cover_type = $account['formData']['cover_type'];
        $tag = $account['formData']['tag'];
        $category = $account['formData']['category'];
        $apply = $account['formData']['apply'];
        $access_token = self::getQQAccessToken($account_info['user_id']);
        $client = new Client();
        //$url = 'https://api.om.qq.com/articlev2/authpubpic?access_token='.$access_token.'&openid='.$account_info['openid'].'&title='.$title.'&content='.urlencode($content).'&cover_pic='.$cover_pic[0].'&cover_type='.$cover_type.'&tag='.$tag.'&category='.$category.'&apply='.$apply;
        $url = 'https://api.om.qq.com/articlev2/authpubpic';
        $params = [
            'access_token'=>$access_token,
            'openid'=>$account_info['openid'],
            'title'=>$title,
            'content'=>$content,
            'cover_pic'=>implode(',',$cover_pic),
            'cover_type'=>$cover_type,
            'tag'=>$tag,
            'category'=>$category,
            'apply'=>$apply,

        ];
        if($apply==1) {
            $params['original_platform']=1;
        }
        $response = $client->request('POST',$url,['form_params'=>$params]);
        $res = $response->getBody()->getContents();
        $res_arr = json_decode($res,true);
        \Log::info($res);
        if($res_arr['code']==0) {
            $article_id = $res_arr['data']['article_id'];
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>$article_id,
                'contents_info'=>json_encode($params,JSON_UNESCAPED_UNICODE),
                'response'=>$res
            ];
            Publishes::create($publish_data);
            return response()->success();
        }else{
            $article_id = 0;
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>$article_id,
                'contents_info'=>json_encode($params,JSON_UNESCAPED_UNICODE),
                'reason'=>$res_arr['msg'],
                'status'=>'UPLOAD_FAILED',
                'response'=>$res
            ];
            Publishes::create($publish_data);
        }
        return response()->error('PUBLISH_FAILED');
        //return $res;
    }

    public static function publishZhiHu($account,$account_info) {
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $cover_pic = $account['formData']['cover_pic'];
        $img_list = $account['imgList'];
        $client = new Client();
        //$cover_pic = is_array($cover_pic)?$cover_pic:[];
        $json_arr = ['titleImage'=>is_array($cover_pic)?$cover_pic[0]:'','isTitleImageFullScreen'=>false,'title'=>$title,'content'=>$content,'delta_time'=>3];
        $response = $client->request('POST','https://zhuanlan.zhihu.com/api/articles/drafts',['headers'=>['Cookie'=>$account_info['cookie']],'json'=>$json_arr]);
        $resp = $response->getBody()->getContents();
        \Log::info($resp);
        $res = json_decode($resp,true);
        if(is_array($res)) {
            $id = $res['id'];
            $json_arr2 = ['column'=>'','commentPermission'=>'anyone'];
            $re = $client->request('PUT','https://zhuanlan.zhihu.com/api/articles/'.$id.'/publish',['headers'=>['Cookie'=>$account_info['cookie']],'json'=>$json_arr2]);
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>$id,
                'contents_info'=>json_encode($json_arr,JSON_UNESCAPED_UNICODE),
                'response'=>$resp,
                'status'=>'REVIEW_SUCCESS'
            ];
            Publishes::create($publish_data);
            \Log::info($re->getBody()->getContents());
            return true;
        }else{
            //$id = $res['id'];
            //$json_arr2 = ['column'=>'','commentPermission'=>'anyone'];
            //$re = $client->request('PUT','https://zhuanlan.zhihu.com/api/articles/'.$id.'/publish',['headers'=>['Cookie'=>$account_info['cookie']],'json'=>$json_arr2]);
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>0,
                'contents_info'=>json_encode($json_arr,JSON_UNESCAPED_UNICODE),
                'response'=>$resp,
                'status'=>'UPLOAD_FAILED',
                'reason'=>'未知错误'
            ];
            Publishes::create($publish_data);
        }
        return false;
    }

    public static function publishBjh($account,$account_info) {
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $cover_layout = $account['formData']['cover_layout'];
        if($cover_layout ==1 ||$cover_layout =='1') {
            $cover_layout = 'one';
        }
        if($cover_layout ==3 ||$cover_layout =='3') {
            $cover_layout = 'three';
        }
        $cover_images = $account['formData']['cover_images'];
        $token =$account_info['token'];
        //\Log::info('token----:'.$token);
        $img_list = $account['imgList'];
        $client = new Client();
        $cover_trans = [];
        $cover_map = [];
        foreach ($cover_images as $key=>$cover_image) {
            $cover_trans[] =[
                'src'=>$cover_image,
                //'cropData'=>["x"=>$size_info['x'],"y"=>$size_info['y'],"width"=>$size_info['w'], "height"=>$size_info['h']],
                "machine_chooseimg"=>0,"isLegal"=>1
            ];
            $cover_map[] = ['src'=>$cover_image,'origin_src'=>$cover_image];
        }
        $pub_data = array(
            'title'=>$title,
            'content'=>$content,
            'feed_cat'=>$account['formData']['feed_cat'],
            'len'=>strlen($content),
            'original_status'=>0,
            'announce_id'=>0,
            'isBeautify'=> false,
            'cover_layout'=> $cover_layout,
            'cover_images'=>json_encode($cover_trans),
            '_cover_images_map'=> json_encode($cover_map),
            'subtitle'=>'',
            'bjhtopic_id'=>'',
            'bjhtopic_info'=>'',
            'type'=> 'news'
        );

        if(isset($account['formData']['send_time'])&& !empty($account['formData']['send_time'])) {
            //\Log::info('send_time:'.$);
            $pub_data['timer_time'] = strtotime($account['formData']['send_time']);
            \Log::info(strtotime($account['formData']['send_time']));
        }
        \Log::info(json_encode($pub_data));
        $resp = $client->request('POST','https://baijiahao.baidu.com/builder/author/article/publish',['headers'=>[
            'Cookie'=>$account_info['cookie'],
            'Token'=>$token
        ],'form_params' => $pub_data]);
        $res_z = $resp->getBody()->getContents();
        $token = $resp->getHeader('Token')[0];
        PlatformAccounts::where('id', $account_info['id'])->update(['token'=>$token]);
        $res_arr = json_decode($res_z,true);
        \Log::info($res_z);
        if($res_arr['errno'] == 0){
            $publish_res_info = $res_arr['ret'];
            $article_id = $publish_res_info['article_id'];
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>$article_id,
                'contents_info'=>json_encode($pub_data,JSON_UNESCAPED_UNICODE),
                'response'=>$res_z
            ];
            if(isset($account['formData']['send_time'])&& !empty($account['formData']['send_time'])) {
                //$pub_data['timer_time'] = strtotime($account_info['formData']['send_time']);
                $publish_data['timer'] = $account['formData']['send_time'];
                $publish_data['status'] = 'UPLOADING';
            }
            Publishes::create($publish_data);
            return response()->success();
        }else{
            //$publish_res_info = $res_arr['ret'];
            $article_id = 0;//$publish_res_info['article_id'];
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>$article_id,
                'contents_info'=>json_encode($pub_data,JSON_UNESCAPED_UNICODE),
                'response'=>$res_z,
                'status'=>'UPLOAD_FAILED',
                'reason'=>$res_arr['errmsg']
            ];
            if(isset($account['formData']['send_time'])&& !empty($account['formData']['send_time'])) {
                //$pub_data['timer_time'] = strtotime($account_info['formData']['send_time']);
                $publish_data['timer'] = $account['formData']['send_time'];
                $publish_data['status'] = 'UPLOADING';
            }
            Publishes::create($publish_data);
        }
        return response()->error('PUBLISH_FAILED');
    }

    public static function publishWeibo($account,$account_info) {
        [
            ['label'=>'	封面','field'=>'cover','type'=>'image','required'=>1],
            ['label'=>'	文章导语','field'=>'summary','type'=>'text','required'=>0],
            ['label'=>'	与其绑定短微博内容','field'=>'text','type'=>'text','required'=>0],
        ];
        $title = $account['formData']['title'];
        $content = rawurlencode($account['formData']['content']);
        $cover = $account['formData']['cover'];
        $summary = $account['formData']['summary'];
        $text = $account['formData']['text'];
        $access_token = $account_info['access_token'];
        $client = new Client();
        $response = $client->request('POST','https://api.weibo.com/proxy/article/publish.json',['form_params'=>compact('title','content','cover','summary','text','access_token')]);
        $res = $response->getBody()->getContents();
        \Log::info($res);
        $res_arr = json_decode($res,true);
        if($res_arr['code']==100000){
            $object_id = $res_arr['data']['object_id'];
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>$object_id,
                'contents_info'=>json_encode(compact('title','content','cover','summary','text','access_token'),JSON_UNESCAPED_UNICODE),
                'response'=>$res,
                'status'=>'REVIEW_SUCCESS'
            ];
            Publishes::create($publish_data);
            return response()->success();
        }else{
            //$object_id = $res_arr['data']['object_id'];
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>0,
                'contents_info'=>json_encode(compact('title','content','cover','summary','text','access_token'),JSON_UNESCAPED_UNICODE),
                'response'=>$res,
                'status'=>'UPLOAD_FAILED',
                'reason'=>$res_arr['msg']
            ];
        }
        return $res_arr;
    }

    public static function publishKuaichuan($account,$account_info) {
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $image_style =(isset($account['formData']['image_style']) && !empty($account['formData']['image_style']))?$account['formData']['image_style']:0;
        $realm = $account['formData']['realm'];
        $post_data = [];
        $post_data['image_style'] = $image_style;
        if(isset($account['formData']['image_list']) && count($account['formData']['image_list'])>0){
            \Log::info('image_list:'.json_encode($account['formData']['image_list']));
            foreach ($account['formData']['image_list'] as $key=>$each){
                $post_data['image_list['.$key.']'] =$each;
            }
        }else{
            $post_data['image_style'] = 0;
        }
        $post_data['title'] = $title;
        $post_data['content'] = $content;

        $post_data['exclusive'] = 0;
        $post_data['status_fixed_time'] = 0;
        $post_data['fixed_time'] = 0;
        $post_data['is_promot'] = 0;
        $post_data['realm'] = $realm;

        if(isset($account['formData']['send_time']) && !empty($account['formData']['send_time'])) {
            $post_data['fixed_time'] = $account['formData']['send_time'];
            $post_data['status_fixed_time'] = 1;
        }
        $client = new Client();
        $response_token = $client->request('GET','http://kuaichuan.360.cn/token/gettoken',['headers'=>[
            'Cookie'=>$account_info['cookie'],'Referer'=>'http://kuaichuan.360.cn/','User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36','Host'=>'kuaichuan.360.cn','Origin'=>'http://kuaichuan.360.cn'
        ]]);
        $token_raw = $response_token->getBody()->getContents();
        $response_headers = $response_token->getHeader('Set-Cookie');
        \Log::info($response_headers);
        if($response_headers){
            $cookie_str = self::extractCookie($response_headers);
            $account_info['cookie'].=';'.$cookie_str;
            PlatformAccounts::where('id',$account_info->id)->update(['cookie'=>$account_info['cookie']]);
        }
        \Log::info('token--:'.$token_raw);
        \Log::info('$account_info[\'cookie\']:'.$account_info['cookie']);
        $token_arr = json_decode($token_raw,true);
        if($token_arr['errno']==0) {
            $token = $token_arr['data'];
            $post_data['token'] = $token;
            $response = $client->request('POST','http://kuaichuan.360.cn/articleManage/publish',[
                'headers'=>[
                    'Cookie'=>$account_info['cookie'],'Referer'=>'http://kuaichuan.360.cn/','User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36','Host'=>'kuaichuan.360.cn','Origin'=>'http://kuaichuan.360.cn'
                ],
                'form_params'=>$post_data
            ]);
            $json_re = $response->getBody()->getContents();
            \Log::info('json_re:'.$json_re);
            $res = json_decode($json_re,true);
            if($res['errno'] == 0) {
                $id =  $res['data']['id'];
                $publish_data = [
                    'title'=>$account['formData']['title'],
                    'platform_id'=>$account_info->platform_id,
                    'platform_account_id'=>$account_info->id,
                    'user_id'=>$account_info->user_id,
                    'content_id'=>$id,
                    'contents_info'=>json_encode($post_data,JSON_UNESCAPED_UNICODE),
                    'response'=>$json_re
                ];
                if(isset($account['formData']['send_time'])) {
                    $publish_data['status'] = 'UPLOADING';
                    $publish_data['timer'] = $account['formData']['send_time'];
                }
                Publishes::create($publish_data);
                return response()->success();
            }else{
                $publish_data = [
                    'title'=>$account['formData']['title'],
                    'platform_id'=>$account_info->platform_id,
                    'platform_account_id'=>$account_info->id,
                    'user_id'=>$account_info->user_id,
                    'content_id'=>0,
                    'contents_info'=>json_encode($post_data,JSON_UNESCAPED_UNICODE),
                    'response'=>$json_re,
                    'status'=>'UPLOAD_FAILED',
                    'reason'=>$res['errmsg']
                ];
                if(isset($account['formData']['send_time'])) {
                    $publish_data['status'] = 'UPLOADING';
                    $publish_data['timer'] = $account['formData']['send_time'];
                }
                Publishes::create($publish_data);
            }
        }
        return response()->error('PUBLISH_FAILED');
    }

    public static function publishDayuhao($account,$account_info) {
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $image_style = $account['formData']['image_style'];
        $image_list = $account['formData']['image_list'];
        $author = isset($account['formData']['author'])?$account['formData']['author']:'';
        $extra_info = json_decode($account_info['extra_info'],true);
        $utoken = $extra_info['utoken'];
        $client = new Client();
        $params = ['title'=>$title,'content'=>$content,'author'=>$author,'labor_activity_mode'=>1,'article_type'=>1,'utoken'=>$utoken,'cover_from'=>'','coverImg'=>'','origin_cover_url'=>''];
        if($image_style == 1 && count($image_list)>0) {
            $params['cover_from'] ='auto';
            $params['coverImg'] =$image_list[0];
            $params['origin_cover_url'] = $image_list[0];
        }
        if($image_style == 3 && count($image_list)>0) {
            foreach ($image_list as $key=>$each) {
                $params['covers['.$key.'][url]'] = $each;
                $params['covers['.$key.'][srcUrl]'] = $each;
                $params['covers['.$key.'][from]'] = 'auto';
            }
        }
        if(isset($account['formData']['send_time'])&& !empty($account['formData']['send_time'])) {
            //is_timed_release: true
            //time_for_release: 1585130433000
            $params['is_timed_release'] = true;
            $params['time_for_release'] = strtotime($account['formData']['send_time'])*1000;
        }
        $response_draft = $client->request('POST','https://mp.dayu.com/dashboard/save-draft',[
            'headers'=>['Cookie'=>$account_info['cookie'],'utoken'=>$utoken,'Referer'=>'https://mp.dayu.com/dashboard/article/write?spm=a2s0i.db_contents.menu.4.67c73caa7zgS26','X-Requested-With'=>'XMLHttpRequest'],
            'form_params'=>$params
        ]);
        $res_draft = $response_draft->getBody()->getContents();
        $res = json_decode($res_draft,true);
        \Log::info('dayuhao_draft:'.$res_draft);
        if(isset($res['data'])) {
            $draft_id = $res['data']['_id'];
            $res_publish = $client->request('POST','https://mp.dayu.com/dashboard/submit-article',[
                'headers'=>['Cookie'=>$account_info['cookie'],'utoken'=>$utoken,'Referer'=>'https://mp.dayu.com/dashboard/article/write?spm=a2s0i.db_contents.menu.4.67c73caa7zgS26','X-Requested-With'=>'XMLHttpRequest'],
                'form_params'=>[
                    'dataList[0][_id]'=>$draft_id,
                    'dataList[0][isDraft]'=>1,
                    'dataList[0][reproduce]'=>'',
                    'dataList[0][goods_num]'=>0,
                    'curDaySubmit'=>'false',
                    'utoken'=>$utoken
                ]
            ]);
            $res = $res_publish->getBody()->getContents();
            \Log::info('dayuhao-publish-res:'.$res);
            $status = $res_publish->getStatusCode();
            if($status == 200) {
                $id = $draft_id;
                $publish_data = [
                    'title'=>$account['formData']['title'],
                    'platform_id'=>$account_info->platform_id,
                    'platform_account_id'=>$account_info->id,
                    'user_id'=>$account_info->user_id,
                    'content_id'=>$id,
                    'contents_info'=>json_encode($params,JSON_UNESCAPED_UNICODE),
                    'response'=>$res
                ];
                Publishes::create($publish_data);
                return response()->success();
            }
        }else{
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>0,
                'contents_info'=>json_encode($params,JSON_UNESCAPED_UNICODE),
                'response'=>json_encode($res),
                'status'=>'UPLOAD_FAILED',
                'reason'=>isset($res['error']['message'])?$res['error']['message']:$res['error']
            ];
            Publishes::create($publish_data);
        }
        return response()->error('PUBLISH_FAILED');
    }

    public static function publishToutiao($account,$account_info) {
        $res = [
            ['label'=>'封面类型','field'=>'image_style','type'=>'radio','require'=>1,'type_option'=>[
                ['label'=>'单图','field'=>1],
                ['label'=>'三图','field'=>3],
                ['label'=>'无图','field'=>0,'default'=>1]
            ]],
            ['label'=>'封面','field'=>'pgc_feed_covers','type'=>'image','require'=>1],
            ['label'=>'广告设置','field'=>'article_ad_type','type'=>'radio','require'=>1,'type_option'=>[
                ['label'=>'投放头条广告','field'=>3],
                ['label'=>'不投放广告','field'=>2],

            ]],
        ];
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $image_style = $account['formData']['image_style'];
        $image_list = $account['formData']['pgc_feed_covers'];
        $ad_type = $account['formData']['article_ad_type'];
        $gpc_covers = [];
        foreach ($image_list as $key=>$each) {
            $url = $each;                                                           //
            $each_info = json_decode(Redis::get('toutiao_img:'.$each),true); //[{"id":1121210992937.8523,"url":"http://p1.pstatp.com/list/pgc-image/94b6c3a5dc2f47f1aa36c64450ad36cc","uri":"pgc-image/94b6c3a5dc2f47f1aa36c64450ad36cc","thumb_width":1928,"thumb_height":1168,"ic":false}]
            $gpc_covers[] = array(
                'url'=>$each,
                'uri'=>$each_info['web_uri'],
                'thumb_width'=>$each_info['width'],
                'thumb_height'=>$each_info['height'],
                "ic"=>false
            );
        }
        $gpc_covers = json_encode($gpc_covers);
        $client = new Client();
        $response = $client->request('POST','https://mp.toutiao.com/mp/agw/article/publish?source=mp&type=article',[
            'headers'=>[
                'Cookie'=>$account_info['cookie'],
                'Host'=>'mp.toutiao.com',
                'Origin'=>'https://mp.toutiao.com',
                'Referer'=>'https://mp.toutiao.com/profile_v3/graphic/publish',
                'Sec-Fetch-Dest'=>'empty',
                'Sec-Fetch-Mode'=>'cors',
                'Sec-Fetch-Site'=>'same-origin',
                'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36'
            ],
            'form_params'=>[
                'title'=>$title,
                'content'=>$content,
                'pgc_feed_covers'=>$gpc_covers,
                'article_type'=> 0,
                'source'=> 0,
                'article_ad_type'=>$ad_type,
                'save'=>1
            ]
        ]);
        $response_r = $response->getBody()->getContents();
        $res_arr = json_decode($response_r,true);
        if($res_arr['code'] == 0) {
            \Log::info($res_arr);
            $gpc_id = $res_arr['data']['pgc_id'];
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>$gpc_id,
                'contents_info'=>json_encode([
                    'title'=>$title,
                    'content'=>$content,
                    'pgc_feed_covers'=>$gpc_covers,
                    'article_type'=> 0,
                    'source'=> 0,
                    'article_ad_type'=>$ad_type,
                    'save'=>1
                ],JSON_UNESCAPED_UNICODE),
                'response'=>$response_r
            ];
            Publishes::create($publish_data);
            return response()->success();
        }elseif($res_arr['code'] ==2222){
            $response = $client->request('POST','https://mp.toutiao.com/mp/agw/article/publish?source=mp&type=article',[
                'headers'=>['Cookie'=>$account_info['cookie']],
                'form_params'=>[
                    'title'=>$title,
                    'content'=>$content,
                    'pgc_feed_covers'=>$gpc_covers,
                    'article_type'=> 0,
                    'source'=> 0,
                    'article_ad_type'=>$ad_type,
                    'save'=>1
                ]
            ]);
            $response_r2 = $response->getBody()->getContents();
            $res_arr2 = json_decode($response_r2,true);
            if($res_arr2['code'] == 0) {
                // $gpc_id = $res_arr['data']['gpc_id'];
                $gpc_id = $res_arr['data']['pgc_id'];
                $publish_data = [
                    'title'=>$account['formData']['title'],
                    'platform_id'=>$account_info->platform_id,
                    'platform_account_id'=>$account_info->id,
                    'user_id'=>$account_info->user_id,
                    'content_id'=>$gpc_id,
                    'contents_info'=>json_encode([
                        'title'=>$title,
                        'content'=>$content,
                        'pgc_feed_covers'=>$gpc_covers,
                        'article_type'=> 0,
                        'source'=> 0,
                        'article_ad_type'=>$ad_type,
                        'save'=>1
                    ],JSON_UNESCAPED_UNICODE),
                    'response'=>$response_r
                ];
                Publishes::create($publish_data);
                return response()->success();
            }else{
                $publish_data = [
                    'title'=>$account['formData']['title'],
                    'platform_id'=>$account_info->platform_id,
                    'platform_account_id'=>$account_info->id,
                    'user_id'=>$account_info->user_id,
                    'content_id'=>0,
                    'contents_info'=>json_encode([
                        'title'=>$title,
                        'content'=>$content,
                        'pgc_feed_covers'=>$gpc_covers,
                        'article_type'=> 0,
                        'source'=> 0,
                        'article_ad_type'=>$ad_type,
                        'save'=>1
                    ],JSON_UNESCAPED_UNICODE),
                    'response'=>$response_r,
                    'status'=>'UPLOAD_FAILED',
                    'reason'=>$res_arr2['message']
                ];
                Publishes::create($publish_data);
            }
        }else{
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>0,
                'contents_info'=>json_encode([
                    'title'=>$title,
                    'content'=>$content,
                    'pgc_feed_covers'=>$gpc_covers,
                    'article_type'=> 0,
                    'source'=> 0,
                    'article_ad_type'=>$ad_type,
                    'save'=>1
                ],JSON_UNESCAPED_UNICODE),
                'response'=>$response_r,
                'status'=>'UPLOAD_FAILED',
                'reason'=>$res_arr['message']
            ];
            Publishes::create($publish_data);
        }
        \Log::error($response_r);
        return response()->error('PUBLISH_FAILED');
    }
    public static function publishSougou($account,$account_info) {
        $res = [
            ['label'=>'分类','field'=>'category','type'=>'select','require'=>1,'type_option'=>[
                ['field'=>"娱乐",'label'=> "娱乐"],
                ['field'=>"社会",'label'=> "社会"],
                ['field'=>"历史",'label'=> "历史"],
                ['field'=>"搞笑",'label'=> "搞笑"],
                ['field'=>"教育",'label'=> "教育"],
                ['field'=>"房产",'label'=> "房产"],
                ['field'=>"时尚",'label'=> "时尚"],
                ['field'=>"游戏",'label'=> "游戏"],
                ['field'=>"美食",'label'=> "美食"],
                ['field'=>"汽车",'label'=> "汽车"],
                ['field'=>"情感",'label'=> "情感"],
                ['field'=>"军事",'label'=> "军事"],
                ['field'=>"收藏",'label'=> "收藏"],
                ['field'=>"旅游",'label'=> "旅游"],
                ['field'=>"财经",'label'=> "财经"],
                ['field'=>"健康",'label'=> "健康"],
                ['field'=>"宠物",'label'=> "宠物"],
                ['field'=>"职场",'label'=> "职场"],
                ['field'=>"育儿",'label'=> "育儿"],
                ['field'=>"科学",'label'=> "科学"],
                ['field'=>"健身",'label'=> "健身"],
                ['field'=>"科技",'label'=> "科技"],
                ['field'=>"影视",'label'=> "影视"],
                ['field'=>"动漫",'label'=> "动漫"],
                ['field'=>"家居",'label'=> "家居"],
                ['field'=>"体育",'label'=> "体育"],
                ['field'=>"摄影",'label'=> "摄影"],
                ['field'=>"星座",'label'=> "星座"],
                ['field'=>"生活",'label'=> "生活"],
                ['field'=>"三农",'label'=> "三农"],
                ['field'=>"国际",'label'=> "国际"],
                ['field'=>"文化",'label'=> "文化"],
                ['field'=>"本地",'label'=> "本地"],
                ['field'=>"音乐",'label'=> "音乐"],
                ['field'=>"艺术",'label'=> "艺术"],
                ['field'=>"心理",'label'=> "心理"],
                ['field'=>"才艺",'label'=> "才艺"],
                ['field'=>"传媒",'label'=> "传媒"],
                ['field'=>"创意",'label'=> "创意"],

            ]],
        ];
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $category = $account['formData']['category'];
        $patern = "/(\<p>|\<h1>|\<div>)[\s]*\<img.+?src=[\"|\'](.+?)[\"|\'].*?>[\s]*(\<\/p>|\<\/h1>|\<\/div>)/";

        $patern2 = "/(\<p>)(([^\<][^\/][^p>])+?)(\<img.+?>[\s]*(\<\/p>))/i";
        $patern3 = "/(\<p>)[\s]*(\<img.+?>)(([^\<][^\/][^p>])+?)(\<\/p>)/";

        $res1 = preg_replace($patern2, '$1$2</p><p>$4', $content);
        $res2 = preg_replace($patern3, '$1$2</p><p>$3$5', $res1);

        $patern4= "/((\<p>|\<div>|\<h1>)(.+?)(\<\/p>|\<\/div>|\<\/h1>))/";
        preg_match_all($patern4, $res2,$matched);
        #preg_match($patern2,$content,$matched);
        $block = [];
        $imgs = [];
        $img_index=0;
        if(count($matched)>0) {
            foreach ($matched[0] as $key => $match) {
                # code...
                $key = self::getRandStr(5);
                if(preg_match($patern, $match,$new_matches)) {
                    $img_src = $new_matches[2];
                    $block[]= array(
                        'key'=>$key,
                        'text'=>'',
                        'type'=>'atomic',
                        'depth'=>0,
                        "inlineStyleRanges"=> [],
                        "entityRanges"=> [[
                            "offset"=> 0,
                            "length"=> 1,
                            "key"=> $img_index
                        ]],
                        'data'=>[]
                    );
                    $img_info = Redis::hGetAll('sougou_img:'.$img_src);
                    $data_img=[
                        'src'=>$img_src,
                    ];
                    if($img_info) {
                        $data_img['width']=$img_info['width'];
                        $data_img['height']=$img_info['height'];
                        $data_img['type']=$img_info['type'];
                    }
                    $imgs["$img_index"] = [
                        'type'=>'image',
                        'mutability'=>'IMMUTABLE',
                        'data'=>$data_img
                    ];
                    $img_index++;
                }else{
                    $res_filter = preg_replace('/\<\/?.+?\/?>/','', $match);
                    //$res_filter = $match;
                    $block[]= array(
                        "key"=>$key,
                        "text"=>$res_filter,
                        "type"=>"unstyled",
                        "depth"=>0,
                        "inlineStyleRanges"=>[],
                        "entityRanges"=>[],
                        "data"=>[]
                    );
                }
            }

        }
        $data = [
            'blocks'=>$block,
            'entityMap'=>$imgs
        ];
        $content = json_encode($data);
        \Log::info('content:'.$content);
        $form_data = [
            'id'=>'',
            'title'=>$title,
            'category'=>$category,
            'content'=>$content,
            'type'=>0,
            'topic_activity'=>0,
            'isFetching'=>'false'
        ];
        if(isset($account['formData']['send_time'])) {
            $form_data['timed_publish_at'] = strtotime($account['formData']['send_time']);
        }
        $client = new Client();
        $response = $client->request('POST','http://mp.sogou.com/api/articles',[
            'headers'=>[
                'Cookie'=>$account_info['cookie'],'Host'=> 'mp.sogou.com',
                'Origin'=>'http://mp.sogou.com',
                'Referer'=>'http://mp.sogou.com/new-article',
                'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
            ],
            'form_params'=>$form_data
        ]);
        $response_r = $response->getBody()->getContents();
        \Log::info('r--:'.$response_r);
        if($response_r>0) {
            $id = $response_r;
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>$id,
                'contents_info'=>json_encode($form_data,JSON_UNESCAPED_UNICODE),
                'response'=>$response_r
            ];
            if(isset($account['formData']['send_time'])) {
                $publish_data['status'] = 'UPLOADING';
                $publish_data['timer'] = $account['formData']['send_time'];
            }
            Publishes::create($publish_data);
            return response()->success();
        }else{
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>0,
                'contents_info'=>json_encode($form_data,JSON_UNESCAPED_UNICODE),
                'response'=>$response_r,
                'status'=>'UPLOAD_FAILED',
                'reason'=>'未知错误'
            ];
            if(isset($account['formData']['send_time'])) {
                $publish_data['status'] = 'UPLOADING';
                $publish_data['timer'] = $account['formData']['send_time'];
            }
            Publishes::create($publish_data);
        }
        return response()->error('PUBLISH_FAILED');
    }

    public static function publishSouhu($account,$account_info) {
        [
            ['label'=>'	封面','field'=>'cover','type'=>'image','required'=>1],
            ['label'=>'	摘要','field'=>'brief','type'=>'text','required'=>0],
            ['label'=>'	文章属性','field'=>'attrIds','type'=>'radio','required'=>1,'type_option'=>[
                ['label'=>'观点评论','field'=>9902559],
                ['label'=>'故事传记','field'=>9902563],
                ['label'=>'消息资讯','field'=>14218031],
                ['label'=>'八卦爆料','field'=>14218032],
                ['label'=>'经验教程','field'=>14218033],
                ['label'=>'科普知识','field'=>14218034],
                ['label'=>'测评盘点','field'=>14218035],
                ['label'=>'见闻记录','field'=>14218036],
                ['label'=>'运势','field'=>14218037],
                ['label'=>'搞笑段子','field'=>14218038],
                ['label'=>'美图','field'=>14218039	],
                ['label'=>'美文','field'=>14218040],
            ]],
        ];
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $cover = $account['formData']['cover'][0];
        $attrIds = $account['formData']['attrIds'];
        $brief = $account['formData']['brief'];
        $accountId =$account_info['platform_user_id'];
        $client = new Client();
        $response = $client->request('POST','https://mp.sohu.com/v3/news/publish',[
            'headers'=>['Cookie'=>$account_info['cookie']],
            'form_params'=>compact('title','content','cover','attrIds','brief','accountId')
        ]);
        $res = $response->getBody()->getContents();
        \Log::info('res'.$res);
        if($res>0) {
            $response2 = $client->request('GET','https://mp.sohu.com/v3/users/newsList?psize=50&containTop=false&newsType=1&statusType=1&columnId=&pno=1&accountId='.$accountId,['headers'=>['Cookie'=>$account_info['cookie']]]);
            $response2_r = $response2->getBody()->getContents();
            $arr = json_decode($response2_r,true);
            if(isset($arr['news'])) {
                $publish_info = $arr['news'][0];
                $id = $publish_info['id'];
                $publish_data = [
                    'title'=>$account['formData']['title'],
                    'platform_id'=>$account_info->platform_id,
                    'platform_account_id'=>$account_info->id,
                    'user_id'=>$account_info->user_id,
                    'content_id'=>$id,
                    'contents_info'=>json_encode(compact('title','content','cover','attrIds','brief','accountId'),JSON_UNESCAPED_UNICODE),
                    'response'=>json_encode($arr['news'][0]),

                ];
                Publishes::create($publish_data);
            }
            return response()->success();
        }else{
            $publish_data = [
                'title'=>$account['formData']['title'],
                'platform_id'=>$account_info->platform_id,
                'platform_account_id'=>$account_info->id,
                'user_id'=>$account_info->user_id,
                'content_id'=>0,
                'contents_info'=>json_encode(compact('title','content','cover','attrIds','brief','accountId'),JSON_UNESCAPED_UNICODE),
                'response'=>$res,
                'status'=>'UPLOAD_FAILED',
                'reason'=>$res
            ];
            Publishes::create($publish_data);
        }
        return response()->error('PUBLISH_FAILED');
    }

    public static function getQQAccessToken($uid) {
        //$appid = '1c805ef8c87d1d46e1375d08728fde77';
        $appid = 'b765aa5fca64fe39c4d3e43a1c2392be';
        $refresh_token = Redis::hGet('qq_om_access_token_info:'.$uid,'refresh_token');
        $url = 'https://auth.om.qq.com/omoauth2/refreshtoken?grant_type=refreshtoken&client_id='.$appid.'&refresh_token='.$refresh_token;
        $client = new Client();
        $response = $client->request('POST',$url);
        $res = $response->getBody()->getContents();
        $res = json_decode($res,true);
        if($res['code']==0) {
            $access_token = $res['data']['access_token'];
            $refresh_token = $res['data']['refresh_token'];
            Redis::hMSet('qq_om_access_token_info:'.$uid,['access_token'=>$access_token,'refresh_token'=>$refresh_token]);
            return $access_token;
        }
        return false;
    }

    public static function publishDazhong($account,$account_info) {
        $res = [

            ['label'=>'封面','field'=>'headPic','type'=>'image','require'=>1],

        ];
        $title = $account['formData']['title'];
        $content = $account['formData']['content'];
        $headPic = $account['formData']['headPic'];
        $patern = "/(\<p>|\<h1>|\<div>)[\s]*\<img.+?src=[\"|\'](.+?)[\"|\'].*?>[\s]*(\<\/p>|\<\/h1>|\<\/div>)/";
        $patern2 = "/(\<p>)(([^\<][^\/][^p>])+?)(\<img.+?>[\s]*(\<\/p>))/i";
        $patern3 = "/(\<p>)[\s]*(\<img.+?>)(([^\<][^\/][^p>])+?)(\<\/p>)/";
        $res1 = preg_replace($patern2, '$1$2</p><p>$4', $content);
        $res2 = preg_replace($patern3, '$1$2</p><p>$3$5', $res1);
        $patern4= "/((\<p>|\<div>|\<h1>)(.+?)(\<\/p>|\<\/div>|\<\/h1>))/";
        preg_match_all($patern4, $res2,$matched);

        $data_tmp = [];
        if(count($matched)>0) {
            foreach ($matched[0] as $key => $match) {

                if(preg_match($patern, $match,$new_matches)) {
                    $img_src = $new_matches[2];

                    $data_tmp[]=[
                        "t"=>1,
                        "c"=>[
                                [
                                "t"=>6,"iu"=>$img_src,
                                "ex"=>[
                                    "picKey"=>$img_src
                                ]
                             ],
                             ["t"=>12]
                        ]
                    ];
                }else{
                    $res_filter = preg_replace('/\<\/?.+?\/?>/','', $match);
                    //$res_filter = $match;
                    $data_tmp[] = ["t"=>1,"c"=>[["t"=>4,"x"=>$res_filter],["t"=>12]]];
                }
            }

        }
        $data_format = ["t"=>0,"c"=>$data_tmp];
        $content = json_encode($data_format);
        \Log::info('content:'.$content);
        $client = new Client();
        $res_list = $client->request('GET','https://www.dianping.com/headline/ajax/list?pageIndex=1&limit=10&kw=',[
            'headers'=>['Cookie'=>$account_info['cookie']]
        ]);
        $res_list_r =$res_list->getBody()->getContents();
        $res_list_r_json = json_decode($res_list_r,true);
        \Log::info('res_list:'.$res_list_r);
        if($res_list_r_json && $res_list_r_json['code']==200) {
            $cToken = $res_list_r_json['cToken'];
            $url = 'https://m.dianping.com/toplist/headline/updateHeadline';
            $response = $client->request('POST',$url,[
                'headers'=>[
                    'Cookie'=>$account_info['cookie'],
                    'cToken'=>$cToken
                ],
                'form_params'=>[
                    'title'=>$title,
                    'content'=>$content,
                    'action'=> 'add',
                    'id'=> 0,
                    'headPic'=>$headPic[0],
                    'tagId'=> 0,
                    'shopIds'=>'',
                    'shareSubTitle'=>''
                ]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            \Log::info('$res:'.$res);
            if($res_arr['code']==200) {
                return response()->success();
            }
        }
        return response()->error('PUBLISH_ERROR');
    }

    private static function getRandStr($length) {
        //取随机10位字符串
        $strs="QWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnm";
        $name=substr(str_shuffle($strs),mt_rand(0,strlen($strs)-($length+1)),$length);
        return $name;
    }
    /**
     * 提取有效cookie
     * @param $header
     * @return string
     */
    private static function extractCookie($header) {
        $cookie_filter_arr = [];
        foreach($header as $key=>$cookie) {
            $each_cookie = explode(';',$cookie);
            $cookie_filter_arr[] = $each_cookie[0];
        }
        $cookie_str = implode(';',$cookie_filter_arr);
        return $cookie_str;
    }
}