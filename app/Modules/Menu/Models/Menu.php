<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/12
 * Time: 16:49
 */
namespace App\Modules\Menu\Models;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model {
    //menu

    protected $table = 'promotion_groups_menus';

    protected $fillable = ['pid','name','desc','link','state','created_at'];
}