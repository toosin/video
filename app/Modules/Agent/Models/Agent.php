<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/12
 * Time: 16:49
 */
namespace App\Modules\Agent\Models;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model {
    protected $table = 'promotion_groups_agent_info';

    protected $fillable = ['id','distribution_channel_id','agent_name','agent_intro','created_at','updated_at'];

    static function getAgentByChannelId($distribution_channel_id){
        return self::where('distribution_channel_id',$distribution_channel_id)->first();
    }
}