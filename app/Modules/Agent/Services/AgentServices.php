<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/1/23
 * Time: 9:51
 */

namespace App\Modules\Agent\Services;

use App\Modules\Agent\Models\Agent;

class AgentServices
{
    static function getAgentByChannelId($distribution_channel_id)
    {
        return Agent::getAgentByChannelId($distribution_channel_id);
    }
}