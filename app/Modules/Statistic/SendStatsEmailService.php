<?php
/**
 * Created by PhpStorm.
 * User: z_yang
 * Date: 2018/03/09
 * Time: 20:18
 */

namespace App\Modules\Statistic;

use DB;
use PHPMailer\PHPMailer\PHPMailer;

class SendStatsEmailService
{
    public static function SendHtmlEmailWithAcce(array $to_email, array $param, $accessory = '')
    {
        $mail = new PHPMailer(true);// Passing `true` enables exceptions
        $mail->CharSet = 'UTF-8';//'UTF-8';
        try {
            //Server settings
            $mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.126.com';     //$mail->Host = 'smtp.126.com'; Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'tushengxiang@126.com'; //$mail->Username = 'tushengxiang@126.com';                // SMTP username
            $mail->Password = 'personsincere199';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('tushengxiang@126.com', 'tusx');
            $mail->addAddress($to_email[0]['address'], $to_email[0]['name']);
            array_shift($to_email);
            foreach ($to_email as $item) {
                $mail->addCC($item['address'], $item['name']);     // Add a recipient
            }

            //Attachments
            if ($accessory) $mail->addAttachment($accessory);// Add attachments

            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $param['subject'];
            $mail->Body = $param['body'];
            $mail->send();
            //echo 'Message has been sent';
        } catch (\Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }
}