<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\PublishedStatusQuery::class,
        Commands\BindedStatusQuery::class,
        Commands\consumeVideoPublish::class,
        Commands\temp_task::class,
        Commands\loginValidTimeQuery::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('query_published_status')->everyMinute();
        $schedule->command('bind_status_query')->everyMinute();
        $schedule->command('query_valid_time')->everyFiveMinutes();
    }
}
