<?php
/**
 * Created by PhpStorm.
 * User: tandunzhao
 * Date: 2017/11/20
 * Time: 下午5:26
 */

namespace App\Console\Commands;

use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Publishes;
use App\Modules\User\Models\UserSubscribeBehaviorStats;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use Log;
use GuzzleHttp\Promise;
use Psr\Http\Message\ResponseInterface;
use Sunra\PhpSimple\HtmlDomParser;

class loginValidTimeQuery extends Command
{
    /**
     * 执行命令
     *
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'query_valid_time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $platform_accounts = PlatformAccounts::where('status',0)
            ->whereNotNull('binded_time')
            ->whereNotNull('due_time')
            ->get();
        foreach ($platform_accounts as $key=>$platform_account) {
            if(strtotime($platform_account->due_time)>strtotime($platform_account->binded_time)) {
                $diff = strtotime($platform_account->due_time) - strtotime($platform_account->binded_time);
                $diff_hour = round($diff/3600,3);
                $platform_account->valid_time = $diff_hour;
                $platform_account->save();
            }
        }
    }

}