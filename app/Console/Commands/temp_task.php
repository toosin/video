<?php
/**
 * Created by PhpStorm.
 * User: tandunzhao
 * Date: 2017/11/20
 * Time: 下午5:26
 */

namespace App\Console\Commands;

use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Publishes;
use App\Modules\Platform\Services\ReleaseDouyinServices;
use App\Modules\User\Models\UserSubscribeBehaviorStats;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use Log;
use GuzzleHttp\Promise;
use Psr\Http\Message\ResponseInterface;
use Sunra\PhpSimple\HtmlDomParser;

class temp_task extends Command
{
    /**
     * 执行命令 获取站点的派单数据及服务号信息
     *
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'temp_tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ReleaseDouyinServices::release1();
        return ;
        $client = new client();
        $cookie = 'canary=0; PSTM=1576647166; BIDUPSID=B706D3FB3716932EB7F7DE06F1B02776; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1576759113; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1576759132; BAIDUID=26D48277EA44B2AD5BE8C5D34095AED2:FG=1; gray=1; people=0; BDSFRCVID=o3FsJeCCxG3e9l6uhBeOa2pxvhfpzuq1UpSA3J; H_BDCLCKID_SF=JJkO_D_atKvjDbTnMITHh-F-5fIX5-RLfKQga4OF5lOTJh0RbqobyR-Qeq0fBJv3yeOj0tJLb4DaStJbLjbke65WeaDHt6tsKKJ03bk8KRREJt5kq4bohjPRehOeBtQm05bxob5jJbOhh-J3hpPaXlk1MMjjhJQDtGTjbhDbWDFabnnphPRHhCCV-frb-C62aKDS0t_B-nOrq-FrhlbzKUI8LPbO05J-3HTILloG5bndHt3gbfrtL60lDecMyMQTt2LE_KLafCDKMKvCMJ0_MJ_t2fuX5-RLf5nuLl7F5l8-hxom-JrYjnLIy2FD2-r3Lm-HWn0hyUoxOKQpytQN25bQLtrqL4r92GcmLh5N3KJme4P9bT3v5DuI0p5G2-biW2JM2MbdLP5P_IoG2Mn8M4bb3qOpBtQmJeTxoUJ25DnJhhCGe6Dbe55XjH-s5-70KK0X3RRVM45_KROvhjRoDUKyyxom3bvxtKI8WIQa2poq8nD4Kb7-M-KQ2loDJlQjL2CeaDcJ-J8XhC04jjQP; H_WISE_SIDS=138596_114550_135846_141001_139148_140853_141437_141676_137758_138878_137979_141200_140173_131246_132552_137743_138165_107317_138883_140259_141753_140201_139296_136863_138585_141651_138253_140113_136196_140324_140578_133847_140792_140065_134047_131423_140312_140966_136537_141104_110085_127969_140268_141532_140593_140864_139882_138426_138941_140682_141190_140597; BDUSS=1VYOWRFT0QxZWNoTXVHNVR3aXlLTVEyYzB2bFl5WlFpZzl6aXl0NDZZVTI4R3BlSVFBQUFBJCQAAAAAAAAAAAEAAADJVh4FNTQ4MjM5MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADZjQ142Y0NeV; H_PS_PSSID=; BDORZ=FFFB88E999055A3F8A630C64834BD6D0; delPer=0; PSINO=6; bjhStoken=d38b5ee093f4425236f4eaab789bb9ac54e7add2a64d7bdfdee28437a84b3f5c; devStoken=65a3859a5deb8c6a788b96a85b7ba0df54e7add2a64d7bdfdee28437a84b3f5c';
        //获取视频分类下发的token
        $response=$client->request('GET','https://baijiahao.baidu.com/builder/author/video/videoCategory?',[
            'headers'=>[
                'Cookie'=>$cookie
            ]

        ]);
        $token = $response->getHeader('Token');
        print_r('haokan--token--'.json_encode($token[0])."\r\n");
            //\Log::info('haokan--videocategory--'.json_encode($response->getBody()->getContents())."\n");
        if(!$token){
            return response()->error('PUBLISH_FAILED');
        }

        //先获取appid
        $response = $client->request('GET',"https://baijiahao.baidu.com/builder/app/appinfo",[
            'headers'=>[
                'Cookie'=>$cookie,
            ]
        ]);
        $userInfo = json_decode($response->getBody()->getContents(),true);
        print_r('haokan--userinfo--'.json_encode($userInfo,JSON_UNESCAPED_UNICODE)."\r\n");
        if($userInfo['errno'] !== 0){
            return response()->error('PUBLISH_FAILED');
        }
        $appid = $userInfo['data']['user']['app_id'];
        //获取预上传信息
        $file_path = '/root/1581483642782240.mp4';
        $videoSize = @filesize($file_path);
        $videoMd5 =  md5($file_path);
        $response = $client->request('POST',"https://baijiahao.baidu.com/builder/author/video/preuploadVideo?app_id={$appid}&md5={$videoMd5}&is_pay_column=0",[
            'headers'=>[
                'token' =>$token,
                'Cookie'=>$cookie,
                'X-Requested-With'=>'XMLHttpRequest',
                'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
            ]
        ]);
        $preuploadVideo = json_decode($response->getBody()->getContents(),true);
        print_r('haokan--preupload---'.json_encode($preuploadVideo,JSON_UNESCAPED_UNICODE)."\r\n");
        if(!isset($preuploadVideo['error_code']) || $preuploadVideo['error_code'] !== 20000){
            return response()->error('PUBLISH_FAILED');
        }
        //$uploadCookie = $response->getHeader('')
        $mediaId    = $preuploadVideo['mediaId'];
        print_r('-----mediaId---'.$mediaId."\n\n");
        $upload_key = $preuploadVideo['upload_key'];
        print_r('-----upload_key---'.$upload_key."\n\n");
        $last_chunk = 1;
        if($videoSize>1024*2*1024) {//分片上传
            $chunk_nums = ceil($videoSize/(1024*2*1024));
            $read_buffer = 1024*2*1024;
            $sum_buffer = 0;
            $handle = fopen($file_path, 'rb');
            $file_exp = explode('.',basename($file_path));
            $file_ext = array_pop($file_exp);
            $file_base_name = implode('.',$file_exp);
            $file_index = 0;
            print_r($chunk_nums.':'.$chunk_nums."\r\n");
            //先分片存储
            while(!feof($handle) && $sum_buffer<$videoSize) {
                $file_content = fread($handle, $read_buffer);
                $sum_buffer += $read_buffer;
                $file_save_name = public_path().'/upload/image/'.$file_base_name.'_'.$file_index;
                file_put_contents($file_save_name,$file_content);
                $headers = [
                    'Cookie'    =>$cookie,
                    'Referer'   =>"https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id={$appid}",
                    'Sec-Fetch-Mode'=>'cors',
                    'Sec-Fetch-Site'=>'same-site',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ];
                $videoData = [
                    [
                        'name'      => 'timestamp',
                        'contents'  => time()
                    ], [
                        'name'      =>'md5',
                        'contents'  =>$videoMd5
                    ], [
                        'name'      =>'id',
                        'contents'  =>'WU_FILE_1'
                    ], [
                        'name'      =>'name',
                        'contents'  =>'myclass.mp4'
                    ], [
                        'name'      =>'type',
                        'contents'  =>'video/mp4'//substr(strrchr($videoInfo->relative_path, '.'), 1)
                    ],
                    [
                        'name'      =>'size',
                        'contents'  =>$videoSize
                    ],
                    [
                        'name'      =>'chunks',
                        'contents'  =>$chunk_nums
                    ],
                    [
                        'name'      =>'chunk',
                        'contents'  =>$file_index
                    ],
                    [
                        'name'      =>'upload_key',
                        'contents'  =>$upload_key
                    ],
                    [
                        'name'      =>'file',
                        'contents'  =>@fopen($file_save_name,'r'),
                    ]
                ];
                $response = $client->request('POST',"https://rsbjh.baidu.com/builder/author/video/uploadVideo?app_id={$appid}",[
                    'verify' => false,
                    'headers'=>$headers,
                    'multipart'=>$videoData
                ]);
                $uploadVideo = json_decode($response->getBody()->getContents(),true);
                print_r('$uploadVideo:'.$file_index.":".$response->getBody()->getContents()."\r\n");
                $file_index++;
            }
            print_r($chunk_nums.':'.$chunk_nums."\r\n");
            $last_chunk = $chunk_nums;
        }
        else {
            //上传视频
            $headers = [
                'Cookie'    =>$cookie,
                'Referer'   =>"https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id={$appid}",
                'Sec-Fetch-Mode'=>'cors',
                'Sec-Fetch-Site'=>'same-site',
                'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
            ];
            $videoData = [
                [
                    'name'      => 'timestamp',
                    'contents'  => time()
                ], [
                    'name'      =>'md5',
                    'contents'  =>$videoMd5
                ], [
                    'name'      =>'id',
                    'contents'  =>'WU_FILE_0'
                ], [
                    'name'      =>'name',
                    'contents'  =>'myclass.mp4'
                ], [
                    'name'      =>'type',
                    'contents'  =>'video/mp4'//substr(strrchr($videoInfo->relative_path, '.'), 1)
                ],
                [
                    'name'      =>'size',
                    'contents'  =>$videoSize
                ],
                [
                    'name'      =>'upload_key',
                    'contents'  =>$upload_key
                ],
                [
                    'name'      =>'file',
                    'contents'  =>@fopen(public_path().$videoInfo->relative_path,'r'),
                ]
            ];
            \Log::info("video-info-data".json_encode($videoData)."\n\n");
            \Log::info("video-info-header".json_encode($headers)."\n\n");
            $response = $client->request('POST',"https://rsbjh.baidu.com/builder/author/video/uploadVideo?app_id={$appid}",[
                'verify' => false,
                'headers'=>$headers,
                'multipart'=>$videoData
            ]);
            $uploadVideo = json_decode($response->getBody()->getContents(),true);
            \Log::info('haokan--uploadvideo---'.json_encode($uploadVideo,JSON_UNESCAPED_UNICODE));
            if(!isset($uploadVideo['error_code']) || $uploadVideo['error_code'] !==20000){
                $publishes->content_id=0;
                $publishes->contents_info=json_encode([
                    [
                        'name'      =>'upload_key',
                        'contents'  =>$upload_key
                    ],
                    [
                        'name'      =>'name',
                        'contents'  =>$videoInfo->file_name,
                    ],
                    [
                        'name'      =>'chunks',
                        'contents'  =>1
                    ]
                ],JSON_UNESCAPED_UNICODE);
                $publishes->response = json_encode($uploadVideo);
                $publishes->status = 'UPLOAD_FAILED';
                $publishes->reason = '未知错误';
                $publishes->save();
                /*$publish_data = [
                    'title'=>$formData['title'],
                    'platform_id'=>$accountInfo->platform_id,
                    'platform_account_id'=>$accountInfo->id,
                    'user_id'=>$accountInfo->user_id,
                    'content_id'=>0,
                    'contents_info'=>json_encode([
                        [
                            'name'      =>'upload_key',
                            'contents'  =>$upload_key
                        ],
                        [
                            'name'      =>'name',
                            'contents'  =>$videoInfo->file_name,
                        ],
                        [
                            'name'      =>'chunks',
                            'contents'  =>1
                        ]
                    ],JSON_UNESCAPED_UNICODE),
                    'response'=>json_encode($uploadVideo),
                    'status'=>'UPLOAD_FAILED',
                    'reason'=>'未知错误'
                ];
                Publishes::create($publish_data);*/
                return response()->error('PUBLISH_FAILED');
            }
        }

            //告诉他上传完了
            $response = $client->request('POST',"https://baijiahao.baidu.com/builder/author/video/compuploadVideo?uploadTime=1397&app_id={$appid}",[
                'headers'=>[
                    'Referer'=>"https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id={$appid}",
                    'Sec-Fetch-Mode'=>'cors',
                    'Sec-Fetch-Site'=>'same-origin',
                    'token' =>$token,
                    'Cookie'=>$cookie,
                    'X-Requested-With'=>'XMLHttpRequest',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ],
                'multipart'=>[
                    [
                        'name'      =>'upload_key',
                        'contents'  =>$upload_key
                    ],
                    [
                        'name'      =>'name',
                        'contents'  =>'myclass.mp4',
                    ],
                    [
                        'name'      =>'chunks',
                        'contents'  =>$last_chunk
                    ]
                ]
            ]);
            $compupload = json_decode($response->getBody()->getContents(),true);
            print_r('haokan--compupload---'.json_encode($compupload,JSON_UNESCAPED_UNICODE)."\r\n");

            //上传封面图片
            $response = $client->request('POST','https://baijiahao.baidu.com/builderinner/api/content/file/upload',[
                'headers'=>[
                    'Cookie'=>$cookie,
                    'X-Requested-With'=>'XMLHttpRequest',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ],
                'multipart'=>[
                    ['name'=>'type','contents'=>'image'],
                    ['name'=>'app_id','contents'=>''],
                    ['name'=>'media','contents'=>@fopen(public_path().'/upload/videoCover/20200212/1LG4v9l3qJ6TFYEvLe5BkgDR6pKJ0qcQAQlHbhbg.png','r')],
                ]
            ]);
            $uploadImg = json_decode($response->getBody()->getContents(),true);
            print_r('haokan--uploadimg---'.json_encode($uploadImg,JSON_UNESCAPED_UNICODE)."\r\n");

            $token = $response->getHeader('Token');
            //图片自动裁剪
            $response= $client->request('POST','https://baijiahao.baidu.com/builder/author/article/autocutpic',[
                'headers'=>[
                    'Cookie'=>$cookie,
                    'token' =>$token[0]
                ],
                'multipart'=>[
                    ['name'=>'width','contents'=>1200],
                    ['name'=>'height','contents'=>791],
                    ['name'=>'image','contents'=>$uploadImg['ret']['https_url']],
                    ['name'=>'article_type','contents'=>'video'],
                ]
            ]);
            $result = $response->getBody()->getContents();
           print_r('---图片自动裁剪----'.$result."\r\n");
            $token = $response->getHeader('Token');
            \Log::info('---图片裁剪2--TOKEN----'.$token[0]."\r\n");

            //图片裁剪2
            $response= $client->request('POST','https://baijiahao.baidu.com/builderinner/api/content/file/cuttingPic',[
                'headers'=>[
                    'Cookie'=>$cookie,
                    'token' =>$token[0]
                ],
                'multipart'=>[
                    ['name'=>'auto','contents'=>false],
                    ['name'=>'x','contents'=>0],
                    ['name'=>'y','contents'=>0],
                    ['name'=>'w','contents'=>1200],
                    ['name'=>'h','contents'=>673],
                    ['name'=>'src','contents'=>$uploadImg['ret']['https_url']],
                    ['name'=>'type','contents'=>'video'],
                ]
            ]);
            $result = json_decode($response->getBody()->getContents(),true);
            print_r('---图片裁剪2-result----'.json_encode($result,JSON_UNESCAPED_UNICODE)."\r\n");
            $token = $response->getHeader('Token');
            print_r('---图片裁剪2--TOKEN----'.$token[0]."\r\n");
            $headers = [
                'Sec-Fetch-Mode'=>'cors',
                'Sec-Fetch-Site'=>'same-origin',
                'token' =>$token[0],
                'Cookie'=>$cookie,
                'Referer'=>"https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id={$appid}",
                'X-Requested-With'=>'XMLHttpRequest',
                'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
            ];
            print_r('---保存请求header------'.json_encode($headers)."\r\n");
            $data = [
                ['name' =>'title', 'contents'=> '我的课堂啊啊啊啊啊'],
                ['name' =>'content','contents'=> '[{"title":"'.'我的课堂啊啊啊啊啊'.'","desc":"","mediaId":"'.$mediaId.'","videoName":"'.'myclass.mp4'.'","local":1}]'],
                ['name'      =>'type', 'contents'  =>'video'],
                ['name'=>'cover_images','contents'=>'[{"source":"local","src":"'.$result['data']['src'].'","cropData":{"x":0,"y":0,"width":1200,"height":673},"isLegal":0}]'],
                ['name'=>'_cover_images_map', 'contents'  =>'[{"src":"'.$result['data']['src'].'","origin_src":"'.$result['data']['origin_src'].'"}]'],
                ['name'=>'cover_layout', 'contents'  =>'one'],
                ['name'=>'usingImgFilter','contents' =>false],
                ['name'=>'source_reprinted_allow', 'contents'  =>0],
                ['name'=>'original_status', 'contents'  =>0],
                ['name'=>'announce_id', 'contents'  =>1],
            ];
            print_r('---保存请求data------'.json_encode($data)."\r\n");
            //获取一个发布需要的cookie
            $response = $client->request('POST',"https://baijiahao.baidu.com/builder/author/home/dealToken",[
                'headers'=>[
                    'bjh'=>'b07307529a6ec5d84569725a51ad2d854c3ac2b6fd25d7476dcc6cd573b57082',
                    'dev'=>'a6e03da61922855b2e7cf1d30f983eec4c3ac2b6fd25d7476dcc6cd573b57082',
                    'Cookie'=>$cookie,
                    'token' =>$token[0],
                    'Referer'=>'https://baijiahao.baidu.com/builder/rc/edit?type=video&app_id=1616440569727005',
                    'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
                ]
            ]);
            $cookie_str = self::extractHaokanCookie($response->getHeader('Set-Cookie'),$cookie);
            print_r('haokan----cookie---'.$cookie_str."\r\n");
            //发布视频
            $response = $client->request('POST',"https://baijiahao.baidu.com/builder/author/article/publish",[
                'headers'   =>$headers,
                'multipart' =>$data
            ]);
            $saveInfo = json_decode($response->getBody()->getContents(),true);
            print_r('haokan--save_info1---'.json_encode($saveInfo,JSON_UNESCAPED_UNICODE));

    }
    public static function extractHaokanCookie($getCookie,$oldCookie) {
        if($getCookie){
            $oldCookie = explode(';',$oldCookie);
            foreach($getCookie as $val){
                $each = explode(';',$val);
                if(strstr($each[0],'bjhStoken=')){
                    foreach ($oldCookie as $k=>$v){
                        if(strstr($v,'bjhStoken=')){
                            $oldCookie[$k] = $each[0];
                        }
                    }
                }
                if(strstr($each[0],'devStoken=')){
                    foreach ($oldCookie as $k=>$v){
                        if(strstr($v,'bjhStoken=')){
                            $oldCookie[$k] = $each[0];
                        }
                    }
                }
            }
            return implode(';',$oldCookie);
        }else{
            return $oldCookie;
        }
    }
}