<?php
/**
 * Created by PhpStorm.
 * User: tandunzhao
 * Date: 2017/11/20
 * Time: 下午5:26
 */

namespace App\Console\Commands;

use App\Jobs\SelfRecomend;
use App\Modules\Agent\Services\AgentServices;
use App\Modules\BackCost\Models\HbGzh;
use App\Modules\BackCost\Models\HbMonthStats;
use App\Modules\BackCost\Services\HbGzhsServices;
use App\Modules\BackCost\Services\HbMonthStatsServices;
use App\Modules\Channel\Services\ChannelDailyStatService;
use App\Modules\Platform\Services\ReleaseDouyinServices;
use App\Modules\Platform\Services\ReleaseKuaishouServices;
use App\Modules\Publish\Services\VideoPublishServices;
use App\Modules\SendOrder\Models\SendOrder;
use App\Modules\SendOrder\Services\SendOrderService;
use App\Modules\Trade\Services\OrderService;
use App\Modules\User\Models\UserSubscribeBehaviorStats;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use Log;
use GuzzleHttp\Promise;
use Psr\Http\Message\ResponseInterface;
use Sunra\PhpSimple\HtmlDomParser;

class consumeVideoPublish extends Command
{
    /**
     * 执行命令 获取站点的派单数据及服务号信息
     *
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video_publishs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //while (true){
        $list = DB::table('video_tmp_tasks')->where('status',0)->get();
        //}
        foreach ($list as $key=>$each){
            $platform_id = $each->platform_id;
            $accountInfo = json_decode($each->account_info);
            $videoInfo = json_decode($each->video_info);
            $formData = json_decode($each->form_data,true);
            $uid = $each->uid;
            $coverInfo = json_decode($each->cover_info);
            if($platform_id== 19){//发布抖音视频
                try{
                    ReleaseDouyinServices::release($uid,$accountInfo,$videoInfo,$formData);
                }catch (\Exception $e){
                    \Log::error('douyin_publish error:'.$e->getMessage());
                    $error_msgs[] = '抖音';
                }
            }
            if($platform_id == 21){//发布快手
                try{
                    ReleaseKuaishouServices::release($uid,$accountInfo,$videoInfo,$formData,$coverInfo);
                }catch (\Exception $e){
                    \Log::error('kuaishou_publish error:'.$e->getMessage());
                    $error_msgs[] = '快手';
                }
            }


            if($platform_id == 14){
                try{
                    VideoPublishServices::publishAiqiyi($accountInfo,$videoInfo,$formData);
                }catch (\Exception $e){
                    \Log::error('aiqiyi publish error:'.$e->getMessage());
                    $error_msgs[] = '爱奇艺';
                }
            }elseif($platform_id == 18){
                try{
                    VideoPublishServices::publishHaokan($accountInfo,$videoInfo,$formData,$coverInfo);
                }catch (\Exception $e){
                    \Log::error('haokan publish error:'.$e->getMessage());
                    $error_msgs[] = '好看';
                }
            }elseif($platform_id == 16){//发布优酷视频
                try{
                    VideoPublishServices::publishYouku($accountInfo,$videoInfo,$formData);
                }catch (\Exception $e){
                    \Log::error('youku publish error:'.$e->getMessage());
                    $error_msgs[] = '优酷';
                }
            }
        }

    }


}