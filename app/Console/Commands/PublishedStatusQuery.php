<?php
/**
 * Created by PhpStorm.
 * User: tandunzhao
 * Date: 2017/11/20
 * Time: 下午5:26
 */

namespace App\Console\Commands;

use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Publishes;
use App\Modules\User\Models\UserSubscribeBehaviorStats;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use Log;
use GuzzleHttp\Promise;
use Psr\Http\Message\ResponseInterface;
use Sunra\PhpSimple\HtmlDomParser;

class PublishedStatusQuery extends Command
{
    /**
     * 执行命令 获取站点的派单数据及服务号信息
     *
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'query_published_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $publishes_list = Publishes::whereNotIn('status',['REVIEW_SUCCESS','UPLOAD_FAILED','REVIWE_FAILED','WAITING_UPLOAD'])->get();
        foreach ($publishes_list as $key=>$value) {
            try{
                if($value->platform_id == 5) {
                    $this->queryQQ($value);
                }
                if($value->platform_id == 8) {
                    $this->queryYidian($value);
                }
                if($value->platform_id == 4) {
                    $this->querySouhu($value);
                }
                if($value->platform_id == 1) {
                    $this->queryBjh($value);
                }
                if($value->platform_id == 2) {
                    $this->querySougou($value);
                }
                if($value->platform_id == 3) {
                    $this->queryKuaichuan($value);
                }
                if($value->platform_id == 6) {
                    $this->queryDayuhao($value);
                }
                if($value->platform_id == 7) {
                    $this->queryToutiao($value);
                }
                if($value->platform_id == 18) {
                    $this->queryHaoKan($value);
                }
                if($value->platform_id == 14) {
                    $this->queryIqiyi($value);
                }
                if($value->platform_id == 16) {
                    $this->queryYouku($value);
                }
            }catch (\Exception $e){
                \Log::error('publish_status_query_error_msg:'.$value->platform_id.":".$value->id.':'.$e->getMessage());
            }

        }
    }

    private function queryQQ($published_info) {
        $client = new Client();
        //$access_token = $published_info->access_token;
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $access_token = $platform_account_info->access_token;
        $query_url = 'https://api.om.qq.com/article/infoauth?access_token='.$access_token.'&article_id='.$article_id;
        $response = $client->request('GET',$query_url);
        $res = json_decode($response->getBody()->getContents(),true);
        if($res['code'] == 0) {
            $status = $res['data']['article_info']['article_pub_flag']; //未发布，发布成功，审核中
            $pub_status = '';
            $publish_time = $res['data']['article_info']['article_pub_time'];
            if($status == '未发布') {
                $pub_status = 'REVIEW_FAILED';
            }
            if($status == '审核中') {
                $pub_status = 'REVIEWING';
            }
            if($status == '发布成功') {
                $pub_status = 'REVIEW_SUCCESS';
            }
            $published_info->status = $pub_status;
            $published_info->publish_time = $publish_time;
            $published_info->save();
        }else{
            \Log::error('企鹅号查询失败:'.json_encode($published_info));
        }
    }

    private function queryYidian($published_info) {
        $client = new Client();
        //$access_token = $published_info->access_token;
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $query_url = 'https://mp.yidianzixun.com/model/Article?page=1&page_size=100&status=0,1,2,3,4,5,6,7,9,14&has_data=1&type=original&push_status=&from_time=&to_time=&date=202001&title='.urlencode($published_info->title);
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);
        $list = $res['posts'];
        foreach ($list as $each) {
            if($each['id'] == $article_id) {
                $status = $each['status'];
                $status_str = '';
                switch ($status) {
                    case 2:$status_str='REVIEW_SUCCESS';break;
                    case 6:$status_str='REVIEW_SUCCESS';break;
                    case 7:$status_str='REVIEW_SUCCESS';break;
                    case 1:$status_str='REVIEWING';break;
                    case 4:$status_str='REVIEWING';break;
                    case 5:$status_str='REVIEWING';break;
                    case 14:$status_str='REVIEWING';break;
                    case 3:$status_str='REVIEW_FAILED';break;
                }
                $reason = $each['comment'];
                $published_info->status = $status_str;
                $publish_time = date('Y-m-d H:i:s',$each['updatedTime']);
                $published_info->publish_time = $publish_time;
                $published_info->reason = $reason;
                $published_info->save();
            }
        }
    }

    private function querySouhu($published_info) {
        //https://mp.sohu.com/v3/users/newsList?psize=10&containTop=false&newsType=1&statusType=3&columnId=&pno=1&accountId=120516354&_=1579190467999
        //status 2 shenhezhong 3 weitongguo 4 tongguo
        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $query_url = 'https://mp.sohu.com/v3/users/newsList?psize=100&containTop=false&newsType=1&statusType=1&columnId=&pno=1&accountId='.$platform_account_info->platform_user_id.'&_=1579190467999';
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);
        if(isset($res['news'])) {
            foreach ($res['news'] as $key=>$each) {
                if($each['id'] == $article_id) {
                    $status = '';
                    switch ($each['status']) {
                        case 2:$status='REVIEWING';break;
                        case 3:$status='REVIEW_FAILED';break;
                        case 4:$status='REVIEW_SUCCESS';break;
                    }
                    $publish_time = date('Y-m-d H:i:s',$each['modifiedTime']);
                    $published_info->publish_time = $publish_time;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }
    }

    private function queryBjh($published_info) {
        //https://baijiahao.baidu.com/builder/article/lists?type=&collection=&pageSize=10&currentPage=1&search=&app_id=1609649538589447&dynamic=1;
        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $app_info_response = $client->request('GET','https://baijiahao.baidu.com/builder/app/appinfo?',['headers'=>['Cookie'=>$cookie]]);
        $app_info = json_decode($app_info_response->getBody()->getContents(),true);
        $appid = $app_info['data']['user']['app_id'];
        $query_url = 'https://baijiahao.baidu.com/builder/article/lists?type=&collection=&pageSize=10&currentPage=1&app_id='.$appid.'&dynamic=1&search='.urlencode($published_info->title);
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);
        if($res['errno']==0) {
            $list = $res['data']['list'];
            foreach ($list as $key=>$each) {
                if($article_id == $each['article_id']) {
                    $status = '';
                    switch($each['status']) {
                        case 'publish':$status='REVIEW_SUCCESS';break;
                        case 'rejected':$status='REVIEW_FAILED';break;
                        //TODO 验证审核中的状态是否为pre_publish
                        case 'pre_publish':$status='REVIEWING';break;
                    }
                    $reason = '';
                    if(isset($each['audit_msg'])) {
                        $reason = $each['audit_msg'];
                    }
                    if(isset($each['publish_at'])) {
                        $published_info->publish_time = $each['publish_at'];
                    }
                    $published_info->reason = $reason;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }else{
            \Log::error('百家号查询失败:'.json_encode($res));
        }
    }

    private function queryHaoKan($published_info) {
        //https://baijiahao.baidu.com/builder/article/lists?type=&collection=&pageSize=10&currentPage=1&search=&app_id=1609649538589447&dynamic=1;
        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $app_info_response = $client->request('GET','https://baijiahao.baidu.com/builder/app/appinfo?',['headers'=>['Cookie'=>$cookie]]);
        $app_info = json_decode($app_info_response->getBody()->getContents(),true);
        $appid = $app_info['data']['user']['app_id'];
        $query_url = 'https://baijiahao.baidu.com/builder/article/lists?type=video&collection=&pageSize=10&currentPage=1&search=&app_id='.$appid;
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);

        if($res['errno']==0) {
            print_r('haokan_punblishes_result:'.json_encode($res));
            $list = $res['data']['list'];
            foreach ($list as $key=>$each) {
                if($article_id == $each['article_id']) {
                    $status = '';
                    switch($each['status']) {
                        case 'publish':$status='REVIEW_SUCCESS';break;
                        case 'rejected':$status='REVIEW_FAILED';break;
                        //TODO 验证审核中的状态是否为pre_publish
                        case 'pre_publish':$status='REVIEWING';break;
                    }
                    $reason = '';
                    if(isset($each['audit_msg'])) {
                        $reason = $each['audit_msg'];
                    }
                    if(isset($each['publish_at'])) {
                        $published_info->publish_time = $each['publish_at'];
                    }
                    $published_info->reason = $reason;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }else{
            \Log::error('haokan查询失败:'.json_encode($res));
        }
    }

    private function querySougou($published_info) {
        //http://mp.sogou.com/api/1/articles?status=&type=
        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $query_url = 'http://mp.sogou.com/api/1/articles?status=&type=';
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);
        if(isset($res['list'])) {
            $list = $res['list'];
            foreach ($list as $key=>$value) {
                if($article_id == $value['id']) {
                    $status = '';
                    switch ($value['status']) {
                        case 16:$status = 'REVIEWING';break;
                        case 40:$status = 'REVIEW_FAILED';break;
                        case 1:$status = 'REVIEW_SUCCESS';break;
                    }
                    $reason = isset($value['remark'])?$value['remark']:'';
                    $publish_time = $value['updatedAt'];
                    $published_info->publish_time = $publish_time;
                    $published_info->reason = $reason;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }
    }
    private function queryKuaichuan($published_info) {
        //http://kuaichuan.360.cn/content/contentList?page=1
        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $query_url = 'http://kuaichuan.360.cn/content/search?keyword='.urlencode($published_info->title).'&page=1';
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);
        if($res['errno']==0) {
            $list = $res['data']['list'];
            foreach ($list as $key=>$value) {
                if($article_id == $value['id']) {
                    $status = '';
                    switch ($value['status']) {
                        case 1:$status = 'REVIEWING';break;
                        case 3:$status = 'REVIEW_FAILED';break;
                        case 2:$status = 'REVIEW_SUCCESS';break;
                    }
                    $reason = isset($value['op_reason'])?$value['op_reason']:'';
                    $publish_time = $value['publish_time'];
                    $published_info->publish_time = $publish_time;
                    $published_info->reason = $reason;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }
    }

    private function queryDayuhao($published_info) {
        //https://mp.dayu.com/dashboard/getArticleList?_rid=f46ee5ea942d4178867911dfedd9c5ab&keyword=&currentView=all&source=all&page=1&articleCategory=&_=1579253003990
        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $query_url = 'https://mp.dayu.com/dashboard/getArticleList?keyword=&currentView=all&source=all&page=1&articleCategory=';
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);
        if(isset($res['dataList'])) {
            $list = $res['dataList']['data'];
            foreach ($list as $key=>$value) {
                if($article_id == $value['_id']) {
                    $status = '';
                    switch ($value['status']) {
                        case 3:$status = 'REVIEWING';break;
                        case 4:$status = 'REVIEW_FAILED';break;
                        case 1:$status = 'REVIEW_SUCCESS';break;
                    }
                    $reason = isset($value['_app_extra']['audit_remark'])?$value['_app_extra']['audit_remark']:'';
                    $publish_time = $value['publish_at'];
                    $published_info->publish_time = $publish_time;
                    $published_info->reason = $reason;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }
    }

    private function queryToutiao($published_info) {
        //https://mp.toutiao.com/mp/agw/article/list?size=20&status=all&from_time=0&start_time=0&end_time=0&search_word=&page=1&feature=0&source=all
        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $query_url = 'https://mp.toutiao.com/mp/agw/article/list?size=20&status=all&from_time=0&start_time=0&end_time=0&search_word=&page=1&feature=0&source=all';
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);
        if($res['code'] == 0) {
            $list = $res['data']['content'];
            foreach ($list as $key=>$value) {
                if($article_id == $value['id']) {
                    $status = '';
                    switch ($value['status']) {
                        case 5:$status = 'REVIEWING';break;
                        case 4:$status = 'REVIEW_FAILED';break; //TODO 验证不通过的状态
                        case 3:$status = 'REVIEW_SUCCESS';break;
                    }
                    $reason = isset($value['audit_suppress_reason'])?$value['audit_suppress_reason']:'';
                    $publish_time = date('Y-m-d H:i:s',$value['verify_time']);
                    $published_info->publish_time = $publish_time;
                    $published_info->reason = $reason;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }
    }

    private function queryIqiyi($published_info) {

        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $query_url = 'http://www.iqiyi.com/u/video';
        print_r("publish_id:".$published_info->id."\r\n");
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = $response->getBody()->getContents();
        $response_html = HtmlDomParser::str_get_html($res);
        $lis = $response_html->find('.myVideo-list')[0]->find('li.myVideo-item');
        foreach ($lis as $key=>$li) {
            //var_dump($li->innertext);
            $id = $li->getAttribute('tvid');
            $status_str= $li->find('.myV-info')[0]->find('.myV-dyn')[0]->innertext;
            print_r("\r\n");
            var_dump($status_str);
            $status='';
            print_r('article_id:'.$article_id);
            print_r('id:'.$id);
            if($article_id == $id) {
                switch($status_str) {
                    case '发布成功':$status='REVIEW_SUCCESS';break;
                    case '转码中':$status='REVIEWING';break;
                    default:$status = 'REVIEW_FAILED';
                }
                $reason = '';
                $my_tips = $li->find('.myV-pic')[0]->find('.myV-tips');
                if($my_tips) {
                    $my_tip = $my_tips[0];
                    $reason = trim($my_tip->find('p')[0]->innertext);

                }
                $publish_time = date('Y-m-d H:i:s');
                $published_info->publish_time = $publish_time;
                $published_info->reason = $reason;
                $published_info->status = $status;
                $published_info->save();
            }


        };
        /*$response_html->find('.myVideo-list');
        if($res['code'] == 0) {
            $list = $res['data']['content'];
            foreach ($list as $key=>$value) {
                if($article_id == $value['id']) {
                    $status = '';
                    switch ($value['status']) {
                        case 5:$status = 'REVIEWING';break;
                        case 4:$status = 'REVIEW_FAILED';break; //TODO 验证不通过的状态
                        case 3:$status = 'REVIEW_SUCCESS';break;
                    }
                    $reason = isset($value['audit_suppress_reason'])?$value['audit_suppress_reason']:'';
                    $publish_time = date('Y-m-d H:i:s',$value['verify_time']);
                    $published_info->publish_time = $publish_time;
                    $published_info->reason = $reason;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }*/
    }

    private function queryYouku($published_info) {
        //https://mp.toutiao.com/mp/agw/article/list?size=20&status=all&from_time=0&start_time=0&end_time=0&search_word=&page=1&feature=0&source=all
        $client = new Client();
        $article_id = $published_info->content_id;
        $platform_account_info = PlatformAccounts::getById($published_info->platform_account_id);
        $cookie = $platform_account_info->cookie;
        $query_url = 'https://mp.youku.com/video/get?pageNo=1&pageSize=50&verticalType=false&publishStatus=all&shareState=ALL';
        $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
        $res = json_decode($response->getBody()->getContents(),true);
        if($res['success']) {
            $list = $res['data']['data'];
            foreach ($list as $key=>$value) {
                if($article_id == $value['id']) {
                    $status = '';
                    switch ($value['mpStatus']) {
                        case 'censoring':$status = 'REVIEWING';break;
                        case 'allowed':$status = 'REVIEW_SUCCESS';break;
                        case 'blocked':$status = 'REVIEW_FAILED';break;
                        default:$status = 'REVIEW_FAILED';break;
                    }
                    $reason = isset($value['reason'][0]['ureasonDesc'])?$value['reason'][0]['ureasonDesc']:'';
                    //$publish_time = date('Y-m-d H:i:s',$value['verify_time']);
                    $published_info->publish_time =$value['mpPublishedTime'];
                    $published_info->reason = $reason;
                    $published_info->status = $status;
                    $published_info->save();
                }
            }
        }
    }

    private function imagesContentsFromRemote($image_links) {
        $client = new Client();
        $query_url = 'https://api.om.qq.com/article/infoauth?access_token=ACCESS_TOKEN&article_id=ARTICLE_ID';
        $promises = [];
        foreach ($image_links as $key=>$image_link) {
            $promises[$image_link] = $client->postAsync('http://video.zkh168.com/api/platforms_list',['form_params'=>[
                'type'=>$image_link
            ]]);
        }
        $results = Promise\unwrap($promises);
        $index = 0;
        foreach ($results as $key=>$result) {
            $res = $result->getBody()->getContents();
            echo $res."\r\n\r\n";
        }
    }
}