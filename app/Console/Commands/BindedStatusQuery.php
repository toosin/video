<?php
/**
 * Created by PhpStorm.
 * User: tandunzhao
 * Date: 2017/11/20
 * Time: 下午5:26
 */

namespace App\Console\Commands;

use App\Modules\Platform\Models\PlatformAccounts;
use App\Modules\Platform\Models\Publishes;
use App\Modules\User\Models\UserSubscribeBehaviorStats;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use Log;
use GuzzleHttp\Promise;
use Psr\Http\Message\ResponseInterface;
use Sunra\PhpSimple\HtmlDomParser;

class BindedStatusQuery extends Command
{
    /**
     * 执行命令 获取站点的派单数据及服务号信息
     *
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bind_status_query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $account_list = PlatformAccounts::where('status',1)->get();
        //var_dump($account_list);
        foreach ($account_list as $key=>$value) {
            try{
                if($value->platform_id == 5) {
                    $this->queryQQ($value);
                }
                if($value->platform_id == 8) {
                    $this->queryYidian($value);
                }
                if($value->platform_id == 4) {
                    $this->querySouhu($value);
                }
                if($value->platform_id == 1) {
                    $this->queryBjh($value);
                }
                if($value->platform_id == 2) {
                    $this->querySougou($value);
                }
                if($value->platform_id == 3) {
                    $this->queryKuaichuan($value);
                }
                if($value->platform_id == 6) {
                    $this->queryDayuhao($value);
                }
                if($value->platform_id == 7) {
                    $this->queryToutiao($value);
                }
                if($value->platform_id == 18) {
                    $this->queryBjh($value);
                }
                if($value->platform_id == 14) {
                    //print_r('14');
                    $this->queryIqiyi($value);
                }
                if($value->platform_id == 16) {
                    $this->queryYouku($value);
                }
                if($value->platform_id == 21) {
                    $this->queryKuaishou($value);
                }
                if($value->platform_id == 11) {
                    $this->queryZhihu($value);
                }

            }catch (\Exception $e){
                \Log::error($value->platform_id.':binded_query_error'.$e->getMessage());
            }

        }
    }

    private function queryQQ($published_info) {
        $client = new Client();

        //$access_token = $published_info->access_token;

    }

    private function queryYidian($account_info) {
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $query_url = 'https://mp.yidianzixun.com/model/Agreement';
            $response = $client->request('GET',$query_url,['headers'=>['Cookie'=>$cookie]]);
            $res = json_decode($response->getBody()->getContents(),true);
            $result = $res['result'];
            //\Log::info('yidian:result:'.json_encode($result));
            //\Log::info('yidian:userId:'.intval($result['userId']).':'.$result['userId']);
            if(intval($result['userId'])>0) {
                return true;
            }else{
                $account_info->status=0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status=0;
            $account_info->save();
            \Log::error('yidian binded query error:'.$e->getMessage());
        }

    }

    private function querySouhu($account_info) {
        //https://mp.sohu.com/v3/users/newsList?psize=10&containTop=false&newsType=1&statusType=3&columnId=&pno=1&accountId=120516354&_=1579190467999
        //status 2 shenhezhong 3 weitongguo 4 tongguo
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $response = $client->request('GET','https://mp.sohu.com/mp-accounts/accounts/list?_=1577370253777',[
                'headers'=>['Cookie'=>$cookie]
            ]);
            $res = $response->getBody()->getContents();
            //print($res);
            $res_arr = json_decode($res,true);
            if(isset($res_arr['totalAccount']) && $res_arr['totalAccount']>0) {
                return true;
            }else{
                $account_info->status=0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status=0;
            $account_info->save();
            \Log::error('souhu binded query error:'.$e->getMessage());

        }

    }

    private function queryBjh($account_info) {
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $app_info_response = $client->request('GET','https://baijiahao.baidu.com/builder/app/appinfo?',['headers'=>['Cookie'=>$cookie]]);
            $app_info = json_decode($app_info_response->getBody()->getContents(),true);
            $appid = $app_info['data']['user']['app_id'];
            $user_info_response=$client->request('GET','https://baijiahao.baidu.com/builder/app/appinfo?',['headers'=>['Sec-Fetch-Mode'=> 'cors',
                'Sec-Fetch-Site'=> 'same-origin','Cookie'=>$cookie]]);
            $token = $user_info_response->getHeader('Token')[0];
            $res = json_decode($user_info_response->getBody()->getContents(),true);
            if(isset($res['errno']) && $res['errno']==0) {
                $account_info->token = $token;
                $account_info->save();
            }else{
                $account_info->status = 0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
                //\Log::error('百家号查询失败:'.json_encode($res));
            }
        }catch (\Exception $e) {
            $account_info->status = 0;
            $account_info->due_time=date('Y-m-d H:i:s');
            $account_info->save();
            \Log::error('baijiahao binded query error:'.$e->getMessage());
        }
        //https://baijiahao.baidu.com/builder/article/lists?type=&collection=&pageSize=10&currentPage=1&search=&app_id=1609649538589447&dynamic=1;

    }

    private function querySougou($account_info) {
        //http://mp.sogou.com/api/1/articles?status=&type=
        //$client = new Client();
        try{
            $cookie = $account_info->cookie;
            $client = new Client();
            $url = 'http://mp.sogou.com/api/if-login?status=81';
            $response = $client->request('GET',$url,[
                'headers' => [
                    'Cookie' => $cookie,
                ]
            ]);
            $status = $response->getStatusCode();
            $body = $response->getBody()->getContents();
            $login_info = json_decode($body,true);
            if(is_array($login_info) && isset($login_info['avatar'])) {
                return ;
            }else{
                $account_info->status = 0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e) {
            $account_info->status = 0;
            $account_info->save();
            \Log::error('sougou binded query error:'.$e->getMessage());
        }

    }
    private function queryKuaichuan($account_info) {
        //http://kuaichuan.360.cn/content/contentList?page=1
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $user_info_response = $client->request('GET','http://kuaichuan.360.cn/user/detail',[
                'headers'=>['Cookie'=>$cookie,'Referer'=>'http://kuaichuan.360.cn/','User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36']
            ]);

            $response_in = $user_info_response->getBody()->getContents();
            //\Log::info('jj:'.$response_in);
            $res_arr = json_decode($response_in,true);
            if($res_arr['errno']==0) {
                return true;
            }else{
                $account_info->status=0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status=0;
            $account_info->save();
            \Log::error('kuaichuan binded query error:'.$e->getMessage());
        }

    }

    private function queryDayuhao($account_info) {
        //https://mp.dayu.com/dashboard/getArticleList?_rid=f46ee5ea942d4178867911dfedd9c5ab&keyword=&currentView=all&source=all&page=1&articleCategory=&_=1579253003990
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $user_info_response = $client->request('GET','https://mp.dayu.com/dashboard/index',[
                'headers'=>['Cookie'=>$cookie]
            ]);
            $res = $user_info_response->getBody()->getContents();
            $pos = strpos($res,"https://mp.dayu.com/logout");
            if($pos>0) {
                $account_info->status=0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status=0;
            $account_info->save();
            \Log::error('dayu binded query status error:'.$e->getMessage());
        }

    }

    private function queryToutiao($account_info) {
        //https://mp.toutiao.com/mp/agw/article/list?size=20&status=all&from_time=0&start_time=0&end_time=0&search_word=&page=1&feature=0&source=all
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $response = $client->request('GET','https://mp.toutiao.com/mp/agw/media/get_media_info',[
                'headers'=>['Cookie'=>$cookie]
            ]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            if($res_arr['code'] == 0) {
                return true;
            }else{
                $account_info->status=0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status=0;
            $account_info->save();
            \Log::error('toutiao binded query status error:'.$e->getMessage());
        }

    }

    private function queryIqiyi($account_info) {
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            //print_r('111'."\r\n");
            $response = $client->request('GET','https://www.iqiyi.com/u/point',['headers'=>['Cookie'=>$cookie]]);
            $res = $response->getBody()->getContents();
            //print_r('222'."\r\n");
            //$response_html = HtmlDomParser::str_get_html($res);
            //print_r('333'."\r\n");
            //$title = $response_html->getElementByTagName('title');
            $pos = strpos($res,'爱奇艺登录注册-爱奇艺');
            //print_r('444'."\r\n");
            //var_dump($pos);
            if($pos>0) {
                $account_info->status=0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status=0;
            $account_info->save();
            \Log::error('aiqiyi binded query status error:'.$e->getMessage());
        }


    }

    private function queryYouku($account_info) {
        //https://mp.toutiao.com/mp/agw/article/list?size=20&status=all&from_time=0&start_time=0&end_time=0&search_word=&page=1&feature=0&source=all
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $sign = '';
            $appKey = '24679788';
            $data = '{"req":"{\"deviceType\":\"pc\",\"show\":1}"}';
            $t = timeStamp();
            $urlData = [
                'jsv'   =>'2.4.2',
                'appKey'=> $appKey,
                't'     => $t,
                'sign'  => $sign,
                'data'  => $data
            ];
            $url = "https://acs.youku.com/h5/mtop.youku.vip.xtop.member.profile.get/1.0/?".http_build_query($urlData);
            $response = $client->request('GET',$url,['headers'=>['cookie'=>$cookie]]);
            $result = json_decode($response->getBody()->getContents(),true);
            \Log::info("---RESULT111---".json_encode($result,JSON_UNESCAPED_UNICODE)."\n");
            if($result['ret'][0] == 'FAIL_SYS_ILLEGAL_ACCESS::非法请求'){
                $cookie_arr = $response->getHeader('Set-Cookie');
                foreach ($cookie_arr as $k=>$v){
                    $each_cookie = explode(';',$v);
                    if(strstr($each_cookie[0],'_m_h5_tk=')){
                        $token = substr(explode('=',$each_cookie[0])[1],0,32);
                    }
                    $arr[] = $each_cookie[0];
                }
                $cookie = explode(';',$cookie);
                foreach ($cookie as $k=>$v){
                    if(strstr($v,'_m_h5_tk=')){
                        unset($cookie[$k]);
                    }
                    if(strstr($v,'_m_h5_tk_enc=')){
                        unset($cookie[$k]);
                    }
                }
                $cookie = implode(";",$cookie).";".implode(';',$arr);
                $t = timeStamp();
                $sign =md5($token."&".$t."&".$appKey."&".$data);
                $urlData = [
                    'jsv'   =>'2.4.2',
                    'appKey'=> $appKey,
                    't'     => $t,
                    'sign'  => $sign,
                    'data'  => $data
                ];

                $url = "https://acs.youku.com/h5/mtop.youku.vip.xtop.member.profile.get/1.0/?".http_build_query($urlData);
                $response = $client->request('GET',$url,['headers'=>['cookie'=>$cookie]]);
                $result = json_decode($response->getBody()->getContents(),true);
                \Log::info("---RESULT22---".json_encode($result,JSON_UNESCAPED_UNICODE)."\n");
                if($result['ret'][0] == 'SUCCESS::调用成功'){
                    return true;
                }else{
                    $account_info->status=0;
                    $account_info->due_time=date('Y-m-d H:i:s');
                    $account_info->save();
                }

            }else{
                $account_info->status=0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status=0;
            $account_info->due_time=date('Y-m-d H:i:s');
            $account_info->save();
            \Log::error('youku binded query status error:'.$e->getMessage());
        }

    }

    private function queryKuaishou($account_info) {
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $post_data = [
                "operationName"=> "userInfoQuery",
                "variables"=> [],
                "query"=> "query userInfoQuery {  kshellBalance {    kshell    __typename  }  ownerInfo {    id    principalId    kwaiId    eid    userId    profile    name    description    sex    constellation    cityName    following    living    watchingCount    isNew    privacy    timestamp    feeds {      eid      photoId      thumbnailUrl      timestamp      __typename    }    verifiedStatus {      verified      description      type      new      __typename    }    countsInfo {      fan      follow      photo      liked      open      playback      private      __typename    }    bannedStatus {      banned      defriend      isolate      socialBanned      __typename    }    __typename  }}"
            ];
            $response = $client->request('POST','https://live.kuaishou.com/graphql',['headers'=>['Cookie'=>$cookie],'json'=>$post_data]);
            $res = json_decode($response->getBody()->getContents(),true);
            if(intval($res['data']['ownerInfo']['kwaiId'])>0) {
                return true;
            }else{
                $account_info->status=0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status=0;
            $account_info->due_time=date('Y-m-d H:i:s');
            $account_info->save();
            \Log::error('Kuaishou binded query status error:'.$e->getMessage());
        }

    }

    private function queryZhihu($account_info) {
        try{
            $client = new Client();
            $cookie = $account_info->cookie;
            $response = $client->request('GET','https://www.zhihu.com/api/v4/me?include=ad_type,available_message_types,default_notifications_count,follow_notifications_count,vote_thank_notifications_count,messages_count,draft_count,account_status,email,is_bind_phone,following_question_count,is_force_renamed,renamed_fullname',['headers' => ['Cookie'=>$cookie]]);
            $res = $response->getBody()->getContents();
            $res_arr = json_decode($res,true);
            if(is_array($res_arr)&&isset($res_arr['name'])){
                return true;
            }else{
                $account_info->status = 0;
                $account_info->due_time=date('Y-m-d H:i:s');
                $account_info->save();
            }
        }catch (\Exception $e){
            $account_info->status = 0;
            $account_info->save();
            \Log::error('zhihu binded query status error:'.$e->getMessage());

        }

    }
}