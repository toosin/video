<?php

namespace App\Console\Commands\Agents;

use App\Modules\Agents\Models\AgentPlatformStatistic;

use Log;
use DB;
use Illuminate\Console\Command;
use Sunra\PhpSimple\HtmlDomParser;

class MZYGZHDayStats extends Command
{
    /**
     * 执行命令   php artisan bill_task
     *
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mzy_gzh_day_stats';

    private $ch;
    private $cookie_file;
    private $platform;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '代理商获取麦子云平台公众号日数据';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("mzy day stat task start....");
            $this->init();//初始化

            $this->gatherByAccount();// 首先采集默认账号

            //切换其它账号继续跑
            $other_accounts = $this->getOtherAccounts();

            foreach ($other_accounts as $company_id=>$account)
            {
                Log::info('开始切换账号---');
                Log::info($account);
                if($this->checkAccount($company_id))//切换账号，继续跑
                {
                    $this->gatherByAccount();
                }else{
                    Log::info('切换失败---');
                }
            }

            $this->end();
        Log::info("mzy day stat task end....");
    }

    //采集账号日统计数据
    private function gatherByAccount()
    {
        $gzh_name = $this->getCurrentGzh();

        Log::info('开始采集日统计--');

        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d',strtotime('-1 month'));
        $init_home_url = $home_url = "http://xs.maizibook.com/statistic/OrderReport?pageIndex=1&beginTime={$start_date}&endTime={$end_date}";
        $response = '';
        $i=0;
        while (($response && $this->isHasNextPage($response)) || !$response) //首次进入或者没有下一页是退出
        {
            if($i>50) break;//防止异常，循环50次后自动退出

            if($response)
            {
                $next_page = $this->getNextPage($response);
                $query = parse_url($home_url);
                $_params = [];
                if(isset($query['query']))
                {
                    $_params = parse_str($query['query']);
                }

                $_params['pageindex'] = $next_page;

                $referer_url = $home_url;
                $home_url = $init_home_url.'&'.http_build_query($_params);
            }else{
                $referer_url = 'http://xs.maizibook.com/Home/Login';
            }
            dump('current gzh:'.$gzh_name);
            dump('referer :'.$referer_url);
            dump('home_url:'. $home_url);

            $this->setChOPT( CURLOPT_REFERER, $referer_url);
            $response = $this->getChInfo($home_url);
            $this->isLogout($response);


            //echo $response;

            $deal_res = $this->dealGzhDayList($response, $gzh_name);
            if($deal_res['is_gathered']) break;

            sleep(0.3);
            $i++;
        }
        Log::info('结束日统计--');
    }

    //初始化
    private function init()
    {
        $this->cookie_file = storage_path('cookie_file_mzy.txt');

        $this->platform = DB::table('agent_platforms')->where('platform','mzy')->first();

        if(!$this->platform || $this->platform->cookie_status ==0) dd('not login status');

        file_put_contents($this->cookie_file,$this->platform->cookie);

        $this->ch = curl_init();
    }

    //获取所有账号
    private function getOtherAccounts()
    {
        $response = $this->getChInfo('http://xs.maizibook.com/channelcompanyrelation/list');//获取其它账号

        $response_html = HtmlDomParser::str_get_html($response);
        $trs = $response_html->find('.table')[0]->find('tr');

        $data = [];
        foreach ($trs as $k=>$tr)
        {
            if($k>0)
            {
                $name = $tr->find('td',0)->text();
                $log_name = $tr->find('td',1)->text();
                $principle = $tr->find('td',2)->text();
                $company_id = $tr->find('td',3)->find('button',0)->getAttribute('data-companyid');
                $data[$company_id] = compact('name','log_name','principle','company_id');
            }
        }
        return $data;
    }

    //切换账号
    private function checkAccount($company_id)
    {
        $url = 'http://xs.maizibook.com/Home/Transform';

        $res = $this->postChInfo($url, ['channelCompanyId'=>$company_id]);
        if(strstr($res,'success'))
        {
            return true;
        }
        return false;
    }

    private function setChBaseOPT()
    {
        //curl_reset($this->ch);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->cookie_file);
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookie_file);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
    }

    private function setChOPT($item,$value)
    {
        curl_setopt($this->ch, $item, $value);
    }

    private function getChInfo($url)
    {
        $this->setChBaseOPT();//重置参数
        $this->setChOPT(CURLOPT_URL, $url);

        return curl_exec($this->ch);
    }

    private function postChInfo($url, $post_data=[])
    {
        $this->setChBaseOPT();//重置参数

        $this->setChOPT(CURLOPT_POST, 1);
        $this->setChOPT(CURLOPT_URL, $url);
        $post_data && $this->setChOPT(CURLOPT_POSTFIELDS, ($post_data));
        return curl_exec($this->ch);
    }

    //判断是否退出
    private function isLogout($res)
    {
        $str = "<script type='text/javascript'>top.location.replace('/Home/Login');</script>";
        if(strstr($res,$str))//退出
        {
            DB::table('agent_platforms')->where('platform',$this->platform->platform)->update(['cookie_status'=>0]);
            Log::info($this->platform->platform.' logout ...');
            dd($this->platform->platform.' logout ...');
        }
    }

    //结束
    private function end()
    {
        curl_close($this->ch);
        Log::info('normal ending...');
    }

    //是否有下一页
    private function isHasNextPage($response)
    {
        preg_match('/pageCount: parseInt\(\'(\d+)\'\)/',$response,$match);
        $page_count = $match[1];
        preg_match('/current: parseInt\(\'(\d+)\'\)/',$response,$match);
        $current = $match[1];
        return $current < $page_count;
    }

    //获取下一页数
    private function getNextPage($response)
    {
        preg_match('/pageCount: parseInt\(\'(\d+)\'\)/',$response,$match);
        $page_count = $match[1];
        preg_match('/current: parseInt\(\'(\d+)\'\)/',$response,$match);
        $current = $match[1];

        return max($page_count, $current+1);
    }

    //处理日统计页面数据
    private function dealGzhDayList($response, $gzh_name)
    {
        $home_html = HtmlDomParser::str_get_html($response);
        $trs = $home_html->find('.table')[0]->find('tr');
        foreach ($trs as $key=>$tr){
            if($key!=0){

                $date = trim($tr->find('td',0)->innertext);
                $new_user_recharge = str_replace('￥','',$tr->find('td',1)->find('p',0)->plaintext);
                $recharge_amount = str_replace('￥','',$tr->find('td',3)->find('p',0)->plaintext);
                $paid_user_num = (int)$tr->find('td',3)->find('p',1)->find('span',0)->plaintext;
                $paid_num = (int)$tr->find('td',4)->find('p',0)->plaintext;
                $unpaid_num = (int)$tr->find('td',4)->find('p',1)->find('span',0)->plaintext;
                $order_num = $paid_num+$unpaid_num;
                $platform_id = 2;

                $data = compact('new_user_recharge','recharge_amount','paid_num','order_num','paid_user_num');

                $condition = compact('date','gzh_name','platform_id');

                dump($data);
                dump($condition);

                //更新上报数据
                $static = AgentPlatformStatistic::firstOrCreate($condition,$data);
                if(!$static->wasRecentlyCreated)//判断是否为新建
                {
                    return ['is_gathered'=>1];
                }

            }
        }
        return ['is_gathered'=>0];

    }

    //获取当前公众号
    private function getCurrentGzh()
    {
        $res = $this->getChInfo('http://xs.maizibook.com/Home/Index');
        $this->isLogout($res);

        $res_html = HtmlDomParser::str_get_html($res);
        return trim($res_html->find('#companyname',0)->plaintext);
    }

    //麦子云把当日的实时数据（访问uv）通过此接口获取，当日+历史=累计
    private function getTodayData($home_url)
    {
        //$this->setChOPT(CURLOPT_REFERER,$home_url);
        $res = $this->getChInfo('http://xs.maizibook.com/Promote/GetCountList');
        $re = [];
        try{
            $re = json_decode($res,true);
        }catch (\Exception $e)
        {
            Log::info('today_data formate wrong...');
        }
        return $re;
    }

}