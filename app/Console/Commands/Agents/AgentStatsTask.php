<?php
/**
 * Created by PhpStorm.
 * User: tandunzhao
 * Date: 2017/11/20
 * Time: 下午5:26
 */

namespace App\Console\Commands\Agents;

use App\Modules\Agents\Models\AgentPlatformStatistic;
use App\Modules\Agents\Models\AgentPlatformStats;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Psr7\Request;
use Log;
use Illuminate\Console\Command;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\DomCrawler\Crawler;

class AgentStatsTask extends Command
{
    /**
     * 执行命令   php artisan bill_task
     *
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent_stats_task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '代理商获取平台数据';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $platforms = \DB::table('agent_platforms')->where('platform','youshuge')->first();
        if($platforms->cookie_status == 1){
            $cookie_file = storage_path('cookie_file.txt');
            file_put_contents($cookie_file,$platforms->cookie);
            sleep(3);
            $home_url = 'https://vip.youshuge.com/';

           // $url ='https://vip.youshuge.com/user_login?id=4360';
            $ch = curl_init($home_url);

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
            //$response = curl_exec($ch);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $response = curl_exec($ch);

            $now_domian = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL);

            if($now_domian == 'https://vip.youshuge.com/login') {
                \DB::table('agent_platforms')->where('platform','youshuge')->update(['cookie_status'=>0]);
                print_r('cookie invalid !');
            }else{
                $home_html = HtmlDomParser::str_get_html($response);
                $trs = $home_html->find('table',0)->find('tr');
                foreach ($trs as $key=>$tr){
                    if($key!=0){
                        $url_r = $tr->find('td',10);
                        if($url_r){
                            $url=  $url_r->find('a',0)->href;
                            $url = trim($url);
                            print_r($url);
                            curl_setopt($ch,CURLOPT_URL,$url);
                            $response1 = curl_exec($ch);
                            $this->getDailyStats($response1,$ch);
                            $url_now = curl_getinfo($ch);
                            if($url_now == 'https://vip.youshuge.com/login'){
                                \DB::table('agent_platforms')->where('platform','youshuge')->update(['cookie_status'=>0]);
                                //print_r('cookie invalid !');
                            }else{
                                $start_time = date('Y-m-d 00:00:00',strtotime('-7 day'));
                                if(strtotime($start_time)<strtotime('2019-06-25')){
                                    $start_time = '2019-06-25 00:00:00';
                                }
                                $end_time = date('Y-m-d 00:00:00',strtotime('+1 day'));
                                $params = 'start_time='.urlencode($start_time).'&end_time='.urlencode($end_time);
                                //print_r($params);
                                $url_ = 'https://novel.youshuge.com/activity/follows?'.$params;
                                //print_r($url_);
                                curl_setopt($ch,CURLOPT_URL,$url_);
                                $response =  curl_exec($ch);
                                //file_put_contents(storage_path('res.html'),$response);
                                $html = HtmlDomParser::str_get_html($response);
                                $trs = $html->find('table',0)->find('tr');
                                foreach ($trs as $key=>$tr){
                                    try{
                                        if($key!=0){
                                        $td = $tr->find('td');
                                        //td[0]
                                        $link_wrap = $td[0];
                                        $span =$link_wrap->find('span');
                                        #gx2-2019/06/21-蔡陈飞
                                        $tag = $span[0]->innertext;
                                        $arr = explode('-',$tag);
                                        if(count($arr)>=4) {
                                            $agent = count($arr)>=4?$arr[3]:'';
                                            print_r($agent."\n");
                                            $date = str_replace('/','-',count($arr)>=4?$arr[2]:'');
                                            $agent_info = \DB::table('agent_manage')->where('agent_tag',$agent)->first();
                                            $agent_id = 0;
                                            if($agent_info) {
                                                $agent_id = $agent_info->id;
                                            }
                                            $link = $span[1]->find('button',0)->getAttribute('data-clipboard-text');
                                            preg_match("|ext_redirect\/([0-9]+)|",$link,$matches);
                                            $send_order_id = $matches[1];
                                            $enter_chapter = $td[1]->innertext;
                                            $click_num = $td[2]->innertext;
                                            $new_user = $td[3]->innertext;
                                            $recharge = $td[4]->innertext;
                                            $recharge_num = $td[5]->innertext;
                                            $cost = $td[6]->find('u',0)->innertext;

                                            $gzh_name = $html->find('title',0)->innertext;
                                            $insert=['send_order_id'=>$send_order_id];

                                            $data = [
                                                'platform_id'=>1,
                                                'tag'=>trim($tag),
                                                'link'=>trim($link),
                                                'enter_chapter'=>trim($enter_chapter),
                                                'click_num'=>trim($click_num),
                                                'inc_num'=>trim($new_user),
                                                'recharge_amount'=>trim($recharge),
                                                'recharge_num'=>trim($recharge_num),
                                                'cost'=>trim($cost),
                                                'agent_id'=>$agent_id,
                                                'date'=>$date,
                                                'gzh_name'=>$gzh_name
                                            ];
                                            //print_r($data);
                                            $pre_rec = AgentPlatformStats::where('send_order_id',$send_order_id)->first();
                                            if($pre_rec){
                                                if(empty($pre_rec->uv_twenty_start_time)) {//如果uv大于20的起始时间为空并且uv已大于20，更新时间
                                                    print_r('send_order_id:'.$send_order_id.':'.$data['click_num']."\n");
                                                    if($data['click_num']>=20) {
                                                        $data['uv_twenty_start_time']=date('Y-m-d H:i:s');
                                                    }
                                                }else{
                                                    if(empty($pre_rec->recharge_in_one_day) && (time()-strtotime($pre_rec->uv_twenty_start_time))>=3600*24) {
                                                        $data['recharge_in_one_day'] = $data['recharge_amount'];
                                                        $data['recharge_in_one_day_update_time'] = date('Y-m-d H:i:s');

                                                    }
                                                    if(empty($pre_rec->recharge_in_three_days) && (time()-strtotime($pre_rec->uv_twenty_start_time))>=3600*72) {
                                                        $data['recharge_in_three_days'] = $data['recharge_amount'];
                                                        $data['recharge_in_three_days_update_time'] = date('Y-m-d H:i:s');

                                                    }

                                                }

                                                if($pre_rec->date && empty($pre_rec->recharge_in_one_day_in_days)){
                                                    $s_time = date_create($pre_rec->date);
                                                    $e_date = date_create(date('Y-m-d'));
                                                    $date_diff = date_diff($e_date,$s_time)->format('%a');
                                                    if($date_diff>=1) {
                                                        $data['recharge_in_one_day_in_days'] = $data['recharge_amount'];
                                                        $data['recharge_in_one_day_in_days_update_time'] = date('Y-m-d H:i:s');

                                                    }
                                                }

                                                if($pre_rec->date && empty($pre_rec->recharge_in_three_days_in_days)){
                                                    $s_time = date_create($pre_rec->date);
                                                    $e_date = date_create(date('Y-m-d'));
                                                    $date_diff = date_diff($e_date,$s_time)->format('%a');
                                                    if($date_diff>=3) {
                                                        $data['recharge_in_three_days_in_days'] = $data['recharge_amount'];
                                                        $data['recharge_in_three_days_in_days_update_time'] = date('Y-m-d H:i:s');
                                                    }
                                                }


                                            }else{
                                                if($data['click_num']>=20) {
                                                    $data['uv_twenty_start_time']=date('Y-m-d H:i:s');
                                                }
                                            }
                                            //print_r($data);
                                            AgentPlatformStats::updateOrCreate($insert,$data);
                                        }
                                        //print_r($tag.'--'.$link.'--'.$enter_chapter.'--'.$click_num.'--'.$new_user.'--'.$recharge.'--'.$recharge_num.'--'.$cost."\n");
                                    }
                                     }catch (\Exception $exception){
                                       \Log::error('youshuge crawl exception:'.$exception->getMessage());
                                     }

                                }
                            }
                        }//

                    }
                }
            }
            //print_r($response);

            curl_close($ch);
        }
    }

    private function tmp(){
        $user_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36";

        /*$cookie = 'laravel_session=eyJpdiI6IldVWFRSNU1MUVZvVzFCVU1kWkp3WkE9PSIsInZhbHVlIjoiVFJ0XC94TzBGektKcDlkd21IRVQ0Z3NlS3ZvYzBvVytXWU1GNWtVckhaQ3oyWnVNQ21HTndMTSszcTdLamZXdWNxb0RTYUNWSmhpS3pwUFZmMEFVTGJnPT0iLCJtYWMiOiJjZjdjYTU4ZmRkZGE1MTAxY2Q4YTNhZGE1NWI1OTYzOTZmMGUzOTBlZGE2M2IwOGZmMmVjZTlmNmU2MTZjMzNjIn0%3D';
        $client = new Client();
        $request = $client->request('POST', 'https://vip.youshuge.com/check_login',[
            'headers'=> [
                'Cookie'=>$cookie,
                'Referer'=>'https://vip.youshuge.com/login'
            ],
            'form_params' => [
                'user_name' => '小方先生',
                'password' => 'wzyy2018',
                'captcha'=>'mdua'
            ]
        ]);
        $res = $request->getBody()->getContents();
        print_r($res);
        return ;*/
        $this->test();
        $client = new Client();
        $request = $client->request('POST', 'https://vip.youshuge.com/check_login',[
            'headers'=> [
                'User-Agent'=>$user_agent,
                'Referer'=>'https://vip.youshuge.com/login',
                'Content-Type'=>'application/x-www-form-urlencoded',
                'Cookie'=>file_get_contents(storage_path('cookie.txt'))
            ],
            'form_params' => [
                'user_name' => '小方先生',
                'password' => 'wzyy2018',
                'captcha'=>'7a9m'
            ],
        ]);
        $res = $request->getBody()->getContents();

        dd($res);


        $client = new Client(['cookies' => true]);

        $jar = new CookieJar();
        $r = $client->request('GET', 'https://vip.youshuge.com/captcha',['cookies'=>$jar]);
//
//        $cookie1 = $r->getHeader('Set-Cookie')[0];
//        $cookie1 = explode(';',$cookie1)[0];

        $captcha = $r->getBody()->getContents();
        $captcha = json_decode($captcha,true);
        $captcha_url = $captcha['data']['url'];
        dump($captcha_url."\n");
        //print_r('cookie1:'.$cookie1."\n");
        /* $this->getCaptcha($captcha_url,$cookie1);
         return;*/
        $response = $client->request('GET', $captcha_url,['cookies'=>$jar]);
        //file_put_contents(storage_path('captcha.jpg'),$captcha);
        $cookie = $response->getHeader('Set-Cookie')[0];
        //dd($cookie);

        //$cookie = explode(';',$cookie)[0];
        // print_r($cookie);
        //print_r($response->getHeader(''));//
        //print_r($response->getBody()-);
        $captcha = $response->getBody()->getContents();

        file_put_contents(storage_path('captcha.jpg'),$captcha);
        file_put_contents(storage_path('cookie.txt'),$cookie);
    }

    private function test()
    {
        $user_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36";

        $ch = curl_init('https://vip.youshuge.com');
        $cookie_file = storage_path('cookie2.txt');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
       // curl_setopt($ch, CURLOPT_POST, 1);
        $headers = [
            'User-Agent'=>$user_agent,
            'Referer'=>'https://vip.youshuge.com/login',
            'Content-Type'=>'application/x-www-form-urlencoded',
        ];
        $post_fields = [
            'user_name' => '小方先生',
            'password' => 'wzyy2018',
            'captcha'=>'9ffz'
        ];
       // curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
       // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $response = curl_exec($ch);
        dd($response);

        $ch = curl_init('https://vip.youshuge.com/captcha');
        $cookie_file = storage_path('cookie2.txt');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        $response = curl_exec($ch);
        //dd($response);
        $re = json_decode($response,1);
        $url = $re['data']['url'];


        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);
        file_put_contents(storage_path('captcha.jpg'),$response);

        dd(1);


    }

    private function getCaptcha($url,$cookie) {

        $oCurl = curl_init();
        // 设置请求头, 有时候需要,有时候不用,看请求网址是否有对应的要求
        $header[] = "Cookie: ".$cookie;
        $user_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36";
        curl_setopt($oCurl, CURLOPT_URL, $url);
       // curl_setopt($oCurl, CURLOPT_HTTPHEADER,$header);
        // 返回 response_header, 该选项非常重要,如果不为 true, 只会获得响应的正文
        curl_setopt($oCurl, CURLOPT_HEADER, true);
        // 是否不需要响应的正文,为了节省带宽及时间,在只需要响应头的情况下可以不要正文
        //curl_setopt($oCurl, CURLOPT_NOBODY, true);
        // 使用上面定义的 ua
        curl_setopt($oCurl, CURLOPT_USERAGENT,$user_agent);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );

        // 不用 POST 方式请求, 意思就是通过 GET 请求
        curl_setopt($oCurl, CURLOPT_POST, false);

        $sContent = curl_exec($oCurl);
        // 获得响应结果里的：头大小
        $headerSize = curl_getinfo($oCurl, CURLINFO_HEADER_SIZE);
        // 根据头大小去获取头信息内容
        $header = substr($sContent, 0, $headerSize);

        $r = preg_match("|Set-Cookie:([^;.]+);|",$header,$matches);

        print_r($matches[1]."\n");
        //print_r($sContent);
        $captcha = trim(substr($sContent,$headerSize-1));
        curl_close($oCurl);
        file_put_contents(storage_path('captcha2.jpg'),$captcha);
    }

    private function getDailyStats($response,$ch) {

        //$response =  curl_exec($ch);
        $html = HtmlDomParser::str_get_html($response);
        $this->getPageContents($response);
        /*$pages = $html->find('.box-footer',0)->find('ul',0)->find('li',0)->find('span',0)->innertext;
        preg_match('|[^0-9]*([0-9]+)|',$pages,$matches);
        $pages = $matches[1];
        for($i=2;$i<=$pages;$i++){
            $url = 'https://novel.youshuge.com/?page='.$i;
            curl_setopt($ch, CURLOPT_URL, $url);
            $response =  curl_exec($ch);
            $this->getPageContents($response);
        }*/
    }

    private function getPageContents($response){
        $html = HtmlDomParser::str_get_html($response);

        $trs = $html->find('table',0)->find('tr');
        foreach ($trs as $key => $tr) {
            if($key>1) {
                $tds = $tr->find('td');
                $date_raw = $tds[0]->innertext;
                $date = substr($date_raw,0,4).'-'.substr($date_raw,4,2).'-'.substr($date_raw,6,2);
                $recharge_amount = $tds[1]->innertext;
                $order_num = $tds[2]->innertext;
                $paid_user_num = $tds[3]->innertext;
                $paid_num = $tds[4]->innertext;
                $new_user = $tds[5]->innertext;
                $new_user_sub = $tds[6]->innertext;
                $new_user_recharge= $tds[8]->innertext;
                $accum_user_num= $tds[9]->innertext;
                $accum_sub_num= $tds[10]->innertext;

                $platform_id = 1;
                $gzh_name = $html->find('title',0)->innertext;
                $to_insert = compact('gzh_name','date','platform_id');
                $to_update = compact('recharge_amount','order_num','paid_user_num','paid_num','new_user','new_user_recharge','new_user_sub','accum_sub_num','accum_user_num');
                //print_r($to_update);
                AgentPlatformStatistic::updateOrCreate($to_insert,$to_update);
            }
        }
    }
}