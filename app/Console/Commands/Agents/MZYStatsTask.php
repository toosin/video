<?php
/**
 * Created by PhpStorm.
 * User: tandunzhao
 * Date: 2017/11/20
 * Time: 下午5:26
 */

namespace App\Console\Commands\Agents;

use App\Modules\Agents\Models\AgentPlatformStats;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Psr7\Request;
use Log;
use DB;
use Illuminate\Console\Command;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\DomCrawler\Crawler;

class MZYStatsTask extends Command
{
    /**
     * 执行命令   php artisan bill_task
     *
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mzy_stats_task';

    private $ch;
    private $cookie_file;
    private $platform;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '代理商获取麦子云平台数据';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("mzy stat task start....");
            $this->init();//初始化

            $this->gatherByAccount();// 首先采集默认账号

            //切换其它账号继续跑
            $other_accounts = $this->getOtherAccounts();

            foreach ($other_accounts as $company_id=>$account)
            {
                Log::info('开始切换账号---');
                Log::info($account);
                if($this->checkAccount($company_id))//切换账号，继续跑
                {
                    $this->gatherByAccount();
                }else{
                    Log::info('切换失败---');
                }
            }

            $this->end();
        Log::info("mzy stat task end....");
    }

    //采集账号数据
    private function gatherByAccount()
    {
        Log::info('开始采集--');

        $init_home_url = $home_url = 'http://xs.maizibook.com/promote/list';
        $response = '';
        $i=0;
        $gzh_name = $this->getCurrentGzh();
        while (($response && $this->isHasNextPage($response)) || !$response) //首次进入或者没有下一页是退出
        {
            if($i>50) break;//防止异常，循环50次后自动退出

            if($response)
            {
                $next_page = $this->getNextPage($response);
                $query = parse_url($home_url);
                $_params = [];
                if(isset($query['query']))
                {
                    $_params = parse_str($query['query']);
                }

                $_params['pageindex'] = $next_page;

                $referer_url = $home_url;
                $home_url = $init_home_url.'?'.http_build_query($_params);
            }else{
                $referer_url = 'http://xs.maizibook.com/Home/Login';
            }
            dump('referer :'.$referer_url);
            dump('home_url:'. $home_url);

            $this->setChOPT( CURLOPT_REFERER, $referer_url);
            $response = $this->getChInfo($home_url);
            $this->isLogout($response);

            $today_data = $this->getTodayData($home_url);

            //echo $response;

            $this->dealPromotionList($gzh_name, $response, $today_data);

            sleep(0.3);
            $i++;
        }
        Log::info('开始结束--');
    }

    //初始化
    private function init()
    {
        $this->cookie_file = storage_path('cookie_file_mzy.txt');

        $this->platform = DB::table('agent_platforms')->where('platform','mzy')->first();

        if(!$this->platform || $this->platform->cookie_status ==0) dd('not login status');

        file_put_contents($this->cookie_file,$this->platform->cookie);

        $this->ch = curl_init();
    }

    //获取所有账号
    private function getOtherAccounts()
    {
        $response = $this->getChInfo('http://xs.maizibook.com/channelcompanyrelation/list');//获取其它账号

        $response_html = HtmlDomParser::str_get_html($response);
        $trs = $response_html->find('.table')[0]->find('tr');

        $data = [];
        foreach ($trs as $k=>$tr)
        {
            if($k>0)
            {
                $name = $tr->find('td',0)->text();
                $log_name = $tr->find('td',1)->text();
                $principle = $tr->find('td',2)->text();
                $company_id = $tr->find('td',3)->find('button',0)->getAttribute('data-companyid');
                $data[$company_id] = compact('name','log_name','principle','company_id');
            }
        }
        return $data;
    }

    //切换账号
    private function checkAccount($company_id)
    {
        $url = 'http://xs.maizibook.com/Home/Transform';

        $res = $this->postChInfo($url, ['channelCompanyId'=>$company_id]);
        if(strstr($res,'success'))
        {
            return true;
        }
        return false;
    }

    private function setChBaseOPT()
    {
        //curl_reset($this->ch);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->cookie_file);
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookie_file);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
    }

    private function setChOPT($item,$value)
    {
        curl_setopt($this->ch, $item, $value);
    }

    private function getChInfo($url)
    {
        $this->setChBaseOPT();//重置参数
        $this->setChOPT(CURLOPT_URL, $url);

        return curl_exec($this->ch);
    }

    private function postChInfo($url, $post_data=[])
    {
        $this->setChBaseOPT();//重置参数

        $this->setChOPT(CURLOPT_POST, 1);
        $this->setChOPT(CURLOPT_URL, $url);
        $post_data && $this->setChOPT(CURLOPT_POSTFIELDS, ($post_data));
        return curl_exec($this->ch);
    }

    //判断是否退出
    private function isLogout($res)
    {
        $str = "<script type='text/javascript'>top.location.replace('/Home/Login');</script>";
        if(strstr($res,$str))//退出
        {
            DB::table('agent_platforms')->where('platform',$this->platform->platform)->update(['cookie_status'=>0]);
            Log::info($this->platform->platform.' logout ...');
            dd($this->platform->platform.' logout ...');
        }
    }

    //结束
    private function end()
    {
        curl_close($this->ch);
        Log::info('normal ending...');
    }

    //是否有下一页
    private function isHasNextPage($response)
    {
        preg_match('/pageCount: parseInt\(\'(\d+)\'\)/',$response,$match);
        $page_count = $match[1];
        preg_match('/current: parseInt\(\'(\d+)\'\)/',$response,$match);
        $current = $match[1];
        return $current < $page_count;
    }

    //获取下一页数
    private function getNextPage($response)
    {
        preg_match('/pageCount: parseInt\(\'(\d+)\'\)/',$response,$match);
        $page_count = $match[1];
        preg_match('/current: parseInt\(\'(\d+)\'\)/',$response,$match);
        $current = $match[1];

        return max($page_count, $current+1);
    }

    //处理推广页面数据
    private function dealPromotionList($gzh_name, $response, $today_data)
    {
        $home_html = HtmlDomParser::str_get_html($response);
        $trs = $home_html->find('.table')[0]->find('tr');
        foreach ($trs as $key=>$tr){
            if($key!=0){
                $tag = $tr->find('td',0)->find('p',0)->innertext;
                $tag = preg_replace('/\s*\(.*?\)/','',$tag);

                $link = $tr->find('td',0)->find('p',1)->find('span',0)->innertext;
                $link = htmlspecialchars_decode($link);
                $enter_chapter = '';
                $book_name = $tr->find('td',1)->find('div',0)->find('p',0)->innertext;
                $chapter_name = $tr->find('td',1)->find('div',0)->find('p',1)->innertext;
                $click_num = $tr->find('td',3)->innertext;
                $recharge = $tr->find('td',4)->find('p',0)->innertext;
                $link_create_time = $tr->find('td',0)->find('p',2)->innertext;
                $new_user = $tr->find('td',5)->innertext;
                $promotion_code = str_replace('link_active_','',$tr->find('td',5)->getAttribute('data-active'));

                //增加当日激活数据
                foreach ($today_data as $_tmp_data)
                {
                    if($_tmp_data['PromotionCode'] == $promotion_code)
                    {
                        $new_user += $_tmp_data['ActiveUserCount'];
                    }
                }

                $recharge_num = 0;
                $arr = explode('-',$tag);
                if(count($arr) != 4) continue;
                $date = $arr[2];
                $agent_tag = $arr[3];
                $agent_info = DB::table('agent_manage')->where('agent_tag',$agent_tag)->first();

                $url_query = [];
                parse_str(parse_url($link)['query'],$url_query);
                $send_order_id = $url_query['id'];
                $platform_id = 2;
                $uv_twenty_start_time = null;
                $data = [
                    'platform_id'=>$platform_id,
                    'tag'=>trim($tag),
                    'link'=>trim($link),
                    'enter_chapter'=>$book_name.'-'.$chapter_name,
                    'click_num'=>trim($new_user),
                    'click_pv'=>trim($click_num),
                    'inc_num'=>0,
                    'recharge_amount'=>trim($recharge),
                    'recharge_num'=>trim($recharge_num),
                    'agent_id'=>$agent_info ? $agent_info->id : null,
                    'date'=>$date,
                    'link_create_time'=>$link_create_time,
                    'gzh_name'=>$gzh_name
                ];

                $condition = compact('send_order_id','platform_id');

                $current_time = time();
                $current_datetime = date('Y-m-d H:i:s');
                //更新上报数据
                $stat = AgentPlatformStats::where('send_order_id',$send_order_id)->where('platform_id',$platform_id)->first();

                //uv20时间点
                if((!$stat || !$stat->uv_twenty_start_time) && $data['click_num'] >= 20) $data['uv_twenty_start_time'] = date('Y-m-d H:i:s');


                //24、72小时充值
                if($stat && $stat->uv_twenty_start_time)
                {
                    if(!$stat->recharge_in_one_day && $current_time - strtotime($stat->uv_twenty_start_time) >= 86400)
                    {
                        $data['recharge_in_one_day'] = $data['recharge_amount'];
                        $data['recharge_in_one_day_update_time'] = $current_datetime;
                    }
                    if(!$stat->recharge_in_three_days && $current_time - strtotime($stat->uv_twenty_start_time) >= 3*86400)
                    {
                        $data['recharge_in_three_days'] = $data['recharge_amount'];
                        $data['recharge_in_three_days_update_time'] = $current_datetime;
                    }
                }

                //按date计算24、72小时充值
                if($stat && $stat->date)//统计数据不存在或者字段为空
                {
                    if(!$stat->recharge_in_one_day_in_days_update_time && $current_time - strtotime($stat->date) >= 86400)
                    {
                        $data['recharge_in_one_day_in_days'] = $data['recharge_amount'];
                        $data['recharge_in_one_day_in_days_update_time'] = $current_datetime;
                    }

                    if(!$stat->recharge_in_three_days_in_days_update_time && $current_time - strtotime($stat->date) >= 3*86400)
                    {
                        $data['recharge_in_three_days_in_days'] = $data['recharge_amount'];
                        $data['recharge_in_three_days_in_days_update_time'] = $current_datetime;
                    }

                }

                //按自然日24、72小时充值

                AgentPlatformStats::updateOrCreate($condition,$data);
            }
        }

    }

    //麦子云把当日的实时数据（访问uv）通过此接口获取，当日+历史=累计
    private function getTodayData($home_url)
    {
        //$this->setChOPT(CURLOPT_REFERER,$home_url);
        $res = $this->getChInfo('http://xs.maizibook.com/Promote/GetCountList');
        $re = [];
        try{
            $re = json_decode($res,true);
        }catch (\Exception $e)
        {
            Log::info('today_data formate wrong...');
        }
        return $re;
    }

    //获取当前公众号
    private function getCurrentGzh()
    {
        $res = $this->getChInfo('http://xs.maizibook.com/Home/Index');
        $this->isLogout($res);

        $res_html = HtmlDomParser::str_get_html($res);
        return trim($res_html->find('#companyname',0)->plaintext);
    }

}