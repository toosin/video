@extends('layout.app')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">提现列表</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    提现信息
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:75px;">站点：</label>
                            <input class="form-control" id="distribution_channel_id" name="distribution_channel_id"/>
                        </div>

                        <div class="col-xs-3 col-sm-3 col-md-3"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:180px;">开始时间：</label><input style="background: #fff;"
                                                                                             id="zstart"
                                                                                             class="form-control"
                                                                                             name="zstart" readonly/>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:180px;">结束时间：</label><input style="background: #fff;"
                                                                                             id="zend"
                                                                                             class="form-control"
                                                                                             name="zend" readonly/>&nbsp;
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:100px;">状态：</label>
                            <select id="status" onchange="updateTable()" name="status" class="form-control">
                                <option value="0">请选择状态</option>

                                <option value="自动打款成功">自动打款成功</option>
                                <option value="人工打款成功">人工打款成功</option>

                            </select>
                            <button id="search" class="btn btn-primary" style="margin-left: 20px">搜索</button>
                            &nbsp;<button id="export" class="btn btn-info">导出</button>
                        </div>


                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>站点</th>
                            {{--<th>头像</th>--}}
                            <th>打款时间</th>
                            <th>打款金额</th>
                            <th>打款状态</th>
                            <th>收款银行账户</th>
                            <th>收款账户名称</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <p id="amount_h"
                       style="width: 100%;text-align: right;margin-right: 20px;margin-bottom: 20px;font-weight: bold;color:red;"></p>
                    <div id="pagination" style="float:right;" class="m-style">

                    </div>
                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('jsscript')
    @parent
    <!-- Metis Menu Plugin JavaScript -->

    <!-- DataTables JavaScript -->
    <script src="/js/layer.js"></script>
    <script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="/vendor/pagination/jquery.pagination.js"></script>
    <script src="/vendor/datepick/bootstrap-datepicker.min.js"></script>
    <script src="/vendor/datepick/bootstrap-datepicker.zh-CN.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/reset.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/highlight.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/common.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/pagination.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/pagination.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendor/datepick/bootstrap-datepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://apps.bdimg.com/libs/layer/2.1/skin/layer.css" rel="stylesheet">
    <script src="/js/layer.js"></script>
    <script>
        var table = null;
        var order_column = [1, 'desc'];
        var column_hash = ['date', 'distribution_channel_id', 'cost', 'charge_amount', 'official_account_name',
            'company_name', 'total_subscribe_num', 'new_subscribe_num', 'cancel_subscribe_num',
            'cancel_subscribe_rate', 'promotion_point', 'agent_name']
        $(function () {

            $.fn.datepicker.dates['cn'] = {   //切换为中文显示
                days: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
                daysShort: ["日", "一", "二", "三", "四", "五", "六", "七"],
                daysMin: ["日", "一", "二", "三", "四", "五", "六", "七"],
                months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                today: "今天",
                clear: "清除"
            };

            $('#zstart').datepicker({
                // format: 'yyyy-mm-dd',
                autoclose: true, //自动关闭
                beforeShowDay: $.noop,    //在显示日期之前调用的函数
                calendarWeeks: false,     //是否显示今年是第几周
                clearBtn: false,          //显示清除按钮
                daysOfWeekDisabled: [],   //星期几不可选
                endDate: Infinity,        //日历结束日期
                forceParse: true,         //是否强制转换不符合格式的字符串
                format: 'yyyy-mm-dd',     //日期格式
                keyboardNavigation: true, //是否显示箭头导航
                language: 'cn',           //语言
                minViewMode: 0,
                orientation: "auto",      //方向
                rtl: false,
                startDate: -Infinity,     //日历开始日期
                startView: 0,             //开始显示
                todayBtn: false,          //今天按钮
                todayHighlight: false,    //今天高亮
                weekStart: 0              //星期几是开始
            });
            $('#zend').datepicker({
                // format: 'yyyy-mm-dd',
                autoclose: true, //自动关闭
                beforeShowDay: $.noop,    //在显示日期之前调用的函数
                calendarWeeks: false,     //是否显示今年是第几周
                clearBtn: false,          //显示清除按钮
                daysOfWeekDisabled: [],   //星期几不可选
                endDate: Infinity,        //日历结束日期
                forceParse: true,         //是否强制转换不符合格式的字符串
                format: 'yyyy-mm-dd',     //日期格式
                keyboardNavigation: true, //是否显示箭头导航
                language: 'cn',           //语言
                minViewMode: 0,
                orientation: "auto",      //方向
                rtl: false,
                startDate: -Infinity,     //日历开始日期
                startView: 0,             //开始显示
                todayBtn: false,          //今天按钮
                todayHighlight: false,    //今天高亮
                weekStart: 0              //星期几是开始
            });

            updateTable();
            $('#search').on('click', function () {
                updateTable();
            })

            $('#export').on('click', function () {
                let para =
                    "?start_date=" + $('#zstart').val()
                    + "&end_date=" + $('#zend').val()
                    + "&distribution_channel_id=" + $('#distribution_channel_id').val()
                    + "&status=" + $('#status').val();
                location.href = '/withdraw/export' + para;
            })
        });

        function updateTable(page = 1, action = 'update') {
            var index = layer.load()
            $.ajax({
                url: '/api/withdraw/getWithdrawList',
                type: 'POST',
                data: {
                    'page': page,
                    //'pagesize':20,
                    'start_date': $('#zstart').val(),
                    'end_date': $('#zend').val(),
                    'distribution_channel_id': $('#distribution_channel_id').val(),
                    'status': $('#status').val(),
                    'orderBy': column_hash[order_column[0]],
                    'orderType': order_column[1]
                },
                success: function (res) {
                    console.log(res)
                    layer.close(index)
                    if (res.data.list.length <= 0) {
                        res.data.list = [];
                    }
                    if (table != null) table.destroy();
                    table = $('#dataTables-example').DataTable({
                        data: res.data.list,
                        responsive: true,
                        paging: false,
                        searching: false,
                        retrieve: true,
                        order: order_column,
                        columns: [
                            {data: 'distribution_channel_id'},
                            {data: 'pay_time'},
                            {data: 'amount'},
                            {data: 'status'},
                            {data: 'bank_account'},
                            {data: 'account_name'}
                        ]
                    });
                    //排序
                    $('#dataTables-example').on('order.dt', function () {
                        // This will show: "Ordering on column 1 (asc)", for example
                        var order = table.order();
                        //
                        //console.log(order)
                        /*$.each(order,function (key,value) {

                        })*/
                        if (order.length > 0) {
                            order_column = order[0]
                            updateTable()
                        }
                        /*setTimeout(function () {
                            //updateTable()
                        },1000)*/

                        //$('#orderInfo').html( 'Ordering on column '+order[0][0]+' ('+order[0][1]+')' );
                    });
                    //第一次更新初始化分页
                    if (action == 'update') {
                        $("#pagination").pagination({
                            pageCount: res.data.meta.last_page,
                            jump: true,
                            coping: true,
                            homePage: '首页',
                            endPage: '末页',
                            prevContent: '上页',
                            nextContent: '下页',
                            callback: function (api) {
                                var current_page = api.getCurrent()
                                updateTable(current_page, 'page_change')
                            }
                        });
                    }
                }
            })
            $.ajax({
                url: '/api/withdraw/amount',
                type: 'POST',
                data: {
                    'start_date': $('#zstart').val(),
                    'end_date': $('#zend').val(),
                    'distribution_channel_id': $('#distribution_channel_id').val(),
                    'status': $('#status').val()
                },
                success: function (res) {
                    $('#amount_h').html('打款总额：' + res.data)
                }
            })
        }
    </script>
@endsection

