@extends('layout.app')
@section('content')
<style>


</style>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">信息流日报</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    信息流日报
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:60px;">站点：</label>
                            <input class="form-control" id="distribution_channel_id" name="distribution_channel_id"/>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:100px;">代理商：</label>
                            <input class="form-control" id="agent_name" name="agent_name"/>
                        </div>


                        <div class="col-xs-4 col-sm-4 col-md-4"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:180px;">推广渠道：</label>
                            <input style="background: #fff;" id="promotion_channel" class="form-control" name="promotion_channel" />
                            <button id="search" class="btn btn-primary">搜索</button>
                            &nbsp;<button id="export" class="btn btn-info">导出</button>
                        </div>



                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class=""><a id="tab-toggle-1" href="#home" data-toggle="tab" aria-expanded="false">3天内派单</a>
                        </li>
                        <li class=""><a id="tab-toggle-2" href="#profile" data-toggle="tab" aria-expanded="false">3天以上派单</a>
                        </li>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade" id="home">
                            <table style="min-width: 2048px;" class="table table-striped table-bordered table-hover width-200" id="dataTables-example1">
                                <thead>
                                <tr>
                                    <th class="show_link">展示链接</th>
                                    <th>站点</th>
                                    <th>推广渠道</th>
                                    <th>代理商</th>
                                    <th>推广名称</th>
                                    <th>发送时间</th>
                                    <th>推广员</th>
                                    <th>书名</th>
                                    <th>点击数</th>
                                    <th>注册用户数</th>
                                    <th>注册成本</th>
                                    <th>付费转化率</th>
                                    <th>新增关注</th>
                                    <th>新增关注率</th>
                                    <th>新关成本</th>
                                    <th>付费用户</th>
                                    <th>首充用户数</th>
                                    <th>成本</th>
                                    <th>累计充值</th>
                                    <th>累计充值成本率</th>
                                    <th>72小时累计充值总额</th>
                                    <th>72小时累计充值回本率</th>
                                    <th>72小时充值总额</th>
                                    <th>72小时回本率</th>
                                    <th>备注</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <div id="pagination1" style="float:right;" class="m-style">

                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile">
                            <table style="min-width: 2048px;" class="table table-striped table-bordered table-hover" id="data2">
                                <thead>
                                <tr>
                                    <th class="show_link">展示链接</th>
                                    <th>站点</th>
                                    <th>推广渠道</th>
                                    <th>代理商</th>
                                    <th>推广名称</th>
                                    <th>发送时间</th>
                                    <th>推广员</th>
                                    <th>书名</th>
                                    <th>点击数</th>
                                    <th>注册用户数</th>
                                    <th>注册成本</th>
                                    <th>付费转化率</th>
                                    <th>新增关注</th>
                                    <th>新增关注率</th>
                                    <th>新关成本</th>
                                    <th>付费用户</th>
                                    <th>首充用户数</th>
                                    <th>成本</th>
                                    <th>累计充值</th>
                                    <th>累计充值成本率</th>
                                    <th>72小时累计充值总额</th>
                                    <th>72小时累计充值回本率</th>
                                    <th>72小时充值总额</th>
                                    <th>72小时回本率</th>
                                    <th>备注</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <div id="pagination2" style="float:right;" class="m-style">

                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <!-- /.panel -->
            <div class="panel panel-default">

                <table style="width: 2048px;" class="table table-striped table-bordered table-hover" id="table_sum">
                    <caption>本月汇总</caption>
                    <thead>
                    <tr>

                        <th>注册用户数</th>
                        <th>注册成本</th>
                        <th>付费转化率</th>
                        <th>新增关注</th>
                        {{--<th>新增关注率</th>--}}
                        <th>新关成本</th>
                        <th>付费用户</th>
                        <th>首充用户数</th>
                        <th>成本</th>
                        <th>累计充值</th>
                        <th>累计充值成本率</th>
                        <th>72小时累计充值总额</th>
                        <th>72小时累计充值回本率</th>

                        <th>72小时充值总额</th>
                        <th>72小时回本率</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('jsscript')
    @parent
    <!-- Metis Menu Plugin JavaScript -->

    <!-- DataTables JavaScript -->

    <script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="/vendor/pagination/jquery.pagination.js"></script>
    <script src="/vendor/datepick/bootstrap-datepicker.min.js"></script>
    <script src="/vendor/datepick/bootstrap-datepicker.zh-CN.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/reset.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/highlight.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/common.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/pagination.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/pagination.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendor/datepick/bootstrap-datepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://apps.bdimg.com/libs/layer/2.1/skin/layer.css" rel="stylesheet">
    <script src="/js/layer.js"></script>
    <script>
        var table1=null;
        var table2=null;
        var order_column=[2,'desc'];
        var column_hash =['date','distribution_channel_id','cost','charge_amount','official_account_name',
            'company_name','total_subscribe_num','new_subscribe_num','cancel_subscribe_num',
            'cancel_subscribe_rate','promotion_point','agent_name']
        $(function () {
            $('#tab-toggle-1').tab('show');
            $('#tab-toggle-1').on('click',function (e) {
                $(this).tab('show');
            })
            $('#tab-toggle-2').on('click',function (e) {
                $(this).tab('show');
                updateTable2()
            })
            //$('#dataTables-example1').parent().css('overflow','scroll');
            //$('#dataTables-example2').parent().css('overflow','scroll');
            $.fn.datepicker.dates['cn'] = {   //切换为中文显示
                days: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
                daysShort: ["日", "一", "二", "三", "四", "五", "六", "七"],
                daysMin: ["日", "一", "二", "三", "四", "五", "六", "七"],
                months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                today: "今天",
                clear: "清除"
            };

            $('#zstart').datepicker({
                // format: 'yyyy-mm-dd',
                autoclose: true, //自动关闭
                beforeShowDay: $.noop,    //在显示日期之前调用的函数
                calendarWeeks: false,     //是否显示今年是第几周
                clearBtn: false,          //显示清除按钮
                daysOfWeekDisabled: [],   //星期几不可选
                endDate: Infinity,        //日历结束日期
                forceParse: true,         //是否强制转换不符合格式的字符串
                format: 'yyyy-mm-dd',     //日期格式
                keyboardNavigation: true, //是否显示箭头导航
                language: 'cn',           //语言
                minViewMode: 0,
                orientation: "auto",      //方向
                rtl: false,
                startDate: -Infinity,     //日历开始日期
                startView: 0,             //开始显示
                todayBtn: false,          //今天按钮
                todayHighlight: false,    //今天高亮
                weekStart: 0              //星期几是开始
            });
            $('#zend').datepicker({
                // format: 'yyyy-mm-dd',
                autoclose: true, //自动关闭
                beforeShowDay: $.noop,    //在显示日期之前调用的函数
                calendarWeeks: false,     //是否显示今年是第几周
                clearBtn: false,          //显示清除按钮
                daysOfWeekDisabled: [],   //星期几不可选
                endDate: Infinity,        //日历结束日期
                forceParse: true,         //是否强制转换不符合格式的字符串
                format: 'yyyy-mm-dd',     //日期格式
                keyboardNavigation: true, //是否显示箭头导航
                language: 'cn',           //语言
                minViewMode: 0,
                orientation: "auto",      //方向
                rtl: false,
                startDate: -Infinity,     //日历开始日期
                startView: 0,             //开始显示
                todayBtn: false,          //今天按钮
                todayHighlight: false,    //今天高亮
                weekStart: 0              //星期几是开始
            });


            updateTable();
            $('#search').on('click', function () {
                updateTable();
            })

            $('#export').on('click', function () {
                exportTable();
            })
            $.ajax({
                url: '/api/getSendOrderMonthlyData',
                type: 'GET',
                data: {
                    'promotion_type':'EXTERNAL',

                },
                //点击数	注册用户数	注册成本	付费转化率	新增关注	新增关注率
                success: function (res) {
                    if (res.data.length <= 0) {
                        res.data.list = [];
                    }
                    //if (table1 != null) table1.destroy();
                    $('#table_sum').DataTable({
                        data: res.data,
                        responsive: true,
                        paging: false,
                        searching: false,
                        ordering:false,
                        retrieve: true,
                        autoWidth:false,
                        scrollX:true,
                        columns: [

                            {data: 'register_num'},
                            {data: 'register_cost'},
                            {data: 'pay_user_transfer'},
                            {data: 'fansNum'},
                            /*{data: 'fans_transfer'},*/
                            {data: 'fansNumCost'},
                            {data: 'pay_user_num'},
                            {data: 'first_recharge_user_num'},
                            {data: 'cost'},
                            {data: 'promotionRegUserRechargeAmount'},
                            {data: 'promotionRegUserRechargeAmountCost'},
                            {data: 'promotionRegUserRechargeAmountIn72hours'},
                            {data: 'promotionRegUserRechargeAmountIn72hoursCost'},
                            {data: 'chargeAmountIn72hours'},
                            {data: 'chargeAmountIn72hoursCost'},

                        ]
                    });
                }
            })
        });

        function addRemark(send_order_id,distribution_channel_id){
            layer.prompt({
                formType: 2,
                value: '',
                title: '添加备注',
                area: ['200px', '100px'] //自定义文本域宽高
            }, function(value, index, elem){//alert(value); //得到value
                $.ajax({
                    url:'/api/updateRemark',
                    type:'POST',
                    data:{
                        'remark':value,
                        'id':send_order_id,
                        'distribution_channel_id':distribution_channel_id
                    }
                })
                layer.close(index);
            })
        }

        function exportTable() {
            var promotion_type='EXTERNAL'
            var isShortDate=1

            var distribution_channel_id=$('#distribution_channel_id').val()
            var agent_name= $('#agent_name').val()
            var promotion_channel= $('#promotion_channel').val();


            var url = '/api/exportSendOrderDaliyData?promotion_type='
                + promotion_type + "&isShortDate=" + isShortDate
                + "&distribution_channel_id=" + distribution_channel_id
                + "&promotion_channel=" + promotion_channel
                + "&agent_name=" + agent_name;
            location.href=url;
        }

        function updateTable(page = 1, action = 'update') {
            updateTable1();
            updateTable2();
        }

        function updateTable1(page = 1,action='update') {
            var index = layer.load()
            $.ajax({
                url: '/api/getSendOrderDaliyData',
                type: 'POST',
                data: {
                    'promotion_type':'EXTERNAL',
                    'isShortDate':1,
                    'page': page,
                    'distribution_channel_id': $('#distribution_channel_id').val(),
                    'agent_name': $('#agent_name').val(),
                    'promotion_channel': $('#promotion_channel').val(),
                    'orderBy':column_hash[order_column[0]],
                    'orderType':order_column[1]
                },
                success: function (res) {
                    console.log(res)
                    layer.close(index)
                    if (res.data.list.length <= 0) {
                        res.data.list = [];
                    }
                    if (table1 != null) table1.destroy();
                    table1 = $('#dataTables-example1').DataTable({
                        data: res.data.list,
                        responsive: true,
                        paging: false,
                        searching: false,
                        ordering:false,
                        retrieve: true,
                        autoWidth:false,
                        order:order_column,
                        scrollX:true,
                        columns: [
                            {data: 'promotion_url'},
                            {data: 'distribution_channel_id'},
                            {data: 'promotion_channel'},
                            {data: 'agent_name'},
                            {data: 'name'},
                            {data: 'send_time'},
                            {data: 'promoter_name'},
                            {data: 'book_name'},
                            {data: 'click_num'},

                            {data: 'register_num'},
                            {data: 'register_cost'},
                            {data: 'pay_user_transfer'},
                            {data: 'fansNum'},
                            {data: 'fans_transfer'},
                            {data: 'fansNumCost'},
                            {data: 'pay_user_num'},
                            {data: 'first_recharge_user_num'},
                            {data: 'cost'},
                            {data: 'promotionRegUserRechargeAmount'},
                            {data: 'promotionRegUserRechargeAmountCost'},
                            {data: 'promotionRegUserRechargeAmountIn72hours'},
                            {data: 'promotionRegUserRechargeAmountIn72hoursCost'},
                            {data: 'chargeAmountIn72hours'},
                            {data: 'chargeAmountIn72hoursCost'},
                            {data: 'remark'},
                            {data: 'send_order_id',render:function(data, type, row){
                                return "<button class='btn btn-info' onclick='addRemark("+data+","+row.distribution_channel_id+")'>添加备注</button>"
                            }},
                        ]
                    });

                    //第一次更新初始化分页
                    if(action=='update'){
                        $("#pagination1").pagination( {
                            pageCount: res.data.meta.last_page,
                            jump: true,
                            coping: true,
                            homePage: '首页',
                            endPage: '末页',
                            prevContent: '上页',
                            nextContent: '下页',
                            callback: function (api) {
                                var current_page = api.getCurrent()
                                updateTable1(current_page, 'page_change')
                            }
                        });
                    }

                }
            })
        }

        function updateTable2(page = 1,action='update') {
            var index = layer.load()
            $.ajax({
                url: '/api/getSendOrderDaliyData',
                type: 'POST',
                data: {
                    'promotion_type':'EXTERNAL',
                    'isShortDate':0,
                    'page': page,
                    //'pagesize':20,
                    'date': $('#zstart').val(),
                    'distribution_channel_id': $('#distribution_channel_id').val(),
                    'status': $('#status').val(),
                    'orderBy':column_hash[order_column[0]],
                    'orderType':order_column[1]
                },
                success: function (res) {
                    console.log(res)
                    layer.close(index)
                    if (res.data.list.length <= 0) {
                        res.data.list = [];
                    }
                    if (table2 != null) table2.destroy();
                    table2 = $('#data2').DataTable({
                        data: res.data.list,
                        responsive: true,
                        paging: false,
                        searching: false,
                        ordering:false,
                        retrieve: true,
                        order:order_column,
                        scrollX:true,
                        autoWidth:false,
                        columns: [
                            {data: 'promotion_url'},
                            {data: 'distribution_channel_id'},
                            {data: 'promotion_channel'},
                            {data: 'agent_name'},
                            {data: 'name'},
                            {data: 'send_time'},
                            {data: 'promoter_name'},
                            {data: 'book_name'},
                            {data: 'click_num'},

                            {data: 'register_num'},
                            {data: 'register_cost'},
                            {data: 'pay_user_transfer'},
                            {data: 'fansNum'},
                            {data: 'fans_transfer'},
                            {data: 'fansNumCost'},
                            {data: 'pay_user_num'},
                            {data: 'first_recharge_user_num'},
                            {data: 'cost'},
                            {data: 'promotionRegUserRechargeAmount'},
                            {data: 'promotionRegUserRechargeAmountCost'},
                            {data: 'promotionRegUserRechargeAmountIn72hours'},
                            {data: 'promotionRegUserRechargeAmountIn72hoursCost'},
                            {data: 'chargeAmountIn72hours'},
                            {data: 'chargeAmountIn72hoursCost'},
                            {data: 'remark'},
                            {data: 'send_order_id',render:function(data, type, row){
                                    return "<button class='btn btn-info' onclick='addRemark("+data+","+row.distribution_channel_id+")'>添加备注</button>"
                                }},
                        ]
                    });

                    //第一次更新初始化分页
                    if(action=='update'){
                        $("#pagination2").pagination( {
                            pageCount: res.data.meta.last_page,
                            jump: true,
                            coping: true,
                            homePage: '首页',
                            endPage: '末页',
                            prevContent: '上页',
                            nextContent: '下页',
                            callback: function (api) {
                                var current_page = api.getCurrent()
                                updateTable2(current_page, 'page_change')
                            }
                        });
                    }

                }
            })
        }

    </script>
@endsection

