@extends('layout.app')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">服务号日报</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    服务号日报
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:75px;">站点：</label>
                            <input class="form-control" id="distribution_channel_id" name="distribution_channel_id"/>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:160px;">推广渠道：</label>
                            <input class="form-control" id="promotion_point" name="promotion_point"/>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:120px;">代理商：</label>
                            <input class="form-control" id="agent_name" name="agent_name"/>
                        </div>

                        <button id="search" class="btn btn-primary">搜索</button>
                        <button id="export" class="btn btn-info">导出</button>
                    </div>
                    <div style="margin-top: 15px;" class="row">
                        <div class="col-xs-5 col-sm-5 col-md-5"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:170px;">开始时间：</label><input style="background: #fff;"
                                                                                             id="zstart"
                                                                                             class="form-control"
                                                                                             name="zstart" readonly/>
                        </div>
                        <div class="col-xs-5 col-sm-5 col-md-5"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:170px;">结束时间：</label><input style="background: #fff;"   id="zend"   class="form-control" name="zend" readonly/>&nbsp;
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="well">
                        <p>昨日汇总：</p>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3"
                                 style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                                <span>成本：</span>
                                <p id="cost_sum"></p>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3"
                                 style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                                {{--<label style="line-height:20px;width:160px;">充值：</label>--}}
                                <span>充值：</span>
                                <p id="charge_amount_sum"></p>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3"
                                 style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                                <span>累计关注：</span>
                                <p id="total_subscribe_num_sum"></p>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3"
                                 style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                                <span>新增关注：</span>
                                <p id="new_subscribe_num_sum"></p>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3"
                                 style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                                <span>取消关注：</span>
                                <p id="cancel_subscribe_num_sum"></p>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3"
                                 style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                                <span>取关率：</span>
                                <p id="cancel_subscribe_rate_sum"></p>
                            </div>

                        </div>
                    </div>

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>日期</th>
                            <th>站点</th>
                            {{--<th>头像</th>--}}
                            <th>成本</th>
                            <th>充值</th>
                            <th>服务号名称</th>
                            <th>主体</th>
                            <th>累计关注</th>
                            <th>新增关注</th>
                            <th>取消关注</th>
                            <th>取关率</th>
                            <th>推广渠道</th>
                            <th>代理商</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <div id="pagination" style="float:right;" class="m-style">

                    </div>
                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('jsscript')
    @parent
    <!-- Metis Menu Plugin JavaScript -->

    <!-- DataTables JavaScript -->

    <script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="/vendor/pagination/jquery.pagination.js"></script>
    <script src="/vendor/datepick/bootstrap-datepicker.min.js"></script>
    <script src="/vendor/datepick/bootstrap-datepicker.zh-CN.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/reset.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/highlight.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/common.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/pagination.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/pagination.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendor/datepick/bootstrap-datepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://apps.bdimg.com/libs/layer/2.1/skin/layer.css" rel="stylesheet">
    <script src="/js/layer.js"></script>
    <script>
        var table = null;
        var order_column = [0, 'desc'];
        var column_hash = ['date', 'distribution_channel_id', 'cost', 'charge_amount', 'official_account_name',
            'company_name', 'total_subscribe_num', 'new_subscribe_num', 'cancel_subscribe_num',
            'cancel_subscribe_rate', 'promotion_point', 'agent_name']
        $(function () {

            $.fn.datepicker.dates['cn'] = {   //切换为中文显示
                days: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
                daysShort: ["日", "一", "二", "三", "四", "五", "六", "七"],
                daysMin: ["日", "一", "二", "三", "四", "五", "六", "七"],
                months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                today: "今天",
                clear: "清除"
            };

            $('#zstart').datepicker({
                // format: 'yyyy-mm-dd',
                autoclose: true, //自动关闭
                beforeShowDay: $.noop,    //在显示日期之前调用的函数
                calendarWeeks: false,     //是否显示今年是第几周
                clearBtn: false,          //显示清除按钮
                daysOfWeekDisabled: [],   //星期几不可选
                endDate: Infinity,        //日历结束日期
                forceParse: true,         //是否强制转换不符合格式的字符串
                format: 'yyyy-mm-dd',     //日期格式
                keyboardNavigation: true, //是否显示箭头导航
                language: 'cn',           //语言
                minViewMode: 0,
                orientation: "auto",      //方向
                rtl: false,
                startDate: -Infinity,     //日历开始日期
                startView: 0,             //开始显示
                todayBtn: false,          //今天按钮
                todayHighlight: false,    //今天高亮
                weekStart: 0              //星期几是开始
            });
            $('#zend').datepicker({
                // format: 'yyyy-mm-dd',
                autoclose: true, //自动关闭
                beforeShowDay: $.noop,    //在显示日期之前调用的函数
                calendarWeeks: false,     //是否显示今年是第几周
                clearBtn: false,          //显示清除按钮
                daysOfWeekDisabled: [],   //星期几不可选
                endDate: Infinity,        //日历结束日期
                forceParse: true,         //是否强制转换不符合格式的字符串
                format: 'yyyy-mm-dd',     //日期格式
                keyboardNavigation: true, //是否显示箭头导航
                language: 'cn',           //语言
                minViewMode: 0,
                orientation: "auto",      //方向
                rtl: false,
                startDate: -Infinity,     //日历开始日期
                startView: 0,             //开始显示
                todayBtn: false,          //今天按钮
                todayHighlight: false,    //今天高亮
                weekStart: 0              //星期几是开始
            });

            updateTable();
            $('#search').on('click', function () {
                updateTable();
            })

            $('#export').on('click', function () {
                exportTable();
            })
        });

        function exportTable() {
            var end_date = $('#zend').val();
            var start_date = $('#zstart').val();
            var agent_name = $('#agent_name').val();
            var promotion_point = $('#promotion_point').val();
            var distribution_channel_id = $('#distribution_channel_id').val();

            var url = '/api/exportOfficialAccountDaliyData?start_date='
                + start_date + "&end_date=" + end_date
                + "&promotion_point=" + promotion_point
                + "&distribution_channel_id=" + distribution_channel_id
                + "&agent_name=" + agent_name;
            location.href = url
        }

        function updateTable(page = 1, action = 'update') {
            var index = layer.load()
            $.ajax({
                url: '/api/getOfficialAccountDaliyData',
                type: 'POST',
                data: {
                    'page': page,
                    //'pagesize':20,
                    'start_date': $('#zstart').val(),
                    'end_date': $('#zend').val(),
                    'promotion_point': $('#promotion_point').val(),
                    'distribution_channel_id': $('#distribution_channel_id').val(),
                    'agent_name': $('#agent_name').val(),
                    'orderBy': column_hash[order_column[0]],
                    'orderType': order_column[1]
                },
                success: function (res) {
                    console.log(res)
                    layer.close(index)
                    if (res.data.list.length <= 0) {
                        res.data.list = [];
                    }

                    if (table != null) table.destroy();
                    table = $('#dataTables-example').DataTable({
                        data: res.data.list,
                        responsive: true,
                        paging: false,
                        searching: false,
                        retrieve: true,
                        order: order_column,
                        columns: [
                            {data: 'date'},
                            {data: 'distribution_channel_id'},
                            {data: 'cost'},
                            {data: 'charge_amount'},
                            {data: 'official_account_name'},
                            {data: 'company_name'},
                            {data: 'total_subscribe_num'},
                            {data: 'new_subscribe_num'},
                            {data: 'cancel_subscribe_num'},
                            {data: 'cancel_subscribe_rate'},
                            {data: 'promotion_point'},
                            {data: 'agent_name'},

                        ]
                    });

                    //本页汇总
                    var length = res.data.list.length
                    var sumarize = res.data.list
                    var cost = 0;
                    var charge_amount = 0;
                    var total_subscribe_num = 0;
                    var new_subscribe_num = 0;
                    var cancel_subscribe_num = 0;
                    var cancel_subscribe_rate = 0;
                    console.log(sumarize)
                    /*for (var i = 0; i < length; i++) {
                        console.log(i)
                        console.log(sumarize[i])
                        cost += parseFloat(sumarize[i].cost);
                        charge_amount += parseFloat(sumarize[i].charge_amount)
                        total_subscribe_num += parseFloat(sumarize[i].total_subscribe_num)
                        new_subscribe_num += parseFloat(sumarize[i].new_subscribe_num)
                        cancel_subscribe_num += parseFloat(sumarize[i].cancel_subscribe_num)
                        //cancel_subscribe_rate += sumarize[i].cancel_subscribe_rate
                    }*/
                    //cancel_subscribe_rate = (total_subscribe_num > 0) ? (((cancel_subscribe_num / total_subscribe_num) * 100).toFixed(2)) + '%' : '0%';

                    //排序
                    $('#dataTables-example').on('order.dt', function () {
                        // This will show: "Ordering on column 1 (asc)", for example
                        var order = table.order();
                        //
                        //console.log(order)
                        /*$.each(order,function (key,value) {

                        })*/
                        if (order.length > 0) {
                            order_column = order[0]
                            updateTable()
                        }
                        /*setTimeout(function () {
                            //updateTable()
                        },1000)*/

                        //$('#orderInfo').html( 'Ordering on column '+order[0][0]+' ('+order[0][1]+')' );
                    });
                    //第一次更新初始化分页
                    if (action == 'update') {
                        $("#pagination").pagination({
                            pageCount: res.data.meta.last_page,
                            jump: true,
                            coping: true,
                            homePage: '首页',
                            endPage: '末页',
                            prevContent: '上页',
                            nextContent: '下页',
                            callback: function (api) {
                                var current_page = api.getCurrent()
                                updateTable(current_page, 'page_change')
                            }
                        });
                    }
                }
            })
            $.ajax({
                url:'/api/getOfficialAccountDaliyDataSum',
                type: 'POST',
                data: {
                    'page': page,
                    //'pagesize':20,
                    'start_date': $('#zstart').val(),
                    'end_date': $('#zend').val(),
                    'promotion_point': $('#promotion_point').val(),
                    'distribution_channel_id': $('#distribution_channel_id').val(),
                    'agent_name': $('#agent_name').val(),
                    'orderBy': column_hash[order_column[0]],
                    'orderType': order_column[1]
                },
                success: function (res) {
                    $('#cost_sum').html(res.data[0].cost)
                    $('#charge_amount_sum').html(res.data[0].charge_amount)
                    $('#total_subscribe_num_sum').html(res.data[0].total_subscribe_num)
                    $('#new_subscribe_num_sum').html(res.data[0].new_subscribe_num)
                    $('#cancel_subscribe_num_sum').html(res.data[0].cancel_subscribe_num)
                    $('#cancel_subscribe_rate_sum').html(res.data[0].cancel_subscribe_rate)
                }
            })

        }
    </script>
@endsection

