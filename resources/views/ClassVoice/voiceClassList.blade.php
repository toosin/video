<html style="font-size: 50px;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="./static/reCss/base.css">
    <link rel="stylesheet" type="text/css" href="//at.alicdn.com/t/font_1167566_ga5qydk3qgs.css">
    <title>语音课</title>
    <style type="text/css">body {
            font-family: 'PingFang SC', 'Microsoft YaHei', sans-serif, 'SF UI Text', 'Arial';
        }</style>
    <link href="/index/static/css/app.1c5c64e39637a4757770aab3b6ba3a95.css" rel="stylesheet">
    {{--<script type="text/javascript" charset="utf-8" async="" src="/index/static/js/0.dacb8dd7b8cff85fc0da.js"></script>
    <script type="text/javascript" charset="utf-8" async="" src="/index/static/js/2.e118a1ad6e628a5149e6.js"></script>
    <script type="text/javascript" charset="utf-8" async="" src="/index/static/js/3.acaf62bfc7eade21d6c5.js"></script>--}}
</head>
<body>
<div id="app">
    <div data-v-bac3df10="">
        <div data-v-bac3df10="" class="wrap">
            <div data-v-bac3df10="" class="title"><span data-v-bac3df10="" class="titleText">VIP共读 第1期</span>
                <div data-v-bac3df10="" class="float_R proSign"><p data-v-bac3df10="">学习进度: <span data-v-bac3df10=""
                                                                                                  class="proNum">4%</span>
                    </p>
                    <p data-v-bac3df10="" class="proWrap"></p>
                    <p data-v-bac3df10="" class="pro" style="width: 4%;"></p></div>
            </div>
            <div data-v-bac3df10=""
                 style="background-image: url(&quot;/index/static/img/bzbgc.553a703.png&quot;); background-repeat: no-repeat; background-size: 100% 100%;">
                <div data-v-bac3df10="" class="mainCont"><p data-v-bac3df10="" class="mainCont-title">今日学习</p>
                    <div data-v-bac3df10="" class="flex"><img data-v-bac3df10="" alt="" class="bookImage"
                                                              src="http://apibook.wallstreets.cn/admin/upload/20190420/c45990beebf910c9fcfb12d1e955961d.jpg">
                        <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《红色资本》</p>
                            <p data-v-bac3df10="" class="artWriter"> 卡尔•沃尔特；弗雷泽•豪伊</p>
                            <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                这是一部由美国学者撰写的，关于中国金融体制变革、发展及其现状的学术著作。作者首先肯定，中国改革开放以来取得了举世瞩目的成就，经济上实现了“非凡的崛起”，同时也认为，在中国经济发展的过程中，金融发挥的作用还相当有限。中国的金融体制相对于过去的经济突飞猛进和未来的可持续发展，还具有“脆弱性”和不相适应的地方，还需要进一步改革完善。</p>
                            <div data-v-bac3df10="" class="playWrap clearFloat"><span data-v-bac3df10="">29分39秒</span>
                                <span data-v-bac3df10="">已听100%</span>
                                <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                           src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                           alt=""> <span data-v-bac3df10="">播放</span>
                                </p></div>
                        </div>
                    </div>
                    <div data-v-bac3df10="" class="bookList">
                        <div data-v-bac3df10="" class="listTitle flex"><img data-v-bac3df10=""
                                                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAARCAMAAAAxIdauAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUI0NDFGOTkxRjhFMTFFOUJFMDhDMThERjQwQjM3NjIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUI0NDFGOUExRjhFMTFFOUJFMDhDMThERjQwQjM3NjIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBQjQ0MUY5NzFGOEUxMUU5QkUwOEMxOERGNDBCMzc2MiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBQjQ0MUY5ODFGOEUxMUU5QkUwOEMxOERGNDBCMzc2MiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoPRKcoAAAMAUExURVI6InplUFU7KO/q6E82HlE4H1E4ITohCUowGFI4H2xWPkItF1I6IEsyGFI5IkMqDkUrDFA1HVI4IUErC0UqDVA5IVE4IEMsGFM5Im1WPkw2HjoiBntmUU06IU86HkQtDoBxYYBwYE02HVA7IGxVPEwyGUQuEVE6IuTg2uTg2+Pg2UUrD0sxGUsxGFE5IFM5IU86ITUZAPXz8lI4IEYrEEIqDVM6In5tV0IsDVI5IWtWQUMpC+Tf2U01HDoiCE41HUQvDVU7KU42HVM6IUcvFkctFEEtF1I3IlA6IUswGjshCHplT1A4IHhqV31sW39xYEwwGe/r5+Xg2+Th2mVTNzIZAzIXAODb1fTw7DwmDlU6J005IbWpmW5WPlQ5IO3o41A5IKaaj0crD+7q5ubg3ern4+/q50wvF0wyHINyZX9wYE47IlE7IUguEMG1qmtVPUs1HTogCVU6KHJcREMpEPj180ErDjchAXFeSkoyGlI5IPDr6f/+/kMpDlM5IHlmTks4Hkg0I+bi3ndiTV5KNi0TAEMuFlM6JkIrC1tFLU82H042HjcdBI1+a9fTz0kyGEUvCzccATccA042G085IFI7I1A7I0AuFU80Gv79/EUoClQ/LFA4HlA4IlY+Kc3FuVI8IJSDbz8oCXllUPTy8UctDtzY1WpTO3xtW0owGXtlUPDt6k00G21XPzsgCUIsFW1XOoFxYEctEO7s6zgeBlA4IU45G0ErDG1XPVpDLGhROn9vYKKTiUMpDOvn40swGHRkUE85If///1E5If///8XFxcbGxsfHx8jIyMnJycrKysvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tPT09TU1NXV1dbW1tfX19jY2NnZ2dra2tvb29zc3N3d3d7e3t/f3+Dg4OHh4eLi4uPj4+Tk5OXl5ebm5ufn5+jo6Onp6erq6uvr6+zs7O3t7e7u7u/v7/Dw8PHx8fLy8vPz8/T09PX19fb29vf39/j4+Pn5+fr6+vv7+/z8/P39/f7+/v///x2XY2EAAADFdFJOU/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8ANFbHkQAAAKRJREFUeNpimF1pa6vULSVlP+nIkSMMPfpsbIcP66lvTTIC8grZhA4fPjxXTsgz3uo/gzQfkHPYUlQsgFkSzAOqPHyQzTNwx38G9sNgcJCTg5mLEp6YqB6Qx8rBnP+fwYtH4jADEPCkM68F8tZzu4lzc08Xj1yt8p/B5QgcAN2y80i5gIiFtrbArCPF/xkMEXKG/xmO5PILd9Sabjflt15yBCDAAOqyZW/D8D5sAAAAAElFTkSuQmCC"
                                                                            alt="" class="bookTitleImg"> <span
                                    data-v-bac3df10="">共读书单</span></div>
                        <div data-v-bac3df10="" class="month">
                            <div data-v-bac3df10="" id="month-swiper"
                                 class="swiper-container swiper-container-horizontal">
                                <div data-v-bac3df10="" class="swiper-wrapper"><p data-v-bac3df10=""
                                                                                  class="swiper-slide"><span
                                                data-v-bac3df10="" class="monthCheck">4月共读</span><br data-v-bac3df10="">
                                        <span data-v-bac3df10="" class="line"></span></p>
                                    <p data-v-bac3df10="" class="swiper-slide"><span data-v-bac3df10=""
                                                                                     class="">5月共读</span><br
                                                data-v-bac3df10=""> <!----></p>
                                    <p data-v-bac3df10="" class="swiper-slide"><span data-v-bac3df10=""
                                                                                     class="">6月共读</span><br
                                                data-v-bac3df10=""> <!----></p></div>
                                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/uploads/20190426/21cbd15dc3b80c43370f3b3529b4f3b1.jpeg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《巴菲特致股东的信》</p>
                                <p data-v-bac3df10="" class="artWriter"> 沃伦·巴菲特</p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    作为投资界圣经，这本书是巴菲特一生投资经验的总结，无论哪种层次的投资人都可以在里面找到收获。巴菲特说，“这本书比任何一本关于我的传记都要好，如果让我选一本书去读，那必定是这一本。”</p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">18分12秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190420/a28fb9ffc409734957a9f601a3bfb456.jpg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《从0到1》</p>
                                <p data-v-bac3df10="" class="artWriter"> 彼得·蒂尔</p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    如何建立自己的垄断型竞争力
                                    创新不是从1到N，而是从0到1
                                    失败者才去竞争，创业者应当选择垄断
                                    没有科技公司可以仅靠品牌吃饭
                                </p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">18分12秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190420/90bab728dccd6c631568c44b590354d7.jpg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《垃圾债券之王》</p>
                                <p data-v-bac3df10="" class="artWriter"> 康妮•布鲁克</p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    迈克尔·米尔肯是创造了金融先天地的“垃圾债券之王”
                                    《华尔街时报》认定他为“最伟大的金融思想家”
                                    号称自J·P·摩根一来美国金融界最有影响力的风云人物
                                    曾经呼风唤雨的投资王者，为何却被美国证券市场永远除名？
                                    《垃圾债券之王》为您全景揭秘他的传奇投资生涯</p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">26分22秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190420/bdfebfa5cc737c6ce819db0e63fc86e8.jpg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《管理美元》</p>
                                <p data-v-bac3df10="" class="artWriter"> 【日】船桥洋一</p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    《管理美元》是对广场协议的起因、谈判、实施和早期结果的权威分析。英文原版图书出版于广场协议签署三年后，《朝日新闻》前总编船桥洋一先生，基于对112位广场会议参与者的直接采访，分析展示了参与广场会议各方的实力、决策者间的意见分歧以及促进协作式的经济一体化的实践，对各个方面给出了详细且权威的记录。
                                    这本书曾获得1988年吉野作造奖，对于中国应对当下复杂的国际经济形势有重要的现实和参考意义。</p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">19分29秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190420/1896970fde3a99b8c9db3ada73c98fc7.png"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《1929年大崩盘》</p>
                                <p data-v-bac3df10="" class="artWriter"> 约翰·肯尼斯·加尔布雷思</p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    是学术界和证券投资界经常大量引用的典范性著作。《大西洋月刊评论》认为：“经济文学很少以其娱乐价值而著称，但这本书却做到了。加尔布雷思的文章雍容而机智，并且幽默地鞭挞了这个国家某些金融领域的智者和政策所犯的错误。” </p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">36分8秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190420/576c2c491b7f470084144ccd0a2147dc.jpeg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《彼得林奇的成功投资》</p>
                                <p data-v-bac3df10="" class="artWriter"> 彼得·林奇 / 约翰·罗瑟查尔德 </p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    作者用浅显生动的语言娓娓道出了股票投资的诸多技巧，向广大的中小投资者提供了简单易学的投资分析方法，这些方法是作者多年的经验总结，具有很强的实践性，对于业余投资者来说尤为有益。作者首先告诉投资者的就是这样一条投资规则：不要相信投资专家的建议！作者认为投资者只要对股票认真做一些研究，其成绩不会比专家差。作者认为业余投资者有很多先天的优势，如果他们把这些优势发挥出来，在投资时就会比专家做得还出色。</p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">16分41秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190420/ebe2ddc0efe593f955b68785214e42ac.jpg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《钓愚》</p>
                                <p data-v-bac3df10="" class="artWriter"> 美]乔治·阿克洛夫 / 罗伯特·席勒 </p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    自亚当·斯密以来，经济学的核心信条就是：自由市场制度就像一只“看不见的手”，但现在这只“看不见的手”已经变成了随时准备绊倒消费者的“看不见的脚”。
                                    两位诺奖得主乔治·阿克洛夫、罗伯特·席勒认为，市场在为我们带来福利的同时，也带来了灾难。普遍存在的人性弱点、信息不对称等让我们成为“钓愚”中的受骗者。</p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">16分30秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190420/83b561302403d6b6e5a6b9714249abf7.jpg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《黑天鹅》</p>
                                <p data-v-bac3df10="" class="artWriter"> 纳西姆·尼古拉斯·塔勒布 </p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    在发现澳大利亚的黑天鹅之前，欧洲人认为天鹅都是白色的，“黑天鹅”曾经是欧洲人言谈与写作中的惯用语，用来指不可能存在的事物，但这个不可动摇的信念随着第一只黑天鹅的出现而崩溃。

                                    黑天鹅的存在寓意着不可预测的重大稀有事件，它在意料之外，却又改变一切。人类总是过度相信经验，而不知道一只黑天鹅的出现就足以颠覆一切。然而，无论是在对股市的预期，还是政府的决策中，黑天鹅都是无法预测的。“9•11”事件、美国的次级贷危机、中国的雪灾，都是如此。

                                    生活中，随机性随处可见，在资本市场也是一样。人们总是以自己有限的生活经验和不堪一击的信念来解释不可预测的事件；即便是精于算计的专业人士，也难保不被随机性愚弄，其实我们应该做的是顺应这种不可知的未来。这本书会教你改变自己的思维方式，把握黑天鹅带来的机会，采取应对策略，从中受益。</p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">14分7秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190420/1322f6ee3d6f646a33c206372119191f.jpg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《中国是部金融史》</p>
                                <p data-v-bac3df10="" class="artWriter"> 陈雨露 / 杨忠恕 </p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    中国人民大学校长陈雨露教授全新力作，《世界是部金融史》姊妹篇，透过金融读懂中国三千年把金融那些事儿一次说清楚，三千年来谁著史——文明盛衰的密码是什么？人间沧桑的正道为何物？金权有着怎样的规则？贫富有着怎样的逻辑？</p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">26分12秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                        <div data-v-bac3df10="" class="flex bookContList">
                            <div data-v-bac3df10="" class="bookImgWrap"><img data-v-bac3df10=""
                                                                             src="http://apibook.wallstreets.cn/admin/upload/20190422/96ef0cb5d71ab793077657028b4eca16.jpg"
                                                                             alt="" class="bookImage"> <!----></div>
                            <div data-v-bac3df10=""><p data-v-bac3df10="" class="artTitle">《沃尔特.瑞斯顿与花旗银行》</p>
                                <p data-v-bac3df10="" class="artWriter"> 利普.L.茨威格 </p>
                                <p data-v-bac3df10="" class="artCont" style="-webkit-box-orient: vertical;">
                                    本书描绘了一个目光远大的人如何爬上庞大却又缺乏生气的第一国民城市银行（后易名为花旗银行）的头把交椅，如何着手重建自己的机构乃至美国和全球的银行金融业的经过情形。在本书的著述过程中，作者菲利普・L・茨威格走访当时的许多风云人物。包括前总统杰拉尔德・福特、前国务卿乔治・舒尔茨和亨利・基辛格、前财政部长康纳利、西蒙和里根、前联邦储备委员会主席威廉・米勒和保罗・沃尔克以及瑞斯顿本人。具有绝对的可读性。

                                </p>
                                <div data-v-bac3df10="" class="playWrap clearFloat"><span
                                            data-v-bac3df10="">22分1秒</span> <span data-v-bac3df10="">已听0%</span>
                                    <p data-v-bac3df10="" class="playBtn"><img data-v-bac3df10=""
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAWCAYAAAAinad/AAABd0lEQVQ4jaXUP0hVYRzG8U+mmCCCS0O4SRaEOBUNDtEQCDrYpsSpoS40nHAMOUTRzSJc9IzR4NVWg3SoljtEROgg0ZD9maw1ArN/ODS89+gh0O499zu9vLx8eX7PD94Dc7O3lzAs8A0RltRJFCc755aaaBJ9+IoneIaeeoV5WcYH9KKEc9jArSKy9tzdA3RiDjewabeGupPl2cIlnMQXocMqjhWRZaziOC7jDN7hTlFZxkN0YEZY1k+MFpXBL0xgAO+xiBeVtNxbRJbxpiaMMIiPlbR8t6gsYx5tSHG9kpZfNyOD7ShOrgk9nmptUnawkpanhS5Xmkl2Ads10b0oTgolGxD66sdLXIzi5BONLaADs1jDUZzHYCZqRFbCD8SYqokf//vof2OexoLwm1RxFet7Pd4rWTce4RUOYwRn9xPlZb9zd7HwSY7jJrqwvP8AgfyYJ/AcR/AUV/C5Hkk+2bJQ6lscEkYaalSUJRvDffxBgu+NSjL+AqqMUldBv1FsAAAAAElFTkSuQmCC"
                                                                               alt=""> <span
                                                data-v-bac3df10="">播放</span></p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-v-bac3df10="" class="pendant clearFloat">
                <div data-v-bac3df10="" class="cirWrap"
                     style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABBCAMAAABW61JJAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6M0QzRjQ4RUYxRTc0MTFFOUI5MjZBMTBGQkE3MkUxNTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6M0QzRjQ4RjAxRTc0MTFFOUI5MjZBMTBGQkE3MkUxNTAiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozRDNGNDhFRDFFNzQxMUU5QjkyNkExMEZCQTcyRTE1MCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozRDNGNDhFRTFFNzQxMUU5QjkyNkExMEZCQTcyRTE1MCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PiwHUkcAAAFiUExURfllV/r4+vfy9vj0+P39/fn2+f7+/v////z7/Pj1+Pr3+vfx9ff09+mDef79/umAdvz6/OyYkeqKgfC4tf/+//Te4Oh1aed0aPXi5PTY2eVgUuZnWuyXkPPQ0OZsYPPS0vPPzu+0sPbt8PPV1fHCwPfx9O+yru2loPbt8eh6b/bn6fbo6+h8cfbs7/PPz+ZqXvC7uO+zr++1sfn1+f38/e+xrfbu8edzZ+qJgOdwY+dtYO+wq/HBv+h8cuyblPC2svXg4ud0aeh5bvLMzOqFe/fw9O+wrPPT0+dyZvbp7PTe3/n3+eh2a+yak+dyZ/PU1O+yr/LMy/fv8udwZOuTjPPW1vG+u+yZkuybledvY+uUjfXf4OZsX/TZ2vfw8/C2s+mCeO2jnfPR0eh7cOuUjPHAvudxZfXh4+dtYe2inO2hm+uVjuZqXeyWj+VjVfG/vOqIf/Xh4uh1auyWjvfz9////7iL01AAAAB2dFJOU////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wABYqlHAAADQUlEQVR42rxXZXfiQBSdhAjBAxQoUqh7qfvW3b1bl3W3l/+/mZkEQiGQyZ6z9wMJcObOs3lzH9L+Ecjmd1mIcKLiAQCPInIRQWYiiHt9eKkVHp837pRA4KA2OMEJgSCCPUShEYFk7u4/b70MbmXS6cxW8HLk3F+yQqpL4KWu80NqMbG/eLw0CDC4dLy4nyiqQ7wRDK89gUy3v48le+c+VNqemutdjt0bRsg2BBLxnh8+jeZq+Z+Lng5TK8akmgQhBf/ZFBzI24Uw/yrYRF6UUA0Cibjf0dLVaZ+Ezq6WDhoIqYpAwvvzT1PtUBftU0/EDUV6QSBj//3X4X5ogP7wW5JSUa4kwPHnv023QUO0/fxDbOAqCLz4p+5wDzhAT7ibPL0WAhLA+UQfOEJfYt4SSELgw/mbbQaHaJ4l2fSVCAQcgLUJcIyjNRIGwSTAGfgx4HdO4B+4IJkwCLABkzN5YED+atI0AdEUjkaBCdFRM5VIi+MILOfYCHJJEoU4JsA1UHgDjFgpGLWASA7VOVaCDdXIJJIDugctKVaC1C32ISBrCOfgbgWY8ZvU3biGIvqj9TU7wUIr/oxoCCcxG2MnWMzSRCJchsEhdoLVIC1GhDvRwRK4hKIhfJIzg24JPBrCj7Tb9RBwS4DMF0rA5gJCZYIAjcHBL9b9EQaNgeImjcj8UGgdsBaSvjmiJCKtRMZSRuUocvQsNK+wEhgG4LMwjhvSbYrZAEowTvsBqBusBtBs4n5AOhJjS0Pmw2f2ROamaoD0ROIDa1s36lAu3QuTV3kXBFz5ZoILlqvNhGC5G/m1I+b1ovV2hh3n1/vdxx2ouJ1JJlkExjuo1AeGxGOUOFaFQjUS/8WRyJq+5qs0Ek0l+M+cyLwzP1SrNKoTgf/UUGgmP9P9X+pEqlQdS91qpaprbToqNAVfbdqt3zTFNnhCtmpdl/sz0eday5+jM4bcr63WdS/G6N8n+sAx8aLDpCZ6k7ET44vNvFCaWHQrCtni3sLD6s32+vr2zerDwl4xW+BLR0huODPhlB6OqI/vM+nvX5OP6shh+aTVm5msU5vtCZYazo27dZbvCk4m15Dt5Br6T7OzdXoPuJveWfBXgAEALTOgiIATTCsAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-size: 100% 100%;">
                    <p data-v-bac3df10="">0</p></div>
                <div data-v-bac3df10="" class="pendantText">
                    守约打卡
                </div>
            </div> <!----> <!----></div>
    </div>
</div>
<script type="text/javascript" src="/index/static/js/manifest.69cb9bdfb0a99a1058a6.js"></script>
<script type="text/javascript" src="/index/static/js/vendor.eab77ae3b0567b1a38ca.js"></script>
<script type="text/javascript" src="/index/static/js/app.2215834cfcd5ecc32337.js"></script>
<script>(function (doc, win) {
        var docEl = doc.documentElement,
            resizeEvt = 'onorientationchange' in window ? 'onorientationchange' : 'resize',
            recalc = function () {
                var clientWidth = docEl.clientWidth;
                if (!clientWidth) return;
                if (clientWidth >= 750) {
                    docEl.style.fontSize = '100px';
                } else {
                    docEl.style.fontSize = 100 * (clientWidth / 750) + 'px';
                }
            };

        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false);
    })(document, window);</script>
</body>
</html>