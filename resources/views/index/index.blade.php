@extends('layout.app')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">欢迎</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <p>welcome</p>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('jsscript')
    @parent
    <!-- Metis Menu Plugin JavaScript -->

    <!-- DataTables JavaScript -->

    <script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="/vendor/pagination/jquery.pagination.js"></script>
    <script src="/vendor/datepick/bootstrap-datepicker.min.js"></script>
    <script src="/vendor/datepick/bootstrap-datepicker.zh-CN.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/reset.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/highlight.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/common/common.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/pagination.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/vendor/pagination/pagination.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendor/datepick/bootstrap-datepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://apps.bdimg.com/libs/layer/2.1/skin/layer.css" rel="stylesheet">
    <script src="/js/layer.js"></script>
    <script>
        var table=null;
        $(function() {

            $.fn.datepicker.dates['cn'] = {   //切换为中文显示
                days: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
                daysShort: ["日", "一", "二", "三", "四", "五", "六", "七"],
                daysMin: ["日", "一", "二", "三", "四", "五", "六", "七"],
                months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                today: "今天",
                clear: "清除"
            };

            $('#zstart').datepicker({
                // format: 'yyyy-mm-dd',
                autoclose: true, //自动关闭
                beforeShowDay: $.noop,    //在显示日期之前调用的函数
                calendarWeeks: false,     //是否显示今年是第几周
                clearBtn: false,          //显示清除按钮
                daysOfWeekDisabled: [],   //星期几不可选
                endDate: Infinity,        //日历结束日期
                forceParse: true,         //是否强制转换不符合格式的字符串
                format: 'yyyy-mm-dd',     //日期格式
                keyboardNavigation: true, //是否显示箭头导航
                language: 'cn',           //语言
                minViewMode: 0,
                orientation: "auto",      //方向
                rtl: false,
                startDate: -Infinity,     //日历开始日期
                startView: 0,             //开始显示
                todayBtn: false,          //今天按钮
                todayHighlight: false,    //今天高亮
                weekStart: 0              //星期几是开始
            });
            $('#zend').datepicker({
                // format: 'yyyy-mm-dd',
                autoclose: true, //自动关闭
                beforeShowDay: $.noop,    //在显示日期之前调用的函数
                calendarWeeks: false,     //是否显示今年是第几周
                clearBtn: false,          //显示清除按钮
                daysOfWeekDisabled: [],   //星期几不可选
                endDate: Infinity,        //日历结束日期
                forceParse: true,         //是否强制转换不符合格式的字符串
                format: 'yyyy-mm-dd',     //日期格式
                keyboardNavigation: true, //是否显示箭头导航
                language: 'cn',           //语言
                minViewMode: 0,
                orientation: "auto",      //方向
                rtl: false,
                startDate: -Infinity,     //日历开始日期
                startView: 0,             //开始显示
                todayBtn: false,          //今天按钮
                todayHighlight: false,    //今天高亮
                weekStart: 0              //星期几是开始
            });



            updateTable();
            $('#search').on('click',function(){
                updateTable();
            })
        });

        function updateTable(page=1){
            //var index = layer.load()
            $.ajax({
                url:'/admin/home/statistical_response',
                type:'POST',
                data:{
                    'page':page,
                    'pagesize':20,
                    'zstart':$('#zstart').val(),
                    'zend':$('#zend').val()
                },
                success:function(res){
                    console.log(res)
                    //layer.close(index)
                    if(res.data.length<=0){
                        res.data = [];
                    }
                    if(table != null) table.destroy();
                    table = $('#dataTables-example').DataTable({
                        data:res.data,
                        responsive: true,
                        paging:false,
                        searching:false,
                        retrieve: true,
                        columns: [
                            { data: 'id' },
                            { data: 'date'},

                            { data: 'register_num' },
                            { data: 'task_num' },
                            { data: 'share_num' },
                            { data: 'share_user_num' },
                            { data: 'share_rate' },
                            { data: 'processing_task' },
                            { data: 'recharge_sum' },
                            { data: 'recharge_num' },
                            { data: 'recharge_average' },
                            { data: 'locked_reward' },

                        ]
                    });

                    $("#pagination").pagination( {
                        pageCount: res.pagecount,
                        jump: true,
                        coping: true,
                        homePage: '首页',
                        endPage: '末页',
                        prevContent: '上页',
                        nextContent: '下页',
                        callback: function (api) {
                            var current_page = api.getCurrent()
                            updateTable(current_page)
                        }
                    });
                }
            })
        }
    </script>
@endsection

