@extends('layout.app')
@section('content')
    <style>
        .searchable-select-dropdown{
            z-index: 9999;
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <h5 class="page-header">服务号列表</h5>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table id="dg"></table>

                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>


@endsection
@section('jsscript')
    @parent

    <link rel="stylesheet" type="text/css" href="/jquery-easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="/jquery-easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery.searchableSelect.css">
    <script type="text/javascript" src="/jquery-easyui/jquery.min.js"></script>
    <script type="text/javascript" src="/jquery-easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/js/jquery.searchableSelect.js"></script>
    <script>
        $('#dg').datagrid({
            url:'/api/hbstats/getMonthStats?appid={{$appid}}',
            title:'服务号',
            iconCls:'icon-search',
            method:'get',
            pagination:true,
            pageSize:20,
            fitColumns:true,
            //toolbar:'#tb',
            columns:[[
                {field:'month',title:'月份',width:200},
                {field:'gzh_name',title:'服务号',width:200},
                {field:'cost',title:'成本',width:200},
                {field:'sub_user_num',title:'新关',width:200},
                {field:'actual_sub_num',title:'净关',width:200},
                {field:'accum_actual_sub_num',title:'累计净关',width:200},
                {field:'cancel_sub_num',title:'取关',width:200},
                {field:'total_income',title:'充值',width:200},
                {field:'advertise_income',title:'广告收入',width:200,editor:{
                    type:'text'
                    }},
                {field:'settle_recharge',title:'服务号充值',width:200},
                {field:'outside_recharge',title:'推其它网站充值',width:200},
                {field:'single_recharge',title:'单粉充值',width:200},
                {field:'cancel_new_rate',title:'新关取关率',width:200},
                {field:'cancel_actual_new_rate',title:'净关取关率',width:200},
                {field:'accumu_recharge_cost_rate',title:'累计回本率',width:200},
                {field:'remark',title:'备注',width:200},
                {field:'handle',title:'操作',width:200},

            ]]
        });
        $(function () {
            $('#dd').dialog({
                title: 'My Dialog',
                width: 400,
                height: 300,
                closed: true,
                cache: false,
                //href: 'get_content.php',
                modal: true,
                buttons: [{
                    text:'Ok',
                    iconCls:'icon-ok',
                    handler:function(){
                        submit_official()
                        $('#dd').dialog('close')
                    }
                },{
                    text:'Cancel',
                    handler:function(){
                        $('#dd').dialog('close');
                    }
                }]
            });

            $('#cc').calendar({
                current:new Date()
            });
            $('.search_select').searchableSelect();
        })
        function addItem(){
            $('#dd').dialog('open')
        }

        function myformatter(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
        }
        function myparser(s){
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            var d = parseInt(ss[2],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(y,m-1,d);
            } else {
                return new Date();
            }
        }

        function submit_official() {
            var appid = $('#appid').val();
            var start_date = $('#start_time').val();
            var end_date = $('#end_time').val();
            if(appid==0){
                $.messager.alert('提交错误','请选择服务号！');
                return ;
            }
            if(!start_date){
                $.messager.alert('提交错误','请选择开始日期！');
                return ;
            }
            if(!end_date){
                $.messager.alert('提交错误','请选择结束日期！');
                return ;
            }
            $.ajax({
                url:'/api/hbstats/addOfficialAccounts',
                type:'post',
                data:{
                    'appid':appid,
                    'start_time':start_date,
                    'end_time':end_date
                },
                success(res){
                    if(res.code>0) $.messager.alert('添加失败',res.msg)
                    if(res.code==0){
                        $.messager.show({
                            title:'提示',
                            msg:'添加成功',
                            timeout:500
                        })
                        $('#dg').datagrid('reload');
                    }
                }
            })
        }

        function modify_advertise_income(id,income){
            console.log(id,income)
            $.messager.prompt('Prompt', '请输入广告收入，但前值：'+income, function(r){
                if (r){
                    $.ajax({
                        url:'/api/hbstats/modify_ad_income',
                        type:'post',
                        data:{
                            'stats_id':id,
                            'income':r,
                        },
                        success(res){
                            if(res.code>0) $.messager.alert('添加失败',res.msg)
                            if(res.code==0){
                                $.messager.show({
                                    title:'提示',
                                    msg:'添加成功',
                                    timeout:500
                                })
                                $('#dg').datagrid('reload');
                            }
                        }
                    })
                }
            });
        }
        function modify_outside_income(id,income){
            $.messager.prompt('Prompt', '请输入广告收入，但前值：'+income, function(r){
                if (r){
                    $.ajax({
                        url:'/api/hbstats/modify_ot_income',
                        type:'post',
                        data:{
                            'stats_id':id,
                            'income':r,
                        },
                        success(res){
                            if(res.code>0) $.messager.alert('添加失败',res.msg)
                            if(res.code==0){
                                $.messager.show({
                                    title:'提示',
                                    msg:'添加成功',
                                    timeout:500
                                })
                                $('#dg').datagrid('reload');
                            }
                        }
                    })
                }
            });
        }
    </script>
@endsection

