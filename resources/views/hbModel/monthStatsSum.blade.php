@extends('layout.app')
@section('content')
    <style>
        .searchable-select-dropdown{
            z-index: 9999;
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <h5 class="page-header">服务号列表</h5>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                {{--<div class="panel-heading">
                    服务号日报
                </div>--}}
                {{--<div class="well">
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:75px;">站点：</label>
                            <input class="form-control" id="distribution_channel_id" name="distribution_channel_id"/>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:160px;">推广渠道：</label>
                            <input class="form-control" id="promotion_point" name="promotion_point"/>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:120px;">代理商：</label>
                            <input class="form-control" id="agent_name" name="agent_name"/>
                        </div>

                        <button id="search" class="btn btn-primary">搜索</button>
                        <button id="export" class="btn btn-info">导出</button>
                    </div>
                    <div style="margin-top: 15px;" class="row">
                        <div class="col-xs-5 col-sm-5 col-md-5"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:170px;">开始时间：</label><input style="background: #fff;"
                                                                                             id="zstart"
                                                                                             class="form-control"
                                                                                             name="zstart" readonly/>
                        </div>
                        <div class="col-xs-5 col-sm-5 col-md-5"
                             style="display:flex;flex-direction: row;justify-content: flex-start;align-items: center;">
                            <label style="line-height:20px;width:170px;">结束时间：</label><input style="background: #fff;"   id="zend"   class="form-control" name="zend" readonly/>&nbsp;
                        </div>
                    </div>
                </div>--}}
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table id="dg"></table>

                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>


@endsection
@section('jsscript')
    @parent

    <link rel="stylesheet" type="text/css" href="/jquery-easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="/jquery-easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery.searchableSelect.css">
    <script type="text/javascript" src="/jquery-easyui/jquery.min.js"></script>
    <script type="text/javascript" src="/jquery-easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/js/jquery.searchableSelect.js"></script>
    <script>
        $('#dg').datagrid({
            url:'/api/hbstats/getOfficialAccountStats',
            title:'服务号统计',
            iconCls:'icon-search',
            method:'post',
            pagination:true,
            pageSize:20,
            fitColumns:true,
            //toolbar:'#tb',
            columns:[[
                {field:'distribution_channel_id',title:'站点',width:100},
                {field:'gzh_name',title:'服务号',width:100},
                {field:'appid',title:'appid',width:100},
                {field:'start_time',title:'增粉开始时间',width:100},
                {field:'end_time',title:'增粉结束时间',width:100},
                {field:'sub_user_num_sum',title:'累计新关粉丝量',width:100},
                {field:'accum_actual_sub_num',title:'累计净关粉丝',width:100},
                {field:'cancel_sub_num_sum',title:'累计取关粉丝',width:100},
                {field:'cost_sum',title:'总成本',width:100},
                {field:'total_income_sum',title:'总充值',width:100},
                {field:'cost_recharge_rate',title:'总成本',width:100},
                {field:'detail',title:'明细',width:100}

            ]]
        });
        $(function () {
            $('#dd').dialog({
                title: 'My Dialog',
                width: 400,
                height: 300,
                closed: true,
                cache: false,
                //href: 'get_content.php',
                modal: true,
                buttons: [{
                    text:'Ok',
                    iconCls:'icon-ok',
                    handler:function(){
                        submit_official()
                        $('#dd').dialog('close')
                    }
                },{
                    text:'Cancel',
                    handler:function(){
                        $('#dd').dialog('close');
                    }
                }]
            });

            $('#cc').calendar({
                current:new Date()
            });
            $('.search_select').searchableSelect();
        })
        function addItem(){
            $('#dd').dialog('open')
        }

        function myformatter(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
        }
        function myparser(s){
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            var d = parseInt(ss[2],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(y,m-1,d);
            } else {
                return new Date();
            }
        }

        function submit_official() {
            var appid = $('#appid').val();
            var start_date = $('#start_time').val();
            var end_date = $('#end_time').val();
            if(appid==0){
                $.messager.alert('提交错误','请选择服务号！');
                return ;
            }
            if(!start_date){
                $.messager.alert('提交错误','请选择开始日期！');
                return ;
            }
            if(!end_date){
                $.messager.alert('提交错误','请选择结束日期！');
                return ;
            }
            $.ajax({
                url:'/api/hbstats/addOfficialAccounts',
                type:'post',
                data:{
                    'appid':appid,
                    'start_time':start_date,
                    'end_time':end_date
                },
                success(res){
                    if(res.code>0) $.messager.alert('添加失败',res.msg)
                    if(res.code==0){
                        $.messager.show({
                            title:'提示',
                            msg:'添加成功',
                            timeout:500
                        })
                        $('#dg').datagrid('reload');
                    }
                }
            })
        }

        function jump(appid) {
            location.href='/api/hbstats/monthStatsList?appid='+appid;
        }
    </script>
@endsection

